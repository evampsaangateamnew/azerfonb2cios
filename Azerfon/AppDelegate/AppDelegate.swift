//
//  AppDelegate.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 2/14/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseMessaging
import UserNotifications
import GoogleMaps
import FBSDKCoreKit
import Firebase
import FirebaseDynamicLinks
import KYDrawerController


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        IQKeyboardManager.shared.enable = true
        // IQKeyboardManager.shared.toolbarBarTintColor = UIColor.afRosyPink
        IQKeyboardManager.shared.toolbarTintColor = UIColor.afDarkPink
        IQKeyboardManager.shared.placeholderColor = UIColor.afDarkPink
        
        //init facebook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //init Firebase
        GMSServices.provideAPIKey("AIzaSyC1jemEaQsw0uMu-fO1_xrBRR_tXIFnH5U")
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        registerForPushNotifications()

        if (launchOptions != nil) {
            // opened from a push notification when the app is closed
            let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]
            if (userInfo != nil)  {
                
                // Redirect user to notification screen
                BaseVC.redirectUserToNotificationScreen()
            }
        }
        
        return true
    }
    
    func registerForPushNotifications() {
        
        // Notification Setting
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (granted, error) in
                
                guard granted else { return }
                
                // register remote notification
                UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                    print("Notification settings: \(settings)")
                    
                    if granted {
                        DispatchQueue.main.async {
                            UIApplication.shared.registerForRemoteNotifications()
                        }
                    } else {
                        // handle the error
                    }
                }
            })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
        
    }

    //MARK: - Facebook delegate
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //init Facebook SDK
        AppEvents.activateApp()
        NotificationCenter.default.post(name: NSNotification.Name(Constants.kAppDidBecomeActive), object: nil, userInfo: nil)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
   // @available(iOS 10.0, *)
    func application(received remoteMessage: MessagingRemoteMessage) {
        
        print(remoteMessage.appData);
        
        // Redirect user to notification screen
         BaseVC.redirectUserToNotificationScreen()
    }
    
    // For iOS 9 and above
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print(userInfo)
        
        // Redirect user to notification screen
         BaseVC.redirectUserToNotificationScreen()
    }
    
    // Received notification on background
    /* Note:
     if this is implemented then above application:didReceiveRemoteNotification will not be called
     */
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        // Let FCM know about the message for analytics etc.
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Redirect user to notification screen
         BaseVC.redirectUserToNotificationScreen()
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print(response)
        
        // Redirect user to notification screen
         BaseVC.redirectUserToNotificationScreen()
        
    }
    
    
    // MARK:- DeepLinking
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      let handled = DynamicLinks.dynamicLinks()
        .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
          // ...
            if (error != nil) {
                print("error dertails: \(error ?? NSError())")
            } else {
                print("dynamiclink.url:\(dynamiclink?.url?.absoluteString ?? "dynamic link is not found")")
                
                if AFUserSession.shared.isLoggedIn() {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                        BaseVC.redirectUserToDeepLinkScreen(deepLinkStr: dynamiclink?.url?.absoluteString ?? "dynamic link is not found" )
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()) {
                        if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
                            navigationController.viewControllers.forEach({ (aViewController) in
                                if aViewController.isKind(of: KYDrawerController.classForCoder()) {
                                    navigationController.popToViewController(aViewController, animated: false)
                                } else {
                                    AFUserSession.shared.dynamicLinkRedirectionURL = dynamiclink?.url?.absoluteString ?? "dynamic link is not found"
                                }
                            })
                        }
                    }
                }
            }
        }

      return handled
    }
    
    /*func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        print("userActivity.webpageURL?.absoluteString in restorationHandler:\(userActivity.webpageURL?.absoluteString ?? "webpageURL not found")")
        
//        DispatchQueue.main.async {
//            let alertViewController = UIAlertController(title: "URL received in restorationHandler", message: userActivity.webpageURL?.absoluteString, preferredStyle: .alert)
//
//
//            let okAction = UIAlertAction(title: "Okay", style: .default) { (action) in
//
//            }
//            alertViewController.addAction(okAction)
//            if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController {
//                navigationController.present(alertViewController, animated: true, completion: nil)
//            }
//
//        }
        
        let urlString = (userActivity.webpageURL?.absoluteString ?? "webpageURL not found").getQueryStringParameter(url: userActivity.webpageURL?.absoluteString ?? "webpageURL not found", param: "link")
        //print("dynamic link URL String: \(urlString ?? "not found")")
        /*if AFUserSession.shared.isLoggedIn() {
            if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
                
                navigationController.viewControllers.forEach({ (aViewController) in
                    
                    if aViewController.isKind(of: KYDrawerController.classForCoder()) {
                        navigationController.popToViewController(aViewController, animated: false)
                    }
                })
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                BaseVC.redirectUserToDeepLinkScreen(deepLinkStr: urlString ?? "not found")
    //            self.navigateUserToServices(urlString:dynamiclink?.url?.absoluteString ?? "notFound")
            }
        }*/
      let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
        // ...
        print("dynamiclink with dynamicLinks().handleUniversalLink:\(dynamiclink?.url?.absoluteString ?? " not Found")")
        //print("dynamic link URL String: \(urlString ?? "not found")")
        if error != nil {
            print("error details:\(error ?? NSError())")
        }
        var redirectionLink = dynamiclink?.url?.absoluteString ?? ""
        if redirectionLink == "" {
            print("dynamiclink?.url?.absoluteString not found in restorationHandler")
            redirectionLink = urlString ?? "not found"
            print("updated redirectionLink after seting with default:\(redirectionLink)")
        }
        if AFUserSession.shared.isLoggedIn() {
            DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                BaseVC.redirectUserToDeepLinkScreen(deepLinkStr: redirectionLink )
            }
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
                    navigationController.viewControllers.forEach({ (aViewController) in
                        if aViewController.isKind(of: KYDrawerController.classForCoder()) {
                            navigationController.popToViewController(aViewController, animated: false)
                        }
                    })
                }
            }
        }
       
      }

      return handled
    }*/
        @available(iOS 9.0, *)
        func application(_ app: UIApplication, open url: URL,
                         options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
          return application(app, open: url,
                             sourceApplication: options[UIApplication.OpenURLOptionsKey
                               .sourceApplication] as? String,
                             annotation: "")
        }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let urlString = (url.absoluteString ).getQueryStringParameter(url: url.absoluteString , param: "link")
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            var redirectionLink = dynamicLink.url?.absoluteString ?? ""
            if redirectionLink == "" {
                print("dynamiclink?.url?.absoluteString not found in sourceApplication")
                redirectionLink = urlString ?? "not found"
                print("updated redirectionLink after seting with default:\(redirectionLink)")
            }
            if AFUserSession.shared.isLoggedIn() {
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                    BaseVC.redirectUserToDeepLinkScreen(deepLinkStr: redirectionLink )
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
                        navigationController.viewControllers.forEach({ (aViewController) in
                            if aViewController.isKind(of: KYDrawerController.classForCoder()) {
                                navigationController.popToViewController(aViewController, animated: false)
                            }
                        })
                    }
                }
            }
            return true
          }
        return (ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation))
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        print("url:\(url)")
//        DispatchQueue.main.async {
//            let alertViewController = UIAlertController(title: "URL received in handleOpen", message: url.absoluteString, preferredStyle: .alert)
//
//
//            let okAction = UIAlertAction(title: "Okay", style: .default) { (action) in
//
//            }
//            alertViewController.addAction(okAction)
//            if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController {
//                navigationController.present(alertViewController, animated: true, completion: nil)
//            }
//
//        }
        let redirectionLink = url.absoluteString
        if AFUserSession.shared.isLoggedIn() {
            DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                BaseVC.redirectUserToDeepLinkScreen(deepLinkStr: redirectionLink )
            }
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
                    navigationController.viewControllers.forEach({ (aViewController) in
                        if aViewController.isKind(of: KYDrawerController.classForCoder()) {
                            navigationController.popToViewController(aViewController, animated: false)
                        }
                    })
                }
            }
        }
        return true
    }
}

