//
//  AFTextField.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 2/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//


import SkyFloatingLabelTextField

@IBDesignable
class AFTextField: SkyFloatingLabelTextFieldWithIcon {
    
    enum TextFieldLayoutType: Int {
        case whiteTextField = 1
        case grayTextField = 2
        case searchTextField = 3
        case other = 0
    }
    
    // MARK:- Properties
    var textFieldLayoutTypeInfo : TextFieldLayoutType = .other
    
    @IBInspectable var tipViewText : String?
    
    @IBInspectable var prefixText :String? {
        didSet {
            self.text = prefixText
        }
    }
    
    var prefixLength :Int {
        return prefixText?.length ?? 0
    }
    
    
    @IBInspectable var textFieldLayoutType :Int {
        get {
            return self.textFieldLayoutTypeInfo.rawValue
            
        } set( typeIndex) {
            
            self.textFieldLayoutTypeInfo = TextFieldLayoutType(rawValue: typeIndex) ?? .other
            adjustsTextFieldLayout()
        }
    }
    
    //MARK:- Functions
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        adjustsTextFieldLayout()
    }
    
    override func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {
        if editing {
            return CGRect(x: self.iconWidth + iconMarginLeft, y: 0, width: bounds.size.width, height: titleHeight())
        }
        return CGRect(x: self.iconWidth + iconMarginLeft, y: titleHeight(), width: bounds.size.width, height: titleHeight())
    }
    //MARK: - Functions
    func adjustsTextFieldLayout() {
        
        self.titleFormatter = { (text: String) -> String in return text }
        
        switch textFieldLayoutTypeInfo {
        case .whiteTextField:
            
            self.textColor = UIColor.afWhite
            self.placeholderColor = UIColor.afLightPink
            
            self.lineColor = UIColor.afVeryLightPinkTwo
            self.selectedLineColor = UIColor.afVeryLightPinkTwo
            
            self.titleColor = UIColor.afLightPink
            self.selectedTitleColor = UIColor.afLightPink
            
            self.selectedLineHeight = 0.5
            self.lineHeight = 0.5
            
            self.iconWidth = 32
            self.iconType = .image
            
            self.font = UIFont.b2White
            self.titleFont = UIFont.b4LightPink
            
            break
            
        case .grayTextField:
            
            self.textColor = UIColor.black
            self.placeholderColor = UIColor.afBrownGrey
            
            self.lineColor = UIColor.afVeryLightPinkTwo
            self.selectedLineColor = UIColor.afVeryLightPinkTwo
            
            self.titleColor = UIColor.afBrownGrey
            self.selectedTitleColor = UIColor.afBrownGrey
            
            self.selectedLineHeight = 1.0
            self.lineHeight = 1.0
            
            self.iconWidth = 32
            self.iconType = .image
            
            self.font = UIFont.b2Grey
            self.titleFont = UIFont.b4LightGrey
            
            break
        case .searchTextField:
            self.textColor = UIColor.afWhite
            self.placeholderColor = UIColor.afWhite
            
            self.lineColor = UIColor.afWhite
            self.selectedLineColor = UIColor.afWhite
            
            self.titleColor = UIColor.afWhite
            self.selectedTitleColor = UIColor.clear
            
            self.selectedLineHeight = 1.0
            self.lineHeight = 1.0
            
            self.iconWidth = 0
            self.iconType = .image
            
            self.font = UIFont.h2White
            self.titleFont = UIFont.h2White
            break
            
        default:
            break
        }
    }
//
//    func showTip() {
//
//        if tipViewText != nil {
//            if let tipView = textFieldTipView {
//                tipView.dismiss(withCompletion: {
//                    self.textFieldTipView = nil
//                })
//            } else {
//                var myPreferences = EasyTipView.globalPreferences
//                myPreferences.drawing.foregroundColor = UIColor.afDarkPink
//                myPreferences.drawing.backgroundColor = UIColor.afWhite
//                myPreferences.animating.showInitialAlpha = 0
//                myPreferences.positioning.bubbleHInset = 8
//                myPreferences.drawing.arrowPosition = .any
//
//                let newTipView = EasyTipView(text: tipViewText ?? "", preferences: myPreferences)
//                newTipView.show(animated: true,
//                                forView: self,
//                                withinSuperview: self.superview)
//
//
//                textFieldTipView = newTipView
//            }
//        }
//    }
}
