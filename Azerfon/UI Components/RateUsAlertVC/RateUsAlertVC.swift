//
//  RateUsAlertVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 2/14/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class RateUsAlertVC: BaseVC {
    
    //MARK : - Properties
    fileprivate var rateUsButtonCompletionBlock : AFButtonCompletionHandler = {}
    private var titleMessageString :String?

    
    //MARK: - IBOutlets
    @IBOutlet var alertTitleLabel: AFLabel!
    
    @IBOutlet var rateNowButton: AFButton!
    @IBOutlet var cancelButton: AFButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.afBlackTransparent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadViewLayout()
    }
    
    //MARK: - Functions
    func loadViewLayout()  {
        cancelButton.setTitle(Localized("BtnTitle_MaybeLater"), for: .normal)
        rateNowButton.setTitle(Localized("BtnTitle_RateNow"), for: .normal)
        alertTitleLabel.text = (titleMessageString?.isBlank ?? true) ? Localized("Title_RateUs") : titleMessageString
    }
    
    //MARK: - IBActions
    @IBAction func rateNowButtonAction(_ sender: UIButton) {
        
        /* Save that Rate Us screen presented to user */
        AFUtilities.updateRateUsShownStatus(isShow: true)
        
        /* Save User interaction time with rateus screen */
        AFUtilities.updateRateUsLaterTime()
        
        /* Call Rate Us API to update status*/
        self.callRateUs()
        
    }
    
    @IBAction func laterButtonAction(_ sender: UIButton) {
        
        /* Save that Rate Us screen presented to user */
        AFUtilities.updateRateUsShownStatus(isShow: true)
        
        /* Save User interaction time with rateus screen */
        AFUtilities.updateRateUsLaterTime()
        
        self.dismiss(animated: true, completion:nil)
    }
    
    //MARK:- Functions
    func setRateUs(titleMessage:String?, _ block : @escaping AFButtonCompletionHandler = {}) {
        titleMessageString = titleMessage
        self.rateUsButtonCompletionBlock = block
    }
    
    func callRateUs() {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.rateUs({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.hideActivityIndicator()
            
            if error != nil {
                
                self.dismiss(animated: false, completion:{
                    error?.showServerErrorInViewController(self)
                })
                
            } else {
                /* Updated curent status of rating of application to rated */
                AFUserSession.shared.loggedInUsers?.currentUser?.userInfo?.rateus_ios = "1"
                
                self.dismiss(animated: false, completion:{
                    
                    self.rateUsButtonCompletionBlock()
                })
            }
        })
    }
}

