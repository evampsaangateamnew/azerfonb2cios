//
//  AFButton.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/5/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class AFButton: UIButton {
    
    enum ButtonLayoutType: Int {
        /// layout 1
        case whiteText_ClearBG_Rounded                          = 1
        /// layout 2
        case whiteText_SuperPinkBG_Rounded                      = 2
        /// layout 3
        case superPinkText_WhitBG_NotRounded                    = 3
        /// layout 4
        case superPinkText_WhitBG_Rounded                       = 4
        /// layout 5
        case whiteText_DarkPinkBG_Rounded                       = 5
        /// layout 6
        case purplishBrownText_WhiteBG_Rounded                  = 6
        /// layout 7
        case brownGreyText_ClearBG_Rounded                      = 7
        /// layout 8
        case superPinkText_SameBorder_WhitBG_Rounded            = 8
        /// layout 9
        case whiteText_ClearBG_Rounded_Disabled                 = 9
        /// layout 10
        case superPinkText_SameBorder_WhitBG_Rounded_Disabled   = 10
        /// layout 0
        case other = 0
    }
    
    enum ButtonContentType: Int {
        case other          = 0
        case topup          = 1
        case renew          = 2
        case subscribe      = 3
        case subscribed     = 4
        case deactivate     = 5
    }
    
    var buttonLayoutTypeInfo    : ButtonLayoutType = .other
    
    var buttonContentType       : ButtonContentType = .other
    
    // IB: use the adapter
    @IBInspectable var buttonLayoutType :Int {
        get {
            return self.buttonLayoutTypeInfo.rawValue
            
        } set( labelTypeIndex ) {
            
            self.buttonLayoutTypeInfo = ButtonLayoutType(rawValue: labelTypeIndex) ?? .other
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var cornersRadiusValue :CGFloat = 4
    
    @IBInspectable var roundCorners:Bool = false {
        didSet {
            if self.roundCorners == true {
                self.roundAllCorners(radius: cornersRadiusValue)
                self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
            } else {
                self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
                self.roundAllCorners(radius: 0)
            }
        }
    }
    
    @IBInspectable var borderColor :UIColor = UIColor.white
    
    @IBInspectable var borderWidth :CGFloat = 1
    
    @IBInspectable var showBorder :Bool = false {
        didSet {
            if showBorder {
                self.layer.borderColor = borderColor.cgColor
                self.layer.borderWidth = borderWidth
            }
        }
    }
    
    // MARK: - Life Cycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        adjustsButtonLayout()
    }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustsButtonLayout()
    }
    
    //MARK: - Functions
    
    func adjustsButtonLayout() {
        
        switch buttonLayoutTypeInfo {
        case .whiteText_ClearBG_Rounded:
            
            setButtonLayoutWith(font: UIFont.h3WhiteCenter,
                                titleColor: UIColor.afWhite,
                                roundCorners: true,
                                cornerRadious: self.bounds.height/2,
                                showBorder: true,
                                borderColor: UIColor.afWhite,
                                borderWidth: 1,
                                bcakGroundColor: UIColor.clear)
            
        case .whiteText_SuperPinkBG_Rounded:
            
            setButtonLayoutWith(font: UIFont.h3WhiteCenter,
                                titleColor: UIColor.afWhite,
                                roundCorners: true,
                                cornerRadious: self.bounds.height/2,
                                showBorder: true,
                                borderColor: UIColor.afWhite,
                                borderWidth: 1,
                                bcakGroundColor: UIColor.afSuperPink)
            
        case .superPinkText_WhitBG_NotRounded:
            
            setButtonLayoutWith(font: UIFont.h3WhiteCenter,
                                titleColor: UIColor.afSuperPink,
                                roundCorners: false,
                                cornerRadious: 0,
                                showBorder: false,
                                borderColor: UIColor.clear,
                                borderWidth: 0,
                                bcakGroundColor: UIColor.white)
            
        case .superPinkText_WhitBG_Rounded:
            
            setButtonLayoutWith(font: UIFont.h3WhiteCenter,
                                titleColor: UIColor.afSuperPink,
                                roundCorners: true,
                                cornerRadious: self.bounds.height/2,
                                showBorder: false,
                                borderColor: UIColor.clear,
                                borderWidth: 0,
                                bcakGroundColor: UIColor.white)
            
        case .whiteText_DarkPinkBG_Rounded:
            
            setButtonLayoutWith(font: UIFont.h5WhiteCenter,
                                titleColor: UIColor.afWhite,
                                roundCorners: true,
                                cornerRadious: 10,
                                showBorder: false,
                                borderColor: UIColor.clear,
                                borderWidth: 0,
                                bcakGroundColor: UIColor.afDarkPink)
            
        case .purplishBrownText_WhiteBG_Rounded:
            
            setButtonLayoutWith(font: UIFont.h5GreyCenter,
                                titleColor: UIColor.afPurplishBrown,
                                roundCorners: true,
                                cornerRadious: 10,
                                showBorder: false,
                                borderColor: UIColor.clear,
                                borderWidth: 0,
                                bcakGroundColor: UIColor.afWhite)
            
        case .brownGreyText_ClearBG_Rounded:
            
            setButtonLayoutWith(font: UIFont.h3GreyRight,
                                titleColor: UIColor.afBrownGrey,
                                roundCorners: true,
                                cornerRadious: self.bounds.height/2,
                                showBorder: true,
                                borderColor: UIColor.afVeryLightPinkTwo,
                                borderWidth: 1,
                                bcakGroundColor: UIColor.clear)
            
        case .superPinkText_SameBorder_WhitBG_Rounded:
            
            setButtonLayoutWith(font: UIFont.h3WhiteCenter,
                                titleColor: UIColor.afSuperPink,
                                roundCorners: true,
                                cornerRadious: self.bounds.height/2,
                                showBorder: true,
                                borderColor: UIColor.afSuperPink,
                                borderWidth: 1,
                                bcakGroundColor: UIColor.white)
            
        case .whiteText_ClearBG_Rounded_Disabled:
            
            setButtonLayoutWith(font: UIFont.h3WhiteCenter,
                                titleColor: UIColor.afWhite.withAlphaComponent(0.4),
                                roundCorners: true,
                                cornerRadious: self.bounds.height/2,
                                showBorder: true,
                                borderColor: UIColor.afWhite.withAlphaComponent(0.4),
                                borderWidth: 1,
                                bcakGroundColor: UIColor.clear)
        
        case .superPinkText_SameBorder_WhitBG_Rounded_Disabled:
            
            setButtonLayoutWith(font: UIFont.h3WhiteCenter,
                                titleColor: UIColor.afSoftPink,
                                roundCorners: true,
                                cornerRadious: self.bounds.height/2,
                                showBorder: true,
                                borderColor: UIColor.afSoftPink,
                                borderWidth: 1,
                                bcakGroundColor: UIColor.white)
        default:
            break
        }
    }
    
    func setButtonLayoutWith(font: UIFont,
                             titleColor :UIColor,
                             roundCorners :Bool,
                             cornerRadious :CGFloat,
                             showBorder :Bool,
                             borderColor :UIColor,
                             borderWidth :CGFloat,
                             bcakGroundColor :UIColor) {
        
        self.titleLabel?.font = font
        self.setTitleColor(titleColor, for: UIControl.State.normal)
        
        self.cornersRadiusValue = cornerRadious
        self.roundCorners = roundCorners
        
        self.borderColor = borderColor
        self.borderWidth = borderWidth
        self.showBorder = showBorder
        
        self.backgroundColor = bcakGroundColor
    }
    
    
    func setButtonLayoutType(_ type: ButtonLayoutType?) {
        if let newButtonType = type {
            self.buttonLayoutType = newButtonType.rawValue
        }
    }
    
    func setState(isEnable : Bool) {
        
        if isEnable {
            self.isEnabled = true
            self.titleLabel?.isEnabled = true
        } else {
            self.isEnabled = false
            self.titleLabel?.isEnabled = false
        }
    }
    
}
