//
//  MWSnapableProgressView.swift
//  Azerfon
//
//  Created by Waqas Umar on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

@objc public protocol MWSnapableProgressViewDelegate {
    
    @objc optional func progressBar(_ progressVw: MWSnapableProgressView,
                                    willSelectItemAtIndex index: Int)
    
    @objc optional func progressBar(_ progressVw: MWSnapableProgressView,
                                    didSelectItemAtIndex index: Int)
    
    @objc optional func progressBar(_ progressVw: MWSnapableProgressView,
                                    canSelectItemAtIndex index: Int) -> Bool
    
    @objc optional func progressBar(_ progressVw: MWSnapableProgressView,
                                    textAtIndex index: Int, position: MWSnapableProgressViewTextLocation) -> String
    
}

@objc public enum MWSnapableProgressViewTextLocation: Int {
    case top
    case bottom
}


@IBDesignable open class MWSnapableProgressView: UIView {
    
    //MARK: - Public properties
    
    /// The number of displayed points in the component
    @IBInspectable open var numberOfPoints: Int = 5 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    /// The current selected index
    open var currentIndex: Int = 0 {
        willSet(newValue){
            if let delegate = self.delegate {
                delegate.progressBar?(self, willSelectItemAtIndex: newValue)
            }
        }
        didSet {
            //            animationRendering = true
            self.setNeedsDisplay()
        }
    }
    
    open var viewBackgroundColor: UIColor = UIColor.white
    
    /// The line height between points
    @IBInspectable open var lineHeight: CGFloat = 0.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    fileprivate var _lineHeight: CGFloat {
        get {
            if(lineHeight == 0.0 || lineHeight > self.bounds.height) {
                return self.bounds.height * 0.4
            }
            return lineHeight
        }
    }
    
    /// The point's radius
    @IBInspectable open var radius: CGFloat = 0.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    fileprivate var _radius: CGFloat {
        get{
            if(radius == 0.0 || radius > self.bounds.height / 2.0) {
                return self.bounds.height / 2.0
            }
            return radius
        }
    }
    
    /// The selected points's raduis
    @IBInspectable open var selectedRadius: CGFloat = 0.0 {
        didSet {
            maskLayer.cornerRadius = selectedRadius
            self.setNeedsDisplay()
        }
    }
    
    
    fileprivate var _selectedRadius: CGFloat {
        get {
            if(selectedRadius == 0.0 || selectedRadius > self.bounds.height / 2.0) {
                return self.bounds.height / 2.0
            }
            return selectedRadius
        }
    }
    
    /// The progress line height between points
    @IBInspectable open var progressLineHeight: CGFloat = 0.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    fileprivate var _progressLineHeight: CGFloat {
        get {
            if(progressLineHeight == 0.0 || progressLineHeight > _lineHeight) {
                return _lineHeight
            }
            return progressLineHeight
        }
    }
    
    /// The selection animation duration
    @IBInspectable open var stepAnimationDuration: CFTimeInterval = 0.01
    
    
    /// The text color in the step points
    open var normalTextFont: UIFont? {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    open var lightTextFont: UIFont? {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    
    /// The component's background color
    @IBInspectable open var dimLineColor: UIColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 0.8) {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    /// The component selected background color
    @IBInspectable open var selectedDotColor: UIColor = UIColor(red: 251.0/255.0, green: 167.0/255.0, blue: 51.0/255.0, alpha: 1.0) {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable open var textColor: UIColor = UIColor.black {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable open var lightTextColor: UIColor = UIColor.gray {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    /// The component's delegate
    open weak var delegate: MWSnapableProgressViewDelegate?
    
    
    //MARK: - Private properties
    
    fileprivate var backgroundDotsLayer = CAShapeLayer()
    
    fileprivate var backgroundLineLayer = CAShapeLayer()
    
    fileprivate var progressLayer = CAShapeLayer()
    
    fileprivate var selectionLayer = CAShapeLayer()
    
    fileprivate var clearSelectionLayer = CAShapeLayer()
    
    fileprivate var clearLastStateLayer = CAShapeLayer()
    
    fileprivate var selectionCenterLayer = CAShapeLayer()
    
    fileprivate var roadToSelectionLayer = CAShapeLayer()
    
    fileprivate var clearCentersLayer = CAShapeLayer()
    
    fileprivate var maskLayer = CAShapeLayer()
    
    fileprivate var centerPoints = [CGPoint]()
    
    fileprivate var _textLayers = [Int:CATextLayer]()
    
    fileprivate var _topTextLayers = [Int:CATextLayer]()
    
    fileprivate var _bottomTextLayers = [Int:CATextLayer]()
    
    fileprivate var previousIndex: Int = 0
    
    fileprivate var animationRendering = false
    
    fileprivate var textDistance : CGFloat = 20.0
    
    //MARK: - Life cycle
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
        self.backgroundColor = UIColor.clear
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    convenience init() {
        self.init(frame:CGRect.zero)
    }
    
    func commonInit() {
        
        
        if normalTextFont == nil {
            normalTextFont = UIFont.afSansBold(fontSize: 12)
        }
        
        if lightTextFont == nil {
            lightTextFont = UIFont.afSans(fontSize: 12)
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MWSnapableProgressView.gestureAction(_:)))
        let swipeGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(MWSnapableProgressView.gestureAction(_:)))
        self.addGestureRecognizer(tapGestureRecognizer)
        self.addGestureRecognizer(swipeGestureRecognizer)
        
        self.layer.addSublayer(self.clearCentersLayer)
        
        self.layer.addSublayer(self.backgroundLineLayer)
        self.layer.addSublayer(self.backgroundDotsLayer)
        self.layer.addSublayer(self.progressLayer)
        self.layer.addSublayer(self.clearSelectionLayer)
        self.layer.addSublayer(self.selectionCenterLayer)
        self.layer.addSublayer(self.selectionLayer)
        
        self.layer.addSublayer(self.roadToSelectionLayer)
        self.progressLayer.mask = self.maskLayer
        
        self.contentMode = UIView.ContentMode.redraw
    }
    
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.centerPoints.removeAll()
        
        let largerRadius = fmax(_radius, _selectedRadius)
        let largerLine = fmax(_lineHeight, _progressLineHeight)
        
        let distanceBetweenCircles = (self.bounds.width - (CGFloat(numberOfPoints) * 2 * largerRadius)) / CGFloat(numberOfPoints - 1)
        
        var xCursor: CGFloat = largerRadius
        
        for _ in 0...(numberOfPoints - 1) {
            centerPoints.append(CGPoint(x: xCursor, y: bounds.height / 2))
            xCursor += 2 * largerRadius + distanceBetweenCircles
        }
        
        if(!animationRendering) {
            
            let clearCentersPath = self._shapePath(self.centerPoints, aRadius: largerRadius, aLineHeight: largerLine)
            clearCentersLayer.path = clearCentersPath.cgPath
            clearCentersLayer.fillColor = viewBackgroundColor.cgColor
            
            let bgPath = self._shapePath(self.centerPoints, aRadius: _radius, aLineHeight: _lineHeight)
            backgroundLineLayer.path = bgPath.cgPath
            backgroundLineLayer.fillColor = dimLineColor.cgColor
            
            let bgDotsPath = self._shapePath(self.centerPoints, aRadius: _radius, aLineHeight: 0)
            backgroundDotsLayer.path = bgDotsPath.cgPath
            backgroundDotsLayer.fillColor = selectedDotColor.cgColor
            
            let progressPath = self._shapePath(self.centerPoints, aRadius: _radius, aLineHeight: _progressLineHeight)
            progressLayer.path = progressPath.cgPath
            progressLayer.fillColor = selectedDotColor.cgColor
            
            let clearSelectedPath = self._shapePathForSelected(self.centerPoints[currentIndex], aRadius: _selectedRadius)
            clearSelectionLayer.path = clearSelectedPath.cgPath
            clearSelectionLayer.fillColor = viewBackgroundColor.cgColor
            
            let selectedPath = self._shapePathForSelected(self.centerPoints[currentIndex], aRadius: _radius)
            selectionLayer.path = selectedPath.cgPath
            selectionLayer.fillColor = selectedDotColor.cgColor
            
            let selectedPathCenter = self._shapePathForSelectedPathCenter(self.centerPoints[currentIndex], aRadius: _selectedRadius)
            selectionCenterLayer.path = selectedPathCenter.cgPath
            selectionCenterLayer.fillColor = selectedDotColor.cgColor
            
        }
        self.renderTopTextIndexes()
        self.renderBottomTextIndexes()
        
        let progressCenterPoints = Array<CGPoint>(centerPoints[0..<(currentIndex+1)])
        
        if let currentProgressCenterPoint = progressCenterPoints.last {
            
            let maskPath = self._maskPath(currentProgressCenterPoint)
            maskLayer.path = maskPath.cgPath
            
            CATransaction.begin()
            let progressAnimation = CABasicAnimation(keyPath: "path")
            progressAnimation.duration = stepAnimationDuration //* CFTimeInterval(abs(currentIndex - previousIndex))
            progressAnimation.toValue = maskPath
            progressAnimation.isRemovedOnCompletion = false
            progressAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            
            
            CATransaction.setCompletionBlock { () -> Void in
                if(self.animationRendering) {
                    if let delegate = self.delegate {
                        delegate.progressBar?(self, didSelectItemAtIndex: self.currentIndex)
                    }
                    self.animationRendering = false
                }
            }
            
            maskLayer.add(progressAnimation, forKey: "progressAnimation")
            CATransaction.commit()
        }
        self.previousIndex = self.currentIndex
    }
    
    /**
     Render the text indexes
     */
    fileprivate func renderTopTextIndexes() {
        
        for i in 0...(numberOfPoints - 1) {
            let centerPoint = centerPoints[i]
            
            let textLayer = self._topTextLayer(atIndex: i)
            
            var font : UIFont? = normalTextFont
            var fontSize : CGFloat = (normalTextFont?.pointSize) ?? 14
            var colorr : CGColor = textColor.cgColor
            var alignment : CATextLayerAlignmentMode = .center
            var yPosCons : CGFloat = _radius + textDistance
            var xPosCons : CGFloat = 10
            
            if i == 0 {
                font = lightTextFont
                fontSize = (lightTextFont?.pointSize) ?? 12
                colorr = lightTextColor.cgColor
                alignment = .center
                yPosCons = _radius + textDistance
            }
            else if i == currentIndex {
                fontSize = ((normalTextFont?.pointSize) ?? 14) + 2
                font = UIFont.afSansBold(fontSize: Float(fontSize))
                colorr = textColor.cgColor
                alignment = .center
                yPosCons = _selectedRadius + textDistance
            }
            
            
            textLayer.contentsScale = UIScreen.main.scale
            textLayer.font = font
            textLayer.fontSize = fontSize
            textLayer.alignmentMode = alignment
            textLayer.foregroundColor = colorr
            
            
            if let text = self.delegate?.progressBar?(self, textAtIndex: i, position: MWSnapableProgressViewTextLocation.top) {
                textLayer.string = text
            } else {
                textLayer.string = "\(i)"
            }
            
            textLayer.sizeWidthToFit()
            
            xPosCons = i == 0 ? 10:textLayer.bounds.width/2
            
            textLayer.frame = CGRect(x: centerPoint.x - xPosCons,
                                     y: centerPoint.y - textLayer.bounds.height/2 - yPosCons,
                                     width: textLayer.bounds.width,
                                     height: textLayer.bounds.height)
            
            
        }
    }
    
    fileprivate func renderBottomTextIndexes() {
        
        for i in 0...(numberOfPoints - 1) {
            let centerPoint = centerPoints[i]
            
            let textLayer = self._bottomTextLayer(atIndex: i)
            
            var font : UIFont? = normalTextFont
            var fontSize : CGFloat = (normalTextFont?.pointSize) ?? 14
            var colorr : CGColor = textColor.cgColor
            var alignment : CATextLayerAlignmentMode = .center
            var yPosCons : CGFloat = _radius + textDistance
            var xPosCons : CGFloat = 10
            
            if i == 0 {
                font = lightTextFont
                fontSize = (lightTextFont?.pointSize) ?? 12
                colorr = lightTextColor.cgColor
                alignment = .left
                yPosCons = _radius + textDistance
            }
            else if i == currentIndex {
                font = normalTextFont
                fontSize = ((normalTextFont?.pointSize) ?? 14)
                colorr = textColor.cgColor
                alignment = .center
                yPosCons = _selectedRadius + textDistance
            }
            
            textLayer.contentsScale = UIScreen.main.scale
            textLayer.font = font
            textLayer.fontSize = fontSize
            textLayer.alignmentMode = alignment
            textLayer.foregroundColor = colorr
            
            if let text = self.delegate?.progressBar?(self, textAtIndex: i, position: MWSnapableProgressViewTextLocation.bottom) {
                textLayer.string = text
            } else {
                textLayer.string = "\(i)"
            }
            
            textLayer.sizeWidthToFit()
            
            xPosCons = i == 0 ? 10:textLayer.bounds.width/2
            
            textLayer.frame = CGRect(x: centerPoint.x - xPosCons,
                                     y: centerPoint.y - textLayer.bounds.height/2 + yPosCons,
                                     width: textLayer.bounds.width,
                                     height: textLayer.bounds.height)
            
            
        }
    }
    
    /**
     Provide a text layer for the given index. If it's not in cache, it'll be instanciated.
     
     - parameter index: The index where the layer will be used
     
     - returns: The text layer
     */
    fileprivate func _topTextLayer(atIndex index: Int) -> CATextLayer {
        
        var textLayer: CATextLayer
        if let _textLayer = self._topTextLayers[index] {
            textLayer = _textLayer
        } else {
            textLayer = CATextLayer()
            self._topTextLayers[index] = textLayer
        }
        self.layer.addSublayer(textLayer)
        
        return textLayer
    }
    
    /**
     Provide a text layer for the given index. If it's not in cache, it'll be instanciated.
     
     - parameter index: The index where the layer will be used
     
     - returns: The text layer
     */
    fileprivate func _bottomTextLayer(atIndex index: Int) -> CATextLayer {
        
        var textLayer: CATextLayer
        if let _textLayer = self._bottomTextLayers[index] {
            textLayer = _textLayer
        } else {
            textLayer = CATextLayer()
            self._bottomTextLayers[index] = textLayer
        }
        self.layer.addSublayer(textLayer)
        
        return textLayer
    }
    
    
    /**
     Complete a progress path
     
     - parameter centerPoints: The center points corresponding to the indexes
     - parameter aRadius:      The index radius
     - parameter aLineHeight:  The line height between each index
     
     - returns: The computed path
     */
    fileprivate func _shapePath(_ centerPoints: Array<CGPoint>, aRadius: CGFloat, aLineHeight: CGFloat) -> UIBezierPath {
        
        let nbPoint = centerPoints.count
        
        let path = UIBezierPath()
        
        var distanceBetweenCircles: CGFloat = 0
        
        if let first = centerPoints.first , nbPoint > 2 {
            let second = centerPoints[1]
            distanceBetweenCircles = second.x - first.x - 2 * aRadius
        }
        
        let angle = aLineHeight / 2.0 / aRadius;
        
        var xCursor: CGFloat = 0
        
        
        for i in 0...(2 * nbPoint - 1) {
            
            var index = i
            if(index >= nbPoint) {
                index = (nbPoint - 1) - (i - nbPoint)
            }
            
            let centerPoint = centerPoints[index]
            
            var startAngle: CGFloat = 0
            var endAngle: CGFloat = 0
            
            if(i == 0) {
                
                xCursor = centerPoint.x
                
                startAngle = CGFloat.pi
                endAngle = -angle
                
            } else if(i < nbPoint - 1) {
                
                startAngle = CGFloat.pi + angle
                endAngle = -angle
                
            } else if(i == (nbPoint - 1)){
                
                startAngle = CGFloat.pi + angle
                endAngle = 0
                
            } else if(i == nbPoint) {
                
                startAngle = 0
                endAngle = CGFloat.pi - angle
                
            } else if (i < (2 * nbPoint - 1)) {
                
                startAngle = angle
                endAngle = CGFloat.pi - angle
                
            } else {
                
                startAngle = angle
                endAngle = CGFloat.pi
                
            }
            
            path.addArc(withCenter: CGPoint(x: centerPoint.x, y: centerPoint.y), radius: aRadius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            
            if(i < nbPoint - 1) {
                xCursor += aRadius + distanceBetweenCircles
                path.addLine(to: CGPoint(x: xCursor, y: centerPoint.y - aLineHeight / 2.0))
                xCursor += aRadius
            } else if (i < (2 * nbPoint - 1) && i >= nbPoint) {
                xCursor -= aRadius + distanceBetweenCircles
                path.addLine(to: CGPoint(x: xCursor, y: centerPoint.y + aLineHeight / 2.0))
                xCursor -= aRadius
            }
        }
        return path
    }
    
    fileprivate func _shapePathForSelected(_ centerPoint: CGPoint, aRadius: CGFloat) -> UIBezierPath {
        return UIBezierPath(roundedRect: CGRect(x: centerPoint.x - aRadius, y: centerPoint.y - aRadius, width: 2.0 * aRadius, height: 2.0 * aRadius), cornerRadius: aRadius)
    }
    
    fileprivate func _shapePathForLastState(_ center: CGPoint) -> UIBezierPath {
        //        let angle = CGFloat(M_PI)/4
        let path = UIBezierPath()
        //        path.addArcWithCenter(center, radius: self._progressRadius + _radius, startAngle: angle, endAngle: 2*CGFloat(M_PI) + CGFloat(M_PI)/4, clockwise: true)
        path.addArc(withCenter: center, radius: self._radius, startAngle: 0, endAngle: 4*CGFloat.pi, clockwise: true)
        return path
    }
    
    fileprivate func _shapePathForSelectedPathCenter(_ centerPoint: CGPoint, aRadius: CGFloat) -> UIBezierPath {
        return UIBezierPath(roundedRect: CGRect(x: centerPoint.x - aRadius, y: centerPoint.y - aRadius, width: 2.0 * aRadius, height: 2.0 * aRadius), cornerRadius: aRadius)
    }
    
    /**
     Compute the mask path
     
     - parameter currentProgressCenterPoint: The current progress index's center point
     
     - returns: The computed mask path
     */
    fileprivate func _maskPath(_ currentProgressCenterPoint: CGPoint) -> UIBezierPath {
        
        let angle = self._progressLineHeight / 2.0 / self._selectedRadius;
        let xOffset = cos(angle) * self._selectedRadius
        
        let maskPath = UIBezierPath()
        
        maskPath.move(to: CGPoint(x: 0.0, y: 0.0))
        
        maskPath.addLine(to: CGPoint(x: currentProgressCenterPoint.x + xOffset, y: 0.0))
        
        maskPath.addLine(to: CGPoint(x: currentProgressCenterPoint.x + xOffset, y: currentProgressCenterPoint.y - self._progressLineHeight))
        
        maskPath.addArc(withCenter: currentProgressCenterPoint, radius: self._selectedRadius, startAngle: -angle, endAngle: angle, clockwise: true)
        
        maskPath.addLine(to: CGPoint(x: currentProgressCenterPoint.x + xOffset, y: self.bounds.height))
        
        maskPath.addLine(to: CGPoint(x: 0.0, y: self.bounds.height))
        
        
        maskPath.close()
        
        return maskPath
    }
    
    /**
     Respond to the user action
     
     - parameter gestureRecognizer: The gesture recognizer responsible for the action
     */
    @objc func gestureAction(_ gestureRecognizer: UIGestureRecognizer) {
        if(gestureRecognizer.state == UIGestureRecognizer.State.ended ||
            gestureRecognizer.state == UIGestureRecognizer.State.changed ) {
            
            let touchPoint = gestureRecognizer.location(in: self)
            
            var smallestDistance = CGFloat(Float.infinity)
            
            var selectedIndex = 0
            
            for (index, point) in self.centerPoints.enumerated() {
                let distance = touchPoint.distanceWith(point)
                if(distance < smallestDistance) {
                    smallestDistance = distance
                    selectedIndex = index
                }
            }
            
            
            
            if(self.currentIndex != selectedIndex) {
                if let canSelect = self.delegate?.progressBar?(self, canSelectItemAtIndex: selectedIndex) {
                    if (canSelect) {
                        self.currentIndex = selectedIndex
                        self.animationRendering = false
                    }
                }
            }
        }
    }
    
}
