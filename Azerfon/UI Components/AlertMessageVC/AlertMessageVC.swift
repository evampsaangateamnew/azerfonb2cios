//
//  AlertMessageVC.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 5/22/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class AlertMessageVC: UIViewController {
    
    //MARK : - Properties
    fileprivate var cancelButtonCompletionBlock : AFButtonCompletionHandler = {}
    var alertMessage :NSAttributedString = NSAttributedString.init(string: "")
    var cancelButtonTitle : String = ""
    
    //MARK : - IBOutlets
    @IBOutlet weak var detailView: AFView!
    @IBOutlet weak var alertMessageLabel: AFLabel!
    @IBOutlet weak var cancelButton: AFButton!
    
    

    //MARK : - ViewControllers Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.afBlackTransparent
        alertMessageLabel.attributedText = alertMessage.string.isBlank ? NSAttributedString.init(string: Localized("Message_UnexpectedError")) : alertMessage
        cancelButton.setTitle(cancelButtonTitle, for: UIControl.State.normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        detailView.roundAllCorners(radius: Constants.kViewCornerRadius)

    }
    
    
    //MARK:- Functions

    func setAlertWith(attributedMessage : NSAttributedString? = NSAttributedString.init(string: ""), btnTitle:String = Localized("BtnTitle_OK"), cancelBlock : @escaping AFButtonCompletionHandler = {}) {
    
        self.alertMessage = attributedMessage ?? NSAttributedString.init(string: "")
        cancelButtonTitle = btnTitle
        
        
        cancelButtonCompletionBlock = cancelBlock
    }
    
    //MARK:- IBActions
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion:{
            self.cancelButtonCompletionBlock()
        })
    }
    
}
