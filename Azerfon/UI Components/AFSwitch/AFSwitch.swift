//
//  AFSwitch.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/5/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class AFSwitch: UISwitch {
    
    var sectionTag : Int = 0
    var rowTag : Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.onTintColor = UIColor.afSuperPink
        self.backgroundColor = UIColor.afBrownGrey
        self.layer.cornerRadius = 16.0;
    }
}
