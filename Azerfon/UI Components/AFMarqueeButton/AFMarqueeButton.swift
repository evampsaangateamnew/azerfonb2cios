//
//  AFMarqueeButton.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 6/18/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class AFMarqueeButton: AFButton {
    
    public var myTitleLabel : MarqueeLabel = MarqueeLabel()
    public var marqueeButtonTitle: String = ""
    public var marqueeButtonFontTitleColor: UIColor = UIColor.black
    public var marqueeButtonTitleLabelRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setMarqueeButtonLayout()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        marqueeButtonTitleLabelRect = frame
        setMarqueeButtonLayout()
    }
    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        
        marqueeButtonTitle = title ?? ""
        setMarqueeButtonLayout()
    }
    
    override func setTitleColor(_ color: UIColor?, for state: UIControl.State) {
        super.setTitleColor(color, for: state)
        
        marqueeButtonFontTitleColor = color ?? .black
        setMarqueeButtonLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        marqueeButtonTitleLabelRect = CGRect(x: 8, y: self.bounds.origin.y, width: (self.bounds.size.width - 16), height: self.bounds.size.height)
        setMarqueeButtonLayout()
        
    }
    
    func setMarqueeButtonLayout() {
        
        
        //        let labelRect = CGRect(x: 8, y: self.bounds.origin.y, width: (self.bounds.size.width - 16), height: self.bounds.size.height)
        
        self.titleLabel?.removeFromSuperview()
        self.myTitleLabel.removeFromSuperview()
        
        self.myTitleLabel.frame = marqueeButtonTitleLabelRect
        
        self.myTitleLabel.text = marqueeButtonTitle
        myTitleLabel.textColor = marqueeButtonFontTitleColor
        myTitleLabel.font = self.titleLabel?.font
        myTitleLabel.textAlignment = .center
        
        self.myTitleLabel.setupMarqueeAnimation()
        
        self.addSubview(self.myTitleLabel)
    }
}
