//
//  ConfirmationAlertVC.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 5/22/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class ConfirmationAlertVC: UIViewController {
    
    enum ConfirmationLayoutType {
        case logout
        case other
        case creditConfirm
    }
    //MARK: - Properties
    fileprivate var okButtonCompletionBlock : AFButtonCompletionHandler = {}
    fileprivate var cancelButtonCompletionBlock : AFButtonCompletionHandler = {}
    fileprivate var manageAccountButtonCompletionBlock : AFButtonCompletionHandler = {}
    fileprivate var alertMessage : NSAttributedString = NSAttributedString.init(string: "")
    fileprivate var titleConfirmationStr : NSAttributedString = NSAttributedString.init(string: "")
    fileprivate var balanceTitleStr : NSAttributedString = NSAttributedString.init(string: "")
    fileprivate var userBalancestr : NSAttributedString = NSAttributedString.init(string: "")
    fileprivate var okButtonTitle : String = ""
    fileprivate var cancelButtonTitle : String = ""
    fileprivate var manageAccountButtonTitle : String = ""
    
    var alertLayoutType: ConfirmationLayoutType = ConfirmationLayoutType.other
    var manageNumbersCount :Int = 0
    
    //MARK : - IBOutlets
    @IBOutlet weak var alertMessageLabel                : AFLabel!
    @IBOutlet weak var okButton                         : AFButton!
    @IBOutlet weak var cancelButton                     : AFButton!
    @IBOutlet weak var manageAccountContainorView       : UIView!
    @IBOutlet weak var manageAccountButton              : AFButton!
    @IBOutlet var manageAccountViewHeightConstraint     : NSLayoutConstraint!
    
    @IBOutlet weak var balanceContainorView             : UIView!
    @IBOutlet weak var confirmationLabel                : AFLabel!
    @IBOutlet weak var balanceTitleLabel                : AFLabel!
    @IBOutlet weak var balanceLabel                     : AFLabel!
    
    @IBOutlet var messageTopConst                       : NSLayoutConstraint!
    
    
    //MARK: - ViewControllers Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.afBlackTransparent
        alertMessageLabel.attributedText = alertMessage
       
        okButton.setTitle(okButtonTitle, for: UIControl.State.normal)
        cancelButton.setTitle(cancelButtonTitle, for: UIControl.State.normal)
        manageAccountButton.setTitle(manageAccountButtonTitle, for: UIControl.State.normal)
        
        if alertLayoutType == .logout {
            
            okButton.setButtonLayoutType(.whiteText_SuperPinkBG_Rounded)
            cancelButton.setButtonLayoutType(.brownGreyText_ClearBG_Rounded)
            
        } else {
            okButton.setButtonLayoutType(.whiteText_SuperPinkBG_Rounded)
            cancelButton.setButtonLayoutType(.brownGreyText_ClearBG_Rounded)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        confirmationLabel.text = ""
        balanceTitleLabel.text = ""
        balanceLabel.text = ""
        balanceContainorView.isHidden = true
        
        messageTopConst.constant = 20.0
        if alertLayoutType == .logout && manageNumbersCount > 1 {
            manageAccountContainorView.isHidden = false
            manageAccountButton.isHidden = false
            
            manageAccountViewHeightConstraint.constant = 50
        } else {
            manageAccountContainorView.isHidden = true
            manageAccountButton.isHidden = true
            
            manageAccountViewHeightConstraint.constant = 0
            
            if alertLayoutType == .creditConfirm {
                balanceContainorView.isHidden = false
                messageTopConst.constant = 50.0
                confirmationLabel.attributedText = titleConfirmationStr
                balanceTitleLabel.attributedText = balanceTitleStr
                balanceLabel.attributedText = userBalancestr
            }
        }
    }
    
    //MARK:- Functions
    func setAlertWith(popupType : ConfirmationLayoutType? = .other, message : NSAttributedString? = NSAttributedString.init(string: ""), okBtnTitle:String = Localized("BtnTitle_OK"),caneclBtnTitle:String = Localized("BtnTitle_CANCEL"), okBlock : @escaping AFButtonCompletionHandler = {}, cancelBlock : @escaping AFButtonCompletionHandler = {}) {
        
        alertLayoutType = popupType ?? .other
        okButtonTitle = okBtnTitle
        cancelButtonTitle = caneclBtnTitle
        
        okButtonCompletionBlock = okBlock
        cancelButtonCompletionBlock = cancelBlock
        
       
        alertMessage = message ?? NSAttributedString.init(string: "")
        
        
    }
    func setCreditAlertWith(popupType : ConfirmationLayoutType? = .other, message : NSAttributedString? = NSAttributedString.init(string: ""), okBtnTitle:String = Localized("BtnTitle_OK"),caneclBtnTitle:String = Localized("BtnTitle_CANCEL"), userBalance:String, creditAmount:String, balanceTitle:String, okBlock : @escaping AFButtonCompletionHandler = {}, cancelBlock : @escaping AFButtonCompletionHandler = {}) {
        
        alertLayoutType = .creditConfirm
        okButtonTitle = okBtnTitle
        cancelButtonTitle = caneclBtnTitle
        
        okButtonCompletionBlock = okBlock
        cancelButtonCompletionBlock = cancelBlock
        
        titleConfirmationStr = Localized("Title_Confirmation").createAttributedString(mainStringColor: UIColor.afGunmetal, mainStringFont: UIFont.h2Grey)
        balanceTitleStr = balanceTitle.createAttributedString(mainStringColor: UIColor.afGunmetal, mainStringFont: UIFont.h2PinkRegular)
        
        userBalancestr = userBalance.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afGunmetal, mainStringFont: UIFont.h2Grey, imageSize: CGSize(width: 10, height: 6))
        
        
       
        alertMessage = message ?? NSAttributedString.init(string: "")
        
        
    }
    
    func setLogoutAlert(msisdnCount: Int,
                        _ okBlock : @escaping AFButtonCompletionHandler = {},
                        cancelBlock : @escaping AFButtonCompletionHandler = {},
                        manageAccountBlock : @escaping AFButtonCompletionHandler = {}) {
        
        manageNumbersCount = msisdnCount
        
        alertLayoutType = .logout
        
        var logoutMessage = Localized("Message_LogoutMessage_Single")
        if manageNumbersCount > 1 {
             logoutMessage = Localized("Message_LogoutMessage")
        }

        alertMessage =  logoutMessage.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h2PinkRegular)
        okButtonTitle = Localized("BtnTitle_LOGOUT")
        cancelButtonTitle = Localized("BtnTitle_CANCEL")
        manageAccountButtonTitle = Localized("BtnTitle_MANAGEACCOUNT")
        
        okButtonCompletionBlock = okBlock
        cancelButtonCompletionBlock = cancelBlock
        manageAccountButtonCompletionBlock = manageAccountBlock
    }
    
    //MARK: - IBActions
    @IBAction func okButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion:nil)
        self.okButtonCompletionBlock()
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion:nil)
        self.cancelButtonCompletionBlock()
    }
    
    @IBAction func manageAccountAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion:nil)
        self.manageAccountButtonCompletionBlock()
    }
}
