//
//  AFView.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/13/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class AFView: UIView {
    @IBInspectable var roundedCornersValue : CGFloat = 0.0 {
        didSet {
            setupLayout()
        }
    }
    @IBInspectable var borderWidthValue : CGFloat = 0.0 {
        didSet{
            setupLayout()
        }
    }
    @IBInspectable var borderColor : UIColor = .afGunmetal
        {
        didSet{
            setupLayout()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupLayout()
    }
    
    func setupLayout() {
        self.roundAllCorners(radius: roundedCornersValue)
        self.layer.borderWidth = borderWidthValue
        self.layer.borderColor = borderColor.cgColor
    }
}
