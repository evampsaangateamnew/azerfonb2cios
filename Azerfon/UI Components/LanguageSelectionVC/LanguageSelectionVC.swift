//
//  LanguageSelectionVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/7/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class LanguageSelectionVC: BaseVC {
    
    typealias AFLanguageCompletionHandler = (_ currentLanguage:Constants.AFLanguage) -> ()
    
    //MARK: - Properties
    var selectedLanguage : Constants.AFLanguage = Constants.defaultAppLanguage
    fileprivate var doneBlock : AFLanguageCompletionHandler?
    
    //MARK: - IBOutlet
    
    @IBOutlet var check1Button: UIButton!
    @IBOutlet var check2Button: UIButton!
    @IBOutlet var check3Button: UIButton!
    
    @IBOutlet var doneButton: UIButton!
    
    
    //MARK: - View Controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.afBlackTransparent
        titleLabel?.text = Localized("Title_SelectLanguage")
        doneButton.setTitle(Localized("BtnTitle_APPLY"), for: UIControl.State.normal)
        
        // Selecting current selected language
        selectLanguage(senderType : selectedLanguage)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        doneBlock = nil
    }
    
    //MARK: - FUNCTIONS
    
    private func selectLanguage(senderType : Constants.AFLanguage) {
        
        check1Button .setImage(UIImage.imageFor(name: "radioUnSelected"), for: UIControl.State.normal)
        check2Button .setImage(UIImage.imageFor(name: "radioUnSelected"), for: UIControl.State.normal)
        check3Button .setImage(UIImage.imageFor(name: "radioUnSelected"), for: UIControl.State.normal)
        
        switch senderType {
        case .azeri:
            check1Button .setImage(UIImage.imageFor(name: "radioSelected"), for: UIControl.State.normal)
            selectedLanguage = Constants.AFLanguage.azeri
            
        case .russian:
            check2Button .setImage(UIImage.imageFor(name: "radioSelected"), for: UIControl.State.normal)
            selectedLanguage = Constants.AFLanguage.russian
            
        case .english:
            
            check3Button .setImage(UIImage.imageFor(name: "radioSelected"), for: UIControl.State.normal)
            selectedLanguage = Constants.AFLanguage.english
            
        }
    }
    
    func setLanguageSelectionAlert(_ currentLanguage:Constants.AFLanguage, completionBlock : @escaping AFLanguageCompletionHandler ) {
        
        selectedLanguage = currentLanguage
        self.doneBlock = completionBlock
        
    }
    
    //MARK: - IBACTIONS
    
    @IBAction func donePressed(_ sender: UIButton) {
        
        self.doneBlock?(selectedLanguage)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func check1Pressed(_ sender: Any) {
        selectLanguage(senderType : Constants.AFLanguage.azeri)
    }
    
    @IBAction func check2Pressed(_ sender: Any) {
        selectLanguage(senderType : Constants.AFLanguage.russian)
    }
    
    @IBAction func check3Pressed(_ sender: Any) {
        selectLanguage(senderType : Constants.AFLanguage.english)
        
    }
}

extension LanguageSelectionVC {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.view {
                self.doneBlock = nil
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
