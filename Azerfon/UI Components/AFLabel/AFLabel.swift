//
//  AFLabel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/7/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class AFLabel: UILabel {
    
    enum LabelType: Int {
        case other      = 0
        case vcTitle    = 1
        case body       = 2
    }
    
    // MARK: - Life Cycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // Programmatically: use the enum
    var labelFontType:LabelType = .other
    
    // IB: use the adapter
    @IBInspectable var labelType:Int {
        get {
            return self.labelFontType.rawValue
            
        } set( labelTypeIndex) {
            
            self.labelFontType = LabelType(rawValue: labelTypeIndex) ?? .other
            // adjustsFontSizeToFitDevice()
        }
    }
    
    func adjustsFontSizeToFitDevice() {
        
        var labelNewFont : UIFont = UIFont.afSans(fontSize: 14)
        switch labelFontType {
        case .vcTitle:
            labelNewFont = UIFont.h3WhiteCenter
        case .other,.body:
            labelNewFont = self.font
        }
        
        self.font = labelNewFont
    }
    
    func setLabelFontType(_ type: LabelType?) {
        if let newLabelFontType = type {
            self.labelType = newLabelFontType.rawValue
        }
    }
    
    func loadHTMLString(htmlString: String?) {
        if let htmlData = htmlString?.data(using: String.Encoding.unicode) {
            do {
                let attributedString = try NSMutableAttributedString(data: htmlData,
                                                                     options: [.documentType: NSAttributedString.DocumentType.html],
                                                                     documentAttributes: nil)
                
                attributedString.addAttribute(NSAttributedString.Key.font,
                                              value: self.font,
                                              range: NSRange(location: 0 , length: attributedString.length))
                
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: self.textColor,
                                              range: NSRange(location: 0 , length: attributedString.length))
                
                self.attributedText = attributedString
                
                
            } catch let e as NSError {
                print("Couldn't parse \(htmlString ?? ""): \(e.localizedDescription)")
                
                self.text = ""
            }
        } else {
            self.text = ""
        }
    }
}
