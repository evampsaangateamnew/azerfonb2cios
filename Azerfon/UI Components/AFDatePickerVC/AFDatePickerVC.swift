//
//  MBDatePickerVC.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 7/23/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class AFDatePickerVC: BaseVC {
    
    //MARK: - Properties
    typealias AFDatePickerCompletionHandler = (_ selectedDate:Date) -> ()
    
    var minDate:Date = Date().todayDate()
    var maxDate:Date = Date().todayDate()
    var currentDate:Date = Date().todayDate()
    var doneCompletionBlock:AFDatePickerCompletionHandler?
    
    //MARK: - IBOutlet
//    @IBOutlet var backGroundView: UIView!
    @IBOutlet var myContentView: UIView!
    @IBOutlet var buttonContentView: UIView!
    
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    
    @IBOutlet var datePicker: UIDatePicker!
    
    //MARK: - View Controllers Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor.afBlackTransparent
        
        myContentView.roundAllCorners(radius: 8)
        buttonContentView.roundTopCorners(radius: 8)
        buttonContentView.backgroundColor = UIColor.afDatePickerGrey
        myContentView.backgroundColor = UIColor.afDatePickerGrey
        
        doneButton.setTitle(Localized("BtnTitle_Done"), for: .normal)
        cancelButton.setTitle(Localized("BtnTitle_CANCEL"), for: .normal)
     
        
        var  currentLanguageLocale = Locale(identifier:"en")
        switch AFLanguageManager.userSelectedLanguage() {
            
        case .russian:
            currentLanguageLocale = Locale(identifier:"ru")
        case .english:
            currentLanguageLocale = Locale(identifier:"en")
        case .azeri:
            currentLanguageLocale = Locale(identifier:"az_AZ")
        }
        
        datePicker.calendar.locale = currentLanguageLocale
        datePicker.locale = currentLanguageLocale
        
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        datePicker.setDate(currentDate, animated: true)
        datePicker.timeZone =   TimeZone.appTimeZone()
        datePicker.datePickerMode = .date
        
        datePicker.backgroundColor = UIColor.afDatePickerGrey

    }
    
    //MARK: - Functions
    func setDatePicker(minDate : Date , maxDate : Date, currentDate : Date, didSelectDateBlock : @escaping AFDatePickerCompletionHandler) {
        self.minDate = minDate
        self.maxDate = maxDate
        self.currentDate = currentDate
        
        doneCompletionBlock = didSelectDateBlock
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.view {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - IBAction
    @IBAction func donePressed(_ sender: UIButton) {
        doneCompletionBlock?(datePicker.date)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
