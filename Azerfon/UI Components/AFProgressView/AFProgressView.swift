//
//  AFProgressView.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/6/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class AFProgressView: UIProgressView {
    
    lazy private var gradientLayer: CAGradientLayer! = self.initGradientLayer()
    lazy private var alphaLayer: CALayer! = self.initAlphaLayer()
    
    lazy private var lineView : UIView = self.initBackgroundLineView()
    
    @IBInspectable var backgroundLineHeight : CGFloat = 1.0 {
        didSet {
            self.updateBackgroundLineViewWidth()
        }
    }
    @IBInspectable var backgroundLineColor : UIColor = UIColor.afBerry {
        didSet {
            lineView.backgroundColor = backgroundLineColor
        }
    }
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        
        self.initColors()
        self.layer.insertSublayer(self.gradientLayer, at: 0)

        self.addSubview(lineView)
        self.sendSubviewToBack(lineView)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initColors()
        self.layer.insertSublayer(self.gradientLayer, at: 0)
        
        self.addSubview(lineView)
        self.sendSubviewToBack(lineView)
    }
    
    func initColors() {
        self.backgroundColor = UIColor.clear
        self.trackTintColor = UIColor.clear
        self.progressTintColor = UIColor.clear
    }
    
    /**
     Create and return a layer.
     
     - returns: CALayer.
     */
    func initAlphaLayer() -> CALayer {
        let alphaLayer = CALayer()
        alphaLayer.frame = self.bounds
        
        alphaLayer.anchorPoint = CGPoint(x: 0, y: 0)
        alphaLayer.position = CGPoint(x: 0, y: 0)
        
        alphaLayer.backgroundColor = UIColor.white.cgColor
        alphaLayer.cornerRadius = 3
        
        return alphaLayer
    }
    
    func initBackgroundLineView() -> UIView {
        let myLineView :UIView = UIView()
        myLineView.backgroundColor = self.backgroundLineColor
        myLineView.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: backgroundLineHeight)
        myLineView.center = self.bounds.center
        myLineView.roundAllCorners(radius: backgroundLineHeight/2)
        
        return myLineView
    }
    // MARK: Lazy initializers
    
    /**
     Create and returns a CAGradientLayer.
     
     - parameter gradientColors: Gradiant colors array.
     
     - returns: CAGradientLayer.
     */
    func initGradientLayer(gradientColors: Array<CGColor> = [UIColor.afSuperPink.cgColor, UIColor.afSuperPink.cgColor]) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        
        gradientLayer.anchorPoint = CGPoint(x: 0, y: 0)
        gradientLayer.position = CGPoint(x: 0, y: 0)
        
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0);
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0);
        
        gradientLayer.colors = gradientColors
        
        gradientLayer.mask = self.alphaLayer
        gradientLayer.cornerRadius = 3
        
        return gradientLayer
    }
    
    /**
     Change Gradiant layer colors.
     
     - parameter newGradientColors: Gradiant colors array.
     - parameter newBackgroundColor: View back ground color.
     
     - returns: Void.
     */
    func changeGradientLayerColors(newGradientColors: Array<CGColor> = [ UIColor.afSuperPink.cgColor, UIColor.afSuperPink.cgColor ], newBackgroundColor: UIColor = UIColor.afBerry) {
        
        self.backgroundLineColor = newBackgroundColor
        let newGradientLayer: CAGradientLayer = self.initGradientLayer(gradientColors: newGradientColors)
        
        self.layer.replaceSublayer(gradientLayer, with: newGradientLayer)
        
        gradientLayer = newGradientLayer
        
    }
    
    
    // MARK: Layout
    
    func updateAlphaLayerFrames() {
        self.alphaLayer.frame =
            self.bounds.sizeByPercentage(width: CGFloat(self.progress))
    }
    
    func updateBackgroundLineViewWidth() {
        lineView.backgroundColor = backgroundLineColor
        lineView.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: backgroundLineHeight)
        lineView.center = self.bounds.center
        lineView.roundAllCorners(radius: backgroundLineHeight/2)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        self.gradientLayer.frame = self.bounds
        self.updateAlphaLayerFrames()
        
        updateBackgroundLineViewWidth()
    }
    
    /**
     Set progress bar value.
     
     - parameter prgress : prgress value.
     - parameter animated: Animation or not.
     
     - returns: Void.
     */
    override public func setProgress(_ progress: Float, animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            // Your code with delay
            super.setProgress(progress, animated: animated)
            self.updateAlphaLayerFrames()
        }
    }
}
