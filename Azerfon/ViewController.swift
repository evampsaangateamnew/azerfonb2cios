//
//  ViewController.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 2/14/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
// jenkins mac-mini safety commit by Mr Fazil

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet var textField: AFTextField!
    @IBOutlet var myView: UIView!
    @IBOutlet var secondView: UIView!
    
    @IBOutlet var mySwitch: AFSwitch!
    
    @IBOutlet var progreess: AFProgressView!
    var isFlip : Bool = false
    
    var circle = UIView()
    var startingPoint = CGPoint.zero {
        didSet {
            circle.center = startingPoint
        }
    }
    
    var circleColor = UIColor.red
    
    var duration = 0.5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.placeholder = "Enter Mobile Number"
        
        //  let colorScheme = MDCColorScheme()
        //  MDCTextFieldColorThemer.apply(colorScheme, to: textField)
        
        
    }
    
    
    @IBAction func valueDidChange(_ sender: AFSwitch) {
        
    }
    
    @IBAction func flip(_ sender: AFButton) {
        
        progreess.setProgress(Float.random(in: 0 ... 1), animated: true)
        
        startingPoint = CGPoint(x: myView.bounds.size.width, y: 0)
        
        
        circle = UIView()
        
        circleColor = UIColor.random
        mySwitch.alpha = 0
        
        circle.frame = frameForCircle(startPoint: startingPoint)
        
        circle.layer.cornerRadius = circle.frame.size.height / 2
        circle.center = startingPoint
        circle.backgroundColor = circleColor
        circle.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        myView.addSubview(circle)
        myView.sendSubviewToBack(circle)
        
        UIView.animate(withDuration: duration, animations: {
            
            self.circle.transform = CGAffineTransform.identity
            
        }, completion: { (success:Bool) in
            if success {
                self.myView.backgroundColor = self.circleColor
                self.circle.removeFromSuperview()
                self.mySwitch.alpha = 1
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    
                }
            }
        })
        
        /*
         if isFlip {
         UIView.transition(from: secondView, to: myView, duration: 2, options: [.transitionFlipFromRight, .showHideTransitionViews], completion: nil)
         isFlip = false
         } else {
         UIView.transition(from: myView, to: secondView, duration: 2, options: [.transitionFlipFromRight, .showHideTransitionViews], completion: nil)
         isFlip = true
         }
         */
        
        
        /*
         UIView.transition(with: myView, duration: 2, options: [.transitionFlipFromRight, .showHideTransitionViews], animations: nil, completion: nil)
         */
        
        
        
    }
    
    func frameForCircle (startPoint:CGPoint) -> CGRect {
        let xLength = fmax(startPoint.x, myView.bounds.size.width - startPoint.x)
        let yLength = fmax(startPoint.y, myView.bounds.size.height - startPoint.y)
        
        let offestVector = sqrt(xLength * xLength + yLength * yLength) * 2
        let size = CGSize(width: offestVector, height: offestVector)
        
        return CGRect(origin: CGPoint.zero, size: size)
        
    }
    
    
    func animate() {
        let layer = CAGradientLayer()
        let startLocations = [0, 0]
        let endLocations = [1, 2]
        
        layer.colors = [UIColor.red.cgColor, UIColor.white.cgColor]
        layer.frame = myView.bounds
        layer.locations = startLocations as [NSNumber]
        layer.startPoint = CGPoint(x: 0.0, y: 1.0)
        layer.endPoint = CGPoint(x: 1.0, y: 1.0)
        myView.layer.addSublayer(layer)
        
        let anim = CABasicAnimation(keyPath: "locations")
        anim.fromValue = startLocations
        anim.toValue = endLocations
        anim.duration = 0.5
        layer.add(anim, forKey: "loc")
        layer.locations = endLocations as [NSNumber]
    }
}


extension UIView {
    
    func flipAnimation(_ completion: @escaping (() -> Void) = {}) {
        
        let angle = 180.0
        layer.transform = get3DTransformation(angle)
        
        UIView.animate(withDuration: 2, delay: 0, usingSpringWithDamping: 0.77, initialSpringVelocity: 0, options: UIView.AnimationOptions(), animations: { () -> Void in
            self.layer.transform = CATransform3DIdentity
        }) { (finished) -> Void in
            completion()
        }
    }
    
    fileprivate func get3DTransformation(_ angle: Double) -> CATransform3D {
        
        var transform = CATransform3DIdentity
        transform.m34 = -1.0 / 500.0
        transform = CATransform3DRotate(transform, CGFloat(angle * Double.pi / 180.0), 0, 1, 0.0)
        
        return transform
    }
}


extension UIColor {
    /**
     * Returns random color
     * EXAMPLE: self.backgroundColor = UIColor.random
     */
    static var random: UIColor {
        let r:CGFloat  = .random(in: 0 ... 1)
        let g:CGFloat  = .random(in: 0 ... 1)
        let b:CGFloat  = .random(in: 0 ... 1)
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }
}
