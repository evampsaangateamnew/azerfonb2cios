//
//  SingleAttributeCell.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 7/17/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class SingleAttributeCell: UITableViewCell {

    //MARK: - IBOutlets
   
    @IBOutlet var myContentView     : UIView!
    @IBOutlet var iconImageView     : UIImageView!
    @IBOutlet var titleLabel        : AFMarqueeLabel!
    @IBOutlet var valueLabel        : AFLabel!
    @IBOutlet var separatorView     : AFView!
    @IBOutlet var iconImageViewWidthConstraint: NSLayoutConstraint!
    
    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Using in Tariff Postpaid section
    func setAttributeHeader(_ aAttribute : HeaderAttributes?, showSeprator : Bool) {
 
        self.myContentView.backgroundColor = UIColor.afWhite
        
        if showSeprator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.afWhite
        }
        
        if let aAttributeObject = aAttribute {
            
            titleLabel.text = aAttributeObject.title ?? ""
            
            // Text color
            titleLabel.textColor = UIColor.afPurplishBrown
            
            /*if aAttributeObject.metrics?.isHasFreeOrUnlimitedText() == true {
                
                valueLabel.textColor = UIColor.mbBrandRed
                
            } else {
                valueLabel.textColor = UIColor.black
                
            }*/
            
            valueLabel.text = "\(aAttributeObject.value ?? "") \(aAttributeObject.metrics ?? "")"
            
            if aAttributeObject.iconName?.isBlank ?? false {
                iconImageViewWidthConstraint.constant = 0
            } else {
                iconImageViewWidthConstraint.constant = 26
                iconImageView.image = UIImage.imageFor(key: aAttributeObject.iconName)
            }
            
        } else {
            titleLabel.text = ""
            valueLabel.text = ""
        }
    }
    
    func setAttributeHeader(_ aAttribute : AttributeList?, showSeprator : Bool) {
        
        self.myContentView.backgroundColor = UIColor.afWhite
        if showSeprator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.afWhite
        }
        
        
        
        if let aAttributeObject = aAttribute {
            
            titleLabel.text = aAttributeObject.title ?? ""
            
            // Text color
            titleLabel.textColor = UIColor.afPurplishBrown
            
            /*if aAttributeObject.metrics?.isHasFreeOrUnlimitedText() == true {
             
             valueLabel.textColor = UIColor.mbBrandRed
             
             } else {
             valueLabel.textColor = UIColor.black
             
             }*/
            
            valueLabel.text = "\(aAttributeObject.value ?? "") \(aAttributeObject.unit ?? "")"
            
            if aAttributeObject.iconMap?.isBlank ?? false {
                iconImageViewWidthConstraint.constant = 0
            } else {
                iconImageViewWidthConstraint.constant = 26
                iconImageView.image = UIImage.imageFor(key: aAttributeObject.iconMap)
            }
            
        } else {
            titleLabel.text = ""
            valueLabel.text = ""
        }
    }
    
}
