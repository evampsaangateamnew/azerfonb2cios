//
//  ServicesCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class ServicesCell: UITableViewCell {

    //MARKL: - IBOutlet
    @IBOutlet var lblName : UILabel!
    @IBOutlet var serviceImageView : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /**
     Sets Item labels values.
     
     - parameter aItem: Services Names.
     
     - returns: void.
     */
    func setItemInfo(aItem: Items) {
        
        if aItem.identifier.isEqual("services_special_offers", ignorCase: true) {
            let attributedTile = "\(aItem.title) ".createAttributedString(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h2Grey)
            attributedTile.appendImage(imageName: "special_offer_indicator", font: UIFont.h2Grey, imageSize: CGSize(width: 10, height: 10))
            lblName?.attributedText = attributedTile
        } else {
            lblName?.attributedText = aItem.title.createAttributedString(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h2Grey)
        }
        print("aItem.identifier:\(aItem.identifier)")
        print("image name:\(self.imageNameFor(Identifier: aItem.identifier))")
        serviceImageView?.image = UIImage.imageFor(name: self.imageNameFor(Identifier: aItem.identifier))
    
    }
    func setItemInfo2(aItem: SubItems) {
        
        if aItem.identifier.isEqual("services_special_offers", ignorCase: true) {
            let attributedTile = "\(aItem.title) ".createAttributedString(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h2Grey)
            attributedTile.appendImage(imageName: "special_offer_indicator", font: UIFont.h2Grey, imageSize: CGSize(width: 10, height: 10))
            lblName?.attributedText = attributedTile
        } else {
            lblName?.attributedText = aItem.title.createAttributedString(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h2Grey)
        }
        print("aItem.identifier:\(aItem.identifier)")
        print("image name:\(self.imageNameFor(Identifier: aItem.identifier))")
        serviceImageView?.image = UIImage.imageFor(name: self.imageNameFor(Identifier: aItem.identifier))
    }
    
    /**
     Provide image name against identifiers. which are mapped.
     
     - parameter identifier: key for mapped image.
     
     - returns: image name for mapped identifier.
     */
    func imageNameFor(Identifier identifier : String) -> String {
        
        switch identifier.lowercased() {
        
        //services
        case "services_special_offers":
            return "specialOfferServiceIcon"
            
        case "services_supplementary_offers":
            return "packages_icon"
            
        case "services_free_sms":
            return "freeSmsServiceIcon"
            
        case "services_fnf":
            return "fnfServiceIcon"
            
        case "services_vas_services":
            return "vasServiceIcon"
            
        case "services_exchange_service":
            return "exchangeServiceIcon"
            
        case "services_nar_tv":
            return "narTvServiceIcon"
            
        case "service_topup":
            return "balanceService"
            
        //TopUp
        case "topup_topup":
            return "topupTIcon"
            
        case "topup_money_transfer":
            return "moneyTransferTopupIcon"
            
        case "topup_money_request":
            return "monetRequestTopupIcon"
            
        case "topup_loan":
            return "loanTopupIcon"
            
        case "topup_fasttopup":
            return "fastTopup"

        case "topup_autopayment":
            return "autoPayment"
        //MyAccount
        case "myaccount_usage_history":
            return "usageMyAccountIcon"
            
        case "myaccount_operations_history":
            return "operationsMyAccountIcon"
            
        case "myaccount_my_subscriptions":
            return "subscriptionMyAccountIcon"
            
        case "dataUsage_withoutPack":
            return "services_internet"
            
        case "datausage_withoutpack":
            return "services_internet"
            
        default:
            return ""
            
        }
    }
}

