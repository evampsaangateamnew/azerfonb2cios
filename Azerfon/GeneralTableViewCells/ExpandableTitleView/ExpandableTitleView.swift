//
//  FAQsHeaderCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/25/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class ExpandableTitleView: AFAccordionTableViewHeaderView {

    //MARK:- IBOutlets
    @IBOutlet var lineView: UIView!
    @IBOutlet var indicatorImageView: UIImageView!
    @IBOutlet var titleLabel: AFMarqueeLabel!
        
    /**
     Sets value for Title Label.
     
     - parameter text: title text.
     
     - returns: void.
     */
    func setTileLabelText(_ text: String?) {
        titleLabel.text = text ?? ""
    }
    
    /**
     Sets expand image based on expanded status.
     
     - parameter isExpanded: Wheter expanded or not.
     
     - returns: void.
     */
    func setExpandStatus(_ isExpanded : Bool) {
        
        if isExpanded == true {
            self.indicatorImageView.image  = UIImage.imageFor(name: "pinkMinusIcon")
        } else {
            self.indicatorImageView.image = UIImage.imageFor(name: "pinkPlusIcon")
        }
    }
    
}
