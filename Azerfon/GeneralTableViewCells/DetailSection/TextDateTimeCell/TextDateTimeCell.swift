//
//  TextDateTimeCell.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TextDateTimeCell: UITableViewCell {
    
    //MARK:- Properties
    var iconDefaultWidth : CGFloat {
        return 26
    }
    
    //MARK: - IBOutlets
    
    //  title label
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: AFMarqueeLabel!
    @IBOutlet var titleLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var iconImageViewWidthConstraint: NSLayoutConstraint!
    
    //  Detail label
    @IBOutlet var detailLabel: AFLabel!
    
    //  Date and Time View
    @IBOutlet var dateTimeView: UIView!
    @IBOutlet var fromTitleValueLabel: AFMarqueeLabel!
    @IBOutlet var toTitleValueLabel: AFMarqueeLabel!
    @IBOutlet var dateTimeViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var separatorView     : AFView!
    
    //MARK: - Functions 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /**
     Set setTextWithTitleLayoutValues.
     
     - parameter textWithTitle: TextWithTitle.
     
     - returns: Void.
     */
    func setTextWithTitleLayoutValues(textWithTitle : TextWithTitle?, showSeprator : Bool) {
        
        if showSeprator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.clear
        }
        
        if let iconName = textWithTitle?.titleIcon,
            iconName.isBlank == false {
            iconImageView.image = UIImage.imageFor(key: iconName)
            iconImageViewWidthConstraint.constant = iconDefaultWidth
            iconImageView.isHidden = false
            
        } else {
            iconImageViewWidthConstraint.constant = 0
            iconImageView.isHidden = true
        }
        
        titleLabelHeightConstraint.constant     = 22
        dateTimeViewHeightConstraint.constant   = 0
        
        if let textWithTitle = textWithTitle {
            
            titleLabel.text = textWithTitle.title ?? ""
            detailLabel.loadHTMLString(htmlString: textWithTitle.text)
            
        } else {
            titleLabel.text = ""
            detailLabel.text = ""
        }
    }
    
    /**
     Set setTextWithOutTitleLayoutValues.
     
     - parameter textWithOutTitle: String.
     
     - returns: Void.
     */
    func setTextWithOutTitleLayoutValues(textWithOutTitle : String?, showSeprator : Bool) {
        
        if showSeprator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.clear
        }
        
        iconImageViewWidthConstraint.constant = 0
        iconImageView.isHidden = true
        
        titleLabelHeightConstraint.constant     = 0
        dateTimeViewHeightConstraint.constant   = 0
        
        detailLabel.loadHTMLString(htmlString: textWithOutTitle)
        
    }
    
    /**
     Set setDateAndTimeLayoutValues.
     
     - parameter dateAndTime: DateAndTime.
     
     - returns: Void.
     */
    func setDateAndTimeLayoutValues(dateAndTime : Time_DateDetails?, showSeprator : Bool) {
        
        if showSeprator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.clear
        }
        
        if let iconName = dateAndTime?.timeIcon,
            iconName.isBlank == false {
            
            iconImageView.image = UIImage.imageFor(key: iconName)
            iconImageViewWidthConstraint.constant = iconDefaultWidth
            iconImageView.isHidden = false
            
        } else if let iconName = dateAndTime?.dateIcon,
            iconName.isBlank == false {
            
            iconImageView.image = UIImage.imageFor(key: iconName)
            iconImageViewWidthConstraint.constant = iconDefaultWidth
            iconImageView.isHidden = false
            
        } else {
            iconImageViewWidthConstraint.constant = 0
            iconImageView.isHidden = true
        }
        
        titleLabelHeightConstraint.constant     = 0
        dateTimeViewHeightConstraint.constant   = 23
        
        if let dateAndTime = dateAndTime {
            
            detailLabel.loadHTMLString(htmlString: dateAndTime.description)
            
            fromTitleValueLabel.attributedText = "\(dateAndTime.fromTitle ?? "") \(dateAndTime.fromValue ?? "")"
                .createAttributedString(mainStringColor: UIColor.afGunmetal,
                                        mainStringFont: UIFont.b4LightGrey,
                                        highlightedStringColor: UIColor.afBrownGrey,
                                        highlightedStringFont: UIFont.b4BoldLightGrey,
                                        highlighStrings: ["\(dateAndTime.fromValue ?? "")"])
            
            toTitleValueLabel.attributedText = "\(dateAndTime.toTitle ?? "") \(dateAndTime.toValue ?? "")"
                .createAttributedString(mainStringColor: UIColor.afGunmetal,
                                        mainStringFont: UIFont.b4LightGrey,
                                        highlightedStringColor: UIColor.afBrownGrey,
                                        highlightedStringFont: UIFont.b4BoldLightGrey,
                                        highlighStrings: ["\(dateAndTime.toValue ?? "")"])
            
            
            
        } else {
            detailLabel.text = ""
            fromTitleValueLabel.text = ""
            toTitleValueLabel.text = ""
        }
    }
    
    /**
     Set setTextWithPointsLayoutValues.
     
     - parameter textWithPoint: Array of strings.
     
     - returns: Void.
     */
    func setTextWithPointsLayoutValues(textWithPoint : TextWithPoints?, showSeprator : Bool) {
        
        if showSeprator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.clear
        }
        
        if let iconName = textWithPoint?.titleIcon,
            iconName.isBlank == false {
            iconImageView.image = UIImage.imageFor(key: iconName)
            iconImageViewWidthConstraint.constant = iconDefaultWidth
            iconImageView.isHidden = false
            
        } else {
            iconImageViewWidthConstraint.constant = 0
            iconImageView.isHidden = true
        }
        
        titleLabelHeightConstraint.constant     = 22
        dateTimeViewHeightConstraint.constant   = 0
        
        if let textWithTitle = textWithPoint?.title,
            textWithTitle.isBlank == false {
            
            titleLabel.text = textWithTitle
        } else {
            titleLabel.text = ""
        }
        
        if let pointList = textWithPoint?.pointsList {
            
            let attributedText = NSMutableAttributedString()
            let bulletPoint: String = "\u{2022}"
            
            pointList.forEach({ (aString) in
                
                let aAttribute = NSAttributedString(string: "\(bulletPoint) \(aString ?? "")\n",
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.afGunmetal])
                
                attributedText.append(aAttribute)
                
            })
            
            detailLabel.attributedText  = attributedText
        } else {
            detailLabel.text = ""
        }
    }
    
}
