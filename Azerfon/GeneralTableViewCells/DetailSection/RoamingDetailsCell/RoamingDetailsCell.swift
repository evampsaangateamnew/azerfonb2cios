//
//  RoamingDetailsCell.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class RoamingDetailsCell: UITableViewCell {
    
    //MARK:- Properties
    var iconDefaultWidth : CGFloat {
        return 26
    }
    
    //MARK: - IBOutlets
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var iconImageViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var descriptionAboveLabel: AFLabel!
    
    @IBOutlet var roamingCountriesDetailsView: UIView!
    @IBOutlet var roamingCountriesDetailsViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet var descriptionBelowLabel: AFLabel!
    
    @IBOutlet var separatorView     : AFView!

    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    /**
     Set setRoamingDetailsLayout.
     
     - parameter roamingDetails: RoamingDetails.
     
     - returns: Void.
     */
    func setRoamingDetailsLayout(roamingDetails : RoamingDetails?, showSeprator : Bool) {
        
        if showSeprator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.clear
        }

        roamingCountriesDetailsView.removeAllSubViews()
        
        if let roamingDetails = roamingDetails {

            if let iconName = roamingDetails.roamingIcon,
                iconName.isBlank == false {
                
                iconImageView.image = UIImage.imageFor(key: iconName)
                iconImageViewWidthConstraint.constant = iconDefaultWidth
                iconImageView.isHidden = false
                
            } else {
                iconImageViewWidthConstraint.constant = 0
                iconImageView.isHidden = true
            }
            
            descriptionAboveLabel.loadHTMLString(htmlString: roamingDetails.descriptionAbove?.removeNullValues() ?? "")
            descriptionBelowLabel.loadHTMLString(htmlString: roamingDetails.descriptionBelow?.removeNullValues() ?? "")

            var lastView : UIView?
            var allCountryViewHeight : CGFloat = 0.0
            
            roamingDetails.roamingDetailsCountriesList?.forEach({ (aRoamingCountryDetail) in

                let attributesView : CountryView = CountryView.fromNib()
                attributesView.translatesAutoresizingMaskIntoConstraints = false

                let countryViewHeight = attributesView.setRoamingDetailsCountriesListLayout(aRoamingDetailsCountries: aRoamingCountryDetail)

                allCountryViewHeight = allCountryViewHeight + countryViewHeight

                roamingCountriesDetailsView.addSubview(attributesView)

                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingCountriesDetailsView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)

                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingCountriesDetailsView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingCountriesDetailsView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 8)

                    allCountryViewHeight += 8
                }


                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: countryViewHeight)
                
                roamingCountriesDetailsView.addConstraints([leading, trailing, top, height])

                lastView = attributesView

            })

            roamingCountriesDetailsViewHeightConstraint.constant = allCountryViewHeight

        } else {
            descriptionAboveLabel.text = ""
            descriptionBelowLabel.text = ""
        }

    }
    
}
