//
//  TitleSubTitleValueAndDescCell.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TitleSubTitleValueAndDescCell: UITableViewCell {
    //MARK:- Properties
    var iconDefaultWidth : CGFloat {
        return 26
    }
    //MARK: - IBOutlets
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var iconImageViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var mainTitleLabel: AFLabel!
    @IBOutlet var attributedView: UIView!
    @IBOutlet var attributedViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var mainTitleLabelHeightConstraint: NSLayoutConstraint!

    @IBOutlet var separatorView     : AFView!
    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    /**
     Set Sub Title, values and descriptions.
     
     - parameter titleSubTitleValueAndDesc: TitleSubTitleValueAndDesc.
     
     - returns: Void.
     */
    func setTitleSubTitleValueAndDescLayoutValues (titleSubTitleValueAndDesc : TitleSubTitleValueAndDesc?, showSeprator : Bool) {

        if showSeprator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.clear
        }

        if let titleSubTitleValueAndDesc = titleSubTitleValueAndDesc {
            
            if let iconName = titleSubTitleValueAndDesc.iconName,
                iconName.isBlank == false {
                
                iconImageView.image = UIImage.imageFor(key: iconName)
                iconImageViewWidthConstraint.constant = iconDefaultWidth
                iconImageView.isHidden = false
                
            } else {
                iconImageViewWidthConstraint.constant = 0
                iconImageView.isHidden = true
            }
            
            if let title = titleSubTitleValueAndDesc.title,
                title.isBlank == false {
                
                mainTitleLabelHeightConstraint.constant = 26
                mainTitleLabel.text = title
            
            } else {
                mainTitleLabelHeightConstraint.constant = 0
                mainTitleLabel.text = ""
            }
            
            attributedView.subviews.forEach({ (aView) in

                if aView is SubTitleValueAndDescView {
                    aView.removeFromSuperview()
                }
            })

            var lastView : UIView?
            var subViewsHeightValue : CGFloat = 0.0

            titleSubTitleValueAndDesc.attributesList?.forEach({ (aAttribute) in

                let aAttributesView : SubTitleValueAndDescView = SubTitleValueAndDescView.fromNib()
                aAttributesView.translatesAutoresizingMaskIntoConstraints = false

                let aSubViewHeight : CGFloat = aAttributesView.setSubTitleValueAndDescription(aAttribute: aAttribute)
                subViewsHeightValue = subViewsHeightValue + aSubViewHeight

                attributedView.addSubview(aAttributesView)

                let leading = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributedView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributedView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributedView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }

                let height = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: aSubViewHeight)

                attributedView.addConstraints([leading, trailing, top, height])

                lastView = aAttributesView
                
            })
            
            if let _ = titleSubTitleValueAndDesc.attributesList?.count {
                attributedViewHeightConstraint.constant = (subViewsHeightValue)
            } else {
                attributedViewHeightConstraint.constant = 0.0
            }

        } else {
            mainTitleLabelHeightConstraint.constant = 0
            attributedViewHeightConstraint.constant = 0
            iconImageViewWidthConstraint.constant = 0
            iconImageView.isHidden = true
        }
    }
    
}
