//
//  FreeResourceValidityCell.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class FreeResourceValidityCell: UITableViewCell {

    //MARK:- Properties
    var iconDefaultWidth : CGFloat {
        return 26
    }
    
    //MARK: - IBOutlets
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var iconImageViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var titleLabel            : AFLabel!
    @IBOutlet var titleValueLabel       : AFLabel!
    @IBOutlet var subTitleLabel         : AFLabel!
    @IBOutlet var subTilteValueLabel    : AFMarqueeLabel!
    @IBOutlet var descriptionLabel      : AFLabel!
    
    @IBOutlet var separatorView     : AFView!

    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    /**
     Set FreeResourceValidityValues.
     
     - parameter freeResourceValidity: FreeResourceValidity.
     
     - returns: Void.
     */
    func setFreeResourceValidityValues(freeResourceValidity : FreeResourceValidity?, showSeprator : Bool) {
        
        if showSeprator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.clear
        }

        if let freeResourceValidity = freeResourceValidity {
            
            if let iconName = freeResourceValidity.iconName,
                iconName.isBlank == false {
                
                iconImageView.image = UIImage.imageFor(key: iconName)
                iconImageViewWidthConstraint.constant = iconDefaultWidth
                iconImageView.isHidden = false
                
            } else {
                iconImageViewWidthConstraint.constant = 0
                iconImageView.isHidden = true
            }
            
            titleLabel.text         = freeResourceValidity.title ?? ""
            titleValueLabel.text    = freeResourceValidity.titleValue ?? ""
            subTitleLabel.text      = freeResourceValidity.subTitle ?? ""
            subTilteValueLabel.text = freeResourceValidity.subTitleValue ?? ""

            descriptionLabel.loadHTMLString(htmlString: freeResourceValidity.description)
        
        } else {
            titleLabel.text         = ""
            titleValueLabel.text    = ""
            subTitleLabel.text      = ""
            subTilteValueLabel.text = ""
            descriptionLabel.text   = ""
            iconImageViewWidthConstraint.constant = 0
            iconImageView.isHidden = true
        }
    }
    
}
