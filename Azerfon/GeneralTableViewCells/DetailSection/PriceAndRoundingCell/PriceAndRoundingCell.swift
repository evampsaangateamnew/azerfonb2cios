//
//  PriceAndRoundingCell.swift
//  TestForAddingViewRunTimeINTableViewCell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 AbdulRehman Warraich. All rights reserved.
//

import UIKit

class PriceAndRoundingCell: UITableViewCell {
    
    //MARK:- Properties
    var iconDefaultWidth : CGFloat {
        return 26
    }
    
    //MARK:- IBOutlets
    // Main Views
    @IBOutlet var titleAndValueView: UIView!
    @IBOutlet var attributeListView: UIView!
    @IBOutlet var descriptionView: UIView!
    
    // TitleAndValueView iner outlets
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: AFMarqueeLabel!
    @IBOutlet var rightValueLabel: AFMarqueeLabel!
    
    @IBOutlet var iconImageViewWidthConstraint: NSLayoutConstraint!
    
    // AttributeListView iner outlets
    @IBOutlet var attributeListViewHightConstraint: NSLayoutConstraint!
    
    // DescriptionView iner outlets
    @IBOutlet var descriptionLabel: AFLabel!
    @IBOutlet var descriptionViewHightConstraint: NSLayoutConstraint!
    
    // seprator view
    @IBOutlet var sepratorView: UIView!
    
    //MARK:- Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /**
     Using in supplementery offers.
     
     - parameter price: Price details.
     - parameter showIcon: Whether show icon or not. Bydefault it's true.
     
     - returns: void.
     */
    func setPriceLayoutValues (price : Price?, showIcon: Bool = true, showSeprator : Bool) {
        
        if showSeprator {
            sepratorView.backgroundColor = UIColor.afSeparator
        } else {
            sepratorView.backgroundColor = UIColor.clear
        }
        if let price = price {
            
            if showIcon {
                iconImageView.image = UIImage.imageFor(key: price.iconName)
                iconImageViewWidthConstraint.constant = iconDefaultWidth
                iconImageView.isHidden = false
                
            } else {
                iconImageViewWidthConstraint.constant = 0
                iconImageView.isHidden = true
            }
            
            titleLabel.text = price.title ?? ""
            
            if price.value?.isBlank == false,
                let offersCurrency = price.offersCurrency {
                
                if offersCurrency.isEqual("manat", ignorCase: true) {
                    
                    rightValueLabel.attributedText = "\(price.value ?? "")".createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
                    
                } else {
                    rightValueLabel.text = "\(price.value ?? "") \(offersCurrency)"
                }
                
            } else {
                rightValueLabel.text = price.value ?? ""
            }
            
            /* Setting botton discription */
            if let description = price.description {
                descriptionLabel.loadHTMLString(htmlString: description)
                descriptionViewHightConstraint.isActive = false
            } else {
                descriptionLabel.text = ""
                descriptionViewHightConstraint.isActive = true
            }
            
            
            /* Setting attributed values */
            attributeListView.subviews.forEach({ (aView) in
                
                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })
            
            var lastView : UIView?
            price.attributeList?.forEach({ (aAttribute) in
                
                let attributesView : AttributeDetailView = AttributeDetailView.fromNib()
                attributesView.translatesAutoresizingMaskIntoConstraints = false
                
                attributesView.setAttributeValues(showManatSign: true, aAttribute: aAttribute)
                
                attributeListView.addSubview(attributesView)
                
                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                
                var top : NSLayoutConstraint = NSLayoutConstraint()
                
                if lastView == nil{
                    
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)
                    
                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }
                
                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)
                
                attributeListView.addConstraints([leading, trailing, top, height])
                
                lastView = attributesView
                
            })
            
            if let count = price.attributeList?.count {
                attributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                attributeListViewHightConstraint.constant = 0.0
            }
        }
    }
    
    /**
     Set Rounding values.
     
     - parameter rounding: Rounding details.
     - parameter showIcon: Whether show icon or not. Bydefault it's true.
     
     - returns: void.
     */
    func setRoundingLayoutValues (rounding : Rounding?, showIcon: Bool = true, showSeprator : Bool) {
        
        if showSeprator {
            sepratorView.backgroundColor = UIColor.afSeparator
        } else {
            sepratorView.backgroundColor = UIColor.clear
        }
        
        if let rounding = rounding {
            
            if showIcon {
                iconImageView.image = UIImage.imageFor(key: rounding.iconName)
                iconImageViewWidthConstraint.constant = iconDefaultWidth
                iconImageView.isHidden = false
                
            } else {
                iconImageViewWidthConstraint.constant = 0
                iconImageView.isHidden = true
            }
            
            titleLabel.text = rounding.title ?? ""
            rightValueLabel.attributedText = rounding.value?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
            
            if let description = rounding.description {
                
                descriptionLabel.loadHTMLString(htmlString: description)
                descriptionViewHightConstraint.isActive = false
            } else {
                descriptionLabel.text = ""
                descriptionViewHightConstraint.isActive = true
            }
            
            attributeListView.subviews.forEach({ (aView) in
                
                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })
            var lastView : UIView?
            rounding.attributeList?.forEach({ (aAttribute) in
                
                let attributesView : AttributeDetailView = AttributeDetailView.fromNib()
                attributesView.translatesAutoresizingMaskIntoConstraints = false
                
                attributesView.setAttributeValues(showManatSign: false, aAttribute: aAttribute)
                
                attributeListView.addSubview(attributesView)
                
                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                
                var top : NSLayoutConstraint = NSLayoutConstraint()
                
                if lastView == nil{
                    
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)
                    
                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }
                
                
                
                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)
                
                attributeListView.addConstraints([leading, trailing, top, height])
                
                lastView = attributesView
                
            })
            
            if let count = rounding.attributeList?.count{
                attributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                attributeListViewHightConstraint.constant = 0.0
            }
        }
    }
    
}

