//
//  PriceCell.swift
//
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 AbdulRehman Warraich. All rights reserved.
//

import UIKit

class PriceCell: UITableViewCell {
    
    //  MARK:- IBOutlets
    /* Main Views */
    @IBOutlet var titleAndValueView                 : UIView!
    @IBOutlet var attributeListView                 : UIView!
    
    // TitleAndValueView iner outlets
    @IBOutlet var iconImageView                     : UIImageView!
    @IBOutlet var titleLabel                        : AFLabel!
    @IBOutlet var rightValueLabel                   : AFLabel!
    @IBOutlet var leftValueLabel                    : AFLabel!
    @IBOutlet var leftValueLabelWidthConstraint     : NSLayoutConstraint!
    
    @IBOutlet var centerSepratorView                : UIView!
    @IBOutlet var sepratorView: UIView!
    // AttributeListView iner outlets
    @IBOutlet var attributeListViewHightConstraint  : NSLayoutConstraint!
    
    
    //MARK:- Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /**
     Using in Tariff screen.
     
     - parameter internetHeader: InternetPriceSection details.
     
     - returns: void.
     */
    func setInternetLayoutValues (internetHeader : Call_CallPayg?, showSeprator : Bool) {
        
        self.contentView.backgroundColor = UIColor.white
        
        sepratorView.isHidden = false
        if showSeprator {
            sepratorView.backgroundColor = UIColor.afSeparator
        } else {
            sepratorView.backgroundColor = UIColor.clear
        }
        
        centerSepratorView.isHidden = true
        
        if let callHeaderType = internetHeader {
            
            titleLabel.text = callHeaderType.title ?? ""
            
            leftValueLabelWidthConstraint.constant = 0
            
            rightValueLabel.attributedText = callHeaderType.titleValue?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afGunmetal, mainStringFont: UIFont.b3LightGreyRight, imageSize: CGSize(width: 10, height: 6))
            
            iconImageView.image = UIImage.imageFor(key: callHeaderType.iconName)
            
            
            attributeListView.subviews.forEach({ (aView) in
                
                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })
            
            var lastView : UIView?
            callHeaderType.attributes?.forEach({ (aAttribute) in
                
                let attributesView : AttributeDetailView = AttributeDetailView.fromNib()
                attributesView.translatesAutoresizingMaskIntoConstraints = false
                
                attributesView.setAttributeValuesForCall(showManatSign: true, aAttribute: aAttribute, cellType: .internet, priceTemplate: callHeaderType.priceTemplate)
                
                attributeListView.addSubview(attributesView)
                
                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                
                var top : NSLayoutConstraint = NSLayoutConstraint()
                
                if lastView == nil{
                    
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)
                    
                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }
                
                
                
                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)
                
                attributeListView.addConstraints([leading, trailing, top, height])
                
                lastView = attributesView
                
            })
            
            if let count = callHeaderType.attributes?.count {
                attributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                attributeListViewHightConstraint.constant = 0.0
            }
        }
    }
    /**
     Set calls, smsm details.
     
     - parameter callHeaderType: CallSMSPriceSection details.
     - parameter cellType: MBOfferType details.
     - parameter setBackgroundColorWhite: Whether set background color white or not. Bydefault it's true.
     
     - returns: void.
     */
    func setCallAndSMSLayoutValues (callHeaderType : Call_CallPayg?, cellType : Constants.AFOfferType, showSeprator : Bool) {
        
        self.contentView.backgroundColor = UIColor.white
        
        sepratorView.isHidden = false
        if showSeprator {
            sepratorView.backgroundColor = UIColor.afSeparator
        } else {
            sepratorView.backgroundColor = UIColor.clear
        }
        
        
        centerSepratorView.isHidden = true
        
        if let callHeaderType = callHeaderType {
            
            titleLabel.text = callHeaderType.title ?? ""
            
            if (cellType == .call || cellType == .callPayG ){
                rightValueLabel.text = callHeaderType.titleValue ?? ""
                
                // Show double titles and values
                // Note: if "Double Values" then left title will not be displed
                
                if callHeaderType.priceTemplate?.isEqual( Constants.k_DoubleTitlesKey, ignorCase: true) ?? false{
                    
                    leftValueLabelWidthConstraint.constant = 80
                    centerSepratorView.isHidden = false
                    
                    leftValueLabel.text = callHeaderType.titleValueLeft ?? ""
                    
                } else {
                    leftValueLabelWidthConstraint.constant = 0
                    centerSepratorView.isHidden = true
                    
                    leftValueLabel.text = ""
                }
                
                rightValueLabel.text = callHeaderType.titleValueRight ?? ""
                
            } else {
                rightValueLabel.text = callHeaderType.titleValue ?? ""
                leftValueLabelWidthConstraint.constant = 0
            }
            
            rightValueLabel.attributedText = rightValueLabel.text?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afGunmetal, mainStringFont: UIFont.b3LightGreyRight, imageSize: CGSize(width: 10, height: 6))
            
            iconImageView.image = UIImage.imageFor(key: callHeaderType.iconName)
            
            
            attributeListView.subviews.forEach({ (aView) in
                
                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })
            
            var lastView : UIView?
            callHeaderType.attributes?.forEach({ (aAttribute) in
                
                let attributesView : AttributeDetailView = AttributeDetailView.fromNib()
                attributesView.translatesAutoresizingMaskIntoConstraints = false
                
                attributesView.setAttributeValuesForCall(showManatSign: true, aAttribute: aAttribute, cellType: cellType, priceTemplate: callHeaderType.priceTemplate)
                
                attributeListView.addSubview(attributesView)
                
                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                
                var top : NSLayoutConstraint = NSLayoutConstraint()
                
                if lastView == nil{
                    
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)
                    
                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }
                
                
                
                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)
                
                attributeListView.addConstraints([leading, trailing, top, height])
                
                lastView = attributesView
                
            })
            
            if let count = callHeaderType.attributes?.count {
                attributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                attributeListViewHightConstraint.constant = 0.0
            }
        }
    }
    
}

