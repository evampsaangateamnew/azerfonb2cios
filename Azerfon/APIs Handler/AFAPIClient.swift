//
//  AFAPIClient.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//
import UIKit
import Alamofire
import ObjectMapper

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}

class AFAPIClient: AFAPIClientHandler {
    
    
    static var shared: AFAPIClient = {
        
        let baseURL = URL(string: Constants.kAFAPIClientBaseURL)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = Constants.AFAPIClientDefaultTimeOut
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        
        // Configure the trust policy manager
        // With SSL Pining certificate
        /*
         let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
         certificates: ServerTrustPolicy.certificates(),
         validateCertificateChain: true,
         validateHost: true
         )
         */
        
        // With Public key
//        let serverTrustPolicy = ServerTrustPolicy.pinPublicKeys(
//            publicKeys: ServerTrustPolicy.publicKeys(),
//            validateCertificateChain: true,
//            validateHost: true)
        
        // With out SSL Pining
        let serverTrustPolicy = ServerTrustPolicy.disableEvaluation
        
        let serverTrustPolicies = [baseURL?.host ?? "": serverTrustPolicy]
        let serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
        
        // Configure session manager with trust policy
        let instance = AFAPIClient (
            baseURL: baseURL!,
            configuration: configuration,
            serverTrustPolicyManager: serverTrustPolicyManager
        )
        
        // SessionManager instance.
        return instance
    }()
    
    
    // MARK: - verifyAppVersion Request
    
    func verifyAppVersion(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservices/verifyappversion"
        
        let params = ["appversion": UIDevice.appVersion()]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - authenticate User Request
    func authenticateUser(_ msisdn: String, Password password: String,  _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservices/authenticateuser"
        
        let params: [String : String] = ["msisdn" : msisdn, "password" : password]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - verify Number Request
    func verifyNumber(_ msisdn: String, Cause cause: Constants.AFResendType,  _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservices/signup"
        
        let params: [String : String] = ["msisdn" : msisdn, "cause" : cause.rawValue]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - verify OTP Request
    func verifyOTP(_ msisdn: String, Cause cause: Constants.AFResendType, Pin pin:String ,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservices/verifyotp"
        
        let params: [String : String] = ["msisdn" : msisdn, "cause" : cause.rawValue, "pin": pin]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - Resend OTP Request
    func resendPin(_ msisdn: String, Cause cause: Constants.AFResendType,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservices/resendpin"
        
        let params: [String : String] = ["msisdn" : msisdn, "cause" : cause.rawValue]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - Save Customer Request
    func saveCustomer(_ msisdn: String, Password password :String, ConfirmPassword confirmPassword :String, Temp temp :String, isTermsAccepted :Bool,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        let serviceName = "customerservices/savecustomer"
        
        let params: [String : String] = ["msisdn" : msisdn,
                                         "password" : password,
                                         "confirm_password" :confirmPassword,
                                         "temp" :temp,
                                         "terms_and_conditions" : isTermsAccepted ? "1" : "0"]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - forgot Password Request
    func forgotPassword(_ msisdn: String, Password password :String, ConfirmPassword confirmPassword :String, Temp temp :String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        let serviceName = "customerservices/forgotpassword"
        
        let params: [String : String] = ["msisdn" : msisdn,
                                         "password" : password,
                                         "confirmPassword" :confirmPassword,
                                         "temp" :temp]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - App Menu Request
    func getAppMenu(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        let serviceName = "menus/getappmenu"
        
        let params: [String : String] = ["msisdn" : AFUserSession.shared.msisdn,
                                         "offeringName" :AFUserSession.shared.userInfo?.offeringName ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - App Resume Update Dashboard data
    
    func appResume(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservices/appresume"
        
        let params = ["customerId": AFUserSession.shared.userInfo?.customerId ?? "",
                      "entityId":AFUserSession.shared.userInfo?.entityId ?? ""]
        
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    func rateUs(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "rateV2/rateus"
        
        let params = ["entityId":AFUserSession.shared.userInfo?.entityId ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - Roaming API Requests
    
    func getRateUs(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "rateV2/getrateus"
        
        let params = ["entityId":AFUserSession.shared.userInfo?.entityId ?? ""]
        
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getNotificationsCount Request
    func getNotificationsCount(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "notificationsV2/getnotificationscount"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getNotificationsCount Request
    func getHomePage(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "homepageservices/gethomepage"
        
        let params = ["customerType" :AFUserSession.shared.userInfo?.customerType ?? "",
                      "brandId" :AFUserSession.shared.userInfo?.brandId ?? "",
                      "offeringId" :AFUserSession.shared.userInfo?.offeringId ?? "",
                      "subscriberType" :AFUserSession.shared.userInfo?.subscriberType ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - addFCMId Request
    
    func addFCMId(fcmKey : String, ringingStatus : Constants.AFNotificationSoundType, isEnable: Bool, isFromLogin: Bool, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "notifications/addfcm"
        
        var params : [String : String] = ["fcmKey": fcmKey, "ringingStatus":ringingStatus.rawValue]
        
        // On Of notification
        if isEnable {
            params.updateValue("1", forKey: "isEnable")
        } else {
            params.updateValue("0", forKey: "isEnable")
        }
        
        // set cause tyoe
        if isFromLogin {
            params.updateValue("login", forKey: "cause")
        } else {
            params.updateValue("settings", forKey: "cause")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - logOut Request
    
    func logOut(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservices/logout"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - Notifications Request
    
    func getNotifications(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "notifications/getnotifications"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getContactUsDetails Request
    func getContactUsDetails(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservices/getcontactusdetails"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getFAQs Request
    func getFAQs(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservices/getfaqs"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getTariffDetails Request
    func getTariffDetails(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "tariffservices/gettariffdetails"
        
        let params = ["storeId" : AFLanguageManager.userSelectedLanguage().rawValue,
                      "offeringId" : AFUserSession.shared.userInfo?.offeringId ?? "",
                      "subscriberType" : AFUserSession.shared.userInfo?.subscriberType ?? "",
                      "specialTariffIds" : AFUserSession.shared.userInfo?.specialOffersTariffData?.tariffSpecialIds ?? [],
                      "groupIds" : AFUserSession.shared.userInfo?.groupIds ?? ""] as [String : Any]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - changetTariff Request
    func changetTariff(offeringId:String, offerName:String, subscribableValue: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "tariffservices/changetariff"
        
        let params = ["offeringId":offeringId,"tariffName":offerName, "actionType" : subscribableValue]
        
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    // MARK: - getStoresDetails Request
    func getStoresDetails(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservices/getstoresdetails"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - changePassword Request
    func changePassword(_ userName: String, OldPassword oldPassword: String, NewPassword newPassword: String, ConfirmNewPassword confirmNewPassword: String,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservices/changepassword"
        
        //  {"oldPassword":"1234", "newPassword":"12345", "confirmNewPassword":"12345" }
        
        let params : [String : String] = ["userName":userName,
                                          "oldPassword":oldPassword,
                                          "newPassword":newPassword,
                                          "confirmNewPassword":confirmNewPassword]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    
    //MARK: -  Fast Payments API Sections
    
    /**
     - "getfastpayments" Api to get AutoPayment scheduler.
     */
    
    func getFastPayments(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/getfastpayments"
        let params : [String : String] = ["":""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    /**
     "makepayment" Api to get AutoPayment scheduler.
     
     parameters
         - paymentKey
         - amount
         - topupNumber // pphone number
     */
    
    func makepayment(PaymentKey paymentKey : String, Amount amount: String, TopupNumber topupNumber: String ,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/makepayment"

        let params : [String : String] = [
            "paymentKey": paymentKey,
            "amount": amount,
            "topupNumber": topupNumber,
            "msisdn" :AFUserSession.shared.msisdn
        ]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    /**
     "deletepayment" Api to get AutoPayment scheduler.
     
     parameters
        -fastPaymentId
    
     */
    
    func deletePayment(fastPaymentId: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/deletepayment"

        let params : [String : String] = ["id" : fastPaymentId]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    
    
    
    
    
    //MARK: -  AutoPayments API Sections
    
    /**
     - "getscheduledpayments" Api to get AutoPayment scheduler.
     */
    
    func getScheduledPayments(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/getscheduledpayments"

        let params : [String : String] = ["":""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    /**
    "addpaymentscheduler" Api to get AutoPayment scheduler.
     parameters
         - amount
         - billingCycle  // 1.daily, 2. weekly, 3. monthly
         - startDate
         - recurrenceNumber
         - savedCardId
     */
    
    func addPaymentScheduler(amount: String, billingCycle: String, startDate: String,  recurrenceNumber: String, savedCardId: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/addpaymentscheduler"

        let params : [String : String] = [
            "amount": amount,
            "billingCycle": billingCycle,  // 1.daily, 2. weekly, 3. monthly
            "startDate": startDate,
            "recurrenceNumber": recurrenceNumber,
            "savedCardId": savedCardId,
            "recurrenceFrequency" : "1"
        ]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    /**
     - "deletepaymentscheduler" Api to get AutoPayment scheduler.
     parameters
        - paymentSchedulerId
     */
    
    func deletePaymentScheduler(paymentSchedulerId: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/deletepaymentscheduler"

        let params : [String : String] = ["id" : paymentSchedulerId]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    
    
    //MARK: getSavedCards
    func getSavedCards(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/getsavedcards"
        
        let params : [String : String] = ["":""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    //MARK: - deleteSavedCard
    func deleteSavedcard(savedCardId: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/deletesavedcard"

        let params : [String : String] = ["id" : savedCardId]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    
    //MARK: initiatePayment
    func initiatePayment(CardType cardType : String, Amount amount: String,IsSaved isSaved: Bool, TopUpNumber topUpNumber: String ,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
 
        let serviceName = "plasticcard/initiatepayment"
        let params : [String : String] = [
            "cardType": cardType,
            "amount": amount,
            "saved": String(isSaved) ,
            "topupNumber": topUpNumber,
            "msisdn" :AFUserSession.shared.msisdn,
            
        ]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    
    
    
    
    
    
    
    
    
    // MARK: - getTopUp Request
    func requestTopUp(SubscriberType subscriberType: String, CardPin cardPinNumber: String, TopupNumber topupnum : String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "financialservices/requesttopup"
        
        let params : [String : String] = ["subscriberType":subscriberType,"cardPinNumber":cardPinNumber, "topupnum": topupnum]
        
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getLoan Request
    func getLoan(FriendsMSISDN friendsMsisdn: String, LoanAmount loanAmount: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "financialservices/getloan"
        
        //{ "loanAmount":"5"}
        
        let params : [String : String] = ["friendMsisdn":friendsMsisdn, "loanAmount":loanAmount]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getLoanHistory Request
    func getLoanHistory(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "financialservices/getloanhistory"
        
        //{"startDate":"2015-12-06","endDate":"2017-09-25"}
        
        let params : [String : String] = ["startDate":startDate, "endDate":endDate]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getPaymentHistory Request
    func getPaymentHistory(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "financialservices/getpaymenthistory"
        
        //{"startDate":"2015-12-06","endDate":"2017-09-25"}
        
        let params : [String : String] = ["startDate":startDate, "endDate":endDate]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - moneyTransfer Request
    func moneyTransfer(receiverMSISDN: String, Amount amount: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "financialservices/requestmoneytransfer"
        
        let params : [String : String] = ["transferee":receiverMSISDN,"amount":amount]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - moneyRequest API Request
    func moneyRequest(ReceiverMSISDN reseiverMsisdn: String, Amount amount: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "financialservices/requestmoneytransfer"
        
        let params : [String : String] = ["transferee":reseiverMsisdn,"amount":amount, "subscriberType" :AFUserSession.shared.userInfo?.subscriberType ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getOperationsHistory Request
    func getOperationsHistory(StartDate startDate: String, EndDate endDate: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "history/getoperationshistory"
        
        let params : [String : String] = ["startDate":startDate,
                                          "endDate":endDate,
                                          "accountId":AFUserSession.shared.userInfo?.accountId ?? "",
                                          "customerId":AFUserSession.shared.userInfo?.customerId ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getUsageSummaryHistory Request
    func getUsageSummaryHistory(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "history/getusagesummary"
        
        
        let params : [String : String] = ["startDate":startDate,
                                          "endDate":endDate,
                                          "accountId":AFUserSession.shared.userInfo?.accountId ?? "",
                                          "customerId":AFUserSession.shared.userInfo?.customerId ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - verifyAccountDetails Request
    func verifyAccountDetails(PassportNumber passportNumber: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "history/verifyaccountdetails"
        
        //{"accountId":"1010000100572","customerId":"1010000100572","passportNumber":"5464645"}
        
        let params : [String : String] = ["passportNumber":passportNumber, "accountId":AFUserSession.shared.userInfo?.accountId ?? "", "customerId":AFUserSession.shared.userInfo?.customerId ?? ""]
        
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getUsageSummaryHistory Request
    func getUsageHistoryDetail(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "history/getusagedetails"
        
        
        let params : [String : String] = ["startDate":startDate,
                                          "endDate":endDate,
                                          "accountId":AFUserSession.shared.userInfo?.accountId ?? "",
                                          "customerId":AFUserSession.shared.userInfo?.customerId ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - get remianing FreeSMS count Request
    func getFreeSMSCount(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "quickservices/getfreesmsstatus"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - sendFreeSMS Request
    func sendFreeSMS(recieverMsisdns: String, textMessage: String,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "quickservices/sendfreesms"
        
        var params : [String : Any] = ["recieverMsisdn": recieverMsisdns ,
                                       "textmsg":textMessage.toBase64()]
        
        if textMessage.containsOtherThenAllowedCharactersForFreeSMSInEnglish()  == true {
            params.updateValue("NE", forKey: "msgLang")
        } else {
            params.updateValue("EN", forKey: "msgLang")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getfnf Request
    func getFNF(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "fnf/getfnf"
        
        let params = ["offeringId" :AFUserSession.shared.userInfo?.offeringId ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - addFNF Request
    func addFNF(MSISDN msisdn: String, fnfCount :Int, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        let serviceName = "fnf/addfnf"
        
        let params : [String : String] = ["addMsisdn": msisdn,
                                          "offeringId" :AFUserSession.shared.userInfo?.offeringId ?? "",
                                          "isFirstTime" : fnfCount <= 0 ? "true" : "false"]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - updateFNF Request
    func updateFNF(oldMSISDN oldMsisdn: String, newMSISDN newMsisdn: String,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "fnf/updatefnf"
        
        let params : [String : String] = ["oldMsisdn": oldMsisdn, "newMsisdn": newMsisdn, "offeringId" :AFUserSession.shared.userInfo?.offeringId ?? "", "actionType" : "2"]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    func getSupplementaryOfferings(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "supplementaryofferings/getsupplementaryofferings"
        
        let params : [String : Any] = ["offeringName": AFUserSession.shared.userInfo?.offeringId ?? "",
                                       "brandName" : AFUserSession.shared.userInfo?.brandName ?? "",
                                       "specialOfferIds" : AFUserSession.shared.userInfo?.specialOffersTariffData?.offersSpecialIds ?? [] ]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    func getInternetOfferings(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "supplementaryofferings/getinternetofferings"
        
        let params : [String : Any] = ["offeringName": AFUserSession.shared.userInfo?.offeringId ?? "",
                                       "brandName" : AFUserSession.shared.userInfo?.brandName ?? "",
                                       "specialOfferIds" : AFUserSession.shared.userInfo?.specialOffersTariffData?.offersSpecialIds ?? [] ]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    // MARK: - deleteFNF Request
    func deleteFNF(completeFNFList: [FNFList]?, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "fnf/deletefnf"
        var fnfMSISDNList : [String] = []
        if let fnfList = completeFNFList {
            for fnfItem in fnfList {
                fnfMSISDNList.append(fnfItem.msisdn ?? "")
            }
        }
        
        
        let params : [String : Any] = ["offeringId": AFUserSession.shared.userInfo?.offeringId ?? "", "deleteFnf" : fnfMSISDNList]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - VAS - getCoreServices Request
    func getCoreServices(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "coreservices/getcoreservices"
        
        var updatedHeader = Constants.kRequestHeaders
        updatedHeader.updateValue(AFUserSession.shared.userInfo?.subscriberType ?? "", forKey: "subscriberType")
        var accountType : String = "Individual"
        
        //set account type
        switch AFUserSession.shared.customerType() {
            
        case  .dataSimPrepaid, .dataSimPostpaidIndividual, .dataSimPostpaidCorporate:
            accountType = "wxxt"
            break
            
        case .individualCustomer:
            accountType = "individual"
            break
            
        case .corporateCustomer:
            accountType = "corporate"
            break
            
        case .full:
            accountType = "full"
            break
            
        default:
            accountType = "Individual"
        }
        
        //make params
        let params : [String : String] = ["accountType": accountType,
                                          "groupType": "",
                                          "brand": "",
                                          "userType": "",
                                          "isFrom": ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: updatedHeader, completionBlock: completionBlock)
    }
    
    // MARK: - processCoreServices Request
    func processCoreServices(actionType:Bool, offeringId:String, number:String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "coreservices/processcoreservices"
        
        var params = ["offeringId":offeringId,"number":number, "accountType": "", "groupType": ""]
        
        if actionType {
            params.updateValue("1", forKey: "actionType")
        } else {
            params.updateValue("3", forKey: "actionType")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - processCoreServices Request
    func getPAYGstatus( _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "coreservices/getpaygstatus"
        
        let params : [String : Any] = ["": ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    // MARK: - changeSupplementaryOffering Request
    func changeSupplementaryOffering(actionType:Bool, offeringId:String, offerName:String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "supplementaryofferings/changesupplementaryoffering"
        
        var params = ["offeringId":offeringId,"offerName":offerName]
        if !offerName.isBlank {
            
            if actionType {
                params.updateValue("1", forKey: "actionType")
            } else {
                params.updateValue("3", forKey: "actionType")
            }
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    func uploadImage(image : UIImage?, isUploadImage : Bool, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/uploadimage"
        var params : [String : String] = [:]
        
        if isUploadImage {
            params.updateValue("1", forKey: "actionType")
            
            if let myImage = image {
                let (bs64String,imageFormate) = myImage.base64(format: .JPEG(0.5))
                
                params.updateValue(bs64String, forKey: "image")
                params.updateValue(imageFormate, forKey: "ext")
            } else {
                params.updateValue("", forKey: "image")
                params.updateValue(".jpg", forKey: "ext")
            }
            
        } else {
            params.updateValue("3", forKey: "actionType")
            params.updateValue("", forKey: "image")
            params.updateValue(".jpg", forKey: "ext")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - updateCustomerEmail Request
    func updateCustomerEmail(NewEmail newEmail: String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservices/updatecustomeremail"
        
        let params : [String : String] = ["email":newEmail]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getSpecialOffers Request
    func getSpecialOffers(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "supplementaryofferings/getspecialoffers"
        
        // let params : [String : Any] = ["offeringName":"Klass_XL_new","specialOfferIds":["195954974333","1125909660"]]
        let params : [String : Any] = ["offeringName": AFUserSession.shared.userInfo?.offeringId ?? "",
                                       "brandName" : AFUserSession.shared.userInfo?.brandName ?? "",
                                       "specialOfferIds" : AFUserSession.shared.userInfo?.specialOffersTariffData?.offersSpecialIds ?? [],
                                       "groupIds" : AFUserSession.shared.userInfo?.groupIds ?? ""]
        
        
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    //MARK: - billingLanguage Request
    func changeBillingLanguage(language : String ,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request{
        
        let serviceName = "customerservices/changebillinglanguage"
        
        let params : [String : String] = ["language": language]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    //MARK: - NarTv APIs
    //MARK: Get NarTV Plans
    func getNarTvSubscriptions(isFrom : String ,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request{
        
        let serviceName = "nartvservices/getsubscriptions"
        
        let params : [String : String] = ["accountType": "",
                                          "groupType": "",
                                          "brand": "",
                                          "userType": "",
                                          "isFrom": "",
                                          "offeringId" :AFUserSession.shared.userInfo?.offeringId ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
        
    }
    
    //MARK: Migration API
    
    func narTvMigrationApi(subscriberId : String ,planId : String ,_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request{
        
        let serviceName = "nartvservices/migration"
        
        let params : [String : String] = ["subscriberId": subscriberId,
                                          "planId": planId,
                                          "accountType": "",
                                          "groupType": "",
                                          "brand": "",
                                          "userType": "",
                                          "isFrom": ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
        
    }
    
    //MARK:  - Echange Service APIs
    //MARK: Get Exchange Values
    
    func getExchangeServiceValues(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request{
        
        let serviceName = "generalservices/exchangeservicevalues"
        
        let params : [String : Any] = ["offeringId" :AFUserSession.shared.userInfo?.offeringId ?? "", "offeringName": AFUserSession.shared.userInfo?.offeringName ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
        
    }
    
    //MARK: Exchange API
    
    func exchangeServiceApiCall(isDataToVoice :Bool, dataVal:String, voiceVal :String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request{
        
        let serviceName = "generalservices/exchangeservice"
        
        
        let params : [String : String] = ["isDataToVoiceConversion": isDataToVoice ? "true":"false",
                                          "dataValue":dataVal,
                                          "voiceValue":voiceVal,
                                          "offeringId":AFUserSession.shared.userInfo?.offeringId ?? "" ,
                                          "supplementaryOfferingIds": ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
        
    }
    
    // MARK: - getSubscriptions Request
    func getSubscriptions(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "mysubscriptions/getsubscriptions"
        
        // let params : [String : Any] = ["offeringName":"Klass_XL_new","specialOfferIds":["195954974333","1125909660"]]
        let params : [String : Any] = ["offeringName": AFUserSession.shared.userInfo?.offeringId ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
        
    }
    
    // MARK: - getSubscriptions Request
    func suspenNumber(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservices/reportlostsim"
        
        // let params : [String : Any] = ["offeringName":"Klass_XL_new","specialOfferIds":["195954974333","1125909660"]]
        let params : [String : Any] = ["reasonCode": "1"]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
        
    }
    
    // MARK: - saveSurvey Request
    func saveSurvey(comment :String, answerId :String, questionId :String, offeringIdSurvey: String, offeringTypeSurvey: String, surveyId :String, _ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "survey/savesurvey"
        
        let params : [String : Any] = [
            "comment"           : comment,
            "answerId"          : answerId,
            "questionId"        : questionId,
            "offeringIdSurvey"  : offeringIdSurvey,
            "offeringTypeSurvey": offeringTypeSurvey,
            "surveyId"      : surveyId
        ]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
        
    }

    // MARK: - getSurveys Request
    func getSurveys(_ completionBlock: @escaping AFAPIClientCompletionHandler) -> Request {
        
        let serviceName = "survey/getsurveys"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
        
    }
    
    
}
