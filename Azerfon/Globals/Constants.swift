//
//  Constants.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import UIKit

typealias AFButtonCompletionHandler = () -> ()
typealias AFButtonWithParamCompletionHandler = (_ gernralString : String) -> Void

class Constants {
    
    class var kAFAPIClientBaseURL:String {
        
        // Development URL
//        return "http://10.220.48.133:8080/azerfon/api/"
//        return "http://10.220.48.216:8080/azerfon/api/"
//        return "http://10.220.48.218:8181/cxf/"
        
        // Staging URL
//        return "https://ecarestg.nar.az:16444/azerfon/api/"
        
        //  Production URL
        return "https://plusapp.nar.az/azerfon/api/"
    }
    
    class var AFAPIClientDefaultTimeOut: Double {
        return 30.0
    }
    
    class var kRequestHeaders : [String : String] {
        return ["Content-Type": "application/json",
                "deviceID": UIDevice.deviceID(),
                "iP": UIDevice.getWiFiAddress() ?? "",
                "UserAgent": "iPhone",
                "lang": AFLanguageManager.userSelectedLanguage().rawValue,
                "token": AFUserSession.shared.token,
                "msisdn" :AFUserSession.shared.msisdn]
    }
    class var dateDropDownOptions : [String] {
        return [Localized("DropDown_CurrentDay"), Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"), Localized("DropDown_CustomPeriod")]
    }
    
    class var liveChatURL :String {
        
        if let savedLiveChatURL = AFUserSession.shared.predefineData?.liveChat,
            savedLiveChatURL.isEmpty == false {
            print("\n\nLivechat URL: \(savedLiveChatURL)\n\n")
            return savedLiveChatURL.urlEncode
        }
        
        return ""
    }
    
    //Live chat
    class var liveChatUrlRequest : URLRequest? {
        // prepare json data
        let json: [String: String] = ["authenticate": "Ev@mp1iv3Ch@t" ]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json)
            
            // create post request
            if let url = URL(string: liveChatURL) {
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                // insert json data to the request
                request.httpBody = jsonData
                return request
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    //  Constant values
    
    class var defaultAppLanguage: AFLanguage {
        return .azeri
    }
}


// MARK: - Enums
extension Constants {
    
    // API Status code
    enum AFAPIStatusCode: String {
        case succes                         = "00"
        case forceUpdate                    = "8"
        case optionalUpdate                 = "9"
        case serverDown                     = "10"
        case sessionExpired                 = "7"
        case wrongAttemptToChangePassword   = "104"
    }
    
    enum AFUserDashboardType: String {
        case prepaid                = "Prepaid"
        case prepaidFull            = "Prepaid Full"
        case prepaidDataSIM         = "Prepaid Data SIM"
        case postpaid               = "postpaid"
        case postpaidDataSIM        = "Postpaid Data SIM"
    }
    
    enum AFSubscriberType : String {
        case prepaid = "prepaid"
        case postpaid = "postpaid"
        case unSpecified = ""
    }
    
    enum AFCustomerType : String {
        case full                           = "Full"
        case individualCustomer             = "Individual Customer"
        case corporateCustomer              = "Corporate Customer"
        case dataSimPrepaid                 = "Data Sim Prepaid"
        case dataSimPostpaidIndividual      = "Data Sim Postpaid Individual"
        case dataSimPostpaidCorporate       = "Data Sim Postpaid Corporate"
        case unSpecified                    = ""
    }
    
    enum AFPasswordStrength: String {
        case didNotMatchCriteria    = "0"
        case Week                   = "1"
        case Medium                 = "2"
        case Strong                 = "3"
    }
    
    // Resend Type
    enum AFResendType : String {
        case ForgotPassword     = "forgotpassword"
        case SignUp             = "signup"
        case UsageHistory       = "usagehistory-details-view"
    }
    
    // Language types of Application
    enum AFLanguage: String {
        case russian = "2"
        case english = "3"
        case azeri   = "4"
    }
    
    // Notification sound setting
    enum AFNotificationSoundType : String {
        case Tone       = "tone"
        case Vibrate    = "vibrate"
        case Mute       = "mute"
    }
    
    // topup options
    enum AFTopUpType : String {
        case ScratchCard    = "Scratch Card"
        case PlasticCard    = "Plastic Card"
    }
    
    // loan request types
    enum AFLoanRequestType : String {
        case Me        = "Me"
        case Friend    = "Friend"
    }
    
    enum HorizontalMenus :String {
        case dashboard  = "dashboard"
        case tariffs    = "tariffs"
        case services   = "services"
        case topup      = "topup"
        case myaccount  = "myaccount"
        case internet   = "internet"
    }
    
    enum AFFreeResourceType : String {
        case internet   = "DATA"
        case sms        = "SMS"
        case call       = "VOICE"
        
        func unitString() -> String {
            var displayUnit = ""
            switch self {
                
            case .internet:
                displayUnit = "MB"
            case .sms:
                displayUnit = "SMS"
            case .call:
                displayUnit = "MINS"
            }
            return displayUnit
        }
        func sortId() -> Int {
            var sortID = 0
            switch self {
                
            case .internet:
                sortID = 2
            case .sms:
                sortID = 3
            case .call:
                sortID = 1
            }
            return sortID
        }
    }
    
    enum AFStoreLocatorType : String {
        case map    = "Map"
        case list   = "StoreList"
        
        func localizedAPIKey() -> String {
            return self.rawValue.localizedAPIKey()
        }
    }
    
    enum AFUsageHistoryType : String {
        case summary         = "Summary"
        case sevenDigitPin   = "SevenDigitPIN"
        case smsPIN          = "SmsPIN"
        case details         = "Details"
        
        func localizedAPIKey() -> String {
            return self.rawValue.localizedAPIKey()
        }
    }
    
    enum AFLoanScreenType : String {
        case loan      = "Loan"
        case request   = "Request"
        case payment   = "Payment"
        
        func localizedAPIKey() -> String {
            return self.rawValue.localizedAPIKey()
        }
    }
    
    enum RegistrationType :String {
        case registration   = "registration"
        case addAccount     = "addAccount"
    }
    
    enum AFSectionType: String {
        case bonuses        = "Bonuses"
        case prices         = "Prices"
        case detail         = "Detail"
        case usage          = "Usage"
        case offerTitleView = "offerTitleView"
        case unSpecified    = ""
    }
    
    enum AFOfferType: String {
        case price                      = "Price"
        case rounding                   = "Rounding"
        case textWithTitle              = "TextWithTitle"
        case textWithOutTitle           = "TextWithOutTitle"
        case textWithPoints             = "TextWithPoints"
        case date                       = "Date"
        case time                       = "Time"
        case roamingDetails             = "RoamingDetails"
        case freeResourceValidity       = "FreeResourceValidity"
        case titleSubTitleValueAndDesc  = "TitleSubTitleValueAndDesc"
        case offerGroup                 = "OfferGroup"
        case typeTitle                  = "Type"
        case offerValidity              = "OfferValidity"
        case offerActive                = "OfferActive"
        case attributeList              = "attributeList"
        case usage                      = "Usage"
        case topDescription             = "TopDescription"
        case call                       = "Call"
        case SMS                        = "SMS"
        case internet                   = "Internet"
        case callPayG                   = "CallPayG"
        case SMSPayG                    = "SMSPayG"
        case internetPayG               = "InternetPayG"
        case MMS                        = "MMS"
        case destination                = "Destination"
        case internationalOnPeak        = "InternationalOnPeak"
        case internationalOffPeak       = "InternationalOffPeak"
        case advantages                 = "Advantages"
        case classification             = "Classification"
    }
    
    enum AFOfferTabType: String {
        case call                   = "Call"
        case internet               = "Internet"
        case sms                    = "SMS"
        case hybrid                 = "Hybrid"
        case campaign               = "Campaign"
        case tm                     = "TM"
        case roaming                = "Roaming"
        case allInclusiveCall       = "All Inclusive Call"
        case allInclusiveSMS        = "All Inclusive SMS"
        case allInclusiveInternet   = "All Inclusive Internet"
        
        func localizedString() -> String {
            return Localized(self.rawValue)
        }
        
        static func getTitleFor(title:AFOfferTabType) -> String {
            return title.localizedString()
        }
    }
    
    enum AFInternetTabType: String {
        case all            = "All"
        case hourly         = "Hourly"
        case daily          = "Daily"
        case weekly         = "Weekly"
        case monthly        = "Monthly"
        
        func localizedString() -> String {
            return Localized(self.rawValue)
        }
        
        static func getTitleFor(title:AFInternetTabType) -> String {
            return title.localizedString()
        }
    }
    
    enum AFFilterType: String {
        case app        = "app"
        case tablet     = "tab"
        case desktop    = "desktop"
    }
    
    enum AFCoreServiceType: String {
        case coreService           = "coreSservice"
        case coreServiceWithBar    = "coreSserviceWithBar"
        case callForwardCell       = "callForwardCell"
        case internet              = "internet"
        case blockNumber           = "blockNumber"
    }
}

// MARK: - String Values
extension Constants {
    class var K_PayBySUBs :String {
        return "PayBySUBs"
    }
    class var K_PayBySUB :String {
        return "PayBySUB"
    }
    
    class var numberPrefix: String {
        return "+994 "
    }
    
    class var allowedNumbers: String {
        return "0123456789"
    }
    
    class var allowedAlphbeats: String {
        return allowedCapitalAlphabets + allowedSmallAlphabets
    }
    
    class var allowedSmallAlphabets: String {
        return allowedCapitalAlphabets.lowercased()
    }
    
    class var allowedCapitalAlphabets: String {
        return "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    }
    
    class var allowedSpecialCharacters: String {
        return "~`!@#$%^&*()_+=-]}[{|'\";:/?.>,<"
    }
}

// MARK: - String keys
extension Constants {
    
    //New_App_Check Keys
    class var kIsLaunchedBefore: String {
        return "kIsLaunchedBefore"
    }
    
    class var kIsUserLoggedInKey: String {
        return "isUserLoggedIn"
    }
    
    class var kUserSelectedLanguage: String {
        return "UserSelectedLanguage"
    }
    
    class var kIsNotificationEnabled: String {
        return "IsNotificationEnabled"
    }
    
    class var kAppDidBecomeActive: String {
        return "appDidBecomeActive"
    }
    
    class var K_AppStoreURL :String {
        return "AppStoreURL"
    }
    
    class var K_IsBiometricEnabled :String {
        return "IsBiometricEnabled"
    }
    class var K_RateUsTitle :String {
        return "RateUsTitle"
    }
    
    class var K_RateUsMessage :String {
        return "RateUsMessage"
    }
    
    class var K_UserLoggedInTime :String {
        return "UserLoggedInTime"
    }
    
    class var K_RateUsLaterTime :String {
        return "RateUsLaterTime"
    }
    
    class var K_IsRateUsShownBefore :String {
        return "IsRateUsShownBefore"
    }
    
    class var k_SingleValueKey: String {
        return "Single value"
    }
    
    class var k_DoubleValueKey: String {
        return "Double values"
    }
    
    class var k_DoubleTitlesKey: String {
        return "Double titles"
    }
    
    //forward service in coreservices/vas
    class var K_Call_Forward :String {
        return "519637024"
    }
    
    class var kLiveChatURLString: String {
        return "LiveChatURLString"
    }
}

// MARK: - Number Values
extension Constants {
    class var SPLASH_DURATION: Double {
        return 1.0
    }
    
    class var minPasswordLength: Int {
        return 6
    }
    
    class var maxPasswordLength: Int {
        return 15
    }
    
    class var OTPLength: Int {
        return 4
    }
    
    class var PINVerifyBackTime: Int {
        return 30
    }
    
    class var AzriAndRussianFreeSMSLength : Int {
        return 70
    }
    
    class var EnglishFreeSMSLength : Int  {
        return 160
    }
    
    class var MSISDNLength : Int  {
        return 9
    }
    
    class var ValidCardNumberLength : Int  {
        return 13
    }
    
    class var kViewCornerRadius: CGFloat {
        return 6.0
    }
    class var MaxNumberOfManageAccount: Int {
        return 5
    }
    
}

// MARK: - Date Formates
extension Constants {
    
    class var kDisplayFormat: String {
        return "dd/MM/yy"
    }
    
    class var kNewAPIFormat: String {
        return "yyyy-MM-dd HH:mm:ss"
    }
}



//MARK: - Gloable
//APIs Key
enum APIsType : String {
    //Home page
    case homePage                       = "Home_Page"
    case userInformation                = "PIC_User_Information"
    
    //MySubscriptions
    case mySubscriptions                = "My_Subscriptions"
    case supplementaryOffer             = "Supplementary_Offer"
    case internetOffers                 = "Internet_Offers"
    case specialOffers                  = "Special_Offers"
    //Tarrifs
    case tariffDetails                  = "Tariff_Details"
    
    // StoreLocator
    case storeDetails                   = "Store_Details"
    
    // VASServices
    case vasServices                    = "VAS_Services"
    case paygServices                   = "PAYG_Services"
    
    case faqDetails                     = "FAQ_Details"
    case contactUsDetail                = "Contact_Us_Detail"
    case notificationConfigurations     = "notification_Configurations"
    case notificationData               = "notification_Data"
    case notificationCount              = "notification_Count"
    case horizontalMenu                 = "Horizontal_Menu"
    case VerticalMenu                   = "Vertical_Menu"
    case appMenu                        = "App_Menu"
    case loggedInUserInfo               = "loggedIn_UserInfo"
    case exchangeService                = "exchange_service"
    case contactUs                      = "contact_us"
    
    func selectedLocalizedAPIKey() -> String {
        return self.rawValue.localizedAPIKey().currentUserKey()
    }
    func selectedAPIKey() -> String {
        return self.rawValue.currentUserKey()
    }
    
    func selectedRawValue() -> String {
        return self.rawValue.currentUserKey()
    }
    
    func simpleRawValue() -> String {
        return self.rawValue
    }
}
