//
//  AFUserSession.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

// MARK: - Singleton
final class AFUserSession: NSObject {
    
    // Can't init is singleton
    override private init() {
        super.init()
    }
    // MARK: Shared Instance
    static let shared = AFUserSession()
    
    
    
    //MARK:- Properties
    let dateFormatter               : DateFormatter = DateFormatter()
    var loggedInUsers               : ManageAccountModel?
    var appMenu                     : AppMenusModel?
    var liveChatObject              : LiveChatVC?
    var changePasswordRequestCount  : Int = 0
    var isPushNotificationSelected  : Bool = false
    var shouldCallAndUpdateAppResumeData           : Bool = false
    var isOTPVerified               : Bool = false
    var shouldReloadDashboardData   : Bool = false
    var promoMessage                : String?
    var homePageData                : HomePageModel?
    var dynamicLinkRedirectionURL   : String?
    var appSurvays: InAppSurveyMapper?
    
    var userInfo : CustomerData? {
        return loggedInUsers?.currentUser?.userInfo
    }
    
    var predefineData : PredefinedData? {
        return loggedInUsers?.currentUser?.predefineData
    }
    
    var appConfig : AppConfig? {
        return loggedInUsers?.currentUser?.appConfig
    }
    
    var msisdn : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let msisdn = self.userInfo?.msisdn {
            return msisdn
        } else {
            return ""
        }
    }
    
    var token : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let token = self.userInfo?.token {
            return token
        } else {
            return ""
        }
    }
    
    var userType : Constants.AFUserDashboardType {
        if self.subscriberType() == .prepaid {
            
            if self.customerType() == .dataSimPrepaid {
                
                return .prepaidDataSIM
                
            } else if self.customerType() == .full {
                
                return .prepaidFull
            } else {
                
                return .prepaid
            }
            
        } else { /* Postpaid user */
            
            if self.customerType() == .dataSimPostpaidIndividual ||
                self.customerType() == .dataSimPostpaidCorporate{
                return .postpaidDataSIM
            } else {
                return .postpaid
            }
        }
    }
    
    //MARK:- Funtions
    ///Check Wheter user in logged in or not.
    func isLoggedIn() -> Bool {
        // Get Data from UserDefaults
        return UserDefaults.standard.bool(forKey: Constants.kIsUserLoggedInKey)
    }
    
    func isBiometricEnabled() -> Bool {
        // Get Data from UserDefaults
        return UserDefaults.standard.bool(forKey: Constants.K_IsBiometricEnabled)
    }
    
    func loadUserInfomation() -> Bool {
        
        if self.userInfo == nil {
            
            if self.isLoggedIn() {
                // Load customer information
                let usersInfoObject = AFUserInfoUtilities.loadCustomerDataFromUserDefaults()
                self.loggedInUsers = usersInfoObject.usersInfo
                return usersInfoObject.hasInfo
            } else {
                return false
            }
        }
        return true
    }
    
    ///Concatinating UserName
    func userName() -> String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        var name : String = ""
        
        if let firstName = userInfo?.firstName {
            name = firstName
        }
        if let lastName = userInfo?.lastName {
            name = name + " " + lastName
        }
        name = name.replacingOccurrences(of: "  ", with: " ")
        
        if name.isBlank == false {
            return name
        } else {
            return ""
        }
    }
    
    ///Returns Subscriber type.
    func subscriberType() -> Constants.AFSubscriberType {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let subscriberTypeValue = self.userInfo?.subscriberType,
            let subscriberType = Constants.AFSubscriberType(rawValue: subscriberTypeValue) {
            
            return subscriberType
        } else {
            return Constants.AFSubscriberType.unSpecified
        }
    }
    
    func customerType() -> Constants.AFCustomerType {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let customerTypeValue = self.userInfo?.customerType,
            let customerType = Constants.AFCustomerType(rawValue: customerTypeValue) {
            
            return customerType
        } else {
            return Constants.AFCustomerType.unSpecified
        }
    }
    
    func saveLoggedInUsersCurrentStateInfo() {
        /* Save Users info */
        if let msisdnString = self.userInfo?.msisdn,
            let currentUserInfo = loggedInUsers?.currentUser {
            AFUserInfoUtilities.updateUserInfo(loggedInUserMSISDN: msisdnString, userInfo: currentUserInfo)
        }
    }
    
    /**
     Returns wheter user status is active or not..
     */
    func isStatusActive() -> Bool {
        
        if let statusText = self.userInfo?.statusDetails,
            statusText.isEqual("B01", ignorCase: true) {
            return true
        } else {
            return false
        }
    }
    
    /**
     Returns Color accourding to user status
     */
    func isStatusColor() -> UIColor {
        let statusText = self.userInfo?.statusDetails ?? ""
        
        if statusText.isEqual("B01", ignorCase: true) { /* Status is Active */
            return UIColor.green
        } else if statusText.isEqual("B04", ignorCase: true) {/* Status is BLOCK_ONE_WAY */
            return UIColor.afDateOrange
        } else {/* Status is BLOCK_TWO_WAY */
            return UIColor.afBerry
        }
    }
    
    ///Clear user informaiton from userdefault.
    func clearUserSession() {
        
        let userSelectedLanguage = AFLanguageManager.userSelectedLanguage()
        
        let userDefaults = UserDefaults.standard
        
        /* Remove all data from UserDefaults */
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier ?? "" )
        userDefaults.synchronize()
        
        /* Set values in user defaults which are changed only once */
        AFUtilities.updateLaunchedBeforeStatus()
        AFLanguageManager.setUserLanguage(selectedLanguage: userSelectedLanguage)
        
        /* Clear all singleton objects */
        loggedInUsers               = nil
        promoMessage                = nil
        clearUserSessionObjectsForCurrentUser()
    }
    
    func clearUserSessionObjectsForCurrentUser() {
        appMenu                             = nil
        liveChatObject                      = nil
        changePasswordRequestCount          = 0
        isPushNotificationSelected          = false
        shouldCallAndUpdateAppResumeData    = false
        isOTPVerified                       = false
        shouldReloadDashboardData           = false
        promoMessage                        = nil
    }
    
    
    var aSecretKey : String {
        
        let key = "hbnnV6jRcxEUEcdKHs4jMuTbpQyTfEdt+7+Zloen5KU=".aesDecrypt(key: "s@@ng@tibe*&~~~jkm@@st@r")
        return key
        
    }
    
    
    //MARK: - User session updation
    //    func switchCurrentUser(newUserInfo : CustomerData) {
    //
    //        self.userInfo = newUserInfo
    //
    //        self.loggedInUsers?.users?.append(newUserInfo)
    //        self.loggedInUsers?.currentSelectedUser = newUserInfo
    //    }
    
    
}
