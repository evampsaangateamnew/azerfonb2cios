//
//  UserDefaults+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/8/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
extension UserDefaults {
    
    /**
     Save string in user default.
     
     - parameter stringValue: string to save.
     - parameter forKey: key against which you want to save value in userdefault.
     
     - returns: void.
     */
    public class func saveStringForKey(stringValue: String, forKey key: String) {
        UserDefaults.standard.set(stringValue, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    /**
     Save Bool in user default.
     
     - parameter boolValue: bool value to save.
     - parameter forKey: key against which you want to save value in userdefault.
     
     - returns: void.
     */
    public class func saveBoolForKey(boolValue: Bool, forKey key: String) {
        UserDefaults.standard.set(boolValue, forKey: key)
        UserDefaults.standard.synchronize()
    }
}
