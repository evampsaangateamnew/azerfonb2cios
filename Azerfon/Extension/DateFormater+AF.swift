
//
//  DateFormater+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
extension DateFormatter {
    
    func createDate(from dateString:String, dateFormate :String = Constants.kDisplayFormat) -> Date? {
        
        self.timeZone = TimeZone.appTimeZone()
        self.dateFormat = dateFormate
        
        return self.date(from: dateString)
    }
    
    func createString(from fromDate:Date, dateFormate :String = Constants.kDisplayFormat) -> String {
        
        self.timeZone = TimeZone.appTimeZone()
        self.dateFormat = dateFormate
        // self.locale = Locale(identifier: AFLanguageManager.userSelectedLanguageShortCode())
        
        return self.string(from: fromDate)
    }
}
