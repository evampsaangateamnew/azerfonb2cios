//
//  Mappable+MB.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

public extension Mappable {
    
    /// Save JSON string into user defaults
    func saveInUserDefaults (key : String) {
        
        UserDefaults.standard.set(self.toJSONString(), forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    /**
     Load from user default for a specific key.
     
     - parameter key: key for which you want to load value from user default.
     
     - returns: Loaded Value of generic type from user default.
     */
    static func loadFromUserDefaults <T: Mappable> (key : String) -> T? {
        
        if let jsonString = UserDefaults.standard.object(forKey:key) as? String {
            if let parsedData = Mapper <T> ().map(JSONString: jsonString) {
                return parsedData
            }
        }
        
        return nil
    }
    
    /**
     Load arry from user default for a specific key.
     
     - parameter key: key for which you want to load value from user default.
     
     - returns: Loaded array  of generic type from user default.
     */
    static func loadArrayFromUserDefaults <T: Mappable> (key : String) -> [T]? {
        
        if let jsonString = UserDefaults.standard.object(forKey:key) as? String {
            if let parsedData = Mapper <T> ().mapArray(JSONString: jsonString) {
                return parsedData
            }
        }
        return nil
    }
    
}
