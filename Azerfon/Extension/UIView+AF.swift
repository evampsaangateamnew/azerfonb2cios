//
//  UIView+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

extension UIView {
    
    /**
     Round corner of UIView.
     
     - parameter corners: topLeft, topRight, bottomLeft, bottomRight and allCorners.
     - parameter radius: how much we want to round corners.
     - returns: void.
     */
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    /**
     Round top corners of UIView.
     
     - parameter radius: how much we want to round corners.
     - returns: void.
     */
    func roundTopCorners(radius: CGFloat) {
        DispatchQueue.main.async {
            self.roundCorners(corners:[.topLeft, .topRight], radius: radius)
        }
    }
    
    /**
     Round all corners of UIView.
     
     - parameter radius: how much we want to round corners.
     - returns: void.
     */
    func roundAllCorners(radius: CGFloat) {
        //  DispatchQueue.main.async {
        //  self.roundCorners(corners:[.topLeft, .topRight, .bottomLeft, .bottomRight], radius: radius)
        //  }
        
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    ///Remove all subviews from a UIView.
    func removeAllSubViews() {
        self.subviews.forEach { (aView) in
            aView.removeFromSuperview()
        }
    }
    
    public class func fromNib() -> Self {
        return fromNib(nibName: nil)
    }
    
    public class func fromNib(nibName: String?) -> Self {
        func fromNibHelper<T>(nibName: String?) -> T where T : UIView {
            let bundle = Bundle(for: T.self)
            let name = nibName ?? String(describing: T.self)
            return bundle.loadNibNamed(name, owner: nil, options: nil)?.first as? T ?? T()
        }
        return fromNibHelper(nibName: nibName)
    }
    
    /**
     Adds description view as sub view to UIView.
     
     - parameter imageName: Image name for description view.
     - parameter description: Text to be displayed in description view.
     
     - returns: void.
     */
    func showDescriptionViewWithImage(_ imageName:String = "step-3-info",
                                      description: String?,
                                      descriptionColor :UIColor = UIColor.afPurplishBrown,
                                      descriptionFont :UIFont = UIFont.h3WhiteCenter,
                                      centerYConstant :CGFloat = 0) {
        
        if let myDescriptionView = self.viewWithTag(998877) as? DescriptionView {
            
            myDescriptionView.setDescriptionViewWithImage(imageName, description: description, descriptionColor: descriptionColor, descriptionFont: descriptionFont)
            
        } else {
            
            let myDescriptionView: DescriptionView = DescriptionView.fromNib()
            myDescriptionView.setDescriptionViewWithImage(imageName, description: description, descriptionColor: descriptionColor, descriptionFont: descriptionFont)
            myDescriptionView.translatesAutoresizingMaskIntoConstraints = false
            
            self.addSubview(myDescriptionView)
            self.bringSubviewToFront(myDescriptionView)
            
            let leadingConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
            
            let trailingConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            
            let verticalCenterConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: centerYConstant)
            
            let horizantalCenterConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
            
            self.addConstraints([leadingConstraint, trailingConstraint, verticalCenterConstraint, horizantalCenterConstraint])
            
        }
    }
    
    ///Hide description view
    func hideDescriptionView() {
        if let myDescriptionView = self.viewWithTag(998877) as? DescriptionView {
            
            myDescriptionView.isHidden = true
            myDescriptionView.removeFromSuperview()
        }
    }
    
    /**
     Adds dropdown shadow to UIView.
     
     - parameter color: Used to set specific color
     - parameter opacity: used to set opacity
     - parameter offSet: used to set offset
     - parameter radius: used to set radius
     - parameter scale: used to set scale
     
     - returns: void.
     */
    func addDropShadowTo(color: UIColor, opacity: Float, offSet: CGSize, radius: CGFloat, scale: Bool) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}


extension UIView {
    func addBubleAnimation(circleColor :UIColor = .black, duration :Double = 0.5) {
        let circle = UIView()
        
        let startingPoint = CGPoint(x: self.bounds.size.width, y: 0)
        circle.frame = frameForCircle(startPoint: startingPoint)
        
        circle.layer.cornerRadius = circle.frame.size.height / 2
        circle.center = startingPoint
        circle.backgroundColor = circleColor
        circle.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        self.addSubview(circle)
        self.sendSubviewToBack(circle)
        
        UIView.animate(withDuration: duration, animations: {
            
            circle.transform = CGAffineTransform.identity
            
        }, completion: { (success:Bool) in
            if success {
                self.backgroundColor = circleColor
                circle.removeFromSuperview()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    
                }
            }
        })
    }
    
    private func frameForCircle (startPoint:CGPoint) -> CGRect {
        let xLength = fmax(startPoint.x, self.bounds.size.width - startPoint.x)
        let yLength = fmax(startPoint.y, self.bounds.size.height - startPoint.y)
        
        let offestVector = sqrt(xLength * xLength + yLength * yLength) * 2
        let size = CGSize(width: offestVector, height: offestVector)
        
        return CGRect(origin: CGPoint.zero, size: size)
        
    }
}
