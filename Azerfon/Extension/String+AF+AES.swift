//
//  String+AF+AES.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/21/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import CryptoSwift

extension String {
    /**
     Encrypt key.
     
     - parameter key: String to encrypt.
     
     - returns: Encrypted string.
     */
    func aesEncrypt(key: String) -> String {
        
        if key.isBlank {
            return ""
        }
        
        do {
            let data = Array(self.utf8)
            let enc = try AES(key: key.bytes, blockMode:ECB(), padding:.pkcs7).encrypt(data)
            let encData = Data(bytes: enc, count: enc.count)
            let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0));
            
            return String(base64String)
            
        } catch {
            return ""
        }
    }
    
    /**
     Decrypt key.
     
     - parameter key: String to decrypt.
     
     - returns: Decrypted string.
     */
    func aesDecrypt(key: String) -> String {
        
        if key.isBlank {
            return ""
        }
        
        do {
            /* Decode base64string first */
            let data = Array(Data(base64Encoded: self) ?? Data())
            
            //  data = Padding.pkcs7.add(to: data, blockSize: AES.blockSize)
            //  data = Padding.pkcs7.remove(from: data, blockSize: AES.blockSize)
            /* Decrypt data */
            let dec = try AES(key: key.bytes, blockMode: ECB(), padding: .pkcs7).decrypt(data)
            let decData = Data(bytes: dec, count: dec.count)
            let result = NSString(data: decData, encoding: String.Encoding.utf8.rawValue)
            return String(result ?? "")
            
        } catch {
            return ""
        }
    }
    
    
}
