
//
//  UIFont+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

extension UIFont {
    
    /**
     Returns a ALSSchlangesans font with custom size.
     
     - parameter fontSize: Font size.
     
     - returns: UIFont.
     */
    class func afSans(fontSize: Float) -> UIFont {
        return UIFont(name: "ALSSchlangesans", size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize))
    }
    
    /**
     Returns a ALSSchlangesans Bold font with custom size.
     
     - parameter fontSize: Font size.
     
     - returns: UIFont.
     */
    class func afSansBold(fontSize: Float) -> UIFont {
        return UIFont(name: "ALSSchlangesans-Bold", size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize), weight: UIFont.Weight.bold)
    }
    
    class func printAllFonts() {
        for family in UIFont.familyNames {
            print("Font family Name: \(family)")
            print("-------------------------------")
            
            for name in UIFont.fontNames(forFamilyName: family) {
                print("   \(name)")
            }
            print("-------------------------------")
        }
    }
}

// Text styles

extension UIFont {
    
    class var defaultFont: UIFont {
        return UIFont.afSans(fontSize: 14.0)
    }
    
    class var h0Grey: UIFont {
        return UIFont.afSans(fontSize: 60.0)
    }
    
    class var h2Pink: UIFont {
        return UIFont.afSansBold(fontSize: 18.0)
    }
    
    class var h2PinkRegular: UIFont {
        return UIFont.afSans(fontSize: 18.0)
    }
    
    class var h2Grey: UIFont {
        return UIFont.afSansBold(fontSize: 18.0)
    }
    
    class var h2White: UIFont {
        return UIFont.afSans(fontSize: 18.0)
    }
    
    class var h3WhiteCenter: UIFont {
        return UIFont.afSansBold(fontSize: 16.0)
    }
    
    class var h1Grey: UIFont {
        return UIFont.afSansBold(fontSize: 22.0)
    }
    class var h1White: UIFont {
        return UIFont.afSansBold(fontSize: 24.0)
    }
    
    class var b2White: UIFont {
        return UIFont.afSansBold(fontSize: 15.0)
    }
    
    class var h3GreyRight: UIFont {
        return UIFont.afSansBold(fontSize: 16.0)
    }
    
    class var h4WhiteRight: UIFont {
        return UIFont.afSansBold(fontSize: 15.0)
    }
    
    class var h5WhiteCenter: UIFont {
        return UIFont.afSans(fontSize: 14.0)
    }
    
    class var b3LightGreyRight: UIFont {
        return UIFont.afSans(fontSize: 14.0)
    }
    
    class var b3GreyRight: UIFont {
        return UIFont.afSansBold(fontSize: 14.0)
    }
    
    class var h5GreyCenter: UIFont {
        return UIFont.afSansBold(fontSize: 14.0)
    }
    
    class var h5LightGrey: UIFont {
        return UIFont.afSansBold(fontSize: 14.0)
    }
    
    class var b4LightPinkCenter: UIFont {
        return UIFont.afSans(fontSize: 12.0)
    }
    
    class var b4LightPinkRight: UIFont {
        return UIFont.afSans(fontSize: 12.0)
    }
    
    class var b3LightGrey: UIFont {
        return UIFont.afSans(fontSize: 14.0)
    }
    
    class var h6LightGrey: UIFont {
        return UIFont.afSansBold(fontSize: 12.0)
    }
    
    class var b4LightGrey: UIFont {
        return UIFont.afSans(fontSize: 12.0)
    }
    
    class var b4LightPink: UIFont {
        return UIFont.afSans(fontSize: 12.0)
    }
    
    class var b5LightGreyCenter: UIFont {
        return UIFont.afSans(fontSize: 10.0)
    }
    
    class var b4PinkCenter: UIFont {
        return UIFont.afSans(fontSize: 12.0)
    }
    
    class var b5PinkCenter: UIFont {
        return UIFont.afSans(fontSize: 10.0)
    }
    
    class var b2Grey: UIFont {
        return UIFont.afSansBold(fontSize: 15.0)
    }
    
    class var b4LightGreyRight: UIFont {
        return UIFont.afSans(fontSize: 12.0)
    }
    class var b4BoldLightGrey: UIFont {
        return UIFont.afSansBold(fontSize: 12.0)
    }
    class var b3BoldGreyCenter: UIFont {
        return UIFont.afSansBold(fontSize: 14.0)
    }
    
}



extension UIFont {
    class func MBArial(fontSize: Float) -> UIFont {
        return UIFont(name: "ArialMT", size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize))
    }
    class func MBArialBold(fontSize: Float) -> UIFont {
        return UIFont(name: "Arial-BoldMT", size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize), weight: UIFont.Weight.bold)
    }
}
