//
//  NSError+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

extension NSError {
    
    /**
     Show alert controller with title.
     
     - parameter title: title of alert controller.
     
     - returns: UIAlertController.
     */
    func alertControllerWithTitle(_ title: String) -> UIAlertController {
        return UIAlertController(title: title, message: localizedDescription, preferredStyle: .alert)
    }
    
    /**
     Show server error inside UIViewController.
     
     - parameter viewController: View controller in which you want to show error..
     
     - returns: void.
     */
    func showServerErrorInViewController(_ viewController: UIViewController, _ actionBlock : @escaping AFButtonCompletionHandler = {}) {
        
        if (viewController.isViewLoaded && (viewController.view.window != nil)) {
            viewController.showAlert(title: Localized("Title_Error"), message: localizedDescription, btnTitle: Localized("BtnTitle_OK"), actionBlock)
        }
    }
}
