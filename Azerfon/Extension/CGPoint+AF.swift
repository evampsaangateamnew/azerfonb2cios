//
//  CGPoint+AF.swift
//  Azerfon
//
//  Created by Waqas Umar on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import UIKit

extension CGPoint {
    
    func distanceWith(_ p: CGPoint) -> CGFloat {
        return sqrt(pow(self.x - p.x, 2) + pow(self.y - p.y, 2))
    }
    
}

