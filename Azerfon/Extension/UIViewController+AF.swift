//
//  UIViewController+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/7/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import LocalAuthentication
import UserNotifications

extension UIViewController {
    
    //MARK: -  Properties
    var myStoryBoard: UIStoryboard {
        
        return UIStoryboard(name: "Main", bundle: nil)
    }
    var myStoryBoard2: UIStoryboard {
        
        return UIStoryboard(name: "Main2", bundle: nil)
    }
    
    //MARK: -  Functions
    
    /**
     Presents ViewController with animation.
     
     - parameter viewControllerToPresent: ViewController to present.
     - parameter animated: With animation or not.
     - parameter modalTransitionStyle: Presentation style. Bydefault its 'coverVertical'.
     
     - returns: void.
     */
    open func presentPOPUP(_ viewControllerToPresent: UIViewController, animated flag: Bool, modalTransitionStyle:UIModalTransitionStyle = .coverVertical, completion: (() -> Swift.Void)? = nil) {
        DispatchQueue.main.async {
            viewControllerToPresent.modalPresentationStyle = .overCurrentContext
            viewControllerToPresent.modalTransitionStyle = modalTransitionStyle
            
            self.present(viewControllerToPresent, animated: flag, completion: completion)
        }
        
    }
}

extension UIViewController {
    
    /**
     Initialize a nib.
     
     
     - returns: UIViewController.
     */
    public class func fromNib<T>() -> T? where T : UIViewController {
        return fromNib(nibName: nil)
    }
    
    /**
     Initialize a nib.
     
     - parameter nibName: Nib name.
     
     - returns: UIViewController.
     */
    public class func fromNib<T>(nibName: String?) -> T? where T : UIViewController {
        
        let name = nibName ?? String(describing: self)
        return self.init(nibName: name, bundle: Bundle.main) as? T
    }
    
    /**
     Initialize a UIViewController from Storyboard.
     
     
     - returns: UIViewController.
     */
    class func instantiateViewControllerFromStoryboard<T>() -> T? where T : UIViewController {
        return instantiateViewController()
    }
    class func instantiateViewControllerFromStoryboard2<T>() -> T? where T : UIViewController {
        return instantiateViewController2()
    }
    
    /**
     Initialize a UIViewController from Storyboard.
     
     
     - returns: UIViewController.
     */
    fileprivate class func instantiateViewController<T>() -> T? where T : UIViewController  {
        return UIViewController().myStoryBoard.instantiateViewController(withIdentifier: String(describing: self)) as? T
    }
    fileprivate class func instantiateViewController2<T>() -> T? where T : UIViewController  {
        return UIViewController().myStoryBoard2.instantiateViewController(withIdentifier: String(describing: self)) as? T
    }
    
}

extension UIViewController {
    
    ///Show activity indicator with or without animation.
    open func showActivityIndicator(withAnimation isEnable: Bool = false) {
        AFActivityIndicator.shared.showActivityIndicator(withAnimation: isEnable)
        
    }
    
    ///Hide activity indicator with or without animation.
    open func hideActivityIndicator(withAnimation isEnable: Bool = false) {
        AFActivityIndicator.shared.hideActivityIndicator(withAnimation: isEnable)
    }
    
    ///Hide all activity indicators..
    open func removeAllActivityIndicator(){
        AFActivityIndicator.shared.removeAllActivityIndicator()
    }
}

//MARK:- Alerts

extension UIViewController {
    /**
     Show error popup with message.
     
     - parameter message: Message text that need to dispalyed.
     - parameter actionBlock: ok button action.
     
     
     - returns: void.
     */
    func showErrorAlertWithMessage(message: String?, _ actionBlock : @escaping AFButtonCompletionHandler = {}) {
        var messageString :String = ""
        if let messageObject = message,
            messageObject.isBlank == false {
            messageString = messageObject
        } else {
            messageString = Localized("Message_UnexpectedError")
        }
        self.showAlert(title: Localized("Title_Error"), message: messageString, actionBlock)
    }
    
    /**
     Show success popup with message.
     
     - parameter message: Message text that need to dispalyed.
     - parameter actionBlock: ok button action.
     
     
     - returns: void.
     */
    func showSuccessAlertWithMessage(message: String?, _ actionBlock : @escaping AFButtonCompletionHandler = {}) {
        self.showAlert(title: Localized("Title_successful"), message: message, actionBlock)
    }
    
    /**
     Show popup with parameters.
     
     - parameter title: Popup titile.
     - parameter message: Message text that need to dispalyed.
     - parameter btnTitle: Ok button title. Bydefault its 'OK'.
     
     - returns: void.
     */
    func showAlert(title:String?, message: String?, btnTitle:String = Localized("BtnTitle_OK"), _ actionBlock : @escaping AFButtonCompletionHandler = {}) {
        
        self.showAlert(title: title, attributedMessage: message?.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h2PinkRegular), btnTitle: btnTitle, actionBlock)
    }
    
    /**
     Show popup with parameters.
     
     - parameter title: Popup titile.
     - parameter attributedMessage: Message text that need to dispalyed.
     - parameter btnTitle: Ok button title. Bydefault its 'OK'.
     
     - returns: void.
     */
    func showAlert(title:String?, attributedMessage : NSAttributedString?, btnTitle:String = Localized("BtnTitle_OK"), _ actionBlock : @escaping AFButtonCompletionHandler = {}) {
        
        if let alert :AlertMessageVC = AlertMessageVC.fromNib() {
            
            alert.setAlertWith(attributedMessage: attributedMessage, btnTitle: btnTitle, cancelBlock: actionBlock)
            self.presentPOPUP(alert, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
            
        } else {
            let alertViewController = UIAlertController(title: "", message: attributedMessage?.string ?? "", preferredStyle: .alert)
            
            
            let okAction = UIAlertAction(title: btnTitle, style: .default) { (action) in
                actionBlock()
            }
            
            alertViewController.addAction(okAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    /**
     Show success popup with message.
     
     - parameter message: Message text that need to dispalyed.
     
     - returns: void.
     */
    func showAccessAlert(message: String?) {
        
        let alertViewController = UIAlertController(title: Localized("Title_Alert"), message: message ?? "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: Localized("BtnTitle_CANCEL"), style: .cancel) { (action) in }
        
        let settingAction = UIAlertAction(title: Localized("BtnTitle_SETTING"), style: .default) { (action) in
            
            if let url = URL(string: UIApplication.openSettingsURLString ), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }
        
        alertViewController.addAction(cancelAction)
        alertViewController.addAction(settingAction)
        
        self.present(alertViewController, animated: true, completion: nil)
        
    }
    
    /**
     Show confirmation popup. All Parameters are optional.
     
     - parameter message: Message text that need to dispalyed.
     - parameter okBtnTitle: Ok button title. Bydefault its 'OK'.
     - parameter cancelBtnTitle: cancel button title. Bydefault its 'Cancel'
     - parameter attributedText: attirbuted text. Bydefault its empty.
     - parameter alertType: Popup type could be 'logout or other'.
     - parameter okActionBlock: ok button action.
     - parameter cancelActionBlock: cancel button action.
     
     - returns: void.
     */
    func showConfirmationAlert(message: String?, okBtnTitle:String = Localized("BtnTitle_YES"), cancelBtnTitle:String = Localized("BtnTitle_NO"), alertType : ConfirmationAlertVC.ConfirmationLayoutType? = .other, _ okActionBlock : @escaping AFButtonCompletionHandler = {}, _ cancelActionBlock : @escaping AFButtonCompletionHandler = {}) {
        
        
        
        self.showConfirmationAlert(attributedMessage:  message?.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h2PinkRegular), okBtnTitle: okBtnTitle, cancelBtnTitle: cancelBtnTitle, alertType: alertType, okActionBlock, cancelActionBlock)
    }
    func showCreditConfirmationAlert(message: String?, okBtnTitle:String = Localized("BtnTitle_YES"), cancelBtnTitle:String = Localized("BtnTitle_NO"), alertType : ConfirmationAlertVC.ConfirmationLayoutType? = .other, _ okActionBlock : @escaping AFButtonCompletionHandler = {}, _ cancelActionBlock : @escaping AFButtonCompletionHandler = {}) {
        
        
        
        self.showCreditConfirmationAlert(attributedMessage:  message?.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h2PinkRegular), okBtnTitle: okBtnTitle, cancelBtnTitle: cancelBtnTitle, alertType: alertType, okActionBlock, cancelActionBlock)
    }
    
    /**
     Show confirmation popup. All Parameters are optional.
     
     - parameter message: Message text that need to dispalyed.
     - parameter okBtnTitle: Ok button title. Bydefault its 'OK'.
     - parameter cancelBtnTitle: cancel button title. Bydefault its 'Cancel'
     - parameter attributedText: attirbuted text. Bydefault its empty.
     - parameter alertType: Popup type could be 'logout or other'.
     - parameter okActionBlock: ok button action.
     - parameter cancelActionBlock: cancel button action.
     
     - returns: void.
     */
    func showConfirmationAlert(attributedMessage: NSAttributedString? = NSAttributedString.init(string: ""), okBtnTitle:String = Localized("BtnTitle_OK"), cancelBtnTitle:String = Localized("BtnTitle_NO"), alertType : ConfirmationAlertVC.ConfirmationLayoutType? = .other, _ okActionBlock : @escaping AFButtonCompletionHandler = {}, _ cancelActionBlock : @escaping AFButtonCompletionHandler = {}) {
        
        if let alert : ConfirmationAlertVC = ConfirmationAlertVC.fromNib() {
            
            alert.setAlertWith(popupType: alertType, message: attributedMessage, okBtnTitle: okBtnTitle, caneclBtnTitle: cancelBtnTitle, okBlock: okActionBlock, cancelBlock: cancelActionBlock)
            
            self.presentPOPUP(alert, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
            
        } else {
            let alertViewController = UIAlertController(title: title, message: attributedMessage?.string ?? "", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: okBtnTitle, style: .default) { (action) in
                okActionBlock()
            }
            let cancelAction = UIAlertAction(title: cancelBtnTitle, style: .default) { (action) in
                cancelActionBlock()
            }
            
            alertViewController.addAction(okAction)
            alertViewController.addAction(cancelAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    func showCreditConfirmationAlert(attributedMessage: NSAttributedString? = NSAttributedString.init(string: ""), okBtnTitle:String = Localized("BtnTitle_OK"), cancelBtnTitle:String = Localized("BtnTitle_NO"), alertType : ConfirmationAlertVC.ConfirmationLayoutType? = .other, _ okActionBlock : @escaping AFButtonCompletionHandler = {}, _ cancelActionBlock : @escaping AFButtonCompletionHandler = {}) {
        
        var userBalance = ""
        var balanceTitle = ""
        var creditAMount = ""
        if let homepageHandler :HomePageModel = HomePageModel.loadFromUserDefaults(key: APIsType.homePage.selectedLocalizedAPIKey()),
            let creditAmount = homepageHandler.credit?.creditTitleValue,
            creditAmount.isBlank == false {
            creditAMount = creditAmount
        }
        
        let currentUserType = AFUserSession.shared.userType
        switch currentUserType {
        case .prepaid, .prepaidFull, .prepaidDataSIM:
            if let homepageData = AFUserSession.shared.homePageData {
                if let prepaidBalanceInfo = homepageData.balance?.prepaid {
                    userBalance = prepaidBalanceInfo.mainWallet?.amount ?? "0.0"
                    balanceTitle = "\(prepaidBalanceInfo.mainWallet?.balanceTypeName ?? ""):"
                }
            }
            break
            
        case .postpaid, .postpaidDataSIM:
            
            if let homepageData = AFUserSession.shared.homePageData {
                if let postpaidBalanceInfo = homepageData.balance?.postpaid {
                    userBalance = postpaidBalanceInfo.balanceIndividualValue ?? "0.0"
                    balanceTitle = "\(postpaidBalanceInfo.balanceLabel ?? ""):"
                }
                
            }
            
            break
        }
        
        
        if let alert : ConfirmationAlertVC = ConfirmationAlertVC.fromNib() {
            alert.alertLayoutType = .creditConfirm
            alert.setCreditAlertWith(popupType: alertType, message: attributedMessage, okBtnTitle: okBtnTitle, caneclBtnTitle: cancelBtnTitle, userBalance: userBalance, creditAmount: creditAMount, balanceTitle: balanceTitle, okBlock: okActionBlock, cancelBlock: cancelActionBlock)
            
            self.presentPOPUP(alert, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
            
        } else {
            let alertViewController = UIAlertController(title: title, message: attributedMessage?.string ?? "", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: okBtnTitle, style: .default) { (action) in
                okActionBlock()
            }
            let cancelAction = UIAlertAction(title: cancelBtnTitle, style: .default) { (action) in
                cancelActionBlock()
            }
            
            alertViewController.addAction(okAction)
            alertViewController.addAction(cancelAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    ///Show logout popup.
    func showLogOutAlert(msisdnCount: Int, logOutActionBlock : @escaping AFButtonCompletionHandler = {}, cancelActionBlock : @escaping AFButtonCompletionHandler = {}, manageAccountActionBlock : @escaping AFButtonCompletionHandler = {}) {
        
        if let alert : ConfirmationAlertVC = ConfirmationAlertVC.fromNib() {
            
            alert.setLogoutAlert(msisdnCount: msisdnCount, logOutActionBlock, cancelBlock: cancelActionBlock, manageAccountBlock: manageAccountActionBlock)
            
            self.presentPOPUP(alert, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
            
        } else {
            
            let alertViewController = UIAlertController(title: Localized("Title_Logout"), message: Localized("Message_LogoutMessage"), preferredStyle: .alert)
            
            let manageAccountAction = UIAlertAction(title: Localized("BtnTitle_MANAGEACCOUNT"), style: .default) { (action) in
                manageAccountActionBlock()
            }
            
            let okAction = UIAlertAction(title: Localized("BtnTitle_LOGOUT"), style: .default) { (action) in
                logOutActionBlock()
            }
            
            let cancelAction = UIAlertAction(title: Localized("BtnTitle_CANCEL"), style: .cancel) { (action) in
                cancelActionBlock()
            }
            
            alertViewController.addAction(manageAccountAction)
            alertViewController.addAction(okAction)
            alertViewController.addAction(cancelAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    /**
     Show date picker as popup.
     
     - parameter minDate: Minimum date.
     - parameter maxDate: Maximum date.
     - parameter currentDate: Current date.
     
     - returns: didSelectDate(Selected date).
     */
    func showNewDatePicker(minDate : Date , maxDate : Date, currentDate : Date, didSelectDate : @escaping AFDatePickerVC.AFDatePickerCompletionHandler) {
        if let datePickerVC : AFDatePickerVC = AFDatePickerVC.fromNib() {
            datePickerVC.setDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate, didSelectDateBlock: didSelectDate)
            self.presentPOPUP(datePickerVC, animated: true)
        }
    }
}

//MARK:- Error message for Bio authitication
extension UIViewController {
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int?) -> (message:String, isUserAction:Bool) {
        
        var message :String?
        var userCanceled :Bool = false
        
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            message = Localized("Error_FailedCredentials")
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            userCanceled = true
            
        case LAError.userFallback.rawValue:
            message = "The user choose to use the fallback"
            userCanceled = true
            
        case LAError.systemCancel.rawValue:
            message = Localized("Error_SystemCancel")
            
        case LAError.passcodeNotSet.rawValue:
            message = Localized("Error_PasscodeNotSet")
            
        case LAError.appCancel.rawValue:
            message = Localized("Auth_AppCancel")
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = Localized("Error_NotInteractive")
            
        default:
            if #available(iOS 11.0, macOS 10.13, *) {
                switch errorCode {
                case LAError.biometryNotAvailable.rawValue:
                    message = Localized("Error_BiometryNotAvailable")
                    
                case LAError.biometryNotEnrolled.rawValue:
                    message = Localized("Error_BiometryNotEnrolled")
                    
                case LAError.biometryLockout.rawValue:
                    message = Localized("Error_BiometricLockout")
                    
                default:
                    break
                }
            } else {
                switch errorCode {
                case LAError.touchIDNotAvailable.rawValue:
                    message = Localized("Error_BiometryNotAvailable")
                    
                case LAError.touchIDNotEnrolled.rawValue:
                    message = Localized("Error_BiometryNotEnrolled")
                    
                case LAError.touchIDLockout.rawValue:
                    message = Localized("Error_BiometricLockout")
                default:
                    break
                }
            }
        }
        
        return ((message ?? Localized("Error_AuthGeneral")) ,userCanceled)
    }
}


extension UIViewController {
    
    /**
     Initialize a call.
     
     - parameter number: Number on which call need to initialized.
     
     - returns: void.
     */
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number.trimmWhiteSpace)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
    }
    
    /**
     Opens a URL in Safari.
     
     - parameter urlString: URL to open.
     
     - returns: void.
     */
    func openURLInSafari(urlString : String?) {
        
        // Not Valid URL string
        if urlString == nil {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        } else {
            
            var newURLString = urlString
            if (urlString?.containsSubString(subString: "http://") ?? false) == false &&
                (urlString?.containsSubString(subString: "https://") ?? false) == false{
                
                newURLString = "http://\(urlString ?? "")"
            }
            
            if let requestUrl = URL(string: newURLString?.trimmWhiteSpace ?? "") {
                
                if UIApplication.shared.canOpenURL(requestUrl) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(requestUrl, options: [:], completionHandler:nil)
                    } else {
                        UIApplication.shared.openURL(requestUrl)
                    }
                }  else {
                    self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
                }
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }
    }
    
    
    ///Check whether notification are enabled or not.
    func isUserEnabledNotification(completionHandler: @escaping (Bool) -> Void) {
        
        
        if #available(iOS 10.0, *) {
            
            var isEnabled : Bool = false
            let current = UNUserNotificationCenter.current()
            
            current.getNotificationSettings(completionHandler: { (settings) in
                
                if settings.authorizationStatus == .authorized {
                    // Notification permission was already granted
                    isEnabled = true
                    
                } else if settings.authorizationStatus == .notDetermined {
                    // Notification permission has not been asked yet, go for it!
                    isEnabled = false
                    
                } else if settings.authorizationStatus == .denied {
                    // Notification permission was previously denied, go to settings & privacy to re-enable
                    isEnabled = false
                    
                }
                completionHandler(isEnabled)
                
            })
            
        } else {
            
            var isEnabled : Bool = false
            // Fallback on earlier versions
            if let notificationType = UIApplication.shared.currentUserNotificationSettings?.types {
                if notificationType == [] {
                    
                    isEnabled = false
                } else {
                    isEnabled = true
                }
            } else {
                isEnabled = false
            }
            
            completionHandler(isEnabled)
        }
    }
    
    
    /**
     Redirect user to AppStore
     - returns: void
     */
    func redirectUserToAppStore() {
        if let storeURL = URL(string: AFUtilities.getAppStoreURL()),
            UIApplication.shared.canOpenURL(storeURL) {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(storeURL, options: [:], completionHandler: { (success: Bool) in
                    
                    // If failded to open URL
                    if success == false {
                        UIApplication.shared.openURL(storeURL)
                    }
                })
            } else {
                UIApplication.shared.openURL(storeURL)
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_CannotAbleToRedirect"))
        }
    }
}


extension UIViewController {
    /**
     Add ViewController as sub view UIView.
     
     - parameter childController: ViewController to add as sub view.
     - parameter onView: Parent view in which you want to add sub view.
     
     - returns: void.
     */
    func addChildViewController(childController: UIViewController, onView holderView: UIView)  {
        
        self.addChild(childController)
        holderView.addSubview(childController.view)
        childController.didMove(toParent: self)
        
        childController.view.snp.makeConstraints { make in
            make.top.equalTo(holderView.snp.top)
            make.right.equalTo(holderView.snp.right)
            make.left.equalTo(holderView.snp.left)
            make.bottom.equalTo(holderView.snp.bottom)
        }
    }
    
    /**
     Remove ViewController from sub view.
     
     - parameter childController: ViewController to remove from sub view.
     
     - returns: void.
     */
    func removeChildViewController(childController: UIViewController)  {
        
        self.children.forEach { (aViewController) in
            
            if aViewController.isKind(of: childController.classForCoder) {
                
                // Notify the child that it's about to be moved away from its parent
                aViewController.willMove(toParent: nil)
                // Remove the child
                aViewController.removeFromParent()
                // Remove the child view controller's view from its parent
                aViewController.view.removeFromSuperview()
                
            }
        }
    }
}
