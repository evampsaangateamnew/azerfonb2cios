//
//  Data+AF.swift
//  Azerfon
//
//  Created by Mian Waqas Umar on 23/04/2019.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation

extension Data {
    
    ///Returns data type or extenstion of data.
    func format() -> String {
        
        let array = [UInt8](self)
        let ext: String
        switch (array[0]) {
        case 0xFF:
            ext = ".jpg"
        case 0x89:
            ext = ".png"
        case 0x47:
            ext = ".gif"
        case 0x49, 0x4D :
            ext = ".tiff"
        default:
            ext = "unknown"
        }
        return ext
    }
}
