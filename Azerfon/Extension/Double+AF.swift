//
//  Double+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/28/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    /// Convert double to string
    func toString() -> String {
        return "\(self)"
    }
    /// Convert double to plain string
    func toPlanString() -> String {
        return String(format: "%.0f", self)
    }
}
