//
//  CGRect+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

// MARK:- CGRect extension -
extension CGRect {
    var x: CGFloat {
        get {
            return self.origin.x
        }
        set {
            self.origin.x = newValue
        }
    }
    
    var y: CGFloat {
        get {
            return self.origin.y
        }
        
        set {
            self.origin.y = newValue
        }
    }
    
    var center: CGPoint {
        return CGPoint(x: self.x + self.width / 2, y: self.y + self.height / 2)
    }
}

extension CGRect {
    
    /**
     Return a rect with size pecentage.
     
     - parameter width: width of rect.
     - parameter height: height of rect.
     
     - returns: CGRect.
     */
    func sizeByPercentage(width: CGFloat, height: CGFloat = 1.0) -> CGRect {
        let width = self.width * width
        let height = self.height * height
        
        return CGRect(
            x: self.origin.x, y: self.origin.y, width: width, height: height
        )
    }
}
