//
//  CATextLayer+AF.swift
//  Azerfon
//
//  Created by Waqas Umar on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import UIKit

extension CATextLayer {
    
    func sizeWidthToFit() {
        let fontName = CTFontCopyPostScriptName(self.font as! CTFont) as String
        
        let font = UIFont(name: fontName, size: self.fontSize)
        
        let attributes = NSDictionary(object: font!, forKey: NSAttributedString.Key.font as NSCopying)
        
        let attString = NSAttributedString(string: self.string as! String, attributes: attributes as? [NSAttributedString.Key : AnyObject])
        
        var ascent: CGFloat = 0, descent: CGFloat = 0, width: CGFloat = 0
        
        let line = CTLineCreateWithAttributedString(attString)
        width = CGFloat(CTLineGetTypographicBounds( line, &ascent, &descent, nil))
        width = ceil(width)
        
        let frameSetter = CTFramesetterCreateWithAttributedString(attString as CFAttributedString)
        let path = CGMutablePath()
        path.addRect(CGRect(x: 0, y: 0, width: 400, height: 200))
        let frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, attString.length), path, nil)
        let lineCount = CGFloat(CFArrayGetCount(CTFrameGetLines(frame)))
        let height = (ascent+descent) * lineCount
        
        self.bounds = CGRect(x: 0, y: 0, width: width, height: ceil(height))
    }
}
