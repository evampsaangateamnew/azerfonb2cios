//
//  Int+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/20/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation

extension Int {
    
    ///Convert Int to string
    func toString() -> String {
        return "\(self)"
    }
    ///Convert Int to double
    func toDouble() -> Double {
        return Double(self)
    }
}
