//
//  UIColor+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

extension UIColor {
    

     class var  afSoftPink: UIColor {
        return UIColor(red: 253.0 / 255.0, green: 173.0 / 255.0, blue: 195.0 / 255.0, alpha: 1.0)
    }
    
    class var  afSuperPink: UIColor {
        return UIColor(red: 212.0 / 255.0, green: 49.0 / 255.0, blue: 87.0 / 255.0, alpha: 1.0)
    }
    
    class var afLightPink: UIColor {
        return UIColor(red: 253.0 / 255.0, green: 173.0 / 255.0, blue: 195.0 / 255.0, alpha: 1.0)
    }
    
    class var afRosyPink: UIColor {
        return UIColor(red: 240.0 / 255.0, green: 115.0 / 255.0, blue: 144.0 / 255.0, alpha: 1.0)
    }
    
    class var  afVeryLightPink: UIColor {
        return UIColor(white: 240.0 / 255.0, alpha: 1.0)
    }
    
    class var  afPurplishBrown: UIColor {
        return UIColor(red: 51.0 / 255.0, green: 49.0 / 255.0, blue: 50.0 / 255.0, alpha: 1.0)
    }
    
    class var  afBrownGrey: UIColor {
        return UIColor(white: 128.0 / 255.0, alpha: 1.0)
    }
    
    class var  afDarkPink: UIColor {
        return UIColor(red: 181.0 / 255.0, green: 31.0 / 255.0, blue: 69.0 / 255.0, alpha: 1.0)
    }
    
    class var  afGunmetal: UIColor {
        return UIColor(red: 92.0 / 255.0, green: 93.0 / 255.0, blue: 93.0 / 255.0, alpha: 1.0)
    }
    
    class var  afGreyishBrown: UIColor {
        return UIColor(red: 70.0 / 255.0, green: 70.0 / 255.0, blue: 68.0 / 255.0, alpha: 1.0)
    }
    
    class var  afVeryLightPinkTwo: UIColor {
        return UIColor(white: 204.0 / 255.0, alpha: 1.0)
    }
    
    class var  veryLightPinkThree: UIColor {
        return UIColor(white: 240.0 / 255.0, alpha: 1.0)
    }
    
    public class var afLightBlueGrey: UIColor {
        return UIColor(red: 207.0 / 255.0, green: 210.0 / 255.0, blue: 219.0 / 255.0, alpha: 1.0)
    }
    
    class var  afWhite: UIColor {
        return UIColor.white
    }
    class var afBerry: UIColor {
        return UIColor(red: 128.0 / 255.0, green: 23.0 / 255.0, blue: 44.0 / 255.0, alpha: 1.0)
    }
    
    class var afBlackTransparent: UIColor {
        return UIColor(white: 0.0, alpha: 0.60)
    }
    
    class var afBlackTransparentShadow: UIColor {
        return UIColor(white: 0.0, alpha: 0.3)
    }
    
    public class var afDatePickerGrey : UIColor{
        return UIColor(red: 235.0 / 255.0, green: 235.8 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
    }
    
    public class var afDateOrange : UIColor{
        return UIColor(red: 255 / 255.0, green: 138 / 255.0, blue: 59 / 255.0, alpha: 1.0)
    }
    
    public class var afGreen : UIColor{
        return UIColor.green
    }
    public class var afSeparator : UIColor{
        return UIColor.afRosyPink
    }
    
    
    
    ///MIA
    public class var MBRedColor : UIColor{
        // return UIColor(red: 224/255.0, green: 0/255.0, blue: 52/255.0, alpha: 1.0)
        return UIColor(hexString: "#E00034")
        //return UIColor(hexString: "#E00134")
        
    }//E00134
    
    public class var MBLightGrayColor: UIColor{
        return UIColor(hexString: "#808080")
    }
    public class var MBButtonBackgroundGrayColor: UIColor{
        return UIColor(hexString: "BEBEBE")
    }
    
    public class var MBTextBlack: UIColor{
        return UIColor (hexString: "#363636")
    }
    
    public class var MBStoreGrayColor: UIColor{
        return UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1.0)
    }
    
    public class var MBDimLightGrayColor: UIColor{
        return UIColor(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
    }
    
    public class var MBTextGrayColor: UIColor{
        return UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        //        555555
    }
    
    public class var MBDarkGrayColor: UIColor{
        return UIColor.darkGray
    }
    public class var MBLightProgressTrackColor: UIColor{
        return UIColor (red: 229.0/255.0, green: 79.0/255.0, blue: 114.0/255.0, alpha: 1.0)
    }
    
    public class var MBBorderGrayColor: UIColor{
        return UIColor (hexString: "#ECECEC")
    }
    
    public class var MBGreen: UIColor{
        return UIColor(hexString: "#6DBC36")
    }
    
    public class var MBBarRed: UIColor{
        return UIColor(hexString: "#FF3939")
        
    }
    public class var MBBarOrange: UIColor{
        return UIColor(hexString: "#FF8A3B")
    }
    public class var MBBarGreen: UIColor{
        return UIColor(hexString: "#73F473")
    }
    
    public class var MBProgerssDisableTextColor: UIColor{
        return UIColor(hexString: "#EA456B")
    }
    public class var MBProgerssDisablePathColor: UIColor{
        return UIColor(hexString: "#D71441")
    }
    public class var MBProgerssEnablePathColor: UIColor{
        return UIColor(hexString: "#808080")
    }
    public class var MBLightRedColor : UIColor{
        return UIColor (red: 207.0/255.0, green: 81.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    }


}

// Based on GradientProgressBar Library
extension UIColor {
    /// Create color from RGB
    convenience init(absoluteRed: Int, green: Int, blue: Int) {
        self.init(
            absoluteRed: absoluteRed,
            green: green,
            blue: blue,
            alpha: 1.0
        )
    }
    
    /// Create color from RGBA
    convenience init(absoluteRed: Int, green: Int, blue: Int, alpha: CGFloat) {
        let normalizedRed = CGFloat(absoluteRed) / 255
        let normalizedGreen = CGFloat(green) / 255
        let normalizedBlue = CGFloat(blue) / 255
        
        self.init(
            red: normalizedRed,
            green: normalizedGreen,
            blue: normalizedBlue,
            alpha: alpha
        )
    }
    
    // Color from HEX-Value
    convenience init(hexValue:Int) {
        self.init(
            absoluteRed: (hexValue >> 16) & 0xff,
            green: (hexValue >> 8) & 0xff,
            blue: hexValue & 0xff
        )
    }
    
    // Color from HEX-String
    convenience init(hexString:String) {
        var normalizedHexString = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (normalizedHexString.hasPrefix("#")) {
            normalizedHexString.remove(at: normalizedHexString.startIndex)
        }
        //  normalizedHexString = normalizedHexString.replacingOccurrences(of: "#", with: "")
        
        // Convert to hexadecimal integer
        var hexValue:UInt32 = 0
        Scanner(string: normalizedHexString).scanHexInt32(&hexValue)
        
        self.init(
            hexValue:Int(hexValue)
        )
    }
}
