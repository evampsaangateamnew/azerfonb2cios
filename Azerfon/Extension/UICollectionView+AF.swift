//
//  UICollectionView+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/20/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func reloadUserData(_ dataArray : [Any?]? = [], description : String? = Localized("Message_NoData")) {
        
        if ((dataArray?.count ?? 0) <= 0 ) {
            self.showDescriptionViewWithImage(description: description)
            
        } else {
            self.hideDescriptionView()
        }
        
        self.reloadData()
    }
}

extension UICollectionViewCell {
    /**
     Register cell with UITableView.
     
     - parameter tableView: UITableView to register Cell with.
     
     - returns: void.
     */
    class public func registerReusableCell(with collectionView :UICollectionView?) {
        collectionView?.register(UINib(nibName: self.cellIdentifier(), bundle: Bundle.main), forCellWithReuseIdentifier: self.cellIdentifier())
    }
    
    /**
     Will return class name as string.
     - returns: name of 'self' as string.
     */
    class public func cellIdentifier() -> String {
        return String(describing: self)
    }
    
}

extension UICollectionReusableView {
    /**
     Register cell with UITableView.
     
     - parameter tableView: UITableView to register Cell with.
     
     - returns: void.
     */
    class public func registerReusableView(with collectionView :UICollectionView?, forSupplementaryViewOfKind viewKind:String =  UICollectionView.elementKindSectionHeader) {
        
        collectionView?.register(UINib(nibName: self.viewIdentifier(), bundle: Bundle.main),
                                 forSupplementaryViewOfKind: viewKind,
                                 withReuseIdentifier: self.viewIdentifier())
    }
    
    /**
     Will return class name as string.
     - returns: name of 'self' as string.
     */
    class public func viewIdentifier() -> String {
        return String(describing: self)
    }
    
}
