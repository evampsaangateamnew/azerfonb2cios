//
//  Array+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

public extension Array where Element: Mappable {
    
    /// Save JSON string into user defaults
    func saveInUserDefaults (key : String) {
        
        UserDefaults.standard.set(self.toJSONString(), forKey: key)
        UserDefaults.standard.synchronize()
    }
}
