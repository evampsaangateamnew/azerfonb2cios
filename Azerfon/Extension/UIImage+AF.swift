//
//  UIImage+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/7/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

extension UIImage {
    /**
     Load image from a Assets.
     
     - parameter key: Name of  image.
     
     - returns: UIImage.
     */
    public class func imageFor(name: String?) -> UIImage {
        if name?.isBlank != true {
            return UIImage(named: name ?? "placeHolder") ?? UIImage(named: "placeHolder") ?? UIImage()
        } else {
            return UIImage(named: "placeHolder") ?? UIImage()
        }
    }
    
    
    /**
     Load image from a Assets.
     
     - parameter key: Name of  image.
     
     - returns: UIImage.
     */
    public class func imageFor(key: String?) -> UIImage {
        
        
        let iconKeyString = key?.trimmWhiteSpace.lowercased() ?? ""
        var iconName = ""
        
        switch (iconKeyString) {
        case "calls":
            iconName = "ic_calls"

        case "internet":
            iconName = "ic_internet"
            
        case "whatsapp":
            iconName = "ic_whatsapp"
            
        case "internationalcalls":
            iconName = "ic_calls_international"
            
        case "countrywidesms":
            iconName = "ic_sms"
            
        case "roaminginternet":
            iconName = "ic_raoming_internet"
            
        case "youtube":
            iconName = "ic_youtube"
            
        case "instagram":
            iconName = "ic_insta"
            
        case "facebook":
            iconName = "ic_facebook"
            
        case "mstwitter":
            iconName = "ic_twitter"
            
        case "mssnap":
            iconName = "ic_snapchat"
            
        case "telegramusing":
            iconName = "ic_telegram"
            
        case "mms":
            iconName = "ic_mms"
            
        case "smsinternational":
            iconName = "ic_sms_international"
            
        case "vkonakte":
            iconName = "ic_vkonakte"
            
        case "internetnight":
            iconName = "ic_internet_night"
            
        case "exchange":
            iconName = "ic_exchange"
            
        case "smsroaming":
            iconName = "ic_sms_roaming"
            
        case "flagicon":
            iconName = "ic_flag"
            
        case "socialpack":
            iconName = "ic_social_pack"
            
        case "roamingcallsout":
            iconName = "ic_calls_roaming_out"
            
        case "roamingcallsin":
            iconName = "ic_calls_roaming_in"
            
        case "detailsicon":
            iconName = "ic_notes"
            
        case "tsdrounding":
            iconName = "ic_tsDRounding"
            
        default:
            break
        }
        
        return UIImage.imageFor(name:iconName)
    }
    
    /**
     Load marker image from a Assets.
     
     - parameter key: Name of  image.
     - parameter inSmallSize: Size of marker image whether small or default size.
     
     - returns: UIImage.
     */
    public class func markerIconImageFor(key: String?, inSmallSize:Bool = false) -> UIImage {
        
        
        if key != nil {
            
            let iconKeyString = key?.lowercased() ?? ""
            var iconName = ""
            
            switch (iconKeyString) {
            case "Head Office".lowercased(), "Mərkəzi Ofis".lowercased(), "Центральный Офис".lowercased() :
                iconName = "default_marker"
                
            case "Customer care offices".lowercased(), "Müştəri xidmətləri mərkəzləri".lowercased(), "Офисы обслуживания".lowercased() :
                iconName = "serviceCenterMarker"
                
            case "Bakcell-IM".lowercased(), "Bakcell-IM".lowercased(), "Bakcell-IM".lowercased() :
                iconName = "default_marker"
                
            case "Dealer stores".lowercased(), "Diler mağazaları".lowercased(), "Дилерские магазины".lowercased() :
                iconName = "default_marker"
                
            default:
                iconName = "default_marker"
            }
            
            if inSmallSize == true {
                iconName += "_Small"
            }
            return UIImage.imageFor(name: iconName)
            
        } else {
            return UIImage()
        }
    }
}

extension UIImage {
    //
    /// Tint Image
    ///
    /// - Parameter fillColor: UIColor
    /// - Returns: Image with tint color
    func setTint(with fillColor: UIColor) -> UIImage? {
        let image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        fillColor.set()
        image.draw(in: CGRect(origin: .zero, size: size))
        
        guard let imageColored = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        
        UIGraphicsEndImageContext()
        return imageColored
    }
}

// MARK: - UIImage (Base64 Encoding)

extension UIImage {
    
    public enum ImageFormat {
        case PNG
        case JPEG(CGFloat)
    }
    
    /**
     Convert image to base64.
     
     - parameter format: Image format PNG or JPEG.
     
     - returns: base64,imageformate(String,String).
     */
    public func base64(format: ImageFormat) -> (String,String) {
        var imageData: Data
        
        switch format {
            
        case .PNG:
            imageData = self.pngData() ?? Data()
            
        case .JPEG(let compression):
            
            imageData = self.jpegData(compressionQuality: compression)  ?? Data()
        }
        
        let imageFormate = imageData.format()
        
        return (imageData.base64EncodedString(), imageFormate)
    }
}
