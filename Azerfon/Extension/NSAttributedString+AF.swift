//
//  NSAttributedString+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/25/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    func appendImage(imageName :String, font :UIFont, imageSize :CGSize, tintColor :UIColor? = nil) {
        // create our NSTextAttachment
        let iconImage = NSTextAttachment()
        
        if let imageData = UIImage(named: imageName) {
            
            if let newColor = tintColor {
                iconImage.image = imageData.setTint(with: newColor)
            } else {
               iconImage.image = imageData
            }
            // iconImage.image = UIImage(cgImage: cgImageData, scale: 1, orientation: .up)
            
            iconImage.bounds = CGRect(x: 0,
                                      y: (font.capHeight - imageSize.height).rounded(),
                                      width: imageSize.width,
                                      height: imageSize.height)
        }
        
        /* wrap the attachment in its own attributed string and append it */
        self.append(NSAttributedString(attachment: iconImage))
    }
}
