//
//  UINavigationController+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    ///Navigates user to login screen
    func popToLoginViewController() {
        
        var isFoundLoginController = false
        
        self.viewControllers.forEach({ (aViewController) in
            
            if aViewController.isKind(of: LoginVC().classForCoder) {
                isFoundLoginController = true
                self.popToViewController(aViewController, animated: true)
                
            }
            
        })
        
        if !isFoundLoginController {
            
            if let splashVCObj = self.viewControllers.first {
                self.popToViewController(splashVCObj, animated: true)
            } else {
                self.popToRootViewController(animated: false)
            }
            
        }
    }
}
