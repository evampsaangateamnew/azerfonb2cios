//
//  TimeZone+AF.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
extension TimeZone{
    static func appTimeZone() -> TimeZone {
        return self.current
        //return self.init(abbreviation: "UTC") ??  self.current
    }
}
