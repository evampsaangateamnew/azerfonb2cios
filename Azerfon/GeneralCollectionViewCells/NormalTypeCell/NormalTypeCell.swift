//
//  NormalTypeCell.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/29/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class NormalTypeCell: UICollectionViewCell {

    @IBOutlet var myContentView     : AFView!
    @IBOutlet var titleLabel        : AFLabel!
    @IBOutlet var specialImageView  : UIImageView!
    
    @IBOutlet var specialIconWidthConstaint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setLayout(title :String?, isSelected :Bool, isSpecial :Bool) {
        
        titleLabel.text = title ?? ""
        if isSpecial {
            specialIconWidthConstaint.constant = 20
        } else {
            specialIconWidthConstaint.constant = 0
        }
        
        isItemSelected(isSelected)
    }
    
    func isItemSelected(_ isSelected :Bool) {
        
        if isSelected {
            myContentView.backgroundColor = UIColor.afWhite
            titleLabel.font = UIFont.h5GreyCenter
            titleLabel.textColor = UIColor.afPurplishBrown
            specialImageView.image = UIImage.init(named: "special_tariff_icon")
            
        } else {
            myContentView.backgroundColor = UIColor.afDarkPink
            titleLabel.font = UIFont.h5WhiteCenter
            titleLabel.textColor = UIColor.afWhite
            specialImageView.image = UIImage.init(named: "special_tariff_icon_white")
        }
    }

}
