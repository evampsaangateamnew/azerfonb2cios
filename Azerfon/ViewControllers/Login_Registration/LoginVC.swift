//
//  LoginVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
import LocalAuthentication
import KYDrawerController

class LoginVC: BaseVC {
    //MARK: - Properties
    /// An authentication context stored at class scope so it's available for use during UI updates.
    var context :LAContext?
    var errorTooltip :ZGTooltipView?
    var registrationType :Constants.RegistrationType = .registration 
    
    //MARK: - IBOutlet
    @IBOutlet var languageContainerView : UIView!
    @IBOutlet var languageTitleLabel    : AFLabel!
    @IBOutlet var logoImageView         : UIImageView!
    @IBOutlet var userMSISDNTextField   : AFTextField!
    @IBOutlet var passwordTextField     : AFTextField!
    @IBOutlet var bioAuthContainerView  : UIView!
    @IBOutlet var forgotButton          : AFButton!
    @IBOutlet var loginButton           : AFButton!
    @IBOutlet var signUpButton          : AFButton!
    
    @IBOutlet var touchIDButton         : UIButton!
    @IBOutlet var faceIDButton          : UIButton!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userMSISDNTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /* Set Visible controlles accourding to type */
        if registrationType == .addAccount {
            self.interactivePop(true)
            backButton?.isHidden = false
            languageContainerView.isHidden = true
            bioAuthContainerView.isHidden = true
        } else {
            self.interactivePop(false)
            backButton?.isHidden = true
            languageContainerView.isHidden = false
            bioAuthContainerView.isHidden = false
        }
        
        loadViewContent()
        userMSISDNTextField.text = Constants.numberPrefix
        passwordTextField.text = ""
        
        /*#if DEBUG
        userMSISDNTextField.text = Constants.numberPrefix + "776482285"
        passwordTextField.text = "123456"
        #else
        userMSISDNTextField.text = Constants.numberPrefix
        passwordTextField.text = ""
        #endif*/
        
        userMSISDNTextField.text = Constants.numberPrefix
        passwordTextField.text = ""
        
        if LAContext().biometricType == .faceID {
            
            faceIDButton.isEnabled = true
            touchIDButton.isEnabled = false
            
        } else if LAContext().biometricType == .touchID {
            
            faceIDButton.isEnabled = false
            touchIDButton.isEnabled = true
            
        } else {
            faceIDButton.isEnabled = false
            touchIDButton.isEnabled = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        context = nil
    }
    //MARK: - IBAction
    
    @IBAction func changeLanguagePressed(_ sender:UIButton) {
        if let changelang :LanguageSelectionVC =  LanguageSelectionVC.fromNib() {
            
            changelang.setLanguageSelectionAlert(AFLanguageManager.userSelectedLanguage()) { (newSelectedLanguage) in
                
                if AFLanguageManager.userSelectedLanguage() != newSelectedLanguage {
                    // Setting user language
                    AFLanguageManager.setUserLanguage(selectedLanguage: newSelectedLanguage)
                    
                    // Reloading layout Text accourding to seletcted language
                    self.loadViewContent()
                }
            }
            
            self.presentPOPUP(changelang, animated: true, completion:  nil)
        }
    }
    
    @IBAction func forgotPressed(_ sender: AFButton) {
        if let verifyNumber :RegistrationNumberVerificationVC = RegistrationNumberVerificationVC.instantiateViewControllerFromStoryboard() {
            verifyNumber.selectedType = .ForgotPassword
            verifyNumber.registrationType = registrationType
            self.navigationController?.pushViewController(verifyNumber, animated: true)
        }
    }
    
    @IBAction func biomatricPressed(_ sender: AFButton) {
        
        if AFUserSession.shared.isBiometricEnabled() &&
            AFUserSession.shared.isLoggedIn() &&
            AFUserSession.shared.loadUserInfomation() {
            
            self.authenticateWithTouchID()
        } else {
            self.showErrorAlertWithMessage(message: Localized("Error_BiometricEnabledOrNotLogin"))
        }
    }
    
    @IBAction func loginPressed(_ sender: AFButton) {
        
        var msisdn : String = ""
        var password : String = ""
        
        // MSISDN validation
        if let userMSISDNText = userMSISDNTextField.text?.formatedMSISDN(),
            userMSISDNText.isValidMSISDN() {
            
            if registrationType == .addAccount,
                AFUserSession.shared.loggedInUsers?.users?.contains(where: {($0.userInfo?.msisdn ?? "").isEqual(userMSISDNText, ignorCase: true)}) ?? false {
                self.showErrorAlertWithMessage(message: Localized("Error_AddNumber"))
                return
            } else {
                msisdn = userMSISDNText
            }
            
        } else {
            showErrorMessage(view: userMSISDNTextField, message: Localized("Message_EnterValidMSISDN"))
            return
        }
        
        // Password validation
        if let passwordText = passwordTextField.text {
            
            let passwordStrength:Constants.AFPasswordStrength = AFUtilities.determinePasswordStrength(Text: passwordText)
            
            switch passwordStrength {
                
            case .didNotMatchCriteria:
                
                showErrorMessage(view: passwordTextField, message: Localized("Message_InvalidPassword"))
                return
            case .Week, .Medium, .Strong:
                password = passwordText
            }
            
        } else {
            showErrorMessage(view: passwordTextField, message: Localized("Message_InvalidPassword"))
            return
        }
        
        _ = userMSISDNTextField.resignFirstResponder()
        _ = passwordTextField.resignFirstResponder()
        
        // API call for user authentication
        authenticateUserAPICall(msisdn, Password: password)
        
    }
    
    @IBAction func signupPressed(_ sender: AFButton) {
        if let verifyNumber :RegistrationNumberVerificationVC = RegistrationNumberVerificationVC.instantiateViewControllerFromStoryboard() {
            verifyNumber.selectedType = .SignUp
            verifyNumber.registrationType = registrationType
            self.navigationController?.pushViewController(verifyNumber, animated: true)
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        languageTitleLabel.text = Localized("Info_Language")
        logoImageView.image = UIImage.imageFor(name: Localized("Img_NARLogo"))
        userMSISDNTextField.placeholder = Localized("Placeholder_MobileNumber")
        passwordTextField.placeholder = Localized("Placeholder_Password")
        forgotButton.setTitle(Localized("BtnTitle_Forgot"), for: .normal)
        loginButton.setTitle(Localized("BtnTitle_Login"), for: .normal)
        
        
        let signUpTitle = String(format: Localized("BtnTitle_DontsHaveAccount"), Localized("BtnTitle_SignUp"))
            .createAttributedString(mainStringColor: UIColor.afLightPink,
                                    mainStringFont: UIFont.b4LightPinkCenter,
                                    highlightedStringColor: UIColor.afWhite,
                                    highlightedStringFont: UIFont.h6LightGrey,
                                    highlighStrings: [Localized("BtnTitle_SignUp")])
        
        signUpButton.setAttributedTitle(signUpTitle, for: .normal)
        
    }
    
    func showErrorMessage(view:UIView, message :String?) {
        
        errorTooltip?.dismiss(remove: true)
        
        errorTooltip = ZGTooltipView(direction: .bottomLeft, customView: ZGTooltipView.createLabel(text: message ?? "", tipTextFont: UIFont.b4PinkCenter, tipTextColor: UIColor.afSuperPink), originView: view, removeOnDismiss: true)
        errorTooltip?.backgroundColor = UIColor.afWhite
        errorTooltip?.isBubbleXPositionFix = true
        errorTooltip?.bubbleFixXPosition = 60
        errorTooltip?.displayTooltip()
        
    }
    
    func redirectToDashboardScreen(isLoggedInNow :Bool) {
        
        if let count = self.navigationController?.viewControllers.count,
            count > 1,
            let firstVC = self.navigationController?.viewControllers.first,
            let mainViewController :TabbarController = TabbarController.instantiateViewControllerFromStoryboard() ,
            let drawerViewController :SideMenuVC = SideMenuVC.instantiateViewControllerFromStoryboard() {
            
            mainViewController.isLoggedInNow = isLoggedInNow
            let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
            drawerController.mainViewController = mainViewController
            drawerController.drawerViewController = drawerViewController
            
            self.navigationController?.viewControllers = [firstVC, drawerController]
        }
    }
    
    func authenticateWithTouchID() {
        
        context = LAContext()
        
        /* First check if we have the needed hardware support. */
        var error: NSError?
        if context?.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) ?? false {
            
            let reason = Localized("Biometric_Login")
            context?.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                DispatchQueue.main.async {
                    if success {
                        self.redirectToDashboardScreen(isLoggedInNow: false)
                        
                    } else {
                        /* Show error alert if biometry/TouchID/FaceID is lockout or not enrolled */
                        self.showBioAuthError(errorCode: error?._code)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                /* Show error alert if biometry/TouchID/FaceID is lockout or not enrolled */
                self.showBioAuthError(errorCode: error?._code)
            }
        }
    }
    
    func showBioAuthError(errorCode: Int?) {
        let errorDescription = self.evaluateAuthenticationPolicyMessageForLA(errorCode: errorCode)
        if errorDescription.isUserAction == false {
            self.showErrorAlertWithMessage(message: errorDescription.message, {
                // self.redirectToLoginScreen()
            })
        } else {
            // self.redirectToLoginScreen()
        }
    }
    
    //MARK: - API's Calls
    
    /**
     Call 'authenticateUser' API with the specified `MSISDN` and `Password`.
     
     - parameter MSISDN: MSISDN of current user.
     - parameter pasword: pasword of current user.
     
     - returns: void
     */
    func authenticateUserAPICall(_ msisdn : String , Password pasword : String){
        
        self.showActivityIndicator()
        _ = AFAPIClient.shared.authenticateUser(msisdn , Password:pasword ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "login", contentType:"user_login" , successStatus:"0" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let authanticateUser = Mapper<LoginModel>().map(JSONObject: resultData) {
                    
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "login", contentType:"user_login" , successStatus:"1" )
                    
                    if self.registrationType == .addAccount {
                        /* Save logged in user information */
                        AFUserInfoUtilities.addNewUser(newUserMSISDN: msisdn, loginInfo: authanticateUser)
                    } else {
                        /* Save logged in user information */
                        AFUserInfoUtilities.saveLoginInfo(loggedInUserMSISDN: msisdn, loginInfo: authanticateUser)
                    }
                    
                    /* Redirect user to home screen */
                    self.redirectToDashboardScreen(isLoggedInNow: true)
                    
                } else {
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "login", contentType:"user_login" , successStatus:"0" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - UITextFieldDelegate
extension LoginVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == userMSISDNTextField {
            
            /* Check prefix limit and disabling editing in prefix */
            let protectedRange = NSMakeRange(0, userMSISDNTextField.prefixLength)
            let intersection = NSIntersectionRange(protectedRange, range)
            
            if intersection.length > 0 ||
                range.location < (protectedRange.location + protectedRange.length) {
                
                return false
            }
            
            /* Restricting user to enter only number */
            let allowedCharactors = Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            /* Restriction for MSISDN length */
            return newLength <= (Constants.MSISDNLength + userMSISDNTextField.prefixLength)
            
        } else if textField == passwordTextField {
            
            return newLength <= Constants.maxPasswordLength /* Restriction for password length */
            
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        /* Update tip possition if visisble */
        errorTooltip?.updatePosition()
        
        if textField == userMSISDNTextField {
            passwordTextField.becomeFirstResponder()
            return false
        } else if textField == passwordTextField {
            _ = textField.resignFirstResponder()
            loginPressed(AFButton())
            return true
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        /* Update tip possition if visisble */
        errorTooltip?.updatePosition()
    }
}


/*
 let preference = ToolTipPreferences()
 preference.drawing.bubble.color = UIColor.afWhite
 preference.drawing.bubble.spacing = 4
 preference.drawing.bubble.cornerRadius = 5
 preference.drawing.bubble.inset = 4
 preference.drawing.title.color = UIColor.afSuperPink
 preference.drawing.title.font = UIFont.b4PinkCenter
 //preference.drawing.bubble.border.color = UIColor(red: 0.768, green: 0.843, blue: 0.937, alpha: 1.000)
 //preference.drawing.bubble.border.width = 1
 preference.drawing.bubble.isBubbleXPositionFix = true
 preference.drawing.bubble.bubbleFixXPosition = 40
 
 preference.drawing.arrow.tipCornerRadius = 5
 preference.drawing.arrow.isTipXPositionFix = true
 preference.drawing.arrow.tipFixXPosition = 24
 
 preference.drawing.background.color = UIColor.clear
 preference.animating.showDuration = 0.5
 preference.animating.dismissDuration = 0.5
 
 preference.drawing.message.color = UIColor(red: 0.200, green: 0.200, blue: 0.200, alpha: 1.000)
 preference.drawing.message.font = UIFont.systemFont(ofSize: 13, weight: .bold)
 
 passwordTextField.showToolTip(identifier: "", message: "Number alread registered", arrowPosition: .top, preferences: preference, delegate: nil)
 */
