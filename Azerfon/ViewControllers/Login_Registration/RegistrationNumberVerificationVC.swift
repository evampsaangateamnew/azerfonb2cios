//
//  RegistrationNumberVerificationVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class RegistrationNumberVerificationVC: BaseVC {
    //MARK: - Properties
    private var errorTooltip            : ZGTooltipView?
    var selectedType                    : Constants.AFResendType = .SignUp
    var registrationType                : Constants.RegistrationType = .registration
    
    //MARK: - IBOutlet
    @IBOutlet var languageContainerView : UIView!
    @IBOutlet var languageTitleLabel    : AFLabel!
    @IBOutlet var logoImageView         : UIImageView!
    @IBOutlet var userMSISDNTextField   : AFTextField!
    @IBOutlet var pinDescriptionLabel   : AFLabel!
    @IBOutlet var nextButton            : AFButton!

    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userMSISDNTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        /* Set Visible controlles accourding to type */
        if registrationType == .addAccount {
            languageContainerView.isHidden = true
        } else {
            languageContainerView.isHidden = false
        }
        
        loadViewContent()
        userMSISDNTextField.text = Constants.numberPrefix
        
        #if DEBUG
        userMSISDNTextField.text = Constants.numberPrefix + "776480544"
        #else
        userMSISDNTextField.text = Constants.numberPrefix
        #endif
    }
    
    //MARK: - IBAction
    
    @IBAction func changeLanguagePressed(_ sender:UIButton) {
        if let changelang :LanguageSelectionVC =  LanguageSelectionVC.fromNib() {
            
            changelang.setLanguageSelectionAlert(AFLanguageManager.userSelectedLanguage()) { (newSelectedLanguage) in
                
                if AFLanguageManager.userSelectedLanguage() != newSelectedLanguage {
                    // Setting user language
                    AFLanguageManager.setUserLanguage(selectedLanguage: newSelectedLanguage)
                    
                    // Reloading layout Text accourding to seletcted language
                    self.loadViewContent()
                }
            }
            
            self.presentPOPUP(changelang, animated: true, completion:  nil)
        }
    }
    
    @IBAction func nextPressed(_ sender: AFButton) {
        
        var msisdn : String = ""
        
        // MSISDN validation
        if let userMSISDNText = userMSISDNTextField.text?.formatedMSISDN(),
            userMSISDNText.isValidMSISDN() {
            
            if registrationType == .addAccount,
                AFUserSession.shared.loggedInUsers?.users?.contains(where: {($0.userInfo?.msisdn ?? "").isEqual(userMSISDNText, ignorCase: true)}) ?? false {
                self.showErrorAlertWithMessage(message: Localized("Error_AddNumber"))
                return
            } else {
                msisdn = userMSISDNText
            }
            
        } else {
            showErrorMessage(view: userMSISDNTextField, message: Localized("Message_EnterValidMSISDN"))
            return
        }
        
        _ = userMSISDNTextField.resignFirstResponder()
        
        /* API call for MSISDN verification. */
        verifyUserNumber(msisdn, Cause: selectedType)
        
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        languageTitleLabel.text = Localized("Info_Language")
        logoImageView.image = UIImage.imageFor(name: Localized("Img_NARLogo"))
        userMSISDNTextField.placeholder = Localized("Placeholder_MobileNumber")
        pinDescriptionLabel.text = Localized("Info_PinDescription")
        
        nextButton.setTitle(Localized("BtnTitle_NEXT"), for: .normal)
    }
    
    func showErrorMessage(view:UIView, message :String?) {
        
        errorTooltip?.dismiss(remove: true)
        
        errorTooltip = ZGTooltipView(direction: .bottomLeft, customView: ZGTooltipView.createLabel(text: message ?? "", tipTextFont: UIFont.b4PinkCenter, tipTextColor: UIColor.afSuperPink), originView: view, removeOnDismiss: true)
        errorTooltip?.backgroundColor = UIColor.afWhite
        errorTooltip?.isBubbleXPositionFix = true
        errorTooltip?.bubbleFixXPosition = 60
        errorTooltip?.displayTooltip()
        
    }
    
    //MARK: - API's Calls
    
    /**
     Call 'verifyUserNumber' API with to verify user MSISDN for signup and forgot.
     
     - parameter MSISDN: MSISDN of current user.
     - parameter cause: type of current screen
     
     - returns: void
     */
    func verifyUserNumber(_ msisdn : String , Cause cause : Constants.AFResendType){
        
        self.showActivityIndicator()
        _ = AFAPIClient.shared.verifyNumber(msisdn , Cause:cause ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    if let verifyOTP :RegistrationOTPVerificationVC = RegistrationOTPVerificationVC.instantiateViewControllerFromStoryboard() {
                        verifyOTP.selectedType = self.selectedType
                        verifyOTP.userMSISDN = msisdn
                        verifyOTP.registrationType = self.registrationType
                        self.navigationController?.pushViewController(verifyOTP, animated: true)
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - UITextFieldDelegate
extension RegistrationNumberVerificationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == userMSISDNTextField {
            
            /* Check prefix limit and disabling editing in prefix */
            let protectedRange = NSMakeRange(0, userMSISDNTextField.prefixLength)
            let intersection = NSIntersectionRange(protectedRange, range)
            
            if intersection.length > 0 ||
                range.location < (protectedRange.location + protectedRange.length) {
                
                return false
            }
            
            /* Restricting user to enter only number */
            let allowedCharactors = Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            /* Restriction for MSISDN length */
            return newLength <= (Constants.MSISDNLength + userMSISDNTextField.prefixLength)
            
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        /* Update tip possition if visisble */
        errorTooltip?.updatePosition()
        
        if textField == userMSISDNTextField {
            _ = textField.resignFirstResponder()
            nextPressed(AFButton())
            return true
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        /* Update tip possition if visisble */
        errorTooltip?.updatePosition()
    }
}
