//
//  TermsConditionsVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/13/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class TermsConditionsVC: BaseVC {
    //MARK: - Properties
    var termsConditionContent :String?
    
    //MARK: - IBOutlet
    @IBOutlet var termsConditionTextView    : UITextView!
    @IBOutlet var closeButton               : AFButton!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .afBlackTransparent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func closePressed(_ sender :AFButton) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_TermsConditions")
        closeButton.setTitle(Localized("BtnTitle_CLOSE"), for: .normal)
        
        termsConditionTextView.loadHTMLString(htmlString: termsConditionContent ?? "")
    }
    
    //MARK: - API's Calls
}

extension TermsConditionsVC {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.view {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
