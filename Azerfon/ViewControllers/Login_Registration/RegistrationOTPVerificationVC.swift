//
//  RegistrationOTPVerificationVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class RegistrationOTPVerificationVC: BaseVC {
    //MARK: - Properties
    private var errorTooltip            : ZGTooltipView?
    private var countDownTimer          : Timer?
    private var countDownDuration       : Int = Constants.PINVerifyBackTime
    
    var selectedType                    : Constants.AFResendType = .SignUp
    var userMSISDN                      : String = ""
    var registrationType                : Constants.RegistrationType = .registration
    var canGoBack                       : Bool = false
    
    //MARK: - IBOutlet
    @IBOutlet var languageContainerView : UIView!
    @IBOutlet var languageTitleLabel    : AFLabel!
    @IBOutlet var logoImageView         : UIImageView!
    @IBOutlet var otpTextField          : AFTextField!
    @IBOutlet var resentButton          : AFButton!
    @IBOutlet var verifyButton          : AFButton!
    @IBOutlet var backInfoLabel         : AFLabel!
    
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        otpTextField.delegate = self
        backButton?.isEnabled = false
        backInfoLabel.isHidden = false
        countDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(animatebackButtonEnablingCountDown), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(self.canGoBack)
        
        /* Set Visible controlles accourding to type */
        if registrationType == .addAccount {
            languageContainerView.isHidden = true
        } else {
            languageContainerView.isHidden = false
        }
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    
    @IBAction func changeLanguagePressed(_ sender:UIButton) {
        if let changelang :LanguageSelectionVC =  LanguageSelectionVC.fromNib() {
            
            changelang.setLanguageSelectionAlert(AFLanguageManager.userSelectedLanguage()) { (newSelectedLanguage) in
                
                if AFLanguageManager.userSelectedLanguage() != newSelectedLanguage {
                    // Setting user language
                    AFLanguageManager.setUserLanguage(selectedLanguage: newSelectedLanguage)
                    
                    // Reloading layout Text accourding to seletcted language
                    self.loadViewContent()
                }
            }
            
            self.presentPOPUP(changelang, animated: true, completion:  nil)
        }
    }
    
    
    @IBAction func verifyPressed(_ sender: AFButton) {
        
        var otpValue : String = ""
        // OTP validation
        if let otpText = otpTextField.text,
            otpText.trimmWhiteSpace.count == Constants.OTPLength {
            
            otpValue = otpText.trimmWhiteSpace
            
        } else {
            showErrorMessage(view: otpTextField, message: Localized("Message_InvalidOTP"))
            return
        }
        
        _ = otpTextField.resignFirstResponder()
        
        // API call for user authentication
        verifyOTP(userMSISDN, Cause: selectedType, Pin: otpValue)
        
    }
    
    @IBAction func resendOTPPressed(_ sender: AFButton) {
        _ = otpTextField.resignFirstResponder()
        otpTextField.text = ""
        
        self.resendOTP(userMSISDN, Cause: selectedType)
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        languageTitleLabel.text = Localized("Info_Language")
        logoImageView.image = UIImage.imageFor(name: Localized("Img_NARLogo"))
        otpTextField.placeholder = Localized("Placeholder_OTP")
        verifyButton.setTitle(Localized("BtnTitle_VERIFY"), for: .normal)
        
        
        let forgotButtonTitle = String(format: Localized("BtnTitle_ResendOTP"), Localized("BtnTitle_RESEND"))
            .createAttributedString(mainStringColor: UIColor.afLightPink,
                                    mainStringFont: UIFont.b4LightPinkCenter,
                                    highlightedStringColor: UIColor.afWhite,
                                    highlightedStringFont: UIFont.h6LightGrey,
                                    highlighStrings: [Localized("BtnTitle_RESEND")])
        
        resentButton.setAttributedTitle(forgotButtonTitle, for: .normal)
        
    }
    
    func showErrorMessage(view:UIView, message :String?) {
        
        errorTooltip?.dismiss(remove: true)
        
        errorTooltip = ZGTooltipView(direction: .bottomLeft, customView: ZGTooltipView.createLabel(text: message ?? "", tipTextFont: UIFont.b4PinkCenter, tipTextColor: UIColor.afSuperPink), originView: view, removeOnDismiss: true)
        errorTooltip?.backgroundColor = UIColor.afWhite
        errorTooltip?.isBubbleXPositionFix = true
        errorTooltip?.bubbleFixXPosition = 60
        errorTooltip?.displayTooltip()
        
    }
    /**
     Animate back button Enabling CountDown.
     
     - returns: void
     */
    @objc func animatebackButtonEnablingCountDown() {
        
        if countDownDuration < 0 {
            countDownTimer?.invalidate()
            countDownTimer = nil
            self.backButton?.alpha = 0.5
            UIView.animate(withDuration: 0.3) {
                self.canGoBack = true
                self.interactivePop(true)
                /* Enable Back button */
                self.backButton?.isEnabled = true
                self.backInfoLabel.isHidden = true
                self.backButton?.alpha = 1
            }
            
            
        } else {
            self.backInfoLabel.text = String(format: Localized("Info_EnableBackButton"), "\(countDownDuration)")
        }
        
        countDownDuration -= 1
    }
    
    //MARK: - API's Calls
    
    /**
     Call 'verifyOTP' API with to verify OTP for signup and forgot.
     
     - parameter MSISDN: MSISDN of current user.
     - parameter cause: type of current screen
     - parameter Pin: pin to be verified.
     
     - returns: void
     */
    func verifyOTP(_ msisdn : String , Cause cause : Constants.AFResendType, Pin pin :String) {
        
        self.showActivityIndicator()
        _ = AFAPIClient.shared.verifyOTP(msisdn, Cause: cause, Pin: pin,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue ,
                    let termsContentObj = Mapper<RegistrationVerifyOTPModel>().map(JSONObject: resultData) {
                    
                    if let setPasswordVC :RegistrationSetPasswordVC = RegistrationSetPasswordVC.instantiateViewControllerFromStoryboard() {
                        setPasswordVC.selectedType = self.selectedType
                        setPasswordVC.userMSISDN = msisdn
                        setPasswordVC.tempOTP = pin
                        setPasswordVC.termsContent = termsContentObj.content ?? ""
                        setPasswordVC.registrationType = self.registrationType
                        self.navigationController?.pushViewController(setPasswordVC, animated: true)
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.otpTextField.text = ""
        })
    }
    
    /**
     Call 'verifyOTP' API with to verify OTP for signup and forgot.
     
     - parameter MSISDN: MSISDN of current user.
     - parameter cause: type of current screen
     
     - returns: void
     */
    func resendOTP(_ msisdn : String , Cause cause : Constants.AFResendType){
        
        self.showActivityIndicator()
        _ = AFAPIClient.shared.resendPin(msisdn, Cause: cause,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    // self.showSuccessAlertWithMessage(message: resultDesc)
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - UITextFieldDelegate
extension RegistrationOTPVerificationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == otpTextField {
            
            return newLength <= Constants.OTPLength /* Restriction for OTP length */
            
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        /* Update tip possition if visisble */
        errorTooltip?.updatePosition()
        
        if textField == otpTextField {
            _ = textField.resignFirstResponder()
            verifyPressed(AFButton())
            return true
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        /* Update tip possition if visisble */
        errorTooltip?.updatePosition()
    }
}
