//
//  RegistrationSetPasswordVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper

class RegistrationSetPasswordVC: BaseVC {
    //MARK: - Properties
    private var errorTooltip                : ZGTooltipView?
    private var isTermsConditionAccepted    : Bool = false
    var selectedType                        : Constants.AFResendType = .SignUp
    var registrationType                    : Constants.RegistrationType = .registration
    var userMSISDN                          : String = ""
    var tempOTP                             : String = ""
    var termsContent                        : String = ""
    
    
    //MARK: - IBOutlet
    @IBOutlet var languageContainerView     : UIView!
    @IBOutlet var languageTitleLabel        : AFLabel!
    @IBOutlet var logoImageView             : UIImageView!
    @IBOutlet var passwordTextField         : AFTextField!
    @IBOutlet var confirmPasswordTextField  : AFTextField!
    @IBOutlet var infoButton                : UIButton!
    @IBOutlet var termsCheckBoxButton       : UIButton!
    @IBOutlet var termsConditionsButton     : AFButton!
    @IBOutlet var signUpButton              : AFButton!
    
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        if selectedType == .SignUp {
            termsCheckBoxButton.isHidden = false
            termsConditionsButton.isHidden = false
        } else {
            termsCheckBoxButton.isHidden = true
            termsConditionsButton.isHidden = true
        }
        
        /* layout check box */
        updateTermConditionCheckboxLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        /* Set Visible controlles accourding to type */
        if registrationType == .addAccount {
            languageContainerView.isHidden = true
        } else {
            languageContainerView.isHidden = false
        }
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    
    @IBAction func changeLanguagePressed(_ sender:UIButton) {
        if let changelang :LanguageSelectionVC =  LanguageSelectionVC.fromNib() {
            
            changelang.setLanguageSelectionAlert(AFLanguageManager.userSelectedLanguage()) { (newSelectedLanguage) in
                
                if AFLanguageManager.userSelectedLanguage() != newSelectedLanguage {
                    // Setting user language
                    AFLanguageManager.setUserLanguage(selectedLanguage: newSelectedLanguage)
                    
                    // Reloading layout Text accourding to seletcted language
                    self.loadViewContent()
                }
            }
            
            self.presentPOPUP(changelang, animated: true, completion:  nil)
        }
    }
    
    @IBAction func infoPressed(_ sender: UIButton) {
        showErrorMessage(view: sender, message: "Must be at least 6 characters", isInfoButton: true)
    }
    
    @IBAction func termsCheckboxPressed(_ sender: UIButton) {
        /* */
        isTermsConditionAccepted = !isTermsConditionAccepted
        
        updateTermConditionCheckboxLayout()
    }
    
    @IBAction func termsConditionPressed(_ sender: UIButton) {
        if let termsConditionsVC :TermsConditionsVC =  TermsConditionsVC.instantiateViewControllerFromStoryboard() {
            termsConditionsVC.termsConditionContent = termsContent
            self.presentPOPUP(termsConditionsVC, animated: true)
        }
    }
    
    @IBAction func signUpPressed(_ sender: AFButton) {
        
        var password : String = ""
        var confirmPassword : String = ""
        
        // Password validation
        if let passwordText = passwordTextField.text {
            
            let passwordStrength:Constants.AFPasswordStrength = AFUtilities.determinePasswordStrength(Text: passwordText)
            
            switch passwordStrength {
                
            case .didNotMatchCriteria:
                
                showErrorMessage(view: passwordTextField, message: Localized("Message_InvalidPasswordComplexity"))
                return
            case .Week, .Medium, .Strong:
                password = passwordText
            }
            
        } else {
            showErrorMessage(view: passwordTextField, message: Localized("Message_InvalidPassword"))
            return
        }
        
        // Confirm Password validation
        if let confirmPasswordText = confirmPasswordTextField.text,
            confirmPasswordText == password {
            
            confirmPassword = confirmPasswordText
            
        } else {
            showErrorMessage(view: passwordTextField, message: Localized("Message_InvalidConfirmPassword"))
            return
        }
        
        _ = passwordTextField.resignFirstResponder()
        _ = confirmPasswordTextField.resignFirstResponder()
        
        if selectedType == .SignUp {
            
            if isTermsConditionAccepted == false {
                self.showErrorAlertWithMessage(message: Localized("Message_acceptTerms_Condition"))
            } else {
                saveCustomer(userMSISDN, Password: password, ConfirmPassword: confirmPassword, Temp: tempOTP, isTermsAccepted: isTermsConditionAccepted)
            }
        } else {
            forgotPassword(userMSISDN, Password: password, ConfirmPassword: confirmPassword, Temp: tempOTP)
        }
        
        
        
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        languageTitleLabel.text = Localized("Info_Language")
        logoImageView.image = UIImage.imageFor(name: Localized("Img_NARLogo"))
        passwordTextField.placeholder = Localized("Placeholder_Password")
        confirmPasswordTextField.placeholder = Localized("Placeholder_ConfirmPassword")
        
        signUpButton.setTitle(selectedType == .SignUp ? Localized("BtnTitle_SignUp") : Localized("BtnTitle_UPDATE"), for: .normal)
        
        let termsButtonTitle = String(format: Localized("BtnTitle_IAccept"), Localized("BtnTitle_TermsConditions"))
            .createAttributedString(mainStringColor: UIColor.afLightPink,
                                    mainStringFont: UIFont.b4LightPinkCenter,
                                    highlightedStringColor: UIColor.afWhite,
                                    highlightedStringFont: UIFont.h6LightGrey,
                                    highlighStrings: [Localized("BtnTitle_TermsConditions")])
        
        termsConditionsButton.setAttributedTitle(termsButtonTitle, for: .normal)
    }
    
    func showErrorMessage(view:UIView, message :String?, isInfoButton :Bool = false) {
        
        errorTooltip?.dismiss(remove: true)
        
        errorTooltip = ZGTooltipView(direction: isInfoButton ? .topRight : .bottomLeft , customView: ZGTooltipView.createLabel(text: message ?? "", tipTextFont: UIFont.b4PinkCenter, tipTextColor: UIColor.afSuperPink), originView: view, removeOnDismiss: true)
        
        if isInfoButton == false {
            errorTooltip?.isBubbleXPositionFix = true
            errorTooltip?.bubbleFixXPosition = 60
        } else {
            errorTooltip?.isBubbleXPositionFix = false
            errorTooltip?.bubbleFixXPosition = 0
        }
        
        errorTooltip?.backgroundColor = UIColor.afWhite
        errorTooltip?.displayTooltip()
        
    }
    
    func updateTermConditionCheckboxLayout() {
        if isTermsConditionAccepted {
            termsCheckBoxButton.setImage(UIImage(named: "checkbox_checked"), for: .normal)
        } else {
            termsCheckBoxButton.setImage(UIImage(named: "checkbox_unchecked"), for: .normal)
        }
    }
    
    func redirectToDashboardScreen() {
        
        if let count = self.navigationController?.viewControllers.count,
            count > 1,
            let firstVC = self.navigationController?.viewControllers.first,
            let mainViewController :TabbarController = TabbarController.instantiateViewControllerFromStoryboard() ,
            let drawerViewController :SideMenuVC = SideMenuVC.instantiateViewControllerFromStoryboard() {
            
            mainViewController.isLoggedInNow = true
            let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
            drawerController.mainViewController = mainViewController
            drawerController.drawerViewController = drawerViewController
            
            self.navigationController?.viewControllers = [firstVC, drawerController]
        }
    }
    
    //MARK: - API's Calls
    
    /**
     Call 'saveCustomer' API with to register user.
     
     - returns: void
     */
    func saveCustomer(_ msisdn: String, Password password :String, ConfirmPassword confirmPassword :String, Temp temp :String, isTermsAccepted :Bool){
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.saveCustomer(msisdn, Password: password, ConfirmPassword: confirmPassword, Temp: temp, isTermsAccepted: isTermsAccepted, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "sign_up", contentType:"registered" , successStatus:"0" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let authanticateUser = Mapper<LoginModel>().map(JSONObject: resultData) {
                    
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "sign_up", contentType:"registered" , successStatus:"1" )
                    
                    /* Set user Promo message text */
                    AFUserSession.shared.promoMessage = authanticateUser.promoMessage?.removeNullValues()
                    
                    if self.registrationType == .addAccount {
                        /* Save logged in user information */
                        AFUserInfoUtilities.addNewUser(newUserMSISDN: msisdn, loginInfo: authanticateUser)
                    } else {
                        /* Save logged in user information */
                        AFUserInfoUtilities.saveLoginInfo(loggedInUserMSISDN: msisdn, loginInfo: authanticateUser)
                    }
                    
                    /* Redirect user to home screen */
                    self.redirectToDashboardScreen()
                    
                } else {
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "sign_up", contentType:"registered" , successStatus:"0" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    /**
     Call 'forgotPassword' API with to update user password.
     
     - returns: void
     */
    func forgotPassword(_ msisdn: String, Password password :String, ConfirmPassword confirmPassword :String, Temp temp :String){
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.forgotPassword(msisdn, Password: password, ConfirmPassword: confirmPassword, Temp: temp, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "forgot", contentType:"recovered" , successStatus:"0" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let authanticateUser = Mapper<LoginModel>().map(JSONObject: resultData) {
                    
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "forgot", contentType:"recovered" , successStatus:"1" )
                    
                    if self.registrationType == .addAccount {
                        /* Save logged in user information */
                        AFUserInfoUtilities.addNewUser(newUserMSISDN: msisdn, loginInfo: authanticateUser)
                    } else {
                        /* Save logged in user information */
                        AFUserInfoUtilities.saveLoginInfo(loggedInUserMSISDN: msisdn, loginInfo: authanticateUser)
                    }
                    
                    /* Redirect user to home screen */
                    self.redirectToDashboardScreen()
                    
                } else {
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "forgot", contentType:"recovered" , successStatus:"0" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK:- UITextFieldDelegate
extension RegistrationSetPasswordVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == passwordTextField || textField == confirmPasswordTextField {
            
            return newLength <= Constants.maxPasswordLength /* Restriction for password length */
            
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        /* Update tip possition if visisble */
        errorTooltip?.updatePosition()

        if textField == passwordTextField {
            confirmPasswordTextField.becomeFirstResponder()
            return true
        } else if textField == confirmPasswordTextField {
            _ = textField.resignFirstResponder()
            signUpPressed(AFButton())
            return true
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        /* Update tip possition if visisble */
        errorTooltip?.updatePosition()
    }
}

