//
//  InAppSurveyVC.swift
//  Azerfon
//
//  Created by Touseef Sarwar on 30/08/2021.
//  Copyright © 2021 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper



enum SurveyScreenName: String{
    case dashboard = "dashboard"
    case topup = "topup"
    case tariff = "tariff"
    case tariff_subscribed = "tariff_subscribed"
    case bundle = "bundle"
    case bundle_subscribed = "bundle_subscribed"
    case getLoan = "get_loan"
    case transferMoney = "transfer_money"
    case fnf = "fnf"
}

class InAppSurveyVC: BaseVC {
    
    
        
    //MARK: IBOutlets
    @IBOutlet weak var closeBtn             : UIButton!
    @IBOutlet weak var balanceLbl           : UILabel!
    @IBOutlet weak var titleRedLbl          : UILabel!
    @IBOutlet weak var messageLbl           : UILabel!
    
    @IBOutlet weak var btn1                 : UIButton!
    @IBOutlet weak var btn2                 : UIButton!
    @IBOutlet weak var btn3                 : UIButton!
    @IBOutlet weak var btn4                 : UIButton!
    @IBOutlet weak var btn5                 : UIButton!
    
    @IBOutlet weak var ratingStatusLbl      : UILabel!
    @IBOutlet weak var commentTxt           : UITextField!

    //StackViews...
    @IBOutlet weak var topUpSV              : UIStackView!
    @IBOutlet weak var balanceSV             : UIStackView!
    @IBOutlet weak var starsSV              : UIStackView!
    @IBOutlet weak var answerSV             : UIStackView!
    
    @IBOutlet weak var submitButton         : AFButton!
    
    //Variables
    var callBack : (() -> (Void))?
    var surveyScreen = SurveyScreenName.dashboard.rawValue
    
    
    var showTopUpStackView: Bool = false
    var hideBalanceStackView: Bool = true
    var isSavedSurvey: Bool = false
    var attributedMessage: NSAttributedString?
    var attributedBalance: NSAttributedString?
    var confirmationText: String?
    var toptupTitleText: String?
    
    var currentSurvay   = InAppSurvey.init()
    var survayQuestion  : SurvayQuestions?
    var selectedAnswer  : SurvayAnswer?
    var survayComment   : String?
    
    
    //offeringId
    var offeringId: String? = nil
    var offeringType: String? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        survayQuestion = currentSurvay.questions?[0]
        self.answerSV.isHidden = true
        setLayout()
        if let answer = survayQuestion?.answers?.firstIndex(where: { $0.answerId ==  selectedAnswer?.answerId }) {
            updateStarsFor(selectedIndex: answer)
            self.commentTxt.text = survayComment ?? ""
        }
    }
    
    
    func setLayout() {
        self.view.backgroundColor = UIColor.afBlackTransparent
        self.btn1.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
        self.btn2.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
        self.btn3.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
        self.btn4.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
        self.btn5.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
        
        self.closeBtn.isHidden = false
        self.balanceSV.isHidden = false
        self.starsSV.isHidden = false
        self.answerSV.isHidden = true
        self.submitButton.setTitle(Localized("Btn_Title_Submit"), for: .normal)
        
        if self.showTopUpStackView == false {
            topUpSV.isHidden = true
        } else {
            
            topUpSV.isHidden = false
            self.messageLbl.attributedText = attributedMessage
            if !hideBalanceStackView{
                self.balanceLbl.attributedText = attributedBalance
                balanceSV.isHidden = false
            } else {
                balanceSV.isHidden = true
            }
            self.messageLbl.attributedText = self.attributedMessage
        }
        
        if AFLanguageManager.userSelectedLanguage() == .english{
            self.titleRedLbl.text = survayQuestion?.questionTextEN
            commentTxt.placeholder = currentSurvay.commentsTitleEn
        }else if AFLanguageManager.userSelectedLanguage() == .azeri{
            self.titleRedLbl.text = survayQuestion?.questionTextAZ
            commentTxt.placeholder = currentSurvay.commentsTitleAz
        }else{
            self.titleRedLbl.text = survayQuestion?.questionTextRU
            commentTxt.placeholder = currentSurvay.commentsTitleRu
        }
        
        //Hide comment TextFeild later on after discussion.
        if currentSurvay.commentEnable?.contains("enable") ?? false || currentSurvay.commentEnable ?? "" == "enable"{
            commentTxt.isHidden = false
        }else{
            commentTxt.isHidden = false
        }
    }

    
    func updateStarsFor(selectedIndex: Int) {
        
        if selectedIndex == 0 {
            self.btn1.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn2.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
            self.btn3.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
            self.btn4.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
            self.btn5.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
        } else if selectedIndex == 1 {
            self.btn1.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn2.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn3.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
            self.btn4.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
            self.btn5.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
        } else if selectedIndex == 2 {
            self.btn1.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn2.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn3.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn4.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
            self.btn5.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
        } else if selectedIndex == 3 {
            self.btn1.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn2.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn3.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn4.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn5.setImage(#imageLiteral(resourceName: "RedLineStar"), for: .normal)
            
        } else if selectedIndex == 4 {
            self.btn1.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn2.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn3.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn4.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
            self.btn5.setImage(#imageLiteral(resourceName: "RedFilledStar"), for: .normal)
        }
    }
    
}

//MARK: IBActions....

extension InAppSurveyVC{
    //close
    @IBAction func closeTapped(_ sender: UIButton){
        self.dismiss(animated: true) {
            if self.isSavedSurvey{
                self.callBack?()
            }
        }
    }
    
    
    // Rating action
    @IBAction func rateStarsTapped(_ sender: UIButton){
        if selectedAnswer != nil,
           survayQuestion != nil,
           survayQuestion?.answers?[sender.tag].answerId == selectedAnswer?.answerId  {
            return
        }
        
        if answerSV.isHidden == true{
            UIView.animate(withDuration: 0.3) {
                self.answerSV.isHidden = false
            }
        }
       
        selectedAnswer = survayQuestion?.answers?[sender.tag]
        if AFLanguageManager.userSelectedLanguage() == .english {
            ratingStatusLbl.text = selectedAnswer?.answerTextEN
        } else if AFLanguageManager.userSelectedLanguage() == .russian {
            ratingStatusLbl.text = selectedAnswer?.answerTextRU
        } else {
            ratingStatusLbl.text = selectedAnswer?.answerTextAZ
        }
        updateStarsFor(selectedIndex: sender.tag)
    }
    
    //Submit survey..
    @IBAction func submitTapped(_ sender: UIButton){
       saveSurveys()
    }
}


//MARK: API call's

extension InAppSurveyVC{
    
    
    func saveSurveys() {
     
        self.showActivityIndicator()
        _ = AFAPIClient.shared.saveSurvey(
            //Parameters to send
            comment: commentTxt.text ?? "",
            answerId: self.selectedAnswer?.answerId  ?? "",
            questionId: self.currentSurvay.questions?.first?.questionId ?? "",
            offeringIdSurvey: offeringId ?? "",
            offeringTypeSurvey: offeringType  ?? "",
            surveyId: self.currentSurvay.survayId ?? "",
            
            { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue ,
                    let homePageResponse = Mapper<InAppSurveyMapper>().map(JSONObject:resultData) {
                    AFUserSession.shared.appSurvays = homePageResponse
                    self.topUpSV.isHidden = true
                    self.answerSV.isHidden = true
                    self.starsSV.isHidden = true
                    if AFLanguageManager.userSelectedLanguage() == .english {
                        self.titleRedLbl.text = self.currentSurvay.onTransactionCompleteEn ?? resultDesc
                    } else if AFLanguageManager.userSelectedLanguage() == .russian {
                        self.titleRedLbl.text = self.currentSurvay.onTransactionCompleteRu ?? resultDesc
                    } else {
                        self.titleRedLbl.text = self.currentSurvay.onTransactionCompleteAz ?? resultDesc
                    }
                    UIView.animate(withDuration: 0.25) {
                    }
                  
                    self.isSavedSurvey = true
                    
                    if self.currentSurvay.screenName == SurveyScreenName.dashboard.rawValue {
                        UserDefaults.standard.set(0, forKey: "DashboardScreenViews")
                    } else if self.surveyScreen == SurveyScreenName.fnf.rawValue {
                        UserDefaults.standard.set(0, forKey: "FNFScreenViews")
                    } else if self.surveyScreen == SurveyScreenName.getLoan.rawValue {
                        UserDefaults.standard.set(0, forKey: "GetLoanScreenViews")
                    } else if self.surveyScreen == SurveyScreenName.transferMoney.rawValue {
                        UserDefaults.standard.set(0, forKey: "TransferMoneyScreenViews")
                    } else if self.surveyScreen == SurveyScreenName.topup.rawValue {
                        UserDefaults.standard.set(0, forKey: "TopUpScreenViews")
                    }
                    UserDefaults.standard.synchronize()
                    
                } else {
                    // Show error alert to user
//                    self.showErrorAlertWithMessage(message: resultDesc)
                    self.showErrorAlertWithMessage(message: resultDesc) {
                        self.dismiss(animated: true)
                    }
                }
            }
        })
    }
}
