//
//  TariffVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/20/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
import UPCarouselFlowLayout


class TariffVC: BaseVC {
    //MARK: - Properties
    var titleValue                  : String = Localized("Title_Tariffs")
    var selectedTabIndex            : Int = 0
    var tariffsResponseList         : TariffResponseModel?
    var filteredTariffsList         : [TariffResponseList]?
    var isUserSearch                : Bool = true
    var canUserGoBack               : Bool = false
    var currentMSISDNTariffInfo     : String?
    var currentLanguageTariffInfo   : Constants.AFLanguage?
    
    var redirectToTariff :(isRedireted :Bool, offeringID :String?) = (isRedireted :false, offeringID :nil)
    
    // USED to toggle
    var selectedSectionViewType : Constants.AFSectionType = .bonuses
    let cardLayout  : UPCarouselFlowLayout = UPCarouselFlowLayout()
    
    var collectenViewCurrentIndex:Int = 0
    
    //MARK: - IBOutlet
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var tariffTypesCollectionView: UICollectionView!
    @IBOutlet var tariffContentCollectionView: UICollectionView!
    @IBOutlet var leftArrowButton      : UIButton!
    @IBOutlet var rightArrowButton      : UIButton!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tariffTypesCollectionView.contentInset = UIEdgeInsets(top: 0, left: 26, bottom: 0, right: 26)
        
        tariffTypesCollectionView.delegate = self
        tariffTypesCollectionView.dataSource = self
        tariffTypesCollectionView.allowsMultipleSelection = false
        
        NormalTypeCell.registerReusableCell(with: tariffTypesCollectionView)
        
        tariffContentCollectionView.delegate = self
        tariffContentCollectionView.dataSource = self
        
        TariffCell.registerReusableCell(with: tariffContentCollectionView)
        
        cardLayout.itemSize = CGSize(width: (UIScreen.main.bounds.width - 54) , height:  (UIScreen.main.bounds.height * 0.70))
        cardLayout.scrollDirection = .horizontal
        cardLayout.sideItemScale = 0.8
        cardLayout.sideItemAlpha = 1
        cardLayout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 22)
        tariffContentCollectionView.collectionViewLayout = cardLayout
        
        layoutSearchTextField(false, reloadData: false)
        
        let searchSepratorView = UIView()
        searchSepratorView.backgroundColor = UIColor.afWhite
        self.searchTextField.addSubview(searchSepratorView)
        
        searchSepratorView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.searchTextField.snp.left)
            maker.right.equalTo(self.searchTextField.snp.right)
            maker.bottom.equalTo(self.searchTextField.snp.bottom)
            maker.height.equalTo(1)
        }
        
        self.searchTextField.delegate = self
        self.searchTextField.attributedPlaceholder = NSAttributedString(string: Localized("PlaceHolder_Search"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.afWhite])
        
        if canUserGoBack {
            self.menuButton?.setImage(UIImage(named: "back"), for: .normal)
        } else {
            self.menuButton?.setImage(UIImage(named: "menu_icon"), for: .normal)
        }
        
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "TariffScreenViews")+1, forKey: "TariffScreenViews")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if canUserGoBack {
            self.interactivePop(true)
        } else {
            self.interactivePop(false)
        }
        
        //getting services from session and adding into local array and reloading data
        AFUserSession.shared.appMenu?.menuHorizontal?.forEach({ (aHorizontalMenu) in
            if aHorizontalMenu.identifier == Constants.HorizontalMenus.tariffs.rawValue {
                self.titleValue = aHorizontalMenu.title.isBlank ? Localized("Title_Tariffs") : aHorizontalMenu.title
            }
        })
        
        loadViewContent()
        
        
        /* Clear user tariff info from current objects if he switch to any other number */
        if (currentMSISDNTariffInfo?.isBlank == true ||
            (currentMSISDNTariffInfo?.isEqual(AFUserSession.shared.msisdn, ignorCase: true) ?? false) == false) ||
            (currentLanguageTariffInfo != AFLanguageManager.userSelectedLanguage()){
            
            tariffsResponseList = nil
            filteredTariffsList = nil
            
            currentMSISDNTariffInfo = AFUserSession.shared.msisdn
            currentLanguageTariffInfo = AFLanguageManager.userSelectedLanguage()
            self.layoutSearchTextField(false, reloadData: false)
        }
        
        /* Call API to get Tariff inforamtion */
        self.loadTariffDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async {
            self.cardLayout.itemSize = CGSize(width: self.tariffContentCollectionView.bounds.width - 54 ,
                                              height:  self.tariffContentCollectionView.bounds.height)
        }
    }
    
    //MARK: - IBAction
    override func menuPressed(_ sender: AnyObject) {
        if canUserGoBack {
            self.backPressed(sender)
        } else {
            super.menuPressed(sender)
        }
    }
    
    @IBAction func searchPressed(_ sender: UIButton) {
        layoutSearchTextField(isUserSearch ? false : true)
    }
    
    @IBAction func subscribePressed(_ sender: AFButton) {
        if sender.tag >= 0 ,
            let selectedTariff = self.filteredTariffsList?[self.selectedTabIndex].items?[sender.tag] {
            
            var changeMessageString = Localized("Message_ChangeTariff")
            if let selectedTariffMessage = AFUserSession.shared.predefineData?.tariffMigrationPrices?.filter({$0.key?.isEqual(selectedTariff.header?.offeringId, ignorCase: true) ?? false}).first?.value,
                selectedTariffMessage.isBlank == false {
                
                changeMessageString = selectedTariffMessage
                
            } else if let changeTariffDefaultMessage = AFUserSession.shared.predefineData?.tariffMigrationPrices?.filter({$0.key?.isEqual("default", ignorCase: true) ?? false}).first?.value,
                changeTariffDefaultMessage.isBlank == false {
                
                changeMessageString = changeTariffDefaultMessage
                
            }
            
            /* SHOW Alert and perform operation accordinglly */
            self.showConfirmationAlert(message: changeMessageString, {
                
                /* Make change tariff API call */
                self.changetTariff(offeringId: selectedTariff.header?.offeringId ?? "",
                                   offerName: selectedTariff.header?.name ?? "",
                                   subscribableValue: selectedTariff.subscribable ?? "")
            })
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
    }
    
    @IBAction func moveToOffer(_ sender: AFButton) {
        
        if sender == rightArrowButton {
            if collectenViewCurrentIndex <= (self.filteredTariffsList?[self.selectedTabIndex].items?.count ?? 0) - 2 {
                collectenViewCurrentIndex  = collectenViewCurrentIndex + 1
                self.tariffContentCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredHorizontally, animated: true)
                
            }
        } else {
            if collectenViewCurrentIndex >= 1 {
                collectenViewCurrentIndex = collectenViewCurrentIndex - 1
                self.tariffContentCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
        if (self.filteredTariffsList?[self.selectedTabIndex].items?.count ?? 0) > 1 {
            if collectenViewCurrentIndex == 0 {
                self.rightArrowButton.isHidden = false
                self.leftArrowButton.isHidden = true
            } else if collectenViewCurrentIndex == (self.filteredTariffsList?[self.selectedTabIndex].items?.count ?? 0) - 1 {
                self.rightArrowButton.isHidden = true
                self.leftArrowButton.isHidden = false
            } else {
                self.rightArrowButton.isHidden = false
                self.leftArrowButton.isHidden = false
            }
        } else {
            self.rightArrowButton.isHidden = true
            self.leftArrowButton.isHidden = true
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = self.titleValue
    }
    
    func layoutSearchTextField(_ showTextField :Bool, reloadData :Bool = true) {
        
        isUserSearch = showTextField
        
        if showTextField {
            titleLabel?.isHidden = true
            searchTextField.isHidden = false
            
            searchTextField.text = ""
            _ = searchTextField.becomeFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search_cross") , for: .normal)
            
        } else {
            titleLabel?.isHidden = false
            searchTextField.isHidden = true
            
            searchTextField.text = ""
            _ = searchTextField.resignFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search") , for: .normal)
        }
        
        filteredTariffsList = tariffsResponseList?.tariffResponseList
        
        if reloadData {
            reloadTariffDetailsAndMenuCollectionView(tabIndex: self.selectedTabIndex, tariffItemIndex: 0 )
        }
    }
    
    func loadTariffDetails() {
        
        if self.tariffsResponseList == nil {
            /* Loading initial data from UserDefaults */
            
            if let tariffDetails :TariffResponseModel = TariffResponseModel.loadFromUserDefaults(key: APIsType.tariffDetails.selectedLocalizedAPIKey()) {
                
                self.tariffsResponseList = tariffDetails
                self.filteredTariffsList = tariffDetails.tariffResponseList
                
                if Connectivity.isConnectedToInternet == true &&
                    self.redirectToTariff.isRedireted {
                    
                    self.getTariffsDetail(showIndicator: true)
                    
                } else {
                    
                    self.filterAndLoadSelectedTariff()
                    self.getTariffsDetail(showIndicator: false)
                }
                
            } else {
                self.getTariffsDetail(showIndicator: true)
            }
        } else {
            self.filterAndLoadSelectedTariff()
        }
    }
    
    func filterAndLoadSelectedTariff() {
        
        if redirectToTariff.isRedireted {
            
            layoutSearchTextField(false, reloadData: false)
            
            let selectedTariffIndexed = self.tariffsResponseList?.filterOfferByOfferingID(offeringID: self.redirectToTariff.offeringID)
            
            if let groupIndex = selectedTariffIndexed?.tariffGroupIndex,
                let tariffIndex = selectedTariffIndexed?.tariffItemIndex {
                
                self.selectedTabIndex = groupIndex
                reloadTariffDetailsAndMenuCollectionView(tabIndex: groupIndex, tariffItemIndex: tariffIndex )
                
            } else {
                
                reloadTariffDetailsAndMenuCollectionView(tabIndex: self.selectedTabIndex, tariffItemIndex: 0 )
            }
            /* Reset redirection */
            redirectToTariff = (false, nil)
        } else {
            self.reloadTariffDetailsAndMenuCollectionView(tabIndex: self.selectedTabIndex, tariffItemIndex: 0 )
        }
    }
    
    func reloadTariffDetailsAndMenuCollectionView(tabIndex :Int, tariffItemIndex :Int) {
        /* Reload top groups bar */
        self.leftArrowButton.isHidden = true
        self.rightArrowButton.isHidden = true
        if (self.filteredTariffsList?.count ?? 0) > 0 {
            
            self.tariffTypesCollectionView.reloadData()
            self.tariffTypesCollectionView.performBatchUpdates(nil) { (isCompleted) in
                if isCompleted {
                    self.tariffTypesCollectionView.selectItem(at: IndexPath(item: tabIndex, section: 0), animated: true, scrollPosition: .left)
                }
            }
            
        } else {
            self.tariffTypesCollectionView.reloadData()
        }
        
        
        /* Reload tariff items */
        if (self.filteredTariffsList?.count ?? 0) > 0 &&
            (self.filteredTariffsList?[self.selectedTabIndex].items?.count ?? 0) > 0 {
            if (self.filteredTariffsList?.count ?? 0) > 1 &&
                (self.filteredTariffsList?[self.selectedTabIndex].items?.count ?? 0) > 1  {
                self.rightArrowButton.isHidden = false
            }
            
            self.tariffContentCollectionView.hideDescriptionView()
            
            self.tariffContentCollectionView.reloadData()
            self.tariffContentCollectionView.performBatchUpdates(nil) { (isCompleted) in
                if isCompleted {
                    
                    self.tariffContentCollectionView.scrollToItem(at: IndexPath(item: tariffItemIndex, section: 0), at: .centeredHorizontally, animated: true)
                }
            }
            
        } else {
            self.tariffContentCollectionView.showDescriptionViewWithImage(description: Localized("Message_NoData"),
                                                                          descriptionColor: UIColor.afWhite)
            self.tariffContentCollectionView.reloadData()
        }
    }
    /**
     USED to toggle section.
     
     - returns: Void.
     */
    
    func openSelectedSection() {
        
        let visbleIndexPath = tariffContentCollectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = tariffContentCollectionView.cellForItem(at: aIndexPath) as? TariffCell {
                
                var openSectionIndex : Int? = 0
                
                if let aTariffObject = filteredTariffsList?[selectedTabIndex].items?[cell.tableView.tag] {
                    
                    let type = typeOfDataInSection(aTariffObject)
                    
                    if selectedSectionViewType == .bonuses && type.contains(.bonuses) {
                        
                        openSectionIndex = 0
                        
                    } else if selectedSectionViewType == .prices && type.contains(.prices) {
                        
                        openSectionIndex = type.index(of:.prices)
                        
                    } else if selectedSectionViewType == .detail && type.contains(.detail) {
                        
                        openSectionIndex = type.index(of:.detail)
                        
                    }
                }
                
                cell.tableView.closeAllSectionsExcept(openSectionIndex ?? 0, shouldCallDelegate: false)
                
                openSectionAt(index: openSectionIndex ?? 0, tableView: cell.tableView)
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : AFAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            
            // tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: .top, animated: true)
            
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
    
    //MARK: - API's Calls
    
    /**
     call to get tariff details.
     */
    func getTariffsDetail(showIndicator :Bool) {
        
        if showIndicator {
            self.showActivityIndicator()
        }
        
        /*API call to get data*/
        _ = AFAPIClient.shared.getTariffDetails({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if showIndicator {
                self.hideActivityIndicator()
            }
            
            if error != nil {
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue ,
                    var responseHandler = Mapper<TariffResponseModel>().map(JSONObject: resultData) {
                    
                    /* Remove special offer in case of specialTariffMenu is hiden or
                     special offers item count is less then equal to zero */
                    
                    if responseHandler.tariffResponseList?.contains(where:{ $0.isSpecial?.isTrue() ?? false}) ?? false ,
                        let specialTariffIndex = responseHandler.tariffResponseList?.index(where: { $0.isSpecial?.isTrue() ?? false}) {
                        
                        if (AFUserSession.shared.userInfo?.specialOffersTariffData?.specialTariffMenu?.isEqual("hide", ignorCase: true) ?? false)  ||
                            (responseHandler.tariffResponseList?[specialTariffIndex].items?.count ?? 0) <= 0{
                            responseHandler.tariffResponseList?.remove(at: specialTariffIndex)
                        }
                    }
                    
                    responseHandler.saveInUserDefaults(key: APIsType.tariffDetails.selectedLocalizedAPIKey())
                    
                    self.tariffsResponseList = responseHandler
                    self.filteredTariffsList = responseHandler.tariffResponseList
                    
                    self.layoutSearchTextField(false, reloadData: false)
                }
            }
            
            self.filterAndLoadSelectedTariff()
        })
    }
    
    fileprivate func checkAndShowInAppSurvey(offeringId: String, changeTariffResponse: String) {
        
        if let survey = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.tariff.rawValue}) {
            
            /*if  survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0, survey.visitLimit != "-1",
                survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "TariffScreenViews") {
                
                
                
            } else {
                // Show Error alert
                self.showSuccessAlertWithMessage(message: changeTariffResponse, {
                    /* Remove because tariff is migratied in more then 10 sec */
                    /* Call resume API if use migrate tariff sucessfully */
                    // self.getAppResume()
                })
            }*/
            if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                
                inAppSurveyVC.currentSurvay = survey
                inAppSurveyVC.showTopUpStackView = true
                //inAppSurveyVC.survayComment = userSurvey?.comments
                inAppSurveyVC.offeringId = ""
                inAppSurveyVC.offeringType = ""
                inAppSurveyVC.attributedMessage = changeTariffResponse.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h3GreyRight)
                inAppSurveyVC.callBack = {
//                        self.reDirectUserToSelectedScreens()
                }
                inAppSurveyVC.surveyScreen = SurveyScreenName.tariff.rawValue
                self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
            }
            
        }else{
            // Show Error alert
            self.showSuccessAlertWithMessage(message: changeTariffResponse, {
                /* Remove because tariff is migratied in more then 10 sec */
                /* Call resume API if use migrate tariff sucessfully */
                // self.getAppResume()
            })
        }
    }
    
    /// Call 'changetTariff' API .
    ///
    /// - parameter offeringId:     offerId of selected offer
    /// - parameter offerName:     offerName of selected offer
    /// - parameter subscribableValue:     subscribable value of selected offer
    ///
    /// - returns: Void
    func changetTariff(offeringId: String, offerName: String, subscribableValue: String) {
        
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.changetTariff(offeringId: offeringId, offerName: offerName,subscribableValue: subscribableValue, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                //EventLog
                if self.filteredTariffsList?[self.selectedTabIndex].isSpecial?.isTrue() ?? false {
                    //log event for Special tarif migration
                    AFAnalyticsManager.logEvent(screenName: "special_tariff_migration", contentType:"subscription" , successStatus:"0" )
                } else {
                    //log event for normal tarif migration
                    AFAnalyticsManager.logEvent(screenName: "tariff_migration", contentType:"migration" , successStatus:"0" )
                }
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    if self.filteredTariffsList?[self.selectedTabIndex].isSpecial?.isTrue() ?? false {
                        //log event for Special tarif migration
                        AFAnalyticsManager.logEvent(screenName: "special_tariff_migration", contentType:"subscription" , successStatus:"1" )
                    } else {
                        //log event for normal tarif migration
                        AFAnalyticsManager.logEvent(screenName: "tariff_migration", contentType:"migration" , successStatus:"1" )
                    }
                    
                    // Parssing response data
                    if let changeTariffResponse = Mapper<MessageResponse>().map(JSONObject: resultData){
                      
                       
                            
                        self.checkAndShowInAppSurvey(offeringId: offeringId, changeTariffResponse: changeTariffResponse.message)
                       
                        
                       
                        
                    } else {
                        // Show Error alert
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                } else {
                    //EventLog
                    if self.filteredTariffsList?[self.selectedTabIndex].isSpecial?.isTrue() ?? false {
                        //log event for Special tarif migration
                        AFAnalyticsManager.logEvent(screenName: "special_tariff_migration", contentType:"subscription" , successStatus:"0" )
                    } else {
                        //log event for normal tarif migration
                        AFAnalyticsManager.logEvent(screenName: "tariff_migration", contentType:"migration" , successStatus:"0" )
                    }
                    
                    // Show Error alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    /**
     call to get appResume data and will update current info.
     
     - returns: void.
     */
    func getAppResume() {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.appResume({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.hideActivityIndicator()
            // handling data from API response.
            if resultCode == Constants.AFAPIStatusCode.succes.rawValue ,
                let appResumeHandler = Mapper<LoginModel>().map(JSONObject: resultData) {
                
                /* Update AppResume information in User Defaults */
                AFUserInfoUtilities.updateLoggedInUserInfo(loggedInUserMSISDN: AFUserSession.shared.msisdn,
                                                           loginInfo: appResumeHandler,
                                                           appConfig: AFUserSession.shared.appConfig)
            }
        })
    }
}

//MARK: - UICollectionViewDelegate
extension TariffVC :UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if  collectionView == tariffContentCollectionView {
            return CGSize(width: tariffContentCollectionView.bounds.width - 54 , height:  tariffContentCollectionView.bounds.height)
        } else {
            let str: String = filteredTariffsList?[indexPath.item].groupType ?? ""
            
            return CGSize(width: ((str as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.afSans(fontSize: 14)]).width + 50), height: 36)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if  collectionView == tariffContentCollectionView {
            if self.filteredTariffsList?.indices.contains(self.selectedTabIndex) ?? false,
                let itemsCount = self.filteredTariffsList?[self.selectedTabIndex].items?.count {
                
                return itemsCount
            } else {
                return 0
            }
        } else {
            return filteredTariffsList?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if  collectionView == tariffContentCollectionView {
            if let cell :TariffCell = collectionView.dequeueReusableCell(withReuseIdentifier: TariffCell.cellIdentifier(), for: indexPath) as? TariffCell,
                let aTariffItem = self.filteredTariffsList?[self.selectedTabIndex].items?[indexPath.item] {
                
                cell.delegate = self
                //sete layout of cell
                cell.setItemInfo(anOfferItem: aTariffItem)
                
                //set delegates
                cell.tableView.delegate = self
                cell.tableView.dataSource = self
                
                cell.tableView.tag = indexPath.item
                cell.tableView.allowsSelection = false
                
                cell.subscribeButton.tag = indexPath.item
                cell.subscribeButton.addTarget(self, action: #selector(subscribePressed(_:)), for: .touchUpInside)
                
                cell.tableView.reloadData()
                self.openSelectedSection()
                
                return cell
            }
        } else {
            if let cell :NormalTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier: NormalTypeCell.cellIdentifier(), for: indexPath) as? NormalTypeCell {
                
                /*cell.setLayout(title: tariffResponse?[indexPath.item].groupType ?? "",
                 isSelected: selectedIndex == indexPath ? true : false,
                 isSpecial: selectedIndex == indexPath ? true : false )*/
                cell.setLayout(title: filteredTariffsList?[indexPath.item].groupType ?? "",
                               isSelected: selectedTabIndex == indexPath.item ? true : false,
                               isSpecial: filteredTariffsList?[indexPath.item].isSpecial?.isTrue() ?? false)
                
                
                
                return cell
            }
        }
        
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if  collectionView == tariffContentCollectionView {
            
        } else {
            
            /* Reset opened section to bonuses when user swithc tab */
            selectedSectionViewType = .bonuses
            
            selectedTabIndex = indexPath.item
            if let cell :NormalTypeCell = collectionView.cellForItem(at: indexPath) as? NormalTypeCell {
                cell.setLayout(title: filteredTariffsList?[indexPath.item].groupType ?? "",
                               isSelected: true,
                               isSpecial: filteredTariffsList?[indexPath.item].isSpecial?.isTrue() ?? false)
                
            }
            /* Reload tariff details */
            reloadTariffDetailsAndMenuCollectionView(tabIndex: self.selectedTabIndex, tariffItemIndex: 0 )
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if  collectionView == tariffContentCollectionView{
            
        } else {
            if let cell :NormalTypeCell = collectionView.cellForItem(at: indexPath) as? NormalTypeCell {
                cell.setLayout(title: filteredTariffsList?[indexPath.item].groupType ?? "",
                               isSelected: false,
                               isSpecial: filteredTariffsList?[indexPath.item].isSpecial?.isTrue() ?? false)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scroll did end")
        let visibleRect = CGRect(origin: tariffContentCollectionView.contentOffset, size: tariffContentCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = tariffContentCollectionView.indexPathForItem(at: visiblePoint)
        let currentPage = visibleIndexPath?.item
        print("list count:\(self.filteredTariffsList?[self.selectedTabIndex].items?.count ?? 0)\n current index:\(currentPage ?? 0)")
        if (self.filteredTariffsList?[self.selectedTabIndex].items?.count ?? 0) > 1 {
            if currentPage == 0 {
                self.rightArrowButton.isHidden = false
                self.leftArrowButton.isHidden = true
            } else if currentPage == (self.filteredTariffsList?[self.selectedTabIndex].items?.count ?? 0) - 1 {
                self.rightArrowButton.isHidden = true
                self.leftArrowButton.isHidden = false
            } else {
                self.rightArrowButton.isHidden = false
                self.leftArrowButton.isHidden = false
            }
        } else {
            self.rightArrowButton.isHidden = true
            self.leftArrowButton.isHidden = true
        }
        collectenViewCurrentIndex = currentPage!
    }
    
    /*
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if  collectionView == tariffContentCollectionView {
            return cardLayout.sectionInset
        } else {
            let leftInset = self.tariffContentCollectionView.frame.width / 3
            let rightInset = leftInset
            
            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
            
        }
    }*/
}

//MARK: - UITableViewDelegate
extension TariffVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let aTariffObject = self.filteredTariffsList?[self.selectedTabIndex].items?[tableView.tag]{
            
            return typeOfDataInSection(aTariffObject).count
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let aTariffObject = self.filteredTariffsList?[self.selectedTabIndex].items?[tableView.tag],
            let headerView :ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
            
            let types = typeOfDataInSection(aTariffObject)
            
            var highlightSection = selectedSectionViewType
            if types.contains(selectedSectionViewType) == false,
                let firstSection = types.first {
                
                highlightSection = firstSection
            }
            
            
            switch types[section]{
            case .bonuses:
                headerView.setViewWithTitle(title: aTariffObject.header?.bonusLabel,
                                            isSectionSelected: highlightSection == .bonuses ? true : false,
                                            headerType: .bonuses)
                break
            case .prices:
                headerView.setViewWithTitle(title: aTariffObject.packagePrice?.packagePriceLabel,
                                            isSectionSelected: highlightSection == .prices ? true : false,
                                            headerType: .prices)
                break
            case .detail:
                headerView.setViewWithTitle(title: aTariffObject.details?.detailLabel,
                                            isSectionSelected: highlightSection == .detail ? true : false,
                                            headerType: .detail)
                break
                
            default:
                break
            }
            return headerView
        }
        
        return AFAccordionTableViewHeaderView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let aTariffObject = self.filteredTariffsList?[self.selectedTabIndex].items?[tableView.tag]{
            
            let types = typeOfDataInSection(aTariffObject)
            
            switch types[section]{
            case .bonuses:
                return aTariffObject.header?.attributes?.count ?? 0
                
            case .prices:
                
                return typeOfDataInPackagePrice(aTariffObject.packagePrice).count
            case .detail:
                
                return typeOfDataInDetailSection(aTariffObject.details).count
                
            default:
                break
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let aTariffObject = self.filteredTariffsList?[self.selectedTabIndex].items?[tableView.tag]{
            
            let types = typeOfDataInSection(aTariffObject)
            
            switch types[indexPath.section]{
            case .bonuses:
                
                if let cell : SingleAttributeCell = tableView.dequeueReusableCell() {
                    cell.setAttributeHeader(aTariffObject.header?.attributes?[indexPath.row],
                                            showSeprator: indexPath.row < ((aTariffObject.header?.attributes?.count ?? 0) - 1))
                    
                    return cell
                }
                
            case .prices:
                let priceData = typeOfDataInPackagePrice(aTariffObject.packagePrice)
                
                switch priceData[indexPath.row] {
                case .call:
                    
                    if let cell : PriceCell = tableView.dequeueReusableCell() {
                        
                        cell.setCallAndSMSLayoutValues(callHeaderType: aTariffObject.packagePrice?.call,
                                                       cellType:.call,
                                                       showSeprator: indexPath.row < (priceData.count - 1))
                        return cell
                    }
                case .SMS:
                    
                    if let cell : PriceCell = tableView.dequeueReusableCell() {
                        
                        cell.setCallAndSMSLayoutValues(callHeaderType: aTariffObject.packagePrice?.sms,
                                                       cellType:.SMS,
                                                       showSeprator: indexPath.row < (priceData.count - 1))
                        return cell
                    }
                case .internet:
                    
                    if let cell : PriceCell = tableView.dequeueReusableCell() {
                        
                        cell.setInternetLayoutValues(internetHeader: aTariffObject.packagePrice?.internet,
                                                     showSeprator: indexPath.row < (priceData.count - 1))
                        return cell
                    }
                case .callPayG:
                    
                    if let cell : PriceCell = tableView.dequeueReusableCell() {
                        
                        cell.setCallAndSMSLayoutValues(callHeaderType: aTariffObject.packagePrice?.callPayg,
                                                       cellType:.callPayG,
                                                       showSeprator: indexPath.row < (priceData.count - 1))
                        return cell
                    }
                case .SMSPayG:
                    
                    if let cell : PriceCell = tableView.dequeueReusableCell() {
                        
                        cell.setCallAndSMSLayoutValues(callHeaderType: aTariffObject.packagePrice?.smsPayg,
                                                       cellType:.SMSPayG,
                                                       showSeprator: indexPath.row < (priceData.count - 1))
                        return cell
                    }
                case .internetPayG:
                    
                    if let cell : PriceCell = tableView.dequeueReusableCell() {
                        
                        cell.setInternetLayoutValues(internetHeader: aTariffObject.packagePrice?.internetPayg,
                                                     showSeprator: indexPath.row < (priceData.count - 1))
                        return cell
                    }
                default:
                    break
                }
                
                break
            case .detail:
                let detailsData = typeOfDataInDetailSection(aTariffObject.details)
                switch detailsData[indexPath.row] {
                    
                case .price:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setPriceLayoutValues(price: aTariffObject.details?.price?[indexPath.row],
                                                  showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                    break
                case .rounding:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setRoundingLayoutValues(rounding: aTariffObject.details?.rounding,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .textWithTitle:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithTitleLayoutValues(textWithTitle: aTariffObject.details?.textWithTitle,
                                                          showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithOutTitle:
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aTariffObject.details?.textWithOutTitle,
                                                             showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithPoints:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithPointsLayoutValues(textWithPoint: aTariffObject.details?.textWithPoints,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .date:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aTariffObject.details?.date,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .time:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aTariffObject.details?.time,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .roamingDetails:
                    
                    if let cell:RoamingDetailsCell = tableView.dequeueReusableCell() {
                        cell.setRoamingDetailsLayout(roamingDetails: aTariffObject.details?.roamingDetails,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .freeResourceValidity:
                    
                    if let cell:FreeResourceValidityCell = tableView.dequeueReusableCell() {
                        cell.setFreeResourceValidityValues(freeResourceValidity: aTariffObject.details?.freeResourceValidity,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .titleSubTitleValueAndDesc:
                    
                    if let cell:TitleSubTitleValueAndDescCell = tableView.dequeueReusableCell() {
                        cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aTariffObject.details?.titleSubTitleValueAndDesc,
                                                                      showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                default:
                    break
                }
                break
                
            default:
                break
            }
        }
        
        return UITableViewCell()
    }
}

// MARK: - AFAccordionTableViewDelegate

extension TariffVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: true)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        if let headerView = header as? ExpandableTitleHeaderView {
            
            if selectedSectionViewType != headerView.viewType {
                
                selectedSectionViewType = headerView.viewType
            }
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: false)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            if selectedSectionViewType == headerView.viewType {
                selectedSectionViewType = .bonuses
                tableView.toggleSection(0)
                
            }
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}


//MARK: TariffsCellDelegate
extension TariffVC: TariffsCellDelegate {
    func starTapped(offerId: String) {
        
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            if  let  userSurvey = AFUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offerId}){
                if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == SurveyScreenName.tariff_subscribed.rawValue }){
                    inAppSurveyVC.currentSurvay = survey
                    if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                        inAppSurveyVC.survayQuestion = question
                        if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                            inAppSurveyVC.selectedAnswer = question.answers?[answer]
                        }
                    }
                }
                inAppSurveyVC.survayComment = userSurvey.comments
                inAppSurveyVC.offeringId = offerId
                inAppSurveyVC.offeringType = "2"
            } else {
                if let survey = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.tariff_subscribed.rawValue}) {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.offeringId = offerId
                    inAppSurveyVC.offeringType = "2"
                }
            }
            
            inAppSurveyVC.surveyScreen = SurveyScreenName.tariff.rawValue
            inAppSurveyVC.callBack =  {
                DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                    self.tariffContentCollectionView.reloadData()
                }
            }
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
        
    }
    
    
}

//MARK: - Helping functions
extension TariffVC {
    
    func typeOfDataInSection(_ aTariffItem : OfferItem?) -> [Constants.AFSectionType] {
        
        var dataTypes : [Constants.AFSectionType] = []
        
        if let aTariff =  aTariffItem {
            // 1
            if (aTariff.header?.attributes?.count ?? 0) > 0 {
                
                dataTypes.append(.bonuses)
            }
            // 2
            if aTariff.packagePrice != nil  {
                
                dataTypes.append(.prices)
            }
            // 3
            if aTariff.details != nil {
                
                dataTypes.append(.detail)
            }
        }
        
        return dataTypes
    }
    
    func typeOfDataInPackagePrice(_ aPackagePrice: PackagePrice?) -> [Constants.AFOfferType]  {
        
        var dataTypes : [Constants.AFOfferType] = []
        if let aPackagePriceObject =  aPackagePrice {
            
            // 1
            if aPackagePriceObject.call != nil &&
                (aPackagePriceObject.call?.attributes?.count ?? 0) > 0 {
                
                dataTypes.append(.call)
            }
            // 2
            if aPackagePriceObject.sms != nil &&
                (aPackagePriceObject.sms?.attributes?.count ?? 0) > 0  {
                
                dataTypes.append(.SMS)
            }
            // 3
            if aPackagePriceObject.internet != nil &&
                (aPackagePriceObject.internet?.attributes?.count ?? 0) > 0{
                
                dataTypes.append(.internet)
            }
            
            // 4
            if aPackagePriceObject.callPayg != nil  &&
                (aPackagePriceObject.callPayg?.attributes?.count ?? 0) > 0 {
                
                dataTypes.append(.callPayG)
            }
            // 5
            if aPackagePriceObject.smsPayg != nil  &&
                (aPackagePriceObject.smsPayg?.attributes?.count ?? 0) > 0 {
                
                dataTypes.append(.SMSPayG)
            }
            // 6
            if aPackagePriceObject.internetPayg != nil &&
                (aPackagePriceObject.internetPayg?.attributes?.count ?? 0) > 0 {
                
                dataTypes.append(.internetPayG)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInDetailSection(_ aDetail : Details?) -> [Constants.AFOfferType] {
        
        var dataTypes : [Constants.AFOfferType] = []
        
        if let aDetailObject =  aDetail {
            
            // 1
            if let count = aDetailObject.price?.count {
                
                for _ in 0..<count {
                    dataTypes.append(.price)
                }
            }
            // 2
            if aDetailObject.rounding != nil  {
                
                dataTypes.append(.rounding)
            }
            // 3
            if aDetailObject.textWithTitle != nil {
                
                dataTypes.append(.textWithTitle)
            }
            // 4
            if aDetailObject.textWithOutTitle != nil {
                
                dataTypes.append(.textWithOutTitle)
            }
            // 5
            if aDetailObject.textWithPoints != nil {
                
                dataTypes.append(.textWithPoints)
            }
            // 6
            if aDetailObject.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(.titleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailObject.date != nil {
                
                dataTypes.append(.date)
            }
            // 8
            if aDetailObject.time != nil {
                
                dataTypes.append(.time)
            }
            // 9
            if aDetailObject.roamingDetails != nil {
                
                dataTypes.append(.roamingDetails)
            }
            // 10
            if aDetailObject.freeResourceValidity != nil {
                
                dataTypes.append(.freeResourceValidity)
            }
        }
        return dataTypes
    }
}

//MARK: - UIScrollViewDelegate
extension TariffVC : UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == tariffContentCollectionView {
            openSelectedSection()
        }
    }
}

//MARK: - UITextFieldDelegate
extension TariffVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == searchTextField {
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            // Filter offers with new name
            
            filteredTariffsList = tariffsResponseList?.filterOfferBySearchString(searchString: newString)
            reloadTariffDetailsAndMenuCollectionView(tabIndex: self.selectedTabIndex, tariffItemIndex: 0 )
            
            if newString.length > 0 {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "search", contentType:"tariff_screen" , searchTerm: newString )
            }
            
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
