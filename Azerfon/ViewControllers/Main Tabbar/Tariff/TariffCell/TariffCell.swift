//
//  TariffCell.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/1/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit


protocol TariffsCellDelegate {
    func starTapped(offerId :  String)
}

class TariffCell: UICollectionViewCell {
    
    @IBOutlet var myContentView: AFView!
    @IBOutlet var nameLabel: AFMarqueeLabel!
    @IBOutlet var priceLabel: AFLabel!
    @IBOutlet var descriptionLabel: AFLabel!
    @IBOutlet var tableView: AFAccordionTableView!
    @IBOutlet var subscribeButton: AFButton!
    
    //Stars outlets....
    @IBOutlet weak var btnsStackView: UIStackView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    
    var delegate : TariffsCellDelegate? = nil
    var offer: OfferItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ExpandableTitleHeaderView.registerReusableHeaderView(with: tableView)
        
        tableView.allowMultipleSectionsOpen = false
        tableView.keepOneSectionOpen = true;
        tableView.allowsSelection = false
        
        tableView.estimatedSectionHeaderHeight = 40
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
        tableView.separatorColor = UIColor.clear
        tableView.initialOpenSections = [0]
        self.btnsStackView.isHidden = true
    }
    
    func setItemInfo(anOfferItem : OfferItem) {
        
        self.nameLabel.text = anOfferItem.header?.name
        
        var priceLabelValue = anOfferItem.header?.priceLabel ?? ""
        
        if (anOfferItem.header?.priceLabel?.isBlank ?? true) == false &&
            (anOfferItem.header?.priceValue?.isBlank ?? true) == false {
            priceLabelValue.append(": ")
        }
        
        let priceAttributedString = priceLabelValue.createAttributedString(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.b3GreyRight)
        
        
        
        priceAttributedString.append("\(anOfferItem.header?.priceValue ?? "")".createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown,                                                                                                                        mainStringFont: UIFont.b3GreyRight,                                                                                                                        imageSize: CGSize(width: 10, height: 6)))
        
        self.priceLabel.attributedText = priceAttributedString
        
        
        
        self.descriptionLabel.text = anOfferItem.header?.tariffDescription
        self.offer = anOfferItem
        //layout subscribe button
        self.layoutSubscribeButton(isSubscribed: anOfferItem.subscribable)
        
    }
    
    func layoutSubscribeButton(isSubscribed : String?, tag: Int = 0) {
        
        if isSubscribed == "2" {
            self.subscribeButton.isEnabled = false
            self.subscribeButton.setTitle(Localized("BtnTitle_SUBSCRIBED"), for: UIControl.State.normal)
            self.subscribeButton.setButtonLayoutType(.whiteText_ClearBG_Rounded_Disabled)
            self.btnsStackView.isHidden = false
            self.setRating()
            
        } else if isSubscribed == "3" {
            self.subscribeButton.isEnabled = true
            self.subscribeButton.setTitle(Localized("BtnTitle_RENEW"), for: UIControl.State.normal)
            self.subscribeButton.setButtonLayoutType(.whiteText_ClearBG_Rounded)
            self.btnsStackView.isHidden = false
            self.setRating()
            
        } else if isSubscribed == "1" {
            self.subscribeButton.isEnabled = true
            self.subscribeButton.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: UIControl.State.normal)
            self.subscribeButton.setButtonLayoutType(.whiteText_ClearBG_Rounded)
            self.btnsStackView.isHidden = true
            
        } else if isSubscribed == "0" {
            self.subscribeButton.isEnabled = false
            self.subscribeButton.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: UIControl.State.normal)
            self.subscribeButton.setButtonLayoutType(.whiteText_ClearBG_Rounded)
            self.btnsStackView.isHidden = true
        }
    }
    
    // stars for survey
    func setRating(){
        
        if let surveyToDisplay  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == SurveyScreenName.tariff_subscribed.rawValue }) {
            if  let  userSurvey = AFUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == self.offer?.header?.offeringId ?? ""}) {
                if let  question =  surveyToDisplay.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                    if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                        switch answer {
                        case 0:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 1:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 2:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 3:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 4:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                        default:
                            showUnselectedStars()
                            break
                        }
                        
                    }
                } else {
                    showUnselectedStars()
            }
                
            
            } else {
               showUnselectedStars()
            }
        } else {
            self.btnsStackView?.isHidden = true
        }
    }
    
    func showUnselectedStars() {
        btn1.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
    }
    
    
    @IBAction func tabStar(_ sender: UIButton){
        if  let ofr  = self.offer?.header?.offeringId{
            self.delegate?.starTapped(offerId: ofr )
        }
    }
}
