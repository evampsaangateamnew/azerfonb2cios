//
//  EditManageAccountCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/22/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class EditManageAccountCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var userNameLabel             : AFLabel!
    @IBOutlet var userPhoneNumberLabel      : AFLabel!
    @IBOutlet var deletButton               : UIButton!
    @IBOutlet var dotImageView              : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setLayout(name :String?, msisdn :String?, isSelected :Bool) {
        userNameLabel.text = name
        userPhoneNumberLabel.text = msisdn
        dotImageView.isHidden = !isSelected
    }
}
