//
//  ManageAccountsVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/22/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import KYDrawerController

class ManageAccountsVC: BaseVC {
    
    //MARK:- Properties
    var canNavigateBack : Bool          = true
    var descriptionMessage : String?    = Localized("Message_manageAccountDescription")
    
    //MARK: - IBOutlet
    @IBOutlet var descriptionLabel          : AFLabel!
    @IBOutlet var addNewAccountButton       : AFButton!
    @IBOutlet var manageAccountsTableView   : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manageAccountsTableView.hideDefaultSeprator()
        backButton?.isHidden = canNavigateBack ? false : true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(canNavigateBack)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func AddNewAccountPressed(_ sender: UIButton) {
        
        if (AFUserSession.shared.loggedInUsers?.users?.count ?? 0) < Constants.MaxNumberOfManageAccount {
            if let loginVC :LoginVC = LoginVC.instantiateViewControllerFromStoryboard() {
                loginVC.registrationType = .addAccount
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        } else {
           self.showAlert(title: "", message: Localized("Message_MaxNumberOfAccount"))
        }
        
    }
    
    @IBAction func removeAccountPressed(_ sender: UIButton) {
        
        if let selectedUserMSISDN = AFUserSession.shared.loggedInUsers?.users?[sender.tag].userInfo?.msisdn {
            
            if selectedUserMSISDN.isEqual(AFUserSession.shared.msisdn, ignorCase: true) == false  {
                
                if (AFUserSession.shared.loggedInUsers?.users?.count ?? 0) <= 1 {
                    self.showConfirmationAlert(message: Localized("Message_RemoveLastAccount"), okBtnTitle: Localized("BtnTitle_YES"), cancelBtnTitle: Localized("BtnTitle_NO"), {
                        /* */
                        BaseVC.logout()
                    })
                }  else {
                    AFUserInfoUtilities.removeCustomerDataOf(msisdn: selectedUserMSISDN)
                    manageAccountsTableView.reloadUserData(AFUserSession.shared.loggedInUsers?.users)
                }
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_ErrorCantRemove"))
            }
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        titleLabel?.text = Localized("Title_ManageAccount")
        descriptionLabel.text = descriptionMessage
        addNewAccountButton.setTitle(Localized("BtnTitle_AddNewAccount"), for: .normal)
        
        
    }
    
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension ManageAccountsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return AFUserSession.shared.loggedInUsers?.users?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :EditManageAccountCell = tableView.dequeueReusableCell(),
            let selectedUser = AFUserSession.shared.loggedInUsers?.users?[indexPath.row] {
            
            cell.setLayout(name: selectedUser.userInfo?.firstName,
                           msisdn: selectedUser.userInfo?.msisdn,
                           isSelected: selectedUser.userInfo?.msisdn?.isEqual(AFUserSession.shared.msisdn, ignorCase: true) ?? false)
            
            cell.deletButton.tag = indexPath.row
            cell.deletButton.addTarget(self, action: #selector(removeAccountPressed(_:)), for: .touchDown)
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let aUser = AFUserSession.shared.loggedInUsers?.users?[indexPath.row] {
            
            if (aUser.userInfo?.msisdn?.isEqual(AFUserSession.shared.msisdn, ignorCase: true) ?? false) == true {
                self.showErrorAlertWithMessage(message: Localized("Message_AlreadySelected"))
                
            } else {
                /* Clear user session objects befor switch */
                AFUserSession.shared.clearUserSessionObjectsForCurrentUser()
                
                /* Switch user to new user */
                AFUserInfoUtilities.switchToUser(newMSISDN: aUser.userInfo?.msisdn ?? "", selectedUserInfo: aUser)
                
                /* Cancel all API requests */
                AFAPIClient.shared.cancelAllRequests()
                
                /* Redirect user to dashboard screen */
                redirectUserToDashboard()
            }
            // Deselect user selected row
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
    func redirectUserToDashboard() {
        
        if let navigationStackCount = self.navigationController?.viewControllers.count,
            navigationStackCount > 1,
            let firstVC = self.navigationController?.viewControllers.first,
            let mainViewController :TabbarController = TabbarController.instantiateViewControllerFromStoryboard() ,
            let drawerViewController :SideMenuVC = SideMenuVC.instantiateViewControllerFromStoryboard() {
            
            mainViewController.isLoggedInNow = false
            
            let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
            drawerController.mainViewController = mainViewController
            drawerController.drawerViewController = drawerViewController
            
            self.navigationController?.viewControllers = [firstVC, drawerController]
        }
    }
}

