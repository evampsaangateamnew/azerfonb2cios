//
//  StoreListCell.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/29/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class StoreListCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet var timmingContainorView: UIView!
    @IBOutlet var timmingContainorViewHightConstraint: NSLayoutConstraint!
    
    //MARK: -  Functions
    func setLayoutFor(aStore :Stores?) {
        
        timmingContainorView.subviews.forEach({ (aView) in
            
            if aView is TitleAndValueView {
                aView.removeFromSuperview()
            }
        })
        
        if let aStoreObject = aStore {

            var lastView : UIView?
            aStoreObject.timing?.forEach({ (aStoreOpenTiming) in
                let myTitleAndValueView: TitleAndValueView = TitleAndValueView.fromNib()
                
                myTitleAndValueView.translatesAutoresizingMaskIntoConstraints = false
                
                myTitleAndValueView.setTitleAndValue(aStoreOpenTiming.day, value: aStoreOpenTiming.timings)
                
                timmingContainorView.addSubview(myTitleAndValueView)
                
                if lastView == nil{
                    
                    myTitleAndValueView.snp.makeConstraints { (make) -> Void in
                        make.top.equalTo(timmingContainorView.snp.top)
                        make.right.equalTo(timmingContainorView.snp.right)
                        make.left.equalTo(timmingContainorView.snp.left)
                        make.height.equalTo(14)
                    }
                    
                } else {
                    
                    myTitleAndValueView.snp.makeConstraints { (make) -> Void in
                        make.top.equalTo(lastView?.snp.bottom ?? timmingContainorView.snp.top)
                        make.right.equalTo(timmingContainorView.snp.right)
                        make.left.equalTo(timmingContainorView.snp.left)
                        make.height.equalTo(14)
                    }
                    
                }
                
                lastView = myTitleAndValueView
            })
            
            
            if let count = aStoreObject.timing?.count{
                timmingContainorViewHightConstraint.constant = CGFloat(count * 14)
            } else {
                timmingContainorViewHightConstraint.constant = 0.0
            }
            
        }
    }
    
}
