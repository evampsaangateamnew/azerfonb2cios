//
//  StoreListVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/27/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class StoreListVC: UIViewController {
    //MARK: - Properties
    var storeLocator : StoreLocatorModel?
    
    private var storeLocatorFilter : StoreLocatorModel?
    
    private var selectedCity : String = Localized("DropDown_All")
    private var selectedCategory : String = Localized("DropDown_All")
    
    //MARK: - IBOutlet
    @IBOutlet var categoryDropDownTitle : AFLabel!
    @IBOutlet var categoryDropDown      : UIDropDown!
    @IBOutlet var cityDropDownTitle     : AFLabel!
    @IBOutlet var cityDropDown          : UIDropDown!
    
    @IBOutlet var tableView             : AFAccordionTableView!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.storeLocatorFilter = storeLocator
        setUPDropBox()
        
        tableView.hideDefaultSeprator()
        tableView.allowMultipleSectionsOpen = true
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        tableView.reloadUserData(self.storeLocatorFilter?.stores)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        categoryDropDownTitle.text  = Localized("Title_SelectCategory")
        cityDropDownTitle.text      = Localized("Title_SelectCity")
    }
    
    ///Initialze / Setup dropdown
    func setUPDropBox() {
        
        // set storeCenterDrop
        categoryDropDown.text = Localized("DropDown_All")
        
        var optionsArray : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocator?.type {
            for center in selectedCenter{
                optionsArray.append(center)
            }
        }
        
        categoryDropDown.dataSource = optionsArray
        
        categoryDropDown.didSelectOption { (index, option) in
            self.selectedCategory = option
            
            self.storeLocatorFilter?.stores = StoreLocatorModel.filterStoreBy(city: self.selectedCity,
                                                                              type: self.selectedCategory,
                                                                              stores: self.storeLocator?.stores)
            self.tableView.reloadUserData(self.storeLocatorFilter?.stores)
            
        }
        
        // set storeListDrop
        cityDropDown.text = Localized("DropDown_All")
        
        var optionsArrayCity : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocator?.city {
            
            for center in selectedCenter {
                
                optionsArrayCity.append(center)
            }
        }
        
        cityDropDown.dataSource = optionsArrayCity
        
        cityDropDown.didSelectOption { (index, option) in
            self.selectedCity = option
            
            self.storeLocatorFilter?.stores = StoreLocatorModel.filterStoreBy(city: self.selectedCity,
                                                                              type: self.selectedCategory,
                                                                              stores: self.storeLocator?.stores)
            self.tableView.reloadUserData(self.storeLocatorFilter?.stores)
            
        }
    }
    
    //MARK: - API's Calls
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>
extension StoreListVC :UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.storeLocatorFilter?.stores?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView :StoreListHeaderView = tableView.dequeueReusableHeaderFooterView() {
            
            headerView.setTileLabelText(storeLocatorFilter?.stores?[section], senterTag: section)
            
            return headerView
        }
        return AFAccordionTableViewHeaderView()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :StoreListCell = tableView.dequeueReusableCell() {
            
            cell.setLayoutFor(aStore:  storeLocatorFilter?.stores?[indexPath.section])
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
}


// MARK: - <AFAccordionTableViewDelegate>

extension StoreListVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? StoreListHeaderView {
            headerView.setExpandStatus(true)
            
            storeLocatorFilter?.stores?[section].isSectionExpanded = true
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? StoreListHeaderView {
            
            headerView.setExpandStatus(false)
            
            storeLocatorFilter?.stores?[section].isSectionExpanded = false
            
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}
