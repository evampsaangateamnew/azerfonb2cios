//
//  StoreListHeaderView.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/29/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class StoreListHeaderView: AFAccordionTableViewHeaderView {
    
    //MARK: IBOutlet
    @IBOutlet var expandCellImage: UIImageView!
    @IBOutlet var storeAddressLabel: AFLabel!
    @IBOutlet var storeNameLabel: AFLabel!

    //MARK: - Functions
    func setTileLabelText(_ aStoreItem: Stores?, senterTag:Int) {
        if let aStoreLocation = aStoreItem {
            
            storeNameLabel.text = aStoreLocation.store_name
            storeAddressLabel.text = aStoreLocation.address
            setExpandStatus(aStoreLocation.isSectionExpanded )
            
        } else {
            storeNameLabel.text =  ""
            storeAddressLabel.text = ""
            setExpandStatus(false )
        }
        
    }
    
    func setExpandStatus(_ isExpanded : Bool?) {
        
        if isExpanded == true {
            self.expandCellImage.image = UIImage.imageFor(name: "pinkMinusIcon")
        } else {
            self.expandCellImage.image  = UIImage.imageFor(name: "pinkPlusIcon")
            
        }
    }
}
