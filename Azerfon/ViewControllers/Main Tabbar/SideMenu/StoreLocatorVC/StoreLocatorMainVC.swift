//
//  StoreLocatorMainVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/27/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class StoreLocatorMainVC: BaseVC {
    //MARK: - Properties
    var selectedType : Constants.AFStoreLocatorType = .map
    var storeListVC :  StoreListVC?
    var storeMapVC :  StoreMapVC?
    var storesData :StoreLocatorModel?
    var shouldFocuseToSelectedLocation : Bool = false
    var selectedLocationForFocuse : [String: String]? = [:]
    
    //MARK: - IBOutlet
    @IBOutlet var mainContentView   : AFView!
    @IBOutlet var storeMapButton    : AFMarqueeButton!
    @IBOutlet var storeListButton   : AFMarqueeButton!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectTabOf(type: selectedType)
        self.getStoreLocatorDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func mapPressed(_ sender: AFButton) {
        
        selectedType = .map
        
        self.reDirectUserToSelectedScreen(type: selectedType)
    }
    
    @IBAction func storeListPressed(_ sender: AFButton) {
        
        selectedType = .list
        
        self.reDirectUserToSelectedScreen(type: selectedType)
    }
    
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_STORELOCATOR")
        storeMapButton.setTitle(Localized("BtnTitle_Map"), for: .normal)
        storeListButton.setTitle(Localized("BtnTitle_StoreList"), for: .normal)
    }
    
    func selectTabOf(type :Constants.AFStoreLocatorType) {
        if type == .list {
            
            storeMapButton.setButtonLayoutType(.whiteText_DarkPinkBG_Rounded)
            storeListButton.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
        } else {
            
            storeMapButton.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
            storeListButton.setButtonLayoutType(.whiteText_DarkPinkBG_Rounded)
        }
    }
    
    /**
     Add child view controller to parent view.
     
     - returns: void
     */
    func reDirectUserToSelectedScreen(type :Constants.AFStoreLocatorType) {
        
        selectTabOf(type: type)
        
        if storesData != nil {
            
            mainContentView.hideDescriptionView()
            
            if type == .map {
                
                self.removeChildViewController(childController: StoreListVC())
                
                if (storeMapVC == nil) {
                    storeMapVC = StoreMapVC.instantiateViewControllerFromStoryboard()
                }
                  storeMapVC?.storeLocator = storesData
                
                  storeMapVC?.shouldFocuseToSelectedLocation = self.shouldFocuseToSelectedLocation
                  storeMapVC?.selectedLocationForFocuse = self.selectedLocationForFocuse
                
                self.addChildViewController(childController: storeMapVC ?? StoreMapVC(), onView: mainContentView)
                
                // Clear selected location once map loaded.
                  self.shouldFocuseToSelectedLocation = false
                  self.selectedLocationForFocuse = [:]
                
                
            } else {
                
                self.removeChildViewController(childController: StoreMapVC())
                
                if storeListVC == nil {
                    storeListVC = StoreListVC.instantiateViewControllerFromStoryboard()
                }
                
                 storeListVC?.storeLocator = storesData
                
                self.addChildViewController(childController: storeListVC ?? StoreListVC(), onView: mainContentView)
                
            }
        } else {
            mainContentView.showDescriptionViewWithImage(description: Localized("Message_NoData"))
        }
        
    }
    
    
    //MARK: - API's Calls
    
    /**
     call to get get store details.
     */
    func getStoreLocatorDetail() {
        
        if let responseHandler :StoreLocatorModel = StoreLocatorModel.loadFromUserDefaults(key: APIsType.storeDetails.selectedLocalizedAPIKey()) {
            
            self.storesData = responseHandler
            /* Load update infomation */
            self.reDirectUserToSelectedScreen(type: self.selectedType)
            
        } else {
            self.showActivityIndicator()
        }
        
        /*API call to get data*/
        _ = AFAPIClient.shared.getStoresDetails({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
            } else {
                /* handling data from API response. */
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue ,
                    let responseHandler = Mapper<StoreLocatorModel>().map(JSONObject: resultData) {
                    
                    self.storesData = responseHandler
                    responseHandler.saveInUserDefaults(key: APIsType.storeDetails.selectedLocalizedAPIKey())
                    
                }
            }
            
            /* Load update infomation */
            self.reDirectUserToSelectedScreen(type: self.selectedType)
        })
    }
}
