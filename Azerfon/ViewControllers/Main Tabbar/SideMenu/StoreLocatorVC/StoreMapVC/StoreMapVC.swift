//
//  StoreMapVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/27/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import GoogleMaps

class StoreMapVC: UIViewController {
    //MARK: - Properties
    var storeLocator : StoreLocatorModel?
    var shouldFocuseToSelectedLocation  : Bool = false
    var selectedLocationForFocuse       : [String :String]? = [:]
    
    private var storeLocatorFilter : StoreLocatorModel?
    private var locationManager :CLLocationManager?
    
    private var selectedCity : String = Localized("DropDown_All")
    private var selectedCategory : String = Localized("DropDown_All")
    
    private var threeNeariestStores : [Stores] = []
    
    //MARK: - IBOutlet
    @IBOutlet var categoryDropDownTitle : AFLabel!
    @IBOutlet var categoryDropDown      : UIDropDown!
    @IBOutlet var cityDropDownTitle     : AFLabel!
    @IBOutlet var cityDropDown          : UIDropDown!
    @IBOutlet weak var googleMapView    : GMSMapView!
    
    @IBOutlet var nearestStoresView     : UIView!
    @IBOutlet var nearestTitle          : AFLabel!
    @IBOutlet var firstNearestStores    : UIButton!
    @IBOutlet var secondNearestStores   : UIButton!
    @IBOutlet var thirdNearestStores    : UIButton!
    @IBOutlet var nearestStoreViewHeightConstranint: NSLayoutConstraint!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.storeLocatorFilter = storeLocator
        
        googleMapView.isMyLocationEnabled = true
        googleMapView.settings.compassButton = true;
        googleMapView.settings.myLocationButton = true;
        
        setUPDropBox()
        SetUpMarker()
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
                
            case .restricted, .denied:
                self.showAccessAlert(message: Localized("Message_EnableLocationAccess"))
                
            case .authorizedAlways, .authorizedWhenInUse, .notDetermined:
                loadCurrentLocation()
            }
        } else {
            loadCurrentLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadViewContent()
    }
    
    deinit {
        if let locationManagerObject = self.locationManager {
            locationManagerObject.stopUpdatingLocation()
        }
    }
    
    //MARK: - IBAction
    @IBAction func showSelectedNearestStore(sender:UIButton) {
        
        var selectedStore : Stores?
        if sender.tag == 1 {
            
            if threeNeariestStores.count > 0 {
                selectedStore = threeNeariestStores.first
            }
            
        } else if sender.tag == 2 {
            
            if threeNeariestStores.count > 1 {
                selectedStore = threeNeariestStores[1]
            }
        } else if sender.tag == 3 {
            
            if threeNeariestStores.count > 2 {
                selectedStore = threeNeariestStores.last
            }
        }
        
        if selectedStore != nil {
            
            focusOnLocationOf(Store: selectedStore)
            
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        categoryDropDownTitle.text  = Localized("Title_SelectCategory")
        cityDropDownTitle.text      = Localized("Title_SelectCity")
        nearestTitle.text           = Localized("Title_NearestStores")
    }
    
    func loadCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
        locationManager?.distanceFilter = 50
        
        locationManager?.startUpdatingLocation()
        locationManager?.delegate = self
    }
    
    ///Initialze / Setup dropdown
    func setUPDropBox() {
        
        // set storeCenterDrop
        categoryDropDown.text = Localized("DropDown_All")
        
        var optionsArray : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocator?.type {
            for center in selectedCenter{
                optionsArray.append(center)
            }
        }
        
        categoryDropDown.dataSource = optionsArray
        
        categoryDropDown.didSelectOption { (index, option) in
            self.selectedCategory = option
            
            self.storeLocatorFilter?.stores = StoreLocatorModel.filterStoreBy(city: self.selectedCity,
                                                                              type: self.selectedCategory,
                                                                              stores: self.storeLocator?.stores)
            self.SetUpMarker()
        }
        
        // set storeListDrop
        cityDropDown.text = Localized("DropDown_All")
        
        var optionsArrayCity : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocator?.city {
            
            for center in selectedCenter {
                
                optionsArrayCity.append(center)
            }
        }
        
        cityDropDown.dataSource = optionsArrayCity
        
        cityDropDown.didSelectOption { (index, option) in
            self.selectedCity = option
            
            self.storeLocatorFilter?.stores = StoreLocatorModel.filterStoreBy(city: self.selectedCity,
                                                                              type: self.selectedCategory,
                                                                              stores: self.storeLocator?.stores)
            self.SetUpMarker()
        }
    }
    
    ///Set marks
    func SetUpMarker() {
        
        DispatchQueue.main.async {
            self.googleMapView.clear()
            
            var bounds = GMSCoordinateBounds()
            self.storeLocatorFilter?.stores?.forEach({ (aStoreLocation) in
                // Creates a marker in the center of the map.
                let aStoreMarker : GMSMarker = self.addMarkerOnLocation(storeLatitude: aStoreLocation.latitude, storeLongitude: aStoreLocation.longitude, storeName: aStoreLocation.store_name, storeAddress: aStoreLocation.address, storeType: aStoreLocation.type, focusOnLocation: false)
                
                bounds = bounds.includingCoordinate(aStoreMarker.position)
            })
            
            
            
            //  self.googleMapView.animate(toZoom: 15)
            let updateCamera = GMSCameraUpdate.fit(bounds, withPadding: 20)
            self.googleMapView.animate(with: updateCamera)
            
            if self.storeLocatorFilter?.stores?.count ?? 0 <= 1 {
                if let lastStoreLocation = self.storeLocatorFilter?.stores?.last {
                    
                    let camera = GMSCameraPosition.camera(withLatitude: lastStoreLocation.latitude?.toDouble ?? 0 , longitude: lastStoreLocation.longitude?.toDouble ?? 0, zoom: 15)
                    self.googleMapView.camera = camera
                }
                
            }
            self.focuseOnSelectedLocation()
        }
    }
    
    /**
     Focus on selected Location.
     
     - returns: void
     */
    func focuseOnSelectedLocation() {
        
        if shouldFocuseToSelectedLocation == true {
            
            _ = self.addMarkerOnLocation(storeLatitude: selectedLocationForFocuse?["lati"], storeLongitude: selectedLocationForFocuse?["longi"], storeName: selectedLocationForFocuse?["title"], storeAddress: selectedLocationForFocuse?["address"], storeType: selectedLocationForFocuse?["type"])
            
            // Clear Data
            shouldFocuseToSelectedLocation = false
            selectedLocationForFocuse = [:]
        }
        
    }
    
    /**
     Focus on a specific store.
     
     - parameter Store : Store information.
     
     - returns: void
     */
    func focusOnLocationOf(Store stroreData: Stores?) {
        
        if let selectedStore = stroreData {
            
            _ = self.addMarkerOnLocation(storeLatitude: selectedStore.latitude, storeLongitude: selectedStore.longitude, storeName: selectedStore.store_name, storeAddress: selectedStore.address, storeType:selectedStore.type)
        }
    }
    
    /**
     Create and add a marker to map.
     
     - parameter storeLatitude: Lattitude
     - parameter storeLongitude: Longitude
     - parameter storeName:  Store Name
     - parameter storeAddress: Store Address
     - parameter storeType: Store Type
     - parameter focusOnLocation: Foucus on location or not. Bydefault it's true.
     
     - returns: GMSMarker (Marker)
     */
    func addMarkerOnLocation(storeLatitude: String?, storeLongitude: String?, storeName: String?, storeAddress: String?, storeType: String?, focusOnLocation: Bool = true) -> GMSMarker {
        
        let latitudeRecived = storeLatitude?.toDouble ?? 0.0
        let longitudeRecieved = storeLongitude?.toDouble ?? 0.0
        
        // Creates a marker in the center of the map.
        let aStoreMarker = GMSMarker()
        aStoreMarker.position = CLLocationCoordinate2D(latitude: latitudeRecived, longitude: longitudeRecieved)
        aStoreMarker.title = storeName
        aStoreMarker.snippet = storeAddress
        aStoreMarker.icon = UIImage.markerIconImageFor(key: storeType)
        
        aStoreMarker.map = self.googleMapView
        
        if focusOnLocation == true {
            //  let camera = GMSCameraPosition.camera(withLatitude: latitudeRecived , longitude: longitudeRecieved, zoom: 15)
            //  let updateCamera = GMSCameraUpdate.setCamera(camera)
            //  self.googleMapView.animate(with: updateCamera)
            
            let camera = GMSCameraPosition.camera(withLatitude: latitudeRecived , longitude: longitudeRecieved, zoom: 15)
            self.googleMapView.animate(to: camera)
        }
        
        return aStoreMarker
    }
    
    /**
     Arrange stores in order of shortest distance from specific location.
     
     - parameter location: Location
     
     - returns: stores
     */
    private func shortestDistanceToOrganizationFromLocation(location :CLLocation) -> [Stores]? {
        
        var newStoreListWithDistance : [Stores] = []
        
        storeLocator?.stores?.forEach({ (aStore) in
            
            var aNewStore: Stores = aStore
            
            if let latitude = aStore.latitude?.trimmWhiteSpace,
                let longitude = aStore.longitude?.trimmWhiteSpace,
                latitude.isBlank == false &&
                    longitude.isBlank == false {
                
                let fromLocation = CLLocation(latitude: latitude.toDouble, longitude: longitude.toDouble)
                aNewStore.distance = fromLocation.distance(from: location) /* * 0.00062137119*/
            }
            
            newStoreListWithDistance.append(aNewStore)
            
        })
        
        // Remove nagitive values
        for index in 0..<newStoreListWithDistance.count {
            
            if newStoreListWithDistance[index].distance < 0 {
                newStoreListWithDistance.remove(at: index)
            }
        }
        
        // Sorte objects
        newStoreListWithDistance = newStoreListWithDistance.sorted(by: {$0.distance < $1.distance})
        
        
        if newStoreListWithDistance.count > 3 {
            
            return [newStoreListWithDistance[0],newStoreListWithDistance[1],newStoreListWithDistance[2]]
        } else if newStoreListWithDistance.count > 2 {
            return [newStoreListWithDistance[0],newStoreListWithDistance[1]]
        }
        else if newStoreListWithDistance.count > 1 {
            return [newStoreListWithDistance[0]]
        }
        else{
            return []
        }
    }
    
    
    ///Set Three nearest store details.
    func setThreeNeariestStoresDetail() {
        
        
        nearestStoresView.isHidden = false
        nearestStoreViewHeightConstranint.constant = 55
        
        firstNearestStores.setTitle("", for: .normal)
        secondNearestStores.setTitle("", for: .normal)
        thirdNearestStores.setTitle("", for: .normal)
        
        for i in 0..<threeNeariestStores.count {
            let nameDistance = "\(threeNeariestStores[i].store_name ?? "") \((threeNeariestStores[i].distance / 1000.00).rounded(toPlaces: 2)) \(Localized("Miles_Title"))"
            
            if i == 0 {
                firstNearestStores.setTitle(nameDistance, for: .normal)
                
                firstNearestStores.tag = i + 1
                firstNearestStores.addTarget(self, action: #selector(showSelectedNearestStore), for: UIControl.Event.touchUpInside)
            } else if i == 1 {
                secondNearestStores.setTitle(nameDistance, for: .normal)
                
                secondNearestStores.tag =  i + 1
                secondNearestStores.addTarget(self, action: #selector(showSelectedNearestStore), for: UIControl.Event.touchUpInside)
            } else {
                thirdNearestStores.setTitle(nameDistance, for: .normal)
                
                thirdNearestStores.tag =  i + 1
                thirdNearestStores.addTarget(self, action: #selector(showSelectedNearestStore), for: UIControl.Event.touchUpInside)
            }
            
        }
    }
    
    //MARK: - API's Calls
}


extension StoreMapVC : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let currentLocation = locations.last else {
            return
        }
        //  Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager?.stopUpdatingLocation()
        
        //  Display nearest three stores distance
        threeNeariestStores = shortestDistanceToOrganizationFromLocation(location: currentLocation) ?? []
        
        DispatchQueue.main.async {
            self.setThreeNeariestStoresDetail()
        }
    }
}
