//
//  SideMenuVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/13/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import KYDrawerController
import LiveChat

class SideMenuVC: BaseVC {
    //MARK: - Properties
    
    //MARK: - IBOutlet
    @IBOutlet var userNameLabel            : AFMarqueeLabel!
    @IBOutlet var userPhoneNumberLabel     : AFLabel!
    
    
    @IBOutlet var userProfileImageView     : UIImageView!
    @IBOutlet var menuTableView            : UITableView!
    
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        userProfileImageView.roundAllCorners(radius: 36)
        // Do any additional setup after loading the view.
        
        LiveChat.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewContent()
        
        if let profileImageURL = URL(string: AFUserSession.shared.userInfo?.imageURL ?? ""),
            UIApplication.shared.canOpenURL(profileImageURL) {
            
            AFAPIClient.shared.request(profileImageURL).responseData { response in
                
                if let imageDate = response.result.value,
                    let image = UIImage(data: imageDate) {
                    self.userProfileImageView.image = image
                } else {
                    self.userProfileImageView.image = UIImage.imageFor(name: "menuAvatarPlaceholder")
                }
            }
        } else {
            self.userProfileImageView.image = UIImage.imageFor(name: "menuAvatarPlaceholder")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.closeSideMenu(shouldSelectDashboard: true)
    }
    
    //MARK: - IBAction
    @IBAction func manageAccountsPressed(_ sender: UIButton) {
        if let manageAccountsPopUpVC :ManageAccountsPopUpVC =  ManageAccountsPopUpVC.instantiateViewControllerFromStoryboard() {
            
            manageAccountsPopUpVC.delegate = self
            //getting completion block on ManageAccounts button tap
            manageAccountsPopUpVC.navigateToManageAccountsScreen {
                
                //navigating to ManageAccountsVC to add/delete accounts
                if let manageAccountsVC :ManageAccountsVC = ManageAccountsVC.instantiateViewControllerFromStoryboard() {
                    // manageAccountsVC.delegate = self
                    self.navigationController?.pushViewController(manageAccountsVC, animated: true)
                }
                
            }
            self.presentPOPUP(manageAccountsPopUpVC, animated: true)
        }
    }
    
    @IBAction func userProfilePressed(_ sender: UIButton) {
        
        if let viewProfileVc :ViewProfileVC =  ViewProfileVC.instantiateViewControllerFromStoryboard() {
            
            self.navigationController?.pushViewController(viewProfileVc, animated: true)
            
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_")
        
        /* Menu is empty then show logout option */
        if AFUserSession.shared.appMenu?.menuVertical == nil ||
            (AFUserSession.shared.appMenu?.menuVertical?.count ?? 0) <= 0 {
            AFUserSession.shared.appMenu = AppMenusModel(menuHorizontal: nil, menuVertical: [MenuVertical(identifier: "logout", title: Localized("Title_Logout"), sortOrder: "0", iconName: "")])
        }
        
        menuTableView.reloadUserData(AFUserSession.shared.appMenu?.menuVertical)
        
        userNameLabel.text = AFUserSession.shared.userName()
        userPhoneNumberLabel.text = AFUserSession.shared.msisdn
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier : String) {
        
        switch identifier {
            
        case "notifications":
            // Load Notifications viewController
            if let notifications :NotificationsVC = NotificationsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(notifications, animated: true)
            }
            break
            
        case "contact_us":
            // Load Notifications viewController
            if let contactUsVC :ContactUsVC = ContactUsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(contactUsVC, animated: true)
            }
            break
            
        case "faq":
            // load FAQs viewController
            if let faqs :FAQsVC = FAQsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(faqs, animated: true)
            }
            break
            
        case "tutorial_and_faqs":
            //  load tutorials viewController
            if let tutorialsVC :TutorialsVC = TutorialsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(tutorialsVC, animated: true)
            }
            break
            
        case "settings":
            // load Settings viewController
            if let settingsVC :SettingsVC = SettingsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(settingsVC, animated: true)
            }
            break
            
            
        case "store_locator":
            //  load StoreLocator viewController
            if let store :StoreLocatorMainVC = StoreLocatorMainVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(store, animated: true)
            }
            break
            
            
        case "terms_and_conditions":
            //  load Terms&Conditions viewController
            if let termsAndConditionsVC :TermsAndConditionsVC = TermsAndConditionsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(termsAndConditionsVC, animated: true)
            }
            break
            
        case "live_chat":
            
            //  load LiveChatVC viewController
            
            if Connectivity.isConnectedToInternet ==  true {
                if AFUserSession.shared.liveChatObject == nil ,
                    let liveChatVC :LiveChatVC = LiveChatVC.instantiateViewControllerFromStoryboard() {
                    
                    AFUserSession.shared.liveChatObject = liveChatVC
                    self.navigationController?.pushViewController(liveChatVC, animated: true)
                    
                } else {
                    self.navigationController?.pushViewController(AFUserSession.shared.liveChatObject!, animated: true)
                }
                /*if let liveChatVC :LiveChatVC = LiveChatVC.instantiateViewControllerFromStoryboard() {
                    self.navigationController?.pushViewController(liveChatVC, animated: true)
                }*/
                
                //openLiveChatNow()
                
            } else {
                self.closeSideMenu(shouldSelectDashboard: false, animated: true)
                self.showErrorAlertWithMessage(message: Localized("Message_NoInternet"))
                AFUserSession.shared.liveChatObject = nil
            }
            break
            
        case "logout":
            
            self.closeSideMenu(shouldSelectDashboard: false, animated: true)
            
            // Show logout confirmation alert to User
            self.showLogOutAlert(msisdnCount:AFUserSession.shared.loggedInUsers?.users?.count ?? 0, logOutActionBlock: {
                /* Call logout API if user click on logout button */
                self.logOutUser()
            }, manageAccountActionBlock: {
                /* navigating to ManageAccountsVC to add/delete accounts */
                if let manageAccountsVC :ManageAccountsVC = ManageAccountsVC.instantiateViewControllerFromStoryboard() {
                    // manageAccountsVC.delegate = self
                    self.navigationController?.pushViewController(manageAccountsVC, animated: true)
                }
            })
            break
            
        default:
            self.closeSideMenu(shouldSelectDashboard: false, animated: true)
            break
        }
    }
    
    /**
     Provide image name against identifiers. which are mapped.
     
     - parameter identifier: key for mapped image.
     
     - returns: image name for mapped identifier.
     */
    func imageNameFor(Identifier identifier : String) -> String {
        
        switch identifier.lowercased() {
            
        case "notifications":
            return "sideMenuNotifications"
            
        case "store_locator":
            return "sideMenuStoreLocator"
            
        case "faq":
            return "sideMenuFaQs"
            
        case "tutorial_and_faqs":
            return "sideMenuTutorials"
            
        case "live_chat":
            return "sideMenuLiveChat"
            
        case "contact_us":
            return "sideMenuContactUs"
            
        case "terms_and_conditions":
            return "sideMenuTCs"
            
        case "settings":
            return "sideMenuSettings"
            
        case "logout":
            return "sideMenuLogOut"
            
        default:
            return ""
            
        }
    }
    
    /**
     close side menu.
     
     - returns: void
     */
    
    func closeSideMenu(shouldSelectDashboard:Bool = true, animated :Bool = false) {
        if let drawer = self.parent as? KYDrawerController,
            drawer.drawerState == .opened {
            drawer.setDrawerState(.closed, animated: animated)
            
            if shouldSelectDashboard {
                self.setSelectTabbarIndex(0)
            }
        }
    }
    
    /**
     Set tabbar selected index.
     
     - parameter selectedIndex: Index which need to be set as selected.
     
     - returns: void.
     */
    func setSelectTabbarIndex(_ selectedIndex: Int) {
        if let drawer = self.parent as? KYDrawerController ,
            let tabBarCtr = drawer.mainViewController as? UITabBarController {
            tabBarCtr.selectedIndex = selectedIndex
        }
    }
    
    
    //MARK: - API's Calls
    /**
     Logout user from current session.
     
     - returns: void
     */
    func logOutUser() {
        
        /* API call to Logout user */
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.logOut({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.removeAllActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self, {
                    // Clear user data
                    BaseVC.logout()
                })
                
            } else {
                // handling data from API response.
                if resultCode != Constants.AFAPIStatusCode.succes.rawValue {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc, {
                        // Clear user data
                        BaseVC.logout()
                    })
                } else {
                    // Clear user data
                    BaseVC.logout()
                }
            }
        })
    }
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension SideMenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AFUserSession.shared.appMenu?.menuVertical?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:SideMenuCell = tableView.dequeueReusableCell() {
            
            if let aMenuItem = AFUserSession.shared.appMenu?.menuVertical?[indexPath.row] {
                /* Set cell data and return */
                cell.optionNameLabel.text = aMenuItem.title
                
                let iconName = self.imageNameFor(Identifier: aMenuItem.identifier)
                cell.optionImageView.image = UIImage.imageFor(name: iconName)
                
            } else {
                /* Set empty cell data and return */
                cell.optionNameLabel.text = ""
                cell.optionImageView.image = UIImage.imageFor(name: "")
            }
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Redirect user to selected screen
        redirectUserToSelectedController(Identifier: AFUserSession.shared.appMenu?.menuVertical?[indexPath.row].identifier ?? "")
        
        // Deselect user selected row
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK:- UsersSwitchDelegate
extension SideMenuVC: UsersSwitchDelegate {
    func userDidSelect(_ newUser: UserDataModel?) {
        
        if let newUserInfo = newUser,
            (newUserInfo.userInfo?.msisdn?.isEqual(AFUserSession.shared.msisdn, ignorCase: true) ?? true) == false {
            
            if let drawer = self.parent as? KYDrawerController,
                let tabbarVC = drawer.mainViewController as? TabbarController {
                
                /* Clear user session objects befor switch */
                AFUserSession.shared.clearUserSessionObjectsForCurrentUser()
                
                /* Switch user to new user */
                AFUserInfoUtilities.switchToUser(newMSISDN: newUserInfo.userInfo?.msisdn ?? "", selectedUserInfo: newUserInfo)
                
                /* Cancel all API requests */
                AFAPIClient.shared.cancelAllRequests()
                
                /* Load user information and close side menu */
                tabbarVC.loadCustomerInfo(callResumeAPI: true)
                self.closeSideMenu(shouldSelectDashboard: true, animated: true)
            }
            
        }
    }
}

// MARK: LiveChatDelegate
extension SideMenuVC: LiveChatDelegate {
    
    
    func openLiveChatNow() {
        LiveChat.licenseId = "12862467"
        if AFLanguageManager.userSelectedLanguage() == .english {
            LiveChat.groupId = "0"
        } else if AFLanguageManager.userSelectedLanguage() == .azeri {
            LiveChat.groupId = "1"
        } else if AFLanguageManager.userSelectedLanguage() == .russian {
            LiveChat.groupId = "2"
        }
        
        
        LiveChat.name = AFUserSession.shared.userInfo?.firstName // User name and email can be provided LiveChat.groupId = "1"if known
        LiveChat.email = AFUserSession.shared.userInfo?.email
        LiveChat.setVariable(withKey:"Phone number", value:AFUserSession.shared.msisdn)
        LiveChat.presentChat()
    }

    func received(message: LiveChatMessage) {
        if (!LiveChat.isChatPresented) {
            // Notifying user
            let alert = UIAlertController(title: Localized("Title_Attention"), message: message.text, preferredStyle: .alert)
            let chatAction = UIAlertAction(title: "Go to Chat", style: .default) { alert in
                if !LiveChat.isChatPresented {
                    LiveChat.presentChat()
                }
            }
            alert.addAction(chatAction)

            let cancelAction = UIAlertAction(title: Localized("BtnTitle_CANCEL"), style: .cancel)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

