//
//  FAQsVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/25/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import KYDrawerController
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class FAQsVC: BaseVC {
    
    //MARK: - Properties
    var showSearch : Bool = true
    var faq : [FAQList]?
    var qaList : [QAList]?
    var filterQaList : [QAList]?
    
    //MARK: - IBOutlet
    
    @IBOutlet var dropDown      : UIDropDown!
    @IBOutlet var dropDownTitle : AFLabel!
    @IBOutlet var tableView     : AFAccordionTableView!
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interactivePop()
        
        loadViewLayout()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowMultipleSectionsOpen = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180
        tableView.allowsSelection = false
        
        // reload data first
        self.tableView.reloadUserData(filterQaList)
        // Load FAQs data
        loadFAQsData()
    }
    
    
    //MARK: - FUNCTIONS
    func loadViewLayout() {
        self.titleLabel?.text = Localized("Title_FAQ")
        dropDownTitle.text = Localized("Title_SelectCategory")
    }
    
    //Initialze / Setup dropdown
    func setDropDown() {
        
        if let aFAQ = self.faq?.first {
            self.dropDown.placeholder = aFAQ.title
            self.qaList = aFAQ.qaList
            self.filterQaList = self.qaList
            
            self.tableView.reloadUserData(filterQaList)
            
        }
        
        var titles : [String] = []
        
        faq?.forEach({ (aFAQList) in
            titles.append(aFAQList.title)
        })
        
        self.dropDown.dataSource = titles
        
        
        self.dropDown.didSelectOption { (index, option) in
            
            self.qaList = self.faq?[index].qaList
            self.filterQaList = self.qaList
            
            self.tableView.reloadUserData(self.filterQaList)
            
        }
        
    }
    
    ///Collapse All Sections
    private func collapseAllSections() {
        if let count = self.filterQaList?.count {
            for i in 0..<count {
                if tableView.isSectionOpen(i) {
                    tableView.toggleSection(i)
                }
            }
        }
    }
    
    ///Load FAQ Data
    func loadFAQsData() {
        // Parssing response data
        if let faqResponseHandler :FAQListHandler = FAQListHandler.loadFromUserDefaults(key: APIsType.faqDetails.selectedLocalizedAPIKey()){
            
            self.faq = faqResponseHandler.faqList
            self.qaList = self.faq?[0].qaList
            self.filterQaList = self.qaList
            
            self.tableView.reloadUserData(filterQaList)
            
            self.setDropDown()
            
        } else {
            FAQsAPICall()
        }
    }
    
    
    //MARK: - IBActions
    
    //MARK: - API Calls
    
    /// Call 'FAQs' API.
    ///
    /// - returns: Void
    func FAQsAPICall() {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.getFAQs({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let faqResponseHandler = Mapper<FAQListHandler>().map(JSONObject:resultData) {
                        
                        if let faqList = faqResponseHandler.faqList {
                            // Saving Supplementary Offers data in user defaults
                            faqResponseHandler.saveInUserDefaults(key: APIsType.faqDetails.selectedLocalizedAPIKey())
                            
                            self.faq = faqList
                            self.qaList = self.faq?[0].qaList
                            self.filterQaList = self.qaList
                            
                            self.setDropDown()
                        }
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Reload table View data
            self.tableView.reloadUserData(self.filterQaList)
        })
    }
}

//MARK: - Table View Delegates

extension FAQsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let aQaList = filterQaList {
            return aQaList.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerView:ExpandableTitleView = tableView.dequeueReusableHeaderFooterView() {
            
            headerView.titleLabel.text = filterQaList?[section].question
            headerView.titleLabel.setLabelFontType(.body)
            
            /* Check if section is expanded then set expaned image else set unexpended image */
            headerView.setExpandStatus(filterQaList?[section].isSectionExpanded ?? false)
            
            return headerView
        } else {
            return AFAccordionTableViewHeaderView()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:FAQsCell = tableView.dequeueReusableCell() {
            cell.setDescriptionText(filterQaList?[indexPath.section].answer)
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
}

// MARK: - <AFAccordionTableViewDelegate>

extension FAQsVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleView {
            
            headerView.setExpandStatus(true)
            filterQaList?[section].isSectionExpanded = true
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleView {
            headerView.setExpandStatus(false)
            filterQaList?[section].isSectionExpanded = false
        }
    }
}
