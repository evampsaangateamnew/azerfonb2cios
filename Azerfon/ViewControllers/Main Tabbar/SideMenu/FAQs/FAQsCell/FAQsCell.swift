//
//  FAQsCell.swift
//  Nar+
//
//  Created by Saad Riaz on 6/5/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class FAQsCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet var descriptionLabel: AFLabel!
    
    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setDescriptionText(_ descriptionText : String?) {
        self.descriptionLabel.text = descriptionText ?? ""
    }
    
}
