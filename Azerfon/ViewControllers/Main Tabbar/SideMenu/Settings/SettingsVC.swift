//
//  SettingsVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/26/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
import FirebaseMessaging

class SettingsVC: BaseVC {
    
    //MARK: - Properties
    var ringingType : Constants.AFNotificationSoundType = .Tone
    var isNotificationEnabled : Bool = false
    
    //MARK: - IBOutlet
    @IBOutlet var changeLanguageTitleLabel        : AFLabel!
    @IBOutlet var languageNameLabel               : AFLabel!
    @IBOutlet var notificationTitleLabel          : AFLabel!
    @IBOutlet var notificationDescriptionLabel    : AFLabel!
    @IBOutlet var notificationSettingTitleLabel   : AFLabel!
    @IBOutlet var notificationToneLabel           : AFLabel!
    @IBOutlet var notificationVibrateLabel        : AFLabel!
    @IBOutlet var notificationMuteLabel           : AFLabel!
    @IBOutlet var changePasswordTitleLabel        : AFLabel!
    @IBOutlet var fingerPrintTitleLabel           : AFMarqueeLabel!
    @IBOutlet var appVersionTitleLabel            : AFLabel!
    @IBOutlet var appVersionLabel                 : AFLabel!
    
    @IBOutlet var notificationsTuneButton         : UIButton!
    @IBOutlet var notificationsVibrateButton      : UIButton!
    @IBOutlet var notificationsMuteButton         : UIButton!
    
    @IBOutlet var notificationSwitch              : UISwitch!
    @IBOutlet var biometricSwitch                 : UISwitch!


    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationSwitch.addTarget(self, action:#selector(notificationSwitchValueChanged) , for: UIControl.Event.valueChanged)
        biometricSwitch.addTarget(self, action:#selector(biometricSwitchValueChanged) , for: UIControl.Event.valueChanged)
        
        // get Notifications Configurations from server
        loadNotificationsConfigurations()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // Loading intial layout Text
        loadViewContent()
        
        biometricSwitch.isOn = AFUserSession.shared.isBiometricEnabled()
    }
    
    //MARK: - IBAction
    @IBAction func changeLanguagePressed(_ sender:UIButton) {
        if let changelang :LanguageSelectionVC =  LanguageSelectionVC.fromNib() {
            
            changelang.setLanguageSelectionAlert(AFLanguageManager.userSelectedLanguage()) { (newSelectedLanguage) in
                
                if AFLanguageManager.userSelectedLanguage() != newSelectedLanguage {
                    // Setting user language
                    AFLanguageManager.setUserLanguage(selectedLanguage: newSelectedLanguage)
                    
                    // Reloading layout Text accourding to seletcted language
                    self.loadViewContent()
                    
                    /* Setting check to define user has changed language. */
                    AFUserSession.shared.shouldCallAndUpdateAppResumeData = true
                    AFUserSession.shared.liveChatObject = nil
                }
            }
            
            self.presentPOPUP(changelang, animated: true, completion:  nil)
        }
    }
    
    @IBAction func notificationsTonePressed(_ sender: Any) {
        
        notificationSoundSetting(ringingType: .Tone)
    }
    
    @IBAction func vibratePressed(_ sender: UIButton) {
        
        notificationSoundSetting(ringingType: .Vibrate)
    }
    
    @IBAction func mutePressed(_ sender: UIButton) {
        
        notificationSoundSetting(ringingType: .Mute)
    }
    
    @IBAction func changePasswordPressed(_ sender: UIButton) {
        if let changePassword :ChangePasswordVC = ChangePasswordVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(changePassword, animated: true)
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text                = Localized("Title_Settings")
        changeLanguageTitleLabel.text        = Localized("Title_ChangeLanguage")
        languageNameLabel.text               = Localized("Info_Language")
        notificationTitleLabel.text          = Localized("Title_Notifications")
        notificationDescriptionLabel.text    = Localized("Description_Notifications")
        notificationSettingTitleLabel.text   = Localized("Title_NotificationsSetting")
        notificationToneLabel.text           = Localized("Info_NotificationSettingTune")
        notificationVibrateLabel.text        = Localized("Info_NotificationSettingVibrate")
        notificationMuteLabel.text           = Localized("Info_NotificationSettingMute")
        changePasswordTitleLabel.text        = Localized("Title_ChangePassword")
        fingerPrintTitleLabel.text           = Localized("Title_Biometric")
        appVersionTitleLabel.text            = Localized("Title_AppVersion")
        
        appVersionLabel.text                 = UIDevice.appVersion()
    }
    
    
    func loadNotificationsConfigurations() {
        
        //let jsonString = "{\"ringingStatus\":\"tone\",\"isEnable\":\"1\"}"
        // Parssing response data
        if let responseHandler :AddFCM = AddFCM.loadFromUserDefaults(key: APIsType.notificationConfigurations.selectedLocalizedAPIKey()){
            
            self.isNotificationEnabled = self.checkIsEnableFromString(string: responseHandler.isEnable)
            self.ringingType = Constants.AFNotificationSoundType(rawValue: responseHandler.ringingStatus) ?? .Tone
            
            // Set Notification ON/Off setting
            self.changeNotificationSwitchLayout(isEnable: self.isNotificationEnabled)
            // Set  Notification Sound setting
            self.notificationSoundSettingLayout(ringingType: self.ringingType)
        } else {
            // Set Notification ON/Off setting
            self.changeNotificationSwitchLayout(isEnable: false)
            // Set  Notification Sound setting
            self.notificationSoundSettingLayout(ringingType: .Tone)
            
            self.showActivityIndicator()
        }
        
        getNotificationSettings()
    }
    
    @objc func notificationSwitchValueChanged() {
        
        changeNotificationState(isEnable: notificationSwitch.isOn, ringingStatus: ringingType)
        changeNotificationSwitchLayout(isEnable: notificationSwitch.isOn)
        
    }
    
    @objc func biometricSwitchValueChanged() {
        UserDefaults.saveBoolForKey(boolValue: biometricSwitch.isOn, forKey: Constants.K_IsBiometricEnabled)
    }
    
    func changeNotificationSwitchLayout(isEnable: Bool) {
     
        if isEnable {
            
            notificationSwitch.isOn = true
            notificationTitleLabel.isEnabled = true
            notificationDescriptionLabel.isEnabled = true
            
            notificationsVibrateButton.isEnabled = true
            notificationsTuneButton.isEnabled = true
            notificationsMuteButton.isEnabled = true
            
        } else {
            
            notificationSwitch.isOn = false
            
            notificationsVibrateButton.isEnabled = false
            notificationsTuneButton.isEnabled = false
            notificationsMuteButton.isEnabled = false
        }
    }
    
    func notificationSoundSetting(ringingType: Constants.AFNotificationSoundType) {
        
        // Check if user can chage notification sound type
        if !isNotificationEnabled {
            return
        } else {
            
            notificationSoundSettingLayout(ringingType: ringingType)
            changeNotificationState(isEnable: isNotificationEnabled, ringingStatus: ringingType)
        }
    }
    
    func notificationSoundSettingLayout(ringingType: Constants.AFNotificationSoundType) {
        
        notificationsTuneButton .setImage(#imageLiteral(resourceName: "radioUnSelected"), for: UIControl.State.normal)
        notificationsVibrateButton .setImage(#imageLiteral(resourceName: "radioUnSelected"), for: UIControl.State.normal)
        notificationsMuteButton .setImage(#imageLiteral(resourceName: "radioUnSelected"), for: UIControl.State.normal)
        
        switch ringingType {
            
        case .Tone:
            notificationsTuneButton .setImage(#imageLiteral(resourceName: "radioSelected"), for: UIControl.State.normal)
        case .Vibrate:
            notificationsVibrateButton .setImage(#imageLiteral(resourceName: "radioSelected"), for: UIControl.State.normal)
        case .Mute:
            notificationsMuteButton .setImage(#imageLiteral(resourceName: "radioSelected"), for: UIControl.State.normal)
        }
    }
    
    
    func checkIsEnableFromString(string: String) -> Bool {
        
        if string == "1" {
            return true
        } else {
            return false
        }
    }
    
    
    //MARK: - API Calls
    /// Call 'changeNotificationState' API .
    ///
    /// - returns: Void
    
    func changeNotificationState(isEnable : Bool, ringingStatus: Constants.AFNotificationSoundType) {
        
        self.showActivityIndicator()
        _ = AFAPIClient.shared.addFCMId(fcmKey: Messaging.messaging().fcmToken ?? "", ringingStatus: ringingStatus ,isEnable: isEnable, isFromLogin: false, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let addFCMResponseHandler = Mapper<AddFCM>().map(JSONObject:resultData) {
                        
                        // Save Notification configration
                        addFCMResponseHandler.saveInUserDefaults(key: APIsType.notificationConfigurations.selectedLocalizedAPIKey())
                        
                        // Updating selected Values
                        self.isNotificationEnabled = self.checkIsEnableFromString(string: addFCMResponseHandler.isEnable)
                        self.ringingType = Constants.AFNotificationSoundType(rawValue: addFCMResponseHandler.ringingStatus) ?? .Tone
                        
                        // Showing alert
                        self.showSuccessAlertWithMessage(message: resultDesc, {
                            // Check if user enable or disabled notification swiftch
                            if self.isNotificationEnabled {
                                // Check for Notification setting
                                self.isUserEnabledNotification(completionHandler: { (isEnabled) in
                                    if isEnabled != true {
                                        
                                        DispatchQueue.main.async {
                                            self.showAccessAlert(message: Localized("Message_EnableRemoteNotification"))
                                        }
                                    }
                                })
                            }
                        })
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                } else {
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Will change Notification ON/Off setting in case of failure
            self.changeNotificationSwitchLayout(isEnable: self.isNotificationEnabled)
            // Will change Notification Sound setting in case of failure
            self.notificationSoundSettingLayout(ringingType: self.ringingType)
        })
    }
    
    func getNotificationSettings() {
        
        _ = AFAPIClient.shared.addFCMId(fcmKey: Messaging.messaging().fcmToken ?? "", ringingStatus: self.ringingType, isEnable: true, isFromLogin: true, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    let extractedExpr: Mapper<AddFCM> = Mapper<AddFCM>()
                    if let addFCMResponseHandler = extractedExpr.map(JSONObject:resultData) {
                        
                        // Save Notification configration
                        addFCMResponseHandler.saveInUserDefaults(key: APIsType.notificationConfigurations.selectedLocalizedAPIKey())
                        // Updating selected Values
                        self.isNotificationEnabled = self.checkIsEnableFromString(string: addFCMResponseHandler.isEnable)
                        self.ringingType = Constants.AFNotificationSoundType(rawValue: addFCMResponseHandler.ringingStatus) ?? .Tone
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                    
                } else {
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

