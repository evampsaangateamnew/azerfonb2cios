//
//  ChangePasswordVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/28/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ChangePasswordVC: BaseVC {
    
    //MARK: - Properties
    
    //MARK: - IBOutlet
    @IBOutlet var currentPasswordTextField    : AFTextField!
    @IBOutlet var newPasswordTextField        : AFTextField!
    @IBOutlet var confirmPasswordTextField    : AFTextField!
    
    @IBOutlet var updateButton                : AFButton!
    
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set UITextfields delegates
        currentPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        confirmPasswordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func updatePressed(_ sender: Any) {
        
        if AFUserSession.shared.changePasswordRequestCount < 5 {
            
            var oldPassword : String = ""
            var newPassword : String = ""
            var confirmNewPassword : String = ""
            
            // Current Password validation
            if let oldPasswordText = currentPasswordTextField.text {
                
                let passwordStrength:Constants.AFPasswordStrength = AFUtilities.determinePasswordStrength(Text: oldPasswordText)
                
                switch passwordStrength {
                    
                case Constants.AFPasswordStrength.didNotMatchCriteria :
                    self.showErrorAlertWithMessage(message: Localized("Message_InvalidCurrentPassword"))
                    return
                case Constants.AFPasswordStrength.Week, Constants.AFPasswordStrength.Medium, Constants.AFPasswordStrength.Strong:
                    oldPassword = oldPasswordText
                }
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidCurrentPassword"))
                return
            }
            
            // Password validation
            if let passwordText = newPasswordTextField.text {
                
                let passwordStrength:Constants.AFPasswordStrength = AFUtilities.determinePasswordStrength(Text: passwordText)
                
                switch passwordStrength {
                    
                case Constants.AFPasswordStrength.didNotMatchCriteria:
                    self.showErrorAlertWithMessage(message: Localized("Message_InvalidPasswordComplexity"))
                    return
                case Constants.AFPasswordStrength.Week, Constants.AFPasswordStrength.Medium, Constants.AFPasswordStrength.Strong:
                    newPassword = passwordText
                }
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidPasswordComplexity"))
                return
            }
            
            // Confirm Password validation
            if let confirmPasswordText = confirmPasswordTextField.text,
                confirmPasswordText == newPassword {
                
                confirmNewPassword = confirmPasswordText
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidConfirmPassword"))
                return
            }
            
            // Hiding Key board
            _ = currentPasswordTextField.resignFirstResponder()
            _ = newPasswordTextField.resignFirstResponder()
            _ = confirmPasswordTextField.resignFirstResponder()
            
            if AFUserSession.shared.aSecretKey.isBlank == false {
                
                changePasswordUserAPICall(AFUserSession.shared.msisdn, OldPassword: oldPassword.aesEncrypt(key: AFUserSession.shared.aSecretKey), NewPassword: newPassword.aesEncrypt(key: AFUserSession.shared.aSecretKey), ConfirmNewPassword: confirmNewPassword.aesEncrypt(key: AFUserSession.shared.aSecretKey))
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_GenralError"))
            }
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_ChangePassword")
        updateButton.setTitle(Localized("BtnTitle_UpdatePassword"), for: UIControl.State.normal)
        
        currentPasswordTextField.placeholder = Localized("PlaceHolder_CurrentPass")
        newPasswordTextField.placeholder = Localized("PlaceHolder_NewPass")
        confirmPasswordTextField.placeholder = Localized("PlaceHolder_ConfirmPass")
    }
    
    func redirectToManageNumber() {
        
        /* navigate to Manage AccountsVC to add/delete accounts */
        if let manageAccountsVC :ManageAccountsVC = ManageAccountsVC.instantiateViewControllerFromStoryboard(), (AFUserSession.shared.loggedInUsers?.users?.count ?? 0) >= 1 {
            manageAccountsVC.canNavigateBack = false
            manageAccountsVC.descriptionMessage = Localized("Message_AccountRemovedDescription")
            self.navigationController?.pushViewController(manageAccountsVC, animated: true)
        } else {
            BaseVC.logout()
        }
    }
    
    //MARK: - API's Calls
    /// Call 'changePassword' API with the specified `OldPassword`, 'NewPassword' and `ConfirmNewPassword`.
    ///
    /// - parameter OldPassword:    The OldPassword of user.
    /// - parameter NewPassword:    The User new Password for account.
    /// - parameter ConfirmNewPassword:  The User Confirm new Password for account.
    ///
    /// - returns: Void
    func changePasswordUserAPICall(_ userName :String, OldPassword oldPassword : String , NewPassword newPassword : String, ConfirmNewPassword confirmNewPassword : String) {
        
        self.showActivityIndicator()
        _ = AFAPIClient.shared.changePassword(userName, OldPassword: oldPassword, NewPassword:newPassword , ConfirmNewPassword: confirmNewPassword,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if resultCode == Constants.AFAPIStatusCode.wrongAttemptToChangePassword.rawValue {
                
                AFUserSession.shared.changePasswordRequestCount = AFUserSession.shared.changePasswordRequestCount + 1
                if AFUserSession.shared.changePasswordRequestCount > 4 {
                    
                    /* Remove current user info */
                    AFUserInfoUtilities.removeCustomerDataOf(msisdn: AFUserSession.shared.msisdn)
                    
                    // Reset counter
                    AFUserSession.shared.changePasswordRequestCount = 0
                    
                    self.redirectToManageNumber()
                    
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            } else if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    /* Remove current user info */
                    AFUserInfoUtilities.removeCustomerDataOf(msisdn: AFUserSession.shared.msisdn)
                    
                    // Parsing response data
                    if let changePasswordHandler = Mapper<MessageResponse>().map(JSONObject:resultData) {
                        self.showSuccessAlertWithMessage(message: changePasswordHandler.message, {
                            
                            self.redirectToManageNumber()
                        })
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK:- Textfield delagates
extension ChangePasswordVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length
        
        // Allow charactors check
        let allowedCharactors = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharacters
        let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
        let characterSet = CharacterSet(charactersIn: string)
        
        if allowedCharacters.isSuperset(of: characterSet) != true {
            
            return false
        }
        
        // For other then setPassword textfield.
        return newLength <= Constants.maxPasswordLength // Bool
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == currentPasswordTextField {
            newPasswordTextField.becomeFirstResponder()
            return false
        } else if textField == newPasswordTextField {
            confirmPasswordTextField.becomeFirstResponder()
            return false
        }else if textField == confirmPasswordTextField {
            _ = textField.resignFirstResponder()
            updatePressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
