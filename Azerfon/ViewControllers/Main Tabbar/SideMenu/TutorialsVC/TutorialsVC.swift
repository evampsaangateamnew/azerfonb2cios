//
//  TutorialsVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/29/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class TutorialsVC: BaseVC {
    
    //MARK: - Properties
    var avPlayer: AVPlayer!
    var playerLayer: AVPlayerLayer!
    private var AVPlayerDemoPlaybackViewControllerStatusObservationContext = 0
    
    //MARK: - IBOutlet
    @IBOutlet var skipButton        : AFButton!

    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //adding observer
        NotificationCenter.default.addObserver(self, selector: #selector(resumePlayer(note:)), name: NSNotification.Name(Constants.kAppDidBecomeActive), object: nil)
        
        // Play Tutorial Video
        self.playTutorailVideo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func skipButtonPressed(_ sender: AnyObject) {
        
        self.cancelVideoAndRedirect()
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_Tutorials")
        
        self.skipButton.setTitle(Localized("Btn_Title_Skip"), for: .normal)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func playTutorailVideo() {
        
        if Connectivity.isConnectedToInternet {
            
            var videoUrlString = "https://plusapp.nar.az/azerfon/videos/";
            
            if (AFUserSession.shared.subscriberType() == .prepaid) {
                videoUrlString = videoUrlString + "prepaid";
            } else {
                videoUrlString = videoUrlString + "postpaid";
            }
            
            switch (AFLanguageManager.userSelectedLanguage()) {
                
            case .russian:
                videoUrlString = videoUrlString + "_ru.mp4";
            case .english:
                videoUrlString = videoUrlString + "_en.mp4";
            case .azeri:
                videoUrlString = videoUrlString + "_az.mp4";
            }
            print("playing url: \(videoUrlString)")
            
            if let videoURL = URL(string:videoUrlString) {
                self.showActivityIndicator()
                DispatchQueue.main.async {
                    
                    self.avPlayer = AVPlayer(url: videoURL)
                    
                    self.playerLayer = AVPlayerLayer(player: self.avPlayer)
                    // self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                    self.playerLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                    self.view.layer.addSublayer(self.playerLayer)
                    self.avPlayer.play()
                    self.view.bringSubviewToFront(self.skipButton)
                    
                    
                    self.avPlayer.currentItem?.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: &self.AVPlayerDemoPlaybackViewControllerStatusObservationContext)
                    
                    
                    NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(note:)),
                                                           name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayer.currentItem)
                    
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "tutorial_begin", contentType:"begin" , successStatus:"1" )
                }
            } else {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "tutorial_begin", contentType:"begin" , successStatus:"0" )
                
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError")) {
                    self.cancelVideoAndRedirect()
                }
            }
        } else {
            //EventLog
            AFAnalyticsManager.logEvent(screenName: "tutorial_begin", contentType:"begin" , successStatus:"0" )
            
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError")) {
                self.cancelVideoAndRedirect()
            }
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        //EventLog
        AFAnalyticsManager.logEvent(screenName: "tutorial_complete", contentType:"complete" , successStatus:"1" )
        
        self.cancelVideoAndRedirect()
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if context == &AVPlayerDemoPlaybackViewControllerStatusObservationContext {
            if let change = change as? [NSKeyValueChangeKey: Int]
            {
                if let status = change[NSKeyValueChangeKey.newKey] {
                    self.hideActivityIndicator()
                    switch status {
                    case AVPlayer.Status.unknown.rawValue:
                        print("The status of the player is not yet known because it has not tried to load new media resources for playback")
                        self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError")) {
                            self.cancelVideoAndRedirect()
                        }
                        
                    case AVPlayer.Status.readyToPlay.rawValue:
                        print("The player is Ready to Play")
                        
                    case AVPlayer.Status.failed.rawValue:
                        print("The player failed to load the video")
                        
                        //EventLog
                        AFAnalyticsManager.logEvent(screenName: "tutorial_begin", contentType:"begin" , successStatus:"0" )
                        
                        self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError")) {
                            self.cancelVideoAndRedirect()
                        }
                        
                    default:
                        print("Other status")
                        //EventLog
                        AFAnalyticsManager.logEvent(screenName: "tutorial_begin", contentType:"begin" , successStatus:"0" )
                        
                        self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError")) {
                            self.cancelVideoAndRedirect()
                        }
                    }
                }
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    @objc func resumePlayer(note: NSNotification) {
        
        if self.avPlayer != nil {
            
            self.avPlayer.play()
        } else {
            
            self.playTutorailVideo()
        }
    }
    
    func cancelVideoAndRedirect() {
        
        if self.avPlayer != nil {
            self.avPlayer.currentItem?.removeObserver(self, forKeyPath: "status")
            self.avPlayer.replaceCurrentItem(with: nil)
            self.avPlayer.pause()
            self.playerLayer.removeAllAnimations()
            self.playerLayer.removeFromSuperlayer()
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayer.currentItem)
            
            self.avPlayer = nil
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}

