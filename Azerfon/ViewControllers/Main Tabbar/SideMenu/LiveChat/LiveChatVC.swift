/*//
//  LiveChatVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/29/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import WebKit

class LiveChatVC: BaseVC {
    
    //MARK: - Properties
    
    //MARK: - IBOutlet
    @IBOutlet var myWebView: UIWebView!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
        
        if let myURLRequest = Constants.liveChatUrlRequest {
            myWebView.allowsInlineMediaPlayback = true
            myWebView.mediaPlaybackRequiresUserAction = false
            myWebView.delegate = self
            
            //1. Load web site into my web view
            myWebView.loadRequest(myURLRequest)
            
        } else {
            self.clearWebView()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWebView), name: NSNotification.Name(Constants.kAppDidBecomeActive), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        if let myURL = URL(string: Constants.liveChatURL) {
            
            if myWebView.request?.url != myURL {
                myWebView.stopLoading()
                if let myURLRequest = Constants.liveChatUrlRequest {
                    
                    myWebView.loadRequest(myURLRequest)
                    
                } else {
                    self.hideActivityIndicator()
                    self.clearWebView()
                }
            }
        }
    }
    
    
    //MARK: - IBAction
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_LiveChat")
    }
    
    // Remove notification
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func clearWebView() {
        
        if let clearURL = URL(string: "about:blank") {
            myWebView.loadRequest(URLRequest(url: clearURL))
        }
        
        self.showErrorAlertWithMessage(message: Localized("Message_FailureLiveChat"))
        self.myWebView.showDescriptionViewWithImage(description: Localized("Message_FailureLiveChat"))
        
        AFUserSession.shared.liveChatObject = nil
    }
    
    @objc func reloadWebView() {
        
        if let myURLRequest = Constants.liveChatUrlRequest {
            
            myWebView.loadRequest(myURLRequest)
        }
    }
    
    //MARK: - API's Calls
}

extension LiveChatVC : UIWebViewDelegate {
    
    private func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        
        if navigationType == .linkClicked  {
            if let newURL = request.url,
                UIApplication.shared.canOpenURL(newURL) {
                
                self.openURLInSafari(urlString: newURL.absoluteString)
                return false
                
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.showActivityIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.removeAllActivityIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        if (error as NSError).code !=  NSURLErrorCancelled {
            
            self.removeAllActivityIndicator()
            self.clearWebView()
        }
    }
}

*/

 //
 //  LiveChatVC.swift
 //  Azerfon
 //
 //  Created by Muhammad Waqas on 4/29/19.
 //  Copyright © 2019 Evamp&Saanga. All rights reserved.
 //

 import UIKit
 import WebKit

 class LiveChatVC: BaseVC {
     
     //MARK: - Properties
     
     //MARK: - IBOutlet
     @IBOutlet var myWebView: UIWebView!
     
     var link = URL(string:"google.com")!
     
     //MARK:- ViewController Methods
     override func viewDidLoad() {
         super.viewDidLoad()
         
         WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
         
         /*if let myURLRequest = Constants.liveChatUrlRequest {
             myWebView.allowsInlineMediaPlayback = true
             myWebView.mediaPlaybackRequiresUserAction = false
             myWebView.delegate = self
             
             //1. Load web site into my web view
             myWebView.loadRequest(myURLRequest)
             
         } else {
             self.clearWebView()
         }*/
         myWebView.allowsInlineMediaPlayback = true
         myWebView.mediaPlaybackRequiresUserAction = false
         myWebView.delegate = self
         
         NotificationCenter.default.addObserver(self, selector: #selector(loadWebViewNow), name: NSNotification.Name(Constants.kAppDidBecomeActive), object: nil)
         
         DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
             self.loadWebViewNow()
         }
     }
     
     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
         self.interactivePop(true)
         loadViewContent()
         /*if let myURL = URL(string: Constants.liveChatURL) {
             
             if myWebView.request?.url != myURL {
                 myWebView.stopLoading()
                 if let myURLRequest = Constants.liveChatUrlRequest {
                     
                     myWebView.loadRequest(myURLRequest)
                     
                 } else {
                     self.hideActivityIndicator()
                     self.clearWebView()
                 }
             }
         }*/
     }
     
     
    @objc func loadWebViewNow () {
         WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
         
         if AFLanguageManager.userSelectedLanguage() == .english {
             link = URL(string:"https://secure-fra.livechatinc.com/licence/12830742/v2/open_chat.cgi?name=\(AFUserSession.shared.userName().urlEncode)&email=\(AFUserSession.shared.userInfo?.email ?? "")&params=PhoneNumber%3D\(AFUserSession.shared.msisdn)%26channel=nar&group=3")!
         } else if AFLanguageManager.userSelectedLanguage() == .russian {
             link = URL(string:"https://secure-fra.livechatinc.com/licence/12830742/v2/open_chat.cgi?name=\(AFUserSession.shared.userName().urlEncode)&email=\(AFUserSession.shared.userInfo?.email ?? "")&params=PhoneNumber%3D\(AFUserSession.shared.msisdn)%26channel=nar&group=4")!
         } else if AFLanguageManager.userSelectedLanguage() == .azeri {
             link = URL(string:"https://secure-fra.livechatinc.com/licence/12830742/v2/open_chat.cgi?name=\(AFUserSession.shared.userName().urlEncode)&email=\(AFUserSession.shared.userInfo?.email ?? "")&params=PhoneNumber%3D\(AFUserSession.shared.msisdn)%26channel=nar&group=5")!
         }
         
         let myURLRequest = URLRequest(url: link)
             
         //1. Load web site into my web view
         myWebView.loadRequest(myURLRequest)
         
     }
     
     
     //MARK: - IBAction
     
     //MARK: - Functions
     /**
      Set localized text in viewController
      */
     func loadViewContent() {
         self.titleLabel?.text = Localized("Title_LiveChat")
     }
     
     // Remove notification
     deinit {
         NotificationCenter.default.removeObserver(self)
     }
     
     func clearWebView() {
         
         if let clearURL = URL(string: "about:blank") {
             myWebView.loadRequest(URLRequest(url: clearURL))
         }
         
         self.showErrorAlertWithMessage(message: Localized("Message_FailureLiveChat"))
         self.myWebView.showDescriptionViewWithImage(description: Localized("Message_FailureLiveChat"))
         
         AFUserSession.shared.liveChatObject = nil
     }
     
     @objc func reloadWebView() {
         
         if let myURLRequest = Constants.liveChatUrlRequest {
             
             myWebView.loadRequest(myURLRequest)
         }
     }
     
     //MARK: - API's Calls
 }

 extension LiveChatVC : UIWebViewDelegate {
     
     private func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
         
         if navigationType == .linkClicked  {
             if let newURL = request.url,
                 UIApplication.shared.canOpenURL(newURL) {
                 
                 self.openURLInSafari(urlString: newURL.absoluteString)
                 return false
                 
             } else {
                 return true
             }
         } else {
             return true
         }
     }
     
     func webViewDidStartLoad(_ webView: UIWebView) {
         self.showActivityIndicator()
     }
     
     func webViewDidFinishLoad(_ webView: UIWebView) {
         self.removeAllActivityIndicator()
     }
     
     func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
         
         if (error as NSError).code !=  NSURLErrorCancelled {
             
             self.removeAllActivityIndicator()
             self.clearWebView()
         }
     }
 }

 
