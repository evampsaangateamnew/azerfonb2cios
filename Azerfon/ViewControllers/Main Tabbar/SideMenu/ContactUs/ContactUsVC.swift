//
//  ContactUsVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/25/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import MessageUI
import ObjectMapper

class ContactUsVC: BaseVC {

    //MARK: - Properties
    var contactUsDetail = ContactUsModel()
    
    //MARK: - IBOutlet
    @IBOutlet var addressLabel               : AFLabel!
    
    @IBOutlet var headOfficeTitleLabel       : AFLabel!
    @IBOutlet var infoCenterTitleLabel       : AFLabel!
    @IBOutlet var socialMediaTitleLabel      : AFLabel!
    @IBOutlet var inviteFriendsTitleLabel    : AFLabel!
    
    @IBOutlet var helpLineNumberButton       : AFButton!
    @IBOutlet var contactNumberButton        : AFButton!
    @IBOutlet var emailButton                : AFButton!
    @IBOutlet var websiteButton              : AFButton!
    @IBOutlet var sendButton                 : AFButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /* loading user initial inforamtion */
        loadViewContent()
        
        /* API call to get contact us information */
        getContactUsDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
    }
    
    //MARK: - IBAction
 /*   @IBAction func headOfficeLocationBtnPressed(_ sender: AFButton) {
        
        let lattitude = contactUsDetail.addressLat ?? ""
        let longitude = contactUsDetail.addressLong ?? ""
        
        
        
        if lattitude.isBlank == false && longitude.isBlank == false {
            
            let cordinatesDict = ["lati": lattitude, "longi" : longitude, "title": Localized("Title_HeadOffice"), "address" : contactUsDetail.address ?? "" ]
            
            let store = self.myStoryBoard.instantiateViewController(withIdentifier: "StoreLocatorVC") as! StoreLocatorVC
            store.selectedType = .StoreLocator
            
            store.shouldFocuseToSelectedLocation = true
            store.selectedLocationForFocuse = cordinatesDict
            
            self.navigationController?.pushViewController(store, animated: true)
            
        } else {
            
            self.showErrorAlertWithMessage(message: Localized("Message_NoDataAvalible"))
        }
    }
 */
    
    @IBAction func helpLineBtnPressed(_ sender: AFButton) {
        dialNumber(number: contactUsDetail.customerCareNo ?? "")
    }
    
    @IBAction func phoneBtnPressed(_ sender: AFButton) {
        dialNumber(number: contactUsDetail.phone ?? "")
    }
    
    @IBAction func emialBtnPressed(_ sender: AFButton) {
        
        if MFMailComposeViewController.canSendMail() {
            
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([contactUsDetail.email ?? ""])
            mail.setMessageBody("", isHTML: false)
            
            present(mail, animated: true)
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_SetEmail"))
        }
    }
    
    @IBAction func webSiteBtnPressed(_ sender: AFButton) {
        openURLInSafari(urlString: contactUsDetail.website ?? "")
    }
    
    @IBAction func faceBookBtnPressed(_ sender: AFButton) {
        openURLInSafari(urlString: contactUsDetail.facebookLink)
    }
    
    @IBAction func twitterBtnPressed(_ sender: AFButton) {
        openURLInSafari(urlString: contactUsDetail.twitterLink)
    }
    
    @IBAction func youTubeBtnPressed(_ sender: AFButton) {
        openURLInSafari(urlString: contactUsDetail.youtubeLink)
    }
    
    @IBAction func instagramBtnPressed(_ sender: AFButton) {
        openURLInSafari(urlString: contactUsDetail.instagramLink)
    }
    
    @IBAction func linkinBtnPressed(_ sender: AFButton) {
        openURLInSafari(urlString: contactUsDetail.linkedinLink)
    }
    
    @IBAction func sendPressed(_ sender: AFButton) {
        
        let sharingItems:[AnyObject?] = ["\(contactUsDetail.inviteFriendText ?? "")" as AnyObject]
        
        let activityViewController = UIActivityViewController(activityItems: sharingItems.compactMap({$0}), applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
        
        //EventLog
        AFAnalyticsManager.logEvent(screenName: "share", contentType:"redirected_to_invite" , successStatus:"1" )
    }
    
    //MARK: - Functions
    func loadViewContent() {
        
        /* ContactUs View controller text*/
        self.titleLabel?.text        = Localized("Title_ContactUs")
        headOfficeTitleLabel.text    = Localized("Title_HeadOffice")
        addressLabel.text            = Localized("Info_HeadOfficeAddress")
        infoCenterTitleLabel.text    = Localized("Title_InfoCenter")
        socialMediaTitleLabel.text   = Localized("Title_SocialMedia")
        inviteFriendsTitleLabel.text = Localized("Title_InviteFriends")
        
        helpLineNumberButton.setTitle(Localized("Info_HelpLine"), for: .normal)
        contactNumberButton.setTitle(Localized("Info_PhoneNumber"), for: .normal)
        emailButton.setTitle(Localized("Info_EmailData"), for: .normal)
        websiteButton.setTitle(Localized("Info_webSitre"), for: .normal)
        sendButton.setTitle(Localized("BtnTitle_Send"), for: .normal)
        
    }
    
    func updateInformationOfLayout() {
        
        /* ContactUs View controller text*/
        addressLabel.text  = contactUsDetail.address ?? ""
        
        helpLineNumberButton.setTitle(contactUsDetail.customerCareNo, for: .normal)
        contactNumberButton.setTitle(contactUsDetail.phone, for: .normal)
        emailButton.setTitle(contactUsDetail.email, for: .normal)
        websiteButton.setTitle(contactUsDetail.website, for: .normal)
        
    }
    
    //MARK: - APIs call
    
    /// Call 'getContactUsDetails' API.
    ///
    /// - returns: Void
    func getContactUsDetails() {
        
        if let responseHandler :ContactUsModel = ContactUsModel.loadFromUserDefaults(key: APIsType.contactUs.selectedLocalizedAPIKey()) {
            
            self.contactUsDetail = responseHandler
            self.updateInformationOfLayout()
            
        } else {
            self.showActivityIndicator()
        }
        
        _ = AFAPIClient.shared.getContactUsDetails({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let contactUsResponse = Mapper<ContactUsModel>().map(JSONObject:resultData) {
                        
                        /*Save data into user defaults*/
                        contactUsResponse.saveInUserDefaults(key: APIsType.contactUs.selectedLocalizedAPIKey())
                        
                        self.contactUsDetail = contactUsResponse
                        self.updateInformationOfLayout()
                    }
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - MFMailComposeViewControllerDelegate
extension ContactUsVC : MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

