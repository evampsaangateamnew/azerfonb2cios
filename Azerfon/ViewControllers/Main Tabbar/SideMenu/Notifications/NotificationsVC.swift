//
//  NotificationsVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/22/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class NotificationsVC: BaseVC {
    
    //MARK: - IBOutlet
    @IBOutlet var notificationsTableView  : UITableView!
    
    //MARK: - Properties
    var notificationsList : [NotificationItem]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationsTableView.allowsSelection = true
        notificationsTableView.delegate = self
        notificationsTableView.dataSource = self
        
        /* Load data from UserDefaults */
        if let menuNotificationsData :MenuNotificationsModel = MenuNotificationsModel.loadFromUserDefaults(key: APIsType.notificationData.selectedLocalizedAPIKey()),
            (menuNotificationsData.notificationsList?.count ?? 0) > 0 {
            
            // Set information in local object
            self.notificationsList = menuNotificationsData.notificationsList
            
            /* Load new data in background*/
            self.getNotifications(showLoader: false)
            
        } else {
            self.getNotifications(showLoader: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // Layout with localized string
        layoutViewController()
        
        /* Reset Notification bage to 0 */
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    //MARK: - IBAction
    
    //MARK: - Functions
    func layoutViewController() {
        self.titleLabel?.text = Localized("Title_Notifications")
    }
    
    //MARK: - API Calls
    /**
     call to Get Notifications Data.
     - returns: void
     */
    func getNotifications(showLoader : Bool) {
        
        if (showLoader == true) {
            self.showActivityIndicator()
        }
        
        /*API call to get data*/
        _ = AFAPIClient.shared.getNotifications( { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if (showLoader == true) {
                self.hideActivityIndicator()
            }
            
            if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                let notificationsDataResponse = Mapper<MenuNotificationsModel>().map(JSONObject:resultData) {
                
                /* caching server response */
                notificationsDataResponse.saveInUserDefaults(key: APIsType.notificationData.selectedLocalizedAPIKey())
                
                self.notificationsList = notificationsDataResponse.notificationsList
            } else {
                self.showErrorAlertWithMessage(message: resultDesc)
            }
            
            self.notificationsTableView.reloadUserData(self.notificationsList)
            
        })
    }
    
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension NotificationsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notificationsList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :NotificationsCell = tableView.dequeueReusableCell() {
            
            cell.setItemInfo(aItem: notificationsList?[indexPath.row])
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let selectedItem = notificationsList?[indexPath.row] {
            
            if selectedItem.actionType?.isEqual("Tariff", ignorCase: true) ?? false,
                let tariffVC :TariffVC = TariffVC.instantiateViewControllerFromStoryboard() {
                
                tariffVC.canUserGoBack = true
                tariffVC.redirectToTariff = (true, selectedItem.actionID)
                self.navigationController?.pushViewController(tariffVC, animated: true)
                
            } else if selectedItem.actionType?.isEqual("Supplementary", ignorCase: true) ?? false {
                
                
                if (AFUserSession.shared.userInfo?.specialOffersTariffData?.offersSpecialIds?.contains(selectedItem.actionID ?? "") ?? false) == true {
                    
                    if let specialOffersVC :SpecialOffersVC = SpecialOffersVC.instantiateViewControllerFromStoryboard() {
                        
                        specialOffersVC.redirectToSpecial = (true, selectedItem.actionID)
                        self.navigationController?.pushViewController(specialOffersVC, animated: true)
                    }
                    
                } else {
                    if let supplementaryVC :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard() {
                        supplementaryVC.redirectToSupplementory = (true, selectedItem.actionID)
                        self.navigationController?.pushViewController(supplementaryVC, animated: true)
                    }
                }
            
            }
        }
        // Deselect user selected row
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


