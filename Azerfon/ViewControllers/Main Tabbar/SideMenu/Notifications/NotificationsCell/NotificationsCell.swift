//
//  NotificationsCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/22/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var messageLabel     : AFLabel!
    @IBOutlet var timeLabel         : AFLabel!
    
    @IBOutlet var dotImageView      : UIImageView!
    @IBOutlet var tickImageView     : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setItemInfo(aItem : NotificationItem?) {
        self.messageLabel.text = aItem?.message
        self.timeLabel.text = aItem?.datetime
    }
    
}
