//
//  UserProfileCell.swift
//  Azerfon
//
//  Created by Waqas on 3/21/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class ViewProfileCell: UITableViewCell {
    
    //MARKL: - IBOutlet
    @IBOutlet var optionNameLabel : AFMarqueeLabel!
    @IBOutlet var optionSecondLabel : UILabel!
    @IBOutlet var optionImageView : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
