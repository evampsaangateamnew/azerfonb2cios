//
//  ChangeEmailVC.swift
//  Azerfon
//
//  Created by Mian Waqas Umar on 23/04/2019.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class ChangeEmailVC: BaseVC {
    //MARK: - Properties
    
    //MARK: - IBOutlet
    @IBOutlet var emailTextField    : AFTextField!
    @IBOutlet var descriptionLabel  : AFLabel!
    @IBOutlet var updateButton      : AFButton!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func updatePressed(_ sender: Any) {
        
        if let emailText = emailTextField.text,
            emailText.isValidEmail(),
            emailText.isValidEmailCharactors() {
            
            _ = emailTextField.resignFirstResponder()
            updateCustomerEmail(newEmail:emailTextField.text)
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_invalidEmail"))
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_AddEmail")
        descriptionLabel.text = Localized("Info_DescritionAddEmail")
        updateButton.setTitle(Localized("BtnTitle_UpdatePassword"), for: UIControl.State.normal)
        emailTextField.placeholder = Localized("Placeholder_ChangeEmail")
       
        if let currentEmail = AFUserSession.shared.userInfo?.email,
            currentEmail.isValidEmailCharactors() {
            
            emailTextField.text = currentEmail
        
        } else {
            emailTextField.text = ""
        }
    }
    
    //MARK: - API's Calls
    func updateCustomerEmail(newEmail : String?) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.updateCustomerEmail(NewEmail: newEmail ?? "" ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let responseHandler = Mapper<MessageResponse>().map(JSONObject: resultData){
                        
                        // Update user email
                        /*
                        AFUserSession.shared.loggedInUsers?.currentUser?.userInfo?.email = newEmail
                        AFUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                        */
                        
                        self.showSuccessAlertWithMessage(message: responseHandler.message, {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                    
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
}

//MARK:- Textfield delagates
extension ChangeEmailVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        updatePressed(UIButton())
        return true
    }
}

