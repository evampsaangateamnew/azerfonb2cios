//
//  ViewProfile.swift
//  Azerfon
//
//  Created by Mian Waqas Umar on 22/04/2019.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import ObjectMapper

class ViewProfileVC: BaseVC {
    
    public enum ResourceAccessType {
        case camera
        case gallery
    }
    
    //MARK: - Properties
    private var imagePicker: UIImagePickerController =  UIImagePickerController()
    
    private var titles: [String] = []
    private var images: [String] = ["billingLanguage","iccd","pin","pin","iccd", "email","suspendNumber"]
    private var descriptionArr : [String] = []
    private var updateImageArr: [String] = [Localized("Info_Camera"),Localized("Info_Gallery"),Localized("Info_RemovePhoto")]
    
    //MARK: - IBOutlet
    @IBOutlet var userNameLabel            : AFLabel!
    @IBOutlet var userPhoneNumberLabel     : AFLabel!
    @IBOutlet var profileImageOptionsView  : UIView!
    @IBOutlet var userProfileImageView     : UIImageView!
    @IBOutlet var menuTableView            : UITableView!
    @IBOutlet var heightMenuTableView      : NSLayoutConstraint!
    @IBOutlet var imageUpdateTableView     : UITableView!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userProfileImageView.roundAllCorners(radius: 24)
        imageUpdateTableView.roundAllCorners(radius: 8)
        profileImageOptionsView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        userNameLabel.text = AFUserSession.shared.userName()
        userPhoneNumberLabel.text = AFUserSession.shared.msisdn
    }
    
    //MARK: - IBAction
    @IBAction func imageUpdatePressed(_ sender: UIButton) {
        
        if profileImageOptionsView.isHidden == true {
            
            UIView.transition(with: profileImageOptionsView, duration: 0.5, options: [.transitionFlipFromLeft, .showHideTransitionViews], animations: {
                
                self.profileImageOptionsView.isHidden = false
                
            }, completion:nil)
            
            
        } else {
            
            UIView.transition(with: profileImageOptionsView, duration: 0.5, options: [.transitionFlipFromRight, .showHideTransitionViews], animations: {
                self.profileImageOptionsView.isHidden = true
                
            }, completion:nil)
        }
        
    }
    
    func suspendNumberTapped() {
        
        /* SHOW Alert and perform operation accordinglly */
        self.showConfirmationAlert(message: Localized("Message_SuspendMessage"), okBtnTitle: Localized("BtnTitle_SUSPEND"), cancelBtnTitle: Localized("BtnTitle_NO"), {
            
            /* User confirmed for money request */
            self.suspendNumberAPICall()
        })
    }
    
    //MARK: - Functions
    
    func reloadTableViewData() {
        
        self.titles = [Localized("Info_BillingLanguage"),
                  Localized("Info_SIM"),
                  Localized("Info_PIN"),
                  Localized("Info_PUK"),
                  Localized("Info_ICCD_activation_date"),
                  Localized("Info_Email")]
        
        /* if user corporate then hide suppend number */
        if AFUserSession.shared.customerType() != .corporateCustomer &&
            AFUserSession.shared.customerType() != .dataSimPostpaidCorporate {
            self.titles.append(Localized("Info_SuspendNumber"))
        }
        
        let billingLangCode = AFUserSession.shared.userInfo?.billingLanguage ?? ""
        let billingLang = Constants.AFLanguage(rawValue: billingLangCode) ?? Constants.AFLanguage.azeri
        var billingLanguageString = ""
        switch billingLang {
        case .english:
            billingLanguageString = "English"
        case .russian:
            billingLanguageString = "Русский"
        default:
            billingLanguageString = "Azərbaycan"
        }
        
        self.descriptionArr = [billingLanguageString,
                               AFUserSession.shared.userInfo?.sim ?? "",
                               AFUserSession.shared.userInfo?.pin ?? "",
                               AFUserSession.shared.userInfo?.puk ?? "",
                               AFUserSession.shared.userInfo?.effectiveDate ?? "",
                               (AFUserSession.shared.userInfo?.email?.isValidEmailCharactors() ?? false) ? AFUserSession.shared.userInfo?.email ?? Localized("Info_AddEmail") : Localized("Info_AddEmail")]
        /* if user corporate then hide suppend number */
        if AFUserSession.shared.customerType() != .corporateCustomer &&
            AFUserSession.shared.customerType() != .dataSimPostpaidCorporate {
            self.descriptionArr.append("")
        }
        
        
        self.menuTableView.reloadData()
    }
    
    func loadProfileImage() {
        
        let profileImageURLString = AFUserSession.shared.userInfo?.imageURL ?? ""
    
        if let profileImageURL = URL(string: profileImageURLString),
            UIApplication.shared.canOpenURL(profileImageURL) {
            
            print("image URL: \(profileImageURL.absoluteString)")
            
            // Load profile image of user
            _ = AFAPIClient.shared.request(profileImageURLString).responseData { response in
                
                if response.result.isFailure {
                    print(response.result.error?.localizedDescription ?? "")
                }
                
                if let imageDate = response.result.value ,
                    let image = UIImage(data: imageDate) {
                    self.userProfileImageView.image = image
                } else {
                    self.userProfileImageView.image = UIImage.imageFor(name: "menuAvatarPlaceholder")
                }
            }
        } else {
            self.userProfileImageView.image = UIImage.imageFor(name: "menuAvatarPlaceholder")
        }
 
    }
    
    func loadImagePickerControllerFor(type: ResourceAccessType) {
        
        if type == .camera {
            
            // Get current access status for camera
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            
            switch cameraAuthorizationStatus {
                
            case .authorized:
                self.loadImagePickerControllerForCamera()
                break
                
            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                    if granted {
                        
                        // User enable camera access to app
                        self.loadImagePickerControllerForCamera()
                    } else {
                        // Acces denied
                        self.showAccessAlert(message: Localized("Message_EnableCameraAccess"))
                    }
                }
                break
                
            case .denied, .restricted:
                // Access denied
                self.showAccessAlert(message: Localized("Message_EnableCameraAccess"))
                break
                
            }
            
        } else {
            
            // Get the current authorization state of Photos
            let status = PHPhotoLibrary.authorizationStatus()
            
            if (status == PHAuthorizationStatus.authorized) {
                // Access has been granted.
                self.loadImagePickerControllerForGallery()
                
            } else if (status == PHAuthorizationStatus.notDetermined) {
                
                // Access has not been determined.
                PHPhotoLibrary.requestAuthorization({ (newStatus) in
                    
                    // Get Access
                    if (newStatus == PHAuthorizationStatus.authorized) {
                        
                        self.loadImagePickerControllerForGallery()
                        
                    } else {
                        // Access denied and show user access message
                        self.showAccessAlert(message: Localized("Message_EnablePhotosAccess"))
                    }
                })
                
            } else if (status == PHAuthorizationStatus.denied) || (status == PHAuthorizationStatus.restricted) {
                // Access has been denied.
                self.showAccessAlert(message: Localized("Message_EnablePhotosAccess"))
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }
    }
    
    func loadImagePickerControllerForGallery() {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
        
    }
    
    func loadImagePickerControllerForCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            present(imagePicker, animated: true, completion: nil)
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
        
    }
    
    func changeBillingLanguage() {
        if let changelang :LanguageSelectionVC =  LanguageSelectionVC.fromNib() {
            
            let billingLang = (AFUserSession.shared.userInfo?.billingLanguage ?? "")
            let currentBillingLang = Constants.AFLanguage(rawValue: billingLang) ?? Constants.AFLanguage.azeri
            
            changelang.setLanguageSelectionAlert(currentBillingLang) { (newSelectedLanguage) in
                
                if currentBillingLang != newSelectedLanguage {
                    
                    self.callBillingLanguageChangeApi(language : newSelectedLanguage.rawValue)
                }
            }
            
            self.presentPOPUP(changelang, animated: true, completion:  nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == profileImageOptionsView {
                imageUpdatePressed(UIButton())
            }
        }
        
    }
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_ViewProfile")
        
        loadProfileImage()
        reloadTableViewData()
    }
    
    //MARK: - API's Calls
    func uploadImage(newImage:UIImage?, isUploading :Bool) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.uploadImage(image: newImage, isUploadImage: isUploading, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.

                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let uploadImageResponse = Mapper<UploadImage>().map(JSONObject: resultData) {
                    
                    self.showSuccessAlertWithMessage(message: resultDesc)
                    
                    AFUserSession.shared.loggedInUsers?.currentUser?.userInfo?.imageURL = uploadImageResponse.imageURL
                    AFUserSession.shared.saveLoggedInUsersCurrentStateInfo()
         
                    self.loadProfileImage()
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
        })
    }
    
    func callBillingLanguageChangeApi(language lang : String) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.changeBillingLanguage(language: lang, {(response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let changeLangResponse = Mapper<ChangeBillingLanguage>().map(JSONObject:resultData) {
                    
                    self.showSuccessAlertWithMessage(message: changeLangResponse.message)
                    
                    // Update billing language.
                    AFUserSession.shared.loggedInUsers?.currentUser?.userInfo?.billingLanguage = lang
                    AFUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                    
                    self.reloadTableViewData()
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    func suspendNumberAPICall() {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.suspenNumber({(response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let changeLangResponse = Mapper<ChangeBillingLanguage>().map(JSONObject:resultData) {
                    
                    self.showSuccessAlertWithMessage(message: changeLangResponse.message)
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension ViewProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView === imageUpdateTableView {
            return updateImageArr.count
        } else {
            let cnt = descriptionArr.count
            heightMenuTableView.constant = CGFloat(cnt) * 45
            return cnt
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView === menuTableView {
            
            if let cell : ViewProfileCell = tableView.dequeueReusableCell() {
                
                cell.optionImageView.image = UIImage(named: images[indexPath.row])
                cell.optionNameLabel.text = titles[indexPath.row]
                cell.optionSecondLabel.text = descriptionArr[indexPath.row]
                
                return cell
                
            }
            else {
                return UITableViewCell()
            }
            
        }
        else if tableView === imageUpdateTableView {
            
            let cell = UITableViewCell()
            
            cell.textLabel?.text = updateImageArr[indexPath.row]
            cell.textLabel?.font = UIFont.afSans(fontSize: 14)
            return cell
    
        }
        else {
            return UITableViewCell()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == imageUpdateTableView {
            
            if indexPath.row == 0 {
                
                self.loadImagePickerControllerFor(type: .camera)
                
            } else if indexPath.row == 1 {
                
                self.loadImagePickerControllerFor(type: .gallery)
                
            } else if indexPath.row == 2 {
                
                self.uploadImage(newImage:nil, isUploading: false)
            }
            
            // Hide option TableView
            imageUpdatePressed(UIButton())
            
        } else {
            
            let selectedRowIndex = indexPath.row
            
            switch selectedRowIndex {
            case 0:
                changeBillingLanguage()
            case 5:
                if let vc : ChangeEmailVC = ChangeEmailVC.instantiateViewControllerFromStoryboard() {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            case 6:
                //show confirmation popup and call API if user taps on YES option
                self.suspendNumberTapped()
            default:
                print("do nothing")
            }
            
            
        }
        
        
        // Deselect user selected row
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension ViewProfileVC :UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
            /* Called API to update/upload image */
            uploadImage(newImage: image, isUploading: true)
            
            if picker.sourceType == .camera {
                
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
            
        }
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        // self.showErrorAlertWithMessage(message: error?.localizedDescription)
    }
}




