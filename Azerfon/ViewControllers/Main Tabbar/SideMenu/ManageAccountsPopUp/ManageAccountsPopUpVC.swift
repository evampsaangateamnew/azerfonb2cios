//
//  ManageAccountsPopUpVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/21/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

// Scrolling and selection Protocol
protocol UsersSwitchDelegate: class {
    func userDidSelect(_ newUser: UserDataModel?)
}

extension UsersSwitchDelegate {
    func userDidSelect(_ newUser: UserDataModel?) {
    }
}

class ManageAccountsPopUpVC: BaseVC {
    
    //MARK: - Properties
    fileprivate var manageAccountbuttonCompletionBlock : AFButtonCompletionHandler? = {}
    weak var delegate: UsersSwitchDelegate?
    
    //MARK: - IBOutlet
    @IBOutlet var manageAccountsButton          : AFButton!
    @IBOutlet var manageAccountsTableView       : UITableView!
    @IBOutlet var mainContentView               : UIView!
    @IBOutlet var tableViewHeightConstraint     : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadViewContent()
        
        manageAccountsTableView.hideDefaultSeprator()
        manageAccountsTableView.allowsMultipleSelection = false
    }
    
    //MARK: - IBAction
    @IBAction func manageAccountsPressed(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.manageAccountbuttonCompletionBlock?()
        }
    }
    
    //MARK: - Functions
    func navigateToManageAccountsScreen(_ manageAccountButtonBlock : @escaping AFButtonCompletionHandler = {}) {
        self.manageAccountbuttonCompletionBlock = manageAccountButtonBlock
    }
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.view.backgroundColor = UIColor.clear
        manageAccountsButton.setTitle(Localized("BtnTitle_MANAGEACCOUNT"), for: .normal)
        
        tableViewHeightConstraint.constant = CGFloat((AFUserSession.shared.loggedInUsers?.users?.count ?? 0) * 55);
        
        self.view.layoutIfNeeded()
        //adding dropdown shadow to main content view
        self.mainContentView.addDropShadowTo(color: UIColor.darkGray, opacity: 0.6, offSet: CGSize.zero, radius: 3, scale: true)
    }
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension ManageAccountsPopUpVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return AFUserSession.shared.loggedInUsers?.users?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell : ManageAccountCell = tableView.dequeueReusableCell() {
            
            if let aUser = AFUserSession.shared.loggedInUsers?.users?[indexPath.row] {
                cell.setLayout(name: aUser.userInfo?.firstName,
                               msisdn: aUser.userInfo?.msisdn,
                               isSelected: aUser.userInfo?.msisdn?.isEqual(AFUserSession.shared.msisdn, ignorCase: true) ?? false)
            } else {
                cell.setLayout(name: "___", msisdn: "___", isSelected: false)
            }
            let selectionView = UIView()
            selectionView.backgroundColor = UIColor.afBerry
            cell.selectedBackgroundView = selectionView
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
        if let aUser = AFUserSession.shared.loggedInUsers?.users?[indexPath.row] {
            delegate?.userDidSelect(aUser)
            self.dismiss(animated: true) {
                self.manageAccountbuttonCompletionBlock = nil
            }
        }
        // Deselect user selected row
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension ManageAccountsPopUpVC {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.view {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
