//
//  ManageAccountCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/21/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class ManageAccountCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var selectedIndicatorImageView    : UIImageView!
    @IBOutlet var userNameLabel                 : AFMarqueeLabel!
    @IBOutlet var userPhoneNumberLabel          : AFMarqueeLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setLayout(name :String?, msisdn :String?, isSelected :Bool) {
        userNameLabel.text = name
        userPhoneNumberLabel.text = msisdn
        
        selectedIndicatorImageView.isHidden = !isSelected
    }
}
