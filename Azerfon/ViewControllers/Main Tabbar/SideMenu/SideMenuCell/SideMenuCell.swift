//
//  SideMenuCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/21/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    
    //MARKL: - IBOutlet
    @IBOutlet var optionNameLabel : UILabel!
    
    @IBOutlet var optionImageView : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
