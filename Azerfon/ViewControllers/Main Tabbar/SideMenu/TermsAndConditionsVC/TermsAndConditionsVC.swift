//
//  TermsAndConditionsVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/29/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class TermsAndConditionsVC: BaseVC {
    //MARK: - Properties
    
    //MARK: - IBOutlet
    @IBOutlet var termsAndConditionsLabel   : UILabel!
    
    @IBOutlet var termsAndConditionsTextView   : UITextView!

    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_T&C")
        self.termsAndConditionsLabel?.text = Localized("Title_T&C")
        
        //loading data from UserDefaults
        termsAndConditionsTextView.loadHTMLString(htmlString: AFUserSession.shared.predefineData?.tncContent ?? "")
    }
    
    //MARK: - API's Calls
}

