//
//  MoneyTransferVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/2/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class MoneyTransferVC: BaseVC {
    
    //MARK: - Properties
    var selectedTopUpType : Constants.AFTopUpType = .ScratchCard
    var selectedAmountList : [PrepaidAmount]? = AFUserSession.shared.predefineData?.topup?.moneyTransfer?.selectAmount?.prepaid
    var selectedAmount : PrepaidAmount?
    
    //MARK: - IBOutlet
    @IBOutlet var dropDown: UIDropDown!
    
    @IBOutlet var topDescriptionTextLabel          : AFLabel!
    @IBOutlet var bottomDescriptionTextLabel       : AFLabel!
    @IBOutlet var amountTitleLabel                 : AFLabel!
    
    @IBOutlet var receiverNumberTextField          : AFTextField!
    
    @IBOutlet var transferButton                   : AFButton!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        receiverNumberTextField.delegate = self
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "TransferMoneyScreenViews")+1, forKey: "TransferMoneyScreenViews")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        //setting prsefix
        receiverNumberTextField.text = Constants.numberPrefix
        
        //init dropdown
        setDropDown()
    }
    
    //MARK: - IBAction
    @IBAction func transferButtonPressed(_ sender: UIButton) {
        
        var reciverMSISDN = ""
        if let msisdnText = receiverNumberTextField.text?.formatedMSISDN(),
            msisdnText.isValidMSISDN() {
            
            reciverMSISDN = msisdnText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidMSISDN"))
            return
        }
        
        _ = receiverNumberTextField.resignFirstResponder()
        
        /* SHOW Alert and perform operation accordinglly */
        self.showConfirmationAlert(message: String(format: Localized("Message_TopUpConfirmation"), reciverMSISDN), {
            
            /* User confirmed for money transfer */
            self.moneyTransferCall(ReceiverMSISDN: reciverMSISDN, Amount: self.selectedAmount?.amount ?? "")
        })
        
    }
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text                   = Localized("Title_MoneyTransfer")
        self.topDescriptionTextLabel?.text      = Localized("Info_MoneyTransferDescription")
        self.bottomDescriptionTextLabel?.text   = Localized("Info_MoneyTransferBottomDescription")
        self.amountTitleLabel?.text             = Localized("Title_Amount")
        
        receiverNumberTextField.placeholder     = Localized("Placeholder_MobileNumber")
        
        transferButton.setTitle(Localized("BtnTitle_TRANSFER"), for: UIControl.State.normal)
    }
    
    /*Initialze - Setup dropdown */
    func setDropDown(){
        
        var titles : [String] = []
        
        selectedAmountList?.forEach({ (aPrepaidAmount) in
            titles.append(aPrepaidAmount.amount ?? "")
        })
        
        self.dropDown.dataSource = titles
        if selectedAmountList?.count ?? 0 > 0 {
            self.selectedAmount = self.selectedAmountList?[0]
            self.dropDown.placeholder = selectedAmount?.amount
        }
        
        self.dropDown.didSelectOption { (index, option) in
            
            self.selectedAmount = self.selectedAmountList?[index]
        }
        
    }
    
    
    //MARK: -  in-App-Survery
    func checkTransferMoneySurvey(message:String) {
        
        if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "transfer_money" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "-1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "TransferMoneyScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
//                    inAppSurveyVC.hideBalanceStackView = false
                    inAppSurveyVC.toptupTitleText = Localized("Title_successful")
                    inAppSurveyVC.attributedMessage = NSAttributedString.init(string: message)
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            } else {
                self.showSuccessAlertWithMessage(message: message)
            }
        } else {
            self.showSuccessAlertWithMessage(message: message)
        }
    }
    
    //MARK: - API's Calls
    func moneyTransferCall (ReceiverMSISDN receiverMSISDN : String, Amount amount : String){
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.moneyTransfer(receiverMSISDN: receiverMSISDN, Amount: amount,  { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    /* Set check to true so that dashboad data will be reloaded once user redirected to dashboad */
                    AFUserSession.shared.shouldReloadDashboardData = true
                    
                    //clearing text field
                    // self.receiverNumberTextField.text = Constants.numberPrefix
                    
                    //show success alert
                    
                    self.checkTransferMoneySurvey(message: resultDesc ?? "")
                    
                } else {
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - UITextFieldDelegate
extension MoneyTransferVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == receiverNumberTextField{
            
            /* Check prefix limit and disabling editing in prefix */
            let protectedRange = NSMakeRange(0, receiverNumberTextField.prefixLength)
            let intersection = NSIntersectionRange(protectedRange, range)
            
            if intersection.length > 0 ||
                range.location < (protectedRange.location + protectedRange.length) {
                
                return false
            }
            
            /* Restricting user to enter only number */
            let allowedCharactors = Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            /* Restriction for MSISDN length */
            return newLength <= (Constants.MSISDNLength + receiverNumberTextField.prefixLength)
            
        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        transferButtonPressed(UIButton())
        return true
    }
}
