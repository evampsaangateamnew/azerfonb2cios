//
//  SchedulePaymentVC.swift
//  Bakcell
//
//  Created by Touseef Sarwar on 04/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper
import UPCarouselFlowLayout

enum Schedules {
    case daily
    case weekly
    case monthly
}


class SchedulePaymentVC: BaseVC {
    
    
    //View Controller Identifier.
    static let identifier = "SchedulePaymentVC"
    
    
    
    // MARK: - IBOutlet
    
    //Navigation title...
    @IBOutlet weak var schedulePaymentTitle_lbl: UILabel!
    
    
    //Tabs button...
    @IBOutlet weak var buttonsStackView : UIStackView!
    @IBOutlet weak var dailyBtn : AFMarqueeButton!
    @IBOutlet weak var weeklyBtn : AFMarqueeButton!
    @IBOutlet weak var monthlyBtn : AFMarqueeButton!
    
    
    //Select Card
    @IBOutlet weak var selectCardLabel : UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //Enter Amount
    @IBOutlet weak var amountStackView : UIStackView!
    @IBOutlet weak var enterAmountLabel : UILabel!
    @IBOutlet weak var amountTF : AFTextField!
    
    @IBOutlet weak var parentView: UIStackView!
    
    //Recure every week
    @IBOutlet weak var weekDayView : UIStackView!
    @IBOutlet weak var recureStackView : UIStackView!
    @IBOutlet weak var recureWeekLabel : UILabel!
    @IBOutlet weak var weekOrMonth : UILabel!
    @IBOutlet weak var noOfWeekOrMonthTF : UITextField!
    @IBOutlet weak var weekdaysCollectionView: UICollectionView!
    
    
    //Start date
    @IBOutlet weak var startDayLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var startDateBtn : UIButton!
    @IBOutlet weak var dropdownImage : UIImageView!
    let datePicker = UIDatePicker()
    let myView = UIView()
    
    //Number of recurence
    @IBOutlet weak var numberOfRecureLabel : UILabel!
    @IBOutlet weak var numberOfRecureTF : UITextField!
    //cancel & schedule Button..
    @IBOutlet weak var cancelBtn : UIButton!
    @IBOutlet weak var scheduleBtn : AFButton!
    
    
    //Auto renew checkbox & label
    @IBOutlet weak var autoCheckView: UIStackView!
    @IBOutlet weak var autoCheckBtn: UIButton!
    @IBOutlet weak var autoCheckLabel: UILabel!
    
    
    //MARK: - Properties..
    var selectedSchdule : Schedules = .daily
    var selectedCardIndex : Int = 0
    
    var weekDayIndex : Int? = Calendar.current.component(.weekday, from: Date().todayDate()) - 2
 
    var savedCards : [SavedCards]?
    
    
    var weekDays = [
        Localized("Lbl_Mo"),
        Localized("Lbl_Tu"),
        Localized("Lbl_We"),
        Localized("Lbl_Th"),
        Localized("Lbl_Fr"),
        Localized("Lbl_Sa"),
        Localized("Lbl_Su")
    ]
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        weekdaysCollectionView.delegate = self
        weekdaysCollectionView.dataSource = self
        amountTF.delegate = self
        tableView.register(UINib(nibName: CardCell.identifier, bundle: nil), forCellReuseIdentifier: CardCell.identifier)
        self.numberOfRecureTF.delegate = self
        self.getSavedCards()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        loadViewLayout()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        buttonsStackView.spacing = (buttonsStackView.frame.size.width*9.0)/100.00
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        
        weekdaysCollectionView.layer.cornerRadius = weekdaysCollectionView.bounds.height / 2
        
        schedulePaymentTitle_lbl?.text = Localized("schedulePayment_Title")
        
        selectCardLabel.text = Localized("Lbl_SelectCard")
        enterAmountLabel.text = Localized("Lbl_EnterAmount")
        amountTF.placeholder = Localized("Placeholder_EnterAmount")
        recureWeekLabel.text = Localized("Lbl_RecureWeek")
        weekOrMonth.text = selectedSchdule == .weekly ? Localized("Lbl_Week") : Localized("Lbl_Month")
        startDayLabel.text = Localized("Lbl_StartDate")
        numberOfRecureLabel.text = Localized("Lbl_NoOfReccurrence")
        dateLabel.text = AFUserSession.shared.dateFormatter.createString(from: Date().todayDate(), dateFormate: Constants.kDisplayFormat)
        autoCheckLabel.text = Localized("Label_AutoRenew")
        self.autoCheckBtn.setImage(UIImage(named: "check_off"), for: .normal)
        self.autoCheckView.isHidden =  true
        
        
        amountTF.placeholder = ""
        //MARK: -Tab Buttons Layout
        
        //daily Button
        self.setGrayButton(dailyBtn, withTitle: Localized("BtnTitle_Daily"), isGrayBackground: true)
        dailyBtn.setMarqueeButtonLayout()
        
        //weekly button
        self.setGrayButton(weeklyBtn, withTitle: Localized("BtnTitle_Weekly"), isGrayBackground: false)
        weeklyBtn.setMarqueeButtonLayout()
        
        //Monthly button
        self.setGrayButton(monthlyBtn, withTitle: Localized("BtnTitle_Monthly"), isGrayBackground: false)
        monthlyBtn.setMarqueeButtonLayout()
        
        //MARK: - Setting  layout for cancel & schedule button
        //cancel
//        self.setGrayButton(cancelBtn, withTitle: Localized("BtnTitle_CANCEL"), isGrayBackground: false)
        

        //schedule
        scheduleBtn.setTitle(Localized("BtnTitle_Schedule"), for: UIControl.State.normal)
//        self.setGrayButton(scheduleBtn, withTitle: Localized("BtnTitle_Schedule"), isGrayBackground: true)
        
        
        switch selectedSchdule {
        case .daily:
            self.weekDayView.isHidden = true
        case .weekly:
            self.weekDayView.isHidden = false
            self.weekdaysCollectionView.isHidden = false
        case .monthly:
            self.weekDayView.isHidden = false
            self.weekdaysCollectionView.isHidden = true
        }
    }
    /*
    //MARK: - Setup Dropdown..
    func setDropdown(){
        dateDropdown.textAlignment = NSTextAlignment .center
        dateDropdown.optionsTextAlignment = NSTextAlignment.center
//        dateDropdown.placeholder = Localized("DropDown_SelectAmount")
        dateDropdown.placeholder = "Select Date"
        self.dateDropdown.rowBackgroundColor = UIColor.MBDimLightGrayColor
        dateDropdown.setFont = UIFont.MBArial(fontSize: 12)
        
//        dateDropdown.
        
        self.dateDropdown.tableWillAppear {
            self.dropdownImage.image = UIImage (named: "Drop-down-arrow-state2")
        }
        
        self.dateDropdown.tableWillDisappear {
            self.dropdownImage.image = UIImage (named: "Drop-down-arrow-state1")
        }
        
        dateDropdown.didSelect { (option, index) in
            // self.dropDownLbl.text = " \(option)"
            print("You just select: \(option) at index: \(index)")
//            self.selectedValue = option
            //self.selectedValue = self.selectedValue?.substring(to: (self.selectedValue?.index(before: (self.selectedValue?.endIndex)!))!)
//            print(self.selectedValue ?? "")
            
        }
    }
     */
    
}

// MARK: IBActions
extension SchedulePaymentVC {
    
    //Daily, weekly & Monthly Functionality
    @IBAction func tabButtonActions(_ sender: AFButton){
        
        sender.backgroundColor = UIColor .lightGray
        sender.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        sender.setTitleColor(UIColor .MBTextBlack, for: UIControl.State.normal)
        switch sender {
        case self.dailyBtn:
            self.weekDayView.isHidden = true
            selectedSchdule = .daily
            self.dailyBtn.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
            setGrayButton(weeklyBtn, withTitle: weeklyBtn.currentTitle, isGrayBackground: false)
            setGrayButton(monthlyBtn, withTitle: monthlyBtn.currentTitle, isGrayBackground: false)
            self.autoCheckView.isHidden =  true
            self.recureStackView.isHidden = true
            self.amountStackView.isHidden = false
        case weeklyBtn:
            self.weekDayView.isHidden = false
            self.weekdaysCollectionView.isHidden = false
            selectedSchdule = .weekly
            weekOrMonth.text = Localized("Lbl_Week")
            self.weeklyBtn.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
            setGrayButton(dailyBtn, withTitle: dailyBtn.currentTitle, isGrayBackground: false)
            setGrayButton(monthlyBtn, withTitle: monthlyBtn.currentTitle, isGrayBackground: false)
            self.autoCheckView.isHidden =  true
            self.recureStackView.isHidden = true
            self.amountStackView.isHidden = false
        case monthlyBtn:
            self.weekDayView.isHidden = false
            self.weekdaysCollectionView.isHidden = true
            selectedSchdule = .monthly
            weekOrMonth.text = Localized("Lbl_Month")
            if  AFUserSession.shared.subscriberType() == .postpaid {
                //self.autoCheckView.isHidden =  false
                //self.amountStackView.isHidden = true
            }
            self.recureStackView.isHidden = true
            self.monthlyBtn.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
            setGrayButton(dailyBtn, withTitle: dailyBtn.currentTitle, isGrayBackground: false)
            setGrayButton(weeklyBtn, withTitle: weeklyBtn.currentTitle, isGrayBackground: false)
        default:
            break
        }
    }
    
    //Start date
    @IBAction func startDateBtnAction(_ sender: UIButton) {
        if numberOfRecureTF.isFirstResponder {
            numberOfRecureTF.resignFirstResponder()
        }
        if amountTF.isFirstResponder {
            amountTF.resignFirstResponder()
        }
        let todayDate = AFUtilities.todayDate()
        AFUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        
        let threeMonthsAgos = AFUtilities.todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        //can use predefined date instead of todayDate...
        self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate)
    }
    
    //Schedule
    @IBAction func scheduleButtonAction(_ sender: UIButton){
        if  self.savedCards?.count ?? 0 > 0 && numberOfRecureTF.text?.toInt ?? 0 > 0 && amountTF.text?.toFloat ?? 0 > 0 {
            
            /*if selectedSchdule == .monthly {
                if AFUserSession.shared.subscriberType() == .postpaid {
                    if self.autoCheckBtn.currentImage == UIImage(named: "check_off") {
                        showErrorAlertWithMessage(message: Localized("label_selectCheckBox"))
                        return
                    } else {
                        self.amountTF.text? = ""
                    }
                } else {
                    if self.amountTF.text?.toInt ?? 0 < 1 {
                        showErrorAlertWithMessage(message: Localized("Lbl_EnterAmount"))
                        return
                    }
                }
            } else {
                if self.amountTF.text?.toInt ?? 0 < 1 {
                    showErrorAlertWithMessage(message: Localized("Lbl_EnterAmount"))
                    return
                }
            }*/
            
            
            
            //Call api
            
            let formatedDate = AFUserSession.shared.dateFormatter.createString(from:   AFUserSession.shared.dateFormatter.date(from: self.dateLabel.text ?? "\(Date().todayDate())") ?? Date().todayDate(), dateFormate: Constants.kNewAPIFormat)
            
                        self.addPaymentScheduler(
                amount: self.amountTF.text ?? "0",
                billingCycle: (selectedSchdule == .daily) ? "1" : (selectedSchdule == .weekly) ? "2" : "3",
                startDate: formatedDate,
                recurrenceNumber: self.numberOfRecureTF.text ?? "1",
                savedCardId: self.savedCards?[selectedCardIndex].id ?? "1")
        } else {
            if self.amountTF.text?.toFloat ?? 0 < 1 {
                showErrorAlertWithMessage(message: Localized("Lbl_EnterAmount"))
            } else if self.savedCards?.count ?? 0 <= 0 {
                self.showAlert(title: "", message: Localized("Lbl_AddNewCard"))
            } else if self.numberOfRecureTF.text?.toInt ?? 0 <= 0 {
                self.showAlert(title: "", message: Localized("Lbl_Correctruccrannce"))
            }
        }
    }
    
    //Auto renew check button
    @IBAction func autoCheckAction(_ sender: UIButton){
        
        if self.autoCheckBtn.currentImage == UIImage(named: "check_on") {
            self.autoCheckBtn.setImage(UIImage(named: "check_off"), for: .normal)
//            self.numberOfRecureTF.isEnabled = true
//            self.startDateBtn.isEnabled = true
//            self.noOfWeekOrMonthTF.isEnabled = true
//            self.parentView.backgroundColor = .clear
//            self.weekdaysCollectionView.backgroundColor = .clear
        }else{
            self.autoCheckBtn.setImage(UIImage(named: "check_on"), for: .normal)
//            self.numberOfRecureTF.isEnabled = false
//            self.startDateBtn.isEnabled = false
//            self.noOfWeekOrMonthTF.isEnabled = false
//            self.weekdaysCollectionView.backgroundColor = #colorLiteral(red: 0.9253990054, green: 0.9255540371, blue: 0.925378561, alpha: 1)
//            self.parentView.backgroundColor = #colorLiteral(red: 0.9253990054, green: 0.9255540371, blue: 0.925378561, alpha: 1)
        }
        
    }
    
}

//MARK: - DatePicker Methods
extension SchedulePaymentVC{
    
    func showDatePicker(minDate : Date , maxDate : Date){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: Localized("BtnTitle_Done"), style: UIBarButtonItem.Style.done, target: self, action: #selector(doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Localized("BtnTitle_CANCEL"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        var  currentLanguageLocale = Locale(identifier:"en")
        switch AFLanguageManager.userSelectedLanguage() {
            
        case .russian:
            currentLanguageLocale = Locale(identifier:"ru")
        case .english:
            currentLanguageLocale = Locale(identifier:"en")
        case .azeri:
            currentLanguageLocale = Locale(identifier:"az_AZ")
        }
        
        self.datePicker.calendar.locale = currentLanguageLocale
        self.datePicker.locale = currentLanguageLocale
        
        datePicker.minimumDate = minDate
//        datePicker.maximumDate = maxDate
        datePicker.timeZone =  TimeZone.current
        
        datePicker.backgroundColor = UIColor.lightGray
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            toolbar.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40)
        }
        
        datePicker.frame = CGRect(x: 0, y: 40, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        myView.frame = CGRect(x: 0, y:((UIScreen.main.bounds.height * 0.6) - 40), width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        
        if #available(iOS 13.4, *) {
            datePicker.alignmentRect(forFrame: myView.frame)
        }
        
        myView .addSubview(toolbar)
        myView .addSubview(datePicker)
        myView.backgroundColor = UIColor.white
        
        self.view.addSubview(myView)
        
    }
    
    @objc func doneDatePicker() {
        //For date formate
//        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        AFUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        AFUserSession.shared.dateFormatter.timeZone = TimeZone.current
        
        let selectedDate = AFUserSession.shared.dateFormatter.string(from: datePicker.date)

//        let startFieldDate = MBUserSession.shared.dateFormatter.date(from: dateLabel.text ?? "")
//        let endFieldDate = MBUserSession.shared.dateFormatter.date(from: endDateLb.text ?? "")
        
        //dismiss date picker dialog
        self.view.endEditing(true)
        myView .removeFromSuperview()
        
        AFUserSession.shared.dateFormatter.dateFormat = Constants.kNewAPIFormat
        dateLabel.text = selectedDate
        
        let weekday = Calendar.current.component(.weekday, from: AFUserSession.shared.dateFormatter.createDate(from: selectedDate) ?? Date().todayDate())
        self.weekDayIndex = weekday  - 1
        self.weekdaysCollectionView.reloadData()
        
        
        /*if isStartDate ?? true {
            startDateLb.text = selectedDate
            self.startDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageHistoryAPICall(StartDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date), EndDate: MBUserSession.shared.dateFormatter.string(from:endFieldDate ?? todayDate))
            
        } else {
            
            endDateLb.text = selectedDate
            self.endDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageHistoryAPICall(StartDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date), EndDate: MBUserSession.shared.dateFormatter.string(from:endFieldDate ?? todayDate))
        } */
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
        myView .removeFromSuperview()
        print("cancel")
    }
}


//MARK: - Textfeild delegate
extension SchedulePaymentVC: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if  textField == numberOfRecureTF{
            if  !(self.numberOfRecureTF.text?.isBlank ?? false){
                self.noOfWeekOrMonthTF.text = numberOfRecureTF.text
            }
            if textField.text == "" {
                numberOfRecureTF.placeholder = "0"
            }
        }
        if  textField == amountTF && amountTF.text == "" {
            amountTF.placeholder = Localized("Placeholder_EnterAmount")
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  textField == amountTF {
            amountTF.placeholder = ""
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        amountTF.placeholder = ""
        
        if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
        }
        /* Checking amount field with required values */
        if textField == amountTF {
            
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let arrayOfString = newString.components(separatedBy: ".")
            if amountTF.text?.length == 0 && string.toInt < 1 {
                return false
            } else if  arrayOfString.count > 1{
                if arrayOfString.last?.count ?? 0 > 2 {
                    return false
                }
            }
            return true
        } else {
            if textField == numberOfRecureTF {
                numberOfRecureTF.placeholder = ""
                if numberOfRecureTF.text?.length == 0 && string.toInt < 1 {
                    return false
                } else {
                }
                return true
            }
        }
        return true
    }
    
    
}


// MARK: Table Delegates & Datasource..
extension SchedulePaymentVC : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedCards?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return Localized("DeleteButtonTitle")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CardCell.identifier, for: indexPath) as! CardCell
        cell.setUp(withData: self.savedCards?[indexPath.row])
        if indexPath.row == self.selectedCardIndex{
            cell.cardNumber.textColor = .red
            cell.radioSelect.image = #imageLiteral(resourceName: "radio active")
        }else{
            cell.cardNumber.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            cell.radioSelect.image = #imageLiteral(resourceName: "radioUnSelected")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCardIndex = indexPath.row
        self.tableView.reloadData()
        
    }
    
    func setGrayButton(_ sender : AFButton, withTitle title : String?, isGrayBackground  gray: Bool) {
        sender.setTitle(title, for: UIControl.State.normal)
        if  gray{
            sender.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
        }else{
            sender.setButtonLayoutType(.whiteText_DarkPinkBG_Rounded)
        }
    }
    
    
}

// MARK: CollectionView Delegates & Datasource..
extension SchedulePaymentVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.weekDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = weekdaysCollectionView.dequeueReusableCell(withReuseIdentifier: DayCell.identifier, for: indexPath) as! DayCell
        cell.dayLabel.text = self.weekDays[indexPath.row]
        if indexPath.row == weekDayIndex{
            cell.backgroundColor = UIColor.init(hexString: "D43157")
            cell.dayLabel.textColor = .white
        }else{
            cell.dayLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 0.9253990054, green: 0.9255540371, blue: 0.925378561, alpha: 1)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.autoCheckBtn.currentImage == UIImage(named: "check_off") {
            self.weekDayIndex =  indexPath.row
            self.weekdaysCollectionView.reloadData()
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (weekdaysCollectionView.frame.size.width - 1) / 7
        
        return CGSize(width: width, height: 40.0)
    }
    
    
    
    
    
    
}




//MARK: API Calls...

extension SchedulePaymentVC{
    
    
    /// Call 'addpaymentscheduler' API.
    ///
    /// - returns: Void
    
    
    func  addPaymentScheduler(amount: String, billingCycle: String, startDate: String, recurrenceNumber: String, savedCardId: String){
        self.showActivityIndicator()
        _ = AFAPIClient.shared.addPaymentScheduler(amount: amount, billingCycle: billingCycle, startDate: startDate, recurrenceNumber: recurrenceNumber, savedCardId: savedCardId){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true

                    
                    self.showSuccessAlertWithMessage(message: resultDesc) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
    
    
    
    
    /// Call 'getsavedcards' API.
    ///
    /// - returns: Void
    func  getSavedCards(){
        self.showActivityIndicator()
        _ = AFAPIClient.shared.getSavedCards({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<SavedCardsObj>().map(JSONObject:resultData){
                        self.savedCards = resp.cardDetails

                        //FIXME: - Change the check to ">" for  new/first time case check... otherwise use "<="
                        if self.savedCards?.count ?? 0 <= 0{

                        }else{

                        }
                        self.amountTF.text = resp.lastAmount ?? ""
                        self.tableView.reloadData()
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}
