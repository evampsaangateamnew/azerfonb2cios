//
//  DayCell.swift
//  Bakcell
//
//  Created by Touseef Sarwar on 05/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit

class DayCell: UICollectionViewCell {
    
    static let identifier = "DayCell"
    
    @IBOutlet weak var dayLabel: UILabel!
    
}
