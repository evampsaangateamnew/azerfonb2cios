//
//  CardCell.swift
//  Bakcell
//
//  Created by Touseef Sarwar on 05/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {
    
    
    static let identifier = "CardCell"
    
    
    
    //MARK - IBOutlet
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var radioSelect: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func setUp(withData data: SavedCards?){
        if let infoo = data{
            if infoo.number.isBlank == true {
                self.cardNumber.text = infoo.cardMaskNumber
            } else {
                self.cardNumber.text = infoo.number
            }
            
            self.cardImage.image = infoo.cardType == "master" ? #imageLiteral(resourceName: "masterIcon")  :  #imageLiteral(resourceName: "visaIcon")
        }else{return}
    }
}
