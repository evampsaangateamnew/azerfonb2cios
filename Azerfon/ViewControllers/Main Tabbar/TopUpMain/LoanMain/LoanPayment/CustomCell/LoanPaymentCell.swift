//
//  LoanRequestCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/3/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class LoanPaymentCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var dateTimeLabel : UILabel!
    @IBOutlet var loanIDLabel   : UILabel!
    @IBOutlet var amountLabel   : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /**
     Sets Item labels values.
     
     - parameter aItem: requests data.
     
     - returns: void.
     */
    func setItemInfo(aItem: PaymentHistory) {
        dateTimeLabel.text = aItem.dateTime
        loanIDLabel.text = aItem.loanID
        amountLabel.attributedText = aItem.amount.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey, mainStringFont: UIFont.b4LightGrey, imageSize: CGSize(width: 8, height: 5))
    }
    
}
