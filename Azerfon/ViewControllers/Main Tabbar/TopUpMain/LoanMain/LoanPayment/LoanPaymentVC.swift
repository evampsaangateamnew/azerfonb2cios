//
//  LoanPaymentVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/3/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class LoanPaymentVC: BaseVC {
    
    //MARK: - Properties
    var isStartDate : Bool?
    var paymentHistoryResponse : PaymentHistoryListHandler?
    
    //MARK: - IBOutlet
    @IBOutlet var dropDown: UIDropDown!
    
    @IBOutlet var dateTitleLabel            : UILabel!
    @IBOutlet var loanIDTitleLabel          : UILabel!
    @IBOutlet var amountTitleLabel          : UILabel!
    @IBOutlet var dropdownTitleLabel        : UILabel!
    @IBOutlet var startDateLabel            : AFLabel!
    @IBOutlet var endDateLabel              : AFLabel!
    
    @IBOutlet var paymentsTableView         : UITableView!
    
    @IBOutlet var constDatePickerViewHeight : NSLayoutConstraint!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.paymentsTableView.reloadData()
        
        self.constDatePickerViewHeight.constant = 0
        self.dateDropDownChanged(transacitonIndex: 0)
        
        //init dropdown
        setDropDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func startButtonAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        let threeMonthsAgos = Date().todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLabel.text ,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: AFUserSession.shared.dateFormatter.createDate(from: endDateString, dateFormate: Constants.kDisplayFormat) ?? todayDate,
                                currentDate: currentDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate, currentDate: currentDate)
        }
        
    }
    
    @IBAction func endButtonAction(_ sender: UIButton) {
        
        isStartDate = false;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        self.showDatePicker(minDate: AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate,
                            maxDate: todayDate,
                            currentDate: currentDate)
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        LoanPaymentCell.registerReusableCell(with: paymentsTableView)
        
        self.dateTitleLabel?.text = Localized("Title_DateTime")
        self.loanIDTitleLabel?.text = Localized("Title_LoanID")
        self.amountTitleLabel?.text = Localized("Title_Amount")
        self.dropdownTitleLabel?.text = Localized("Title_SelectPeriod")
    }
    
    /*Initialze - Setup dropdown */
    func setDropDown(){
        
        //Date Drop Down
        self.dropDown.placeholder = Constants.dateDropDownOptions.first ?? ""
        
        self.dropDown.dataSource = Constants.dateDropDownOptions
        self.dropDown.didSelectOption { (index, option) in
            self.dateDropDownChanged( transacitonIndex: index)
        }
    }

    /**
     Date dropdown changed.
     
     - parameter transacitonIndex: Selected drpodown index.
     
     - returns: void
     */
    func dateDropDownChanged( transacitonIndex: Int?)  {
        
        let todayDateString = Date().todayDateString(dateFormate: Constants.kNewAPIFormat)
        switch transacitonIndex{
        case 0:
            print("default")
            self.setDatePickerHeight(constantValue: 0)
            
            self.getPaymentHistory(startDate: todayDateString, endDate: todayDateString)
        case 1:
            print("Last 7 days")
            self.setDatePickerHeight(constantValue: 0)
            
            // Get data of previous seven days
            let sevenDaysAgo = Date().todayDateString(withAdditionalValue: -7)
            self.getPaymentHistory(startDate: sevenDaysAgo, endDate: todayDateString)
            
        case 2:
            print("Last 30 days")
            self.setDatePickerHeight(constantValue: 0)
            
            // Get data of previous thirty days
            let thirtyDaysAgo = Date().todayDateString(withAdditionalValue: -30)
            self.getPaymentHistory(startDate: thirtyDaysAgo, endDate: todayDateString)
            
        case 3:
            print("Previous month")
            self.setDatePickerHeight(constantValue: 0)
            
            // Getting previous month start and end date
            let startOfPreviousMonth = Date().todayDate().getPreviousMonth()?.startOfMonth() ?? Date().todayDate()
            let endOfPreviousMonth = Date().todayDate().getPreviousMonth()?.endOfMonth() ?? Date().todayDate()
            
            let startDateString = AFUserSession.shared.dateFormatter.createString(from: startOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
            let endDateString = AFUserSession.shared.dateFormatter.createString(from: endOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
            
            self.getPaymentHistory(startDate: startDateString, endDate: endDateString)
            
        case 4:
            self.setDatePickerHeight(constantValue: 47)
            
            //For date formate
            self.startDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            self.endDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            
            self.startDateLabel.textColor = UIColor.afBrownGrey
            self.endDateLabel.textColor = UIColor.afBrownGrey
            
            self.getPaymentHistory(startDate: todayDateString, endDate: todayDateString)
            
            print("Selected Date")
            
        default:
            print("destructive")
            
        }
    }
    
    func setDatePickerHeight(constantValue : CGFloat) {
        
        if self.constDatePickerViewHeight.constant !=  constantValue {
            self.constDatePickerViewHeight.constant = constantValue
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    /**
     Show date picker.
     
     - parameter minDate: minimum date of picker.
     - parameter maxDate: maximum date of picker.
     - parameter currentDate: current date of picker.
     
     - returns: void
     */
    func showDatePicker(minDate : Date , maxDate : Date, currentDate : Date){
        self.showNewDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate) { (selectedDate) in
            let todayDate = Date().todayDate(dateFormate: Constants.kDisplayFormat)
            
            let selectedDateString = AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kDisplayFormat)
            
            let startFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            let endFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            
            
            if self.isStartDate ?? true {
                self.startDateLabel.text = selectedDateString
                self.startDateLabel.textColor = UIColor.afDateOrange
                self.getPaymentHistory(startDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat),
                                              endDate: AFUserSession.shared.dateFormatter.createString(from:endFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat))
            } else {
                self.endDateLabel.text = selectedDateString
                self.endDateLabel.textColor = UIColor.afDateOrange
                self.getPaymentHistory(startDate: AFUserSession.shared.dateFormatter.createString(from: startFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat),
                                              endDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat))
            }
        }
    }
    
    //MARK: - API Calls
    /// Call 'getPaymentHistory' API .
    ///
    /// - returns: PaymentHistory
    func getPaymentHistory(startDate:String?, endDate:String?) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.getPaymentHistory(StartDate: startDate ?? "", EndDate: endDate ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let paymentHistoryList = Mapper<PaymentHistoryListHandler>().map(JSONObject: resultData){
                        
                        self.paymentHistoryResponse = paymentHistoryList
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Reload TableView Data
            self.paymentsTableView.reloadUserData(self.paymentHistoryResponse?.paymentHistory)
        })
    }
}

//MARK: - TableView Delegates
extension LoanPaymentVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.paymentHistoryResponse?.paymentHistory?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let myCell: LoanPaymentCell = tableView.dequeueReusableCell() {
            myCell.setItemInfo(aItem: (self.paymentHistoryResponse?.paymentHistory?[indexPath.row])!)
            return myCell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
