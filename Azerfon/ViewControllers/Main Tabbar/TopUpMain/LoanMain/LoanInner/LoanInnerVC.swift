//
//  LoanInnerVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/3/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class LoanInnerVC: BaseVC {
    
    //MARK: - Properties
    var selectedLoanRequestType : Constants.AFLoanRequestType = .Me
    
    //MARK: - IBOutlet
    @IBOutlet var descriptionTextLabel          : AFLabel!
    @IBOutlet var amountTitleLabel              : AFLabel!
    @IBOutlet var loanRequestTitleLabel         : AFLabel!
    @IBOutlet var amountLabel                   : AFLabel!
    @IBOutlet var meLabel                       : AFLabel!
    @IBOutlet var friendLabel                   : AFLabel!
    
    @IBOutlet var receiverNumberTextField       : AFTextField!
    
    @IBOutlet var getLoanButton                 : AFButton!
    @IBOutlet var loanRequestForMeButton        : AFButton!
    @IBOutlet var loanRequestForFriendButton    : AFButton!
    
    @IBOutlet var numberFieldView               : AFView!
    
    @IBOutlet var creditDropDown                : UIDropDown!
    
    var creditAmountMain : String = "0.0"
    
    var predefinedAmounts = [LoanAmount]()
    var seletedAmount : LoanAmount?
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        receiverNumberTextField.delegate = self
        
        //set default option
        self.setLoanRequestLayout(loanRequestType: selectedLoanRequestType)
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "GetLoanScreenViews")+1, forKey: "GetLoanScreenViews")
        UserDefaults.standard.synchronize()
        
        predefinedAmounts = AFUserSession.shared.predefineData?.topup?.getLoan ?? [LoanAmount]()
        
        self.perform(#selector(setupDropdown), with: nil, afterDelay: 0.5)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        self.receiverNumberTextField.text = Constants.numberPrefix
        
        if let homepageHandler :HomePageModel = HomePageModel.loadFromUserDefaults(key: APIsType.homePage.selectedLocalizedAPIKey()),
            let creditAmount = homepageHandler.credit?.creditTitleValue,
            creditAmount.isBlank == false {
            
            //set amount with manat sign
           // amountLabel.attributedText = creditAmount.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
            creditAmountMain = homepageHandler.credit?.creditTitleValue ?? "0.0"
        } else {
           // amountLabel.text = ""
        }
        
        
        
    }
    
    @objc func setupDropdown() {
        creditDropDown.text = ""
        seletedAmount = predefinedAmounts[0]
        creditAmountMain = predefinedAmounts[0].amount
        amountLabel.attributedText = predefinedAmounts[0].amount.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
        var optionsArray = [String]()
        
        for optionToShow in predefinedAmounts {
            optionsArray.append(optionToShow.amount)
        }
        
        creditDropDown.dataSource = optionsArray
        
        creditDropDown.didSelectOption { (index, option) in
            self.seletedAmount = self.predefinedAmounts[index]
            self.amountLabel.attributedText = self.predefinedAmounts[index].amount.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
            self.creditDropDown.text = ""
            self.creditAmountMain = self.predefinedAmounts[index].amount
            
        }
    }
    
    //MARK: -  in-App-Survery
    func checkLoanSurvey(message:String) {
        
        if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "get_loan" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "GetLoanScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
//                    inAppSurveyVC.hideBalanceStackView = false
//                    inAppSurveyVC.attributedBalance = attributedBalanceTitle
                    inAppSurveyVC.toptupTitleText = Localized("Title_successful")
                    inAppSurveyVC.attributedMessage = NSAttributedString.init(string: message)
                    inAppSurveyVC.confirmationText = Localized("Title_successful")
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            } else {
                self.showSuccessAlertWithMessage(message: message)
            }
        } else {
            self.showSuccessAlertWithMessage(message: message)
        }
    }
    
    //MARK: - IBAction
    @IBAction func loanRequestForMePressed(_ sender: Any) {
        
        setLoanRequestLayout(loanRequestType: .Me)
    }
    
    @IBAction func loanRequestForFriendPressed(_ sender: UIButton) {
        
        setLoanRequestLayout(loanRequestType: .Friend)
    }
    
    @IBAction func getLoanButtonPressed(_ sender: UIButton) {
        
        var reciverMSISDN = ""
        var confirmationMessage :String = ""
        
        if selectedLoanRequestType == .Friend {
            
            if let msisdnText = receiverNumberTextField.text?.formatedMSISDN(),
                msisdnText.isValidMSISDN() {
                reciverMSISDN = msisdnText
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_EnterValidMSISDN"))
                return
            }
            
            _ = receiverNumberTextField.resignFirstResponder()
            
            
            confirmationMessage = String(format: Localized("Message_TopUpConfirmation"), reciverMSISDN)
            
            self.showConfirmationAlert(message: confirmationMessage, {
                
                /* User confirmed to get loan */
                if self.selectedLoanRequestType == Constants.AFLoanRequestType.Me {
                    self.getLoanAPICall(FriendsMSISDN: "")
                    
                } else if self.selectedLoanRequestType == Constants.AFLoanRequestType.Friend {
                    self.getLoanAPICall(FriendsMSISDN: reciverMSISDN)
                }
            })
            
        } else {
            
            var message = Localized("Title_SelfCreditConfirmation")
            if Double(seletedAmount?.amount ?? "") == nil &&
                seletedAmount?.amount == predefinedAmounts[predefinedAmounts.count - 1].amount  {
                message = Localized("Title_SelfCreditConfirmation2")
            }
            confirmationMessage = String.localizedStringWithFormat(message, creditAmountMain)
            self.showCreditConfirmationAlert(message: confirmationMessage, {
                
                /* User confirmed to get loan */
                if self.selectedLoanRequestType == Constants.AFLoanRequestType.Me {
                    self.getLoanAPICall(FriendsMSISDN: "")
                    
                } else if self.selectedLoanRequestType == Constants.AFLoanRequestType.Friend {
                    self.getLoanAPICall(FriendsMSISDN: reciverMSISDN)
                }
            })
        }
        
        
        /* SHOW Alert and perform operation accordinglly */
        /**/
        
        
    }
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text               = Localized("Title_TopUp")
        amountTitleLabel.text               = Localized("Title_Amount")
        self.descriptionTextLabel?.text     = Localized("Title_LoanFeaturesDescription")
        self.loanRequestTitleLabel?.text    = Localized("Title_LoanRequestTitle")
        self.meLabel?.text                  = Localized("Title_LoanOptionMe")
        self.friendLabel?.text              = Localized("Title_LoanOptionFriend")
        
        receiverNumberTextField.placeholder = Localized("PlaceHolder_ReceiverNumber")
        
        getLoanButton.setTitle(Localized("BtnTitle_GETLOAN"), for: UIControl.State.normal)
    }
    
    func setLoanRequestLayout(loanRequestType: Constants.AFLoanRequestType) {
        
        loanRequestForMeButton .setImage(#imageLiteral(resourceName: "radioUnSelected"), for: UIControl.State.normal)
        loanRequestForFriendButton .setImage(#imageLiteral(resourceName: "radioUnSelected"), for: UIControl.State.normal)
        
        switch loanRequestType {
            
        case .Me:
            loanRequestForMeButton .setImage(#imageLiteral(resourceName: "radioSelected"), for: UIControl.State.normal)
            self.selectedLoanRequestType = Constants.AFLoanRequestType.Me
            
            receiverNumberTextField.text = "\(Constants.numberPrefix)\(AFUserSession.shared.msisdn)"
            receiverNumberTextField.isEnabled = false
            
            numberFieldView.isHidden = true
            
        case .Friend:
            loanRequestForFriendButton .setImage(#imageLiteral(resourceName: "radioSelected"), for: UIControl.State.normal)
            self.selectedLoanRequestType = Constants.AFLoanRequestType.Friend
            
            receiverNumberTextField.text = Constants.numberPrefix
            receiverNumberTextField.isEnabled = true
            
            numberFieldView.isHidden = false
        }
    }
    
    //MARK: - API Calls
    /// Call 'getLoan' API.
    
    func getLoanAPICall(FriendsMSISDN friendsMsisdn : String) {
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.getLoan(FriendsMSISDN : friendsMsisdn, LoanAmount:seletedAmount?.amount ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                
                //EventLog
                if self.selectedLoanRequestType == Constants.AFLoanRequestType.Friend {
                    //Log event on getting loan for friend
                    AFAnalyticsManager.logEvent(screenName: "get_credit", contentType:"friends_credit" , successStatus:"0" )
                } else {
                    //Log event on getting loan for me
                    AFAnalyticsManager.logEvent(screenName: "get_credit", contentType:"friends_credit" , successStatus:"0" )
                }
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    if self.selectedLoanRequestType == Constants.AFLoanRequestType.Friend {
                        //Log event on getting loan for friend
                        AFAnalyticsManager.logEvent(screenName: "get_credit", contentType:"friends_credit" , successStatus:"1" )
                    } else {
                        //Log event on getting loan for me
                        AFAnalyticsManager.logEvent(screenName: "get_credit", contentType:"friends_credit" , successStatus:"1" )
                    }

                    /* Set check to true so that dashboad data will be reloaded once user redirected to dashboad */
                    AFUserSession.shared.shouldReloadDashboardData = true
                    
                    //clearing text field
                    if self.selectedLoanRequestType == Constants.AFLoanRequestType.Friend {
                        self.receiverNumberTextField.text = Constants.numberPrefix
                    }
                    
                    //show success alert
                    self.checkLoanSurvey(message: resultDesc ?? "")
                    
                }  else {
                    //EventLog
                    if self.selectedLoanRequestType == Constants.AFLoanRequestType.Friend {
                        //Log event on getting loan for friend
                        AFAnalyticsManager.logEvent(screenName: "get_credit", contentType:"friends_credit" , successStatus:"0" )
                    } else {
                        //Log event on getting loan for me
                        AFAnalyticsManager.logEvent(screenName: "get_credit", contentType:"friends_credit" , successStatus:"0" )
                    }

                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
}

//MARK: - UITextFieldDelegate
extension LoanInnerVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == receiverNumberTextField{
            
            /* Check prefix limit and disabling editing in prefix */
            let protectedRange = NSMakeRange(0, receiverNumberTextField.prefixLength)
            let intersection = NSIntersectionRange(protectedRange, range)
            
            if intersection.length > 0 ||
                range.location < (protectedRange.location + protectedRange.length) {
                
                return false
            }
            
            /* Restricting user to enter only number */
            let allowedCharactors = Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            /* Restriction for MSISDN length */
            return newLength <= (Constants.MSISDNLength + receiverNumberTextField.prefixLength)
            
        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        getLoanButtonPressed(UIButton())
        return true
    }
}
