//
//  LoanRequestCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 5/31/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class LoanRequestCell: UITableViewCell {
    
    //MARK: - IBOutlet
    @IBOutlet var loanIDTitleLabel    : UILabel!
    @IBOutlet var loanIDLabel         : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.loanIDTitleLabel.text = Localized("Title_LoanIDTitle")
        
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /**
     Sets Item labels values.
     
     - parameter aItem: requests data.
     
     - returns: void.
     */
    func setItemInfo(aItem: LoanHistory?) {
        if let itemInfo = aItem {
            loanIDLabel.text = itemInfo.loanID
        } else {
            loanIDLabel.text = ""
        }
        
    }
}
