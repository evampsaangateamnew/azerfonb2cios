//
//  LoanRequestCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/3/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class LoanRequestHeaderView: AFAccordionTableViewHeaderView {

    //MARK: - IBOutlet
    @IBOutlet var dateTimeLabel     : UILabel!
    @IBOutlet var statusLabel       : UILabel!
    @IBOutlet var paidAmountLabel   : UILabel!
    @IBOutlet var remainingLabel    : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /**
     Sets Item labels values.
     
     - parameter aItem: requests data.
     
     - returns: void.
     */
    func setItemInfo(aItem: LoanHistory?) {
        
        if let itemInfo = aItem {
            dateTimeLabel.text = itemInfo.dateTime
            statusLabel.text = itemInfo.status
            paidAmountLabel.attributedText = itemInfo.paid.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey, mainStringFont: UIFont.b4LightGrey, imageSize: CGSize(width: 8, height: 5))
            remainingLabel.attributedText = itemInfo.remaining.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey, mainStringFont: UIFont.b4LightGrey, imageSize: CGSize(width: 8, height: 5))
        } else {
            dateTimeLabel.text = ""
            statusLabel.text = ""
            paidAmountLabel.text = ""
            remainingLabel.text = ""
        }
        
    }
    
}
