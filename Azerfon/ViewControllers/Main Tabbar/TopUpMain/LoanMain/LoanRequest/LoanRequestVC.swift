//
//  LoanRequestVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/3/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class LoanRequestVC: BaseVC {
    
    //MARK: - Properties
    var isStartDate : Bool?
    var loanHistoryResponse : LoanHistoryListHandler?
    
    //MARK: - IBOutlet
    @IBOutlet var dropDown: UIDropDown!
    
    @IBOutlet var dateTimeTitleLabel     : UILabel!
    @IBOutlet var statusTitleLabel       : UILabel!
    @IBOutlet var paidAmountTitleLabel   : UILabel!
    @IBOutlet var remainingTitleLabel    : UILabel!
    @IBOutlet var dropdownTitleLabel     : UILabel!
    @IBOutlet var startDateLabel         : AFLabel!
    @IBOutlet var endDateLabel           : AFLabel!
    
    @IBOutlet var requestsTableView : AFAccordionTableView!
    
    @IBOutlet var constDatePickerViewHeight : NSLayoutConstraint!

    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.requestsTableView.allowMultipleSectionsOpen = true
        self.requestsTableView.allowsSelection = false
        self.requestsTableView.delegate = self
        self.requestsTableView.dataSource = self
        self.requestsTableView.estimatedRowHeight = 30
        self.requestsTableView.reloadData()
        
        self.constDatePickerViewHeight.constant = 0
        self.dateDropDownChanged(transacitonIndex: 0)
        
        //init dropdown
        setDropDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        
    }
    
    //MARK: - IBAction
    @IBAction func startButtonAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        let threeMonthsAgos = Date().todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLabel.text ,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: AFUserSession.shared.dateFormatter.createDate(from: endDateString, dateFormate: Constants.kDisplayFormat) ?? todayDate,
                                currentDate: currentDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate, currentDate: currentDate)
        }
        
    }
    
    @IBAction func endButtonAction(_ sender: UIButton) {
        
        isStartDate = false;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        self.showDatePicker(minDate: AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate,
                            maxDate: todayDate,
                            currentDate: currentDate)
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
//        LoanRequestHeaderView.registerReusableCell(with: requestsTableView)
        
        self.dateTimeTitleLabel?.text = Localized("Title_DateTime")
        self.statusTitleLabel?.text = Localized("Title_Status")
        self.paidAmountTitleLabel?.text = Localized("Title_Paid")
        self.remainingTitleLabel?.text = Localized("Title_Remaining")
        self.dropdownTitleLabel?.text = Localized("Title_SelectPeriod")
    }
    
    /*Initialze - Setup dropdown */
    func setDropDown() {
        
        //Date Drop Down
        self.dropDown.placeholder = Constants.dateDropDownOptions.first ?? ""
        
        self.dropDown.dataSource = Constants.dateDropDownOptions
        self.dropDown.didSelectOption { (index, option) in
            self.dateDropDownChanged( transacitonIndex: index)
        }
    }
    
        /**
         Date dropdown changed.
         
         - parameter transacitonIndex: Selected drpodown index.
         
         - returns: void
         */
        func dateDropDownChanged( transacitonIndex: Int?)  {
            
            let todayDateString = Date().todayDateString(dateFormate: Constants.kNewAPIFormat)
            switch transacitonIndex{
            case 0:
                print("default")
                self.setDatePickerHeight(constantValue: 0)
                
                self.getLoanHistryList(startDate: todayDateString, endDate: todayDateString)
            case 1:
                print("Last 7 days")
                self.setDatePickerHeight(constantValue: 0)
                
                // Get data of previous seven days
                let sevenDaysAgo = Date().todayDateString(withAdditionalValue: -7)
                self.getLoanHistryList(startDate: sevenDaysAgo, endDate: todayDateString)
                
            case 2:
                print("Last 30 days")
                self.setDatePickerHeight(constantValue: 0)
                
                // Get data of previous thirty days
                let thirtyDaysAgo = Date().todayDateString(withAdditionalValue: -30)
                self.getLoanHistryList(startDate: thirtyDaysAgo, endDate: todayDateString)
                
            case 3:
                print("Previous month")
                self.setDatePickerHeight(constantValue: 0)
                
                // Getting previous month start and end date
                let startOfPreviousMonth = Date().todayDate().getPreviousMonth()?.startOfMonth() ?? Date().todayDate()
                let endOfPreviousMonth = Date().todayDate().getPreviousMonth()?.endOfMonth() ?? Date().todayDate()
                
                let startDateString = AFUserSession.shared.dateFormatter.createString(from: startOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
                let endDateString = AFUserSession.shared.dateFormatter.createString(from: endOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
                
                self.getLoanHistryList(startDate: startDateString, endDate: endDateString)
                
            case 4:
                self.setDatePickerHeight(constantValue: 47)
                
                //For date formate
                self.startDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
                self.endDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
                
                self.startDateLabel.textColor = UIColor.afBrownGrey
                self.endDateLabel.textColor = UIColor.afBrownGrey
                
                self.getLoanHistryList(startDate: todayDateString, endDate: todayDateString)
                
                print("Selected Date")
                
            default:
                print("destructive")
                
            }
        }
        
        func setDatePickerHeight(constantValue : CGFloat) {
            
            if self.constDatePickerViewHeight.constant !=  constantValue {
                self.constDatePickerViewHeight.constant = constantValue
                
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
            
        }
        
        /**
         Show date picker.
         
         - parameter minDate: minimum date of picker.
         - parameter maxDate: maximum date of picker.
         - parameter currentDate: current date of picker.
         
         - returns: void
         */
        func showDatePicker(minDate : Date , maxDate : Date, currentDate : Date){
            
            self.showNewDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate) { (selectedDate) in
                let todayDate = Date().todayDate(dateFormate: Constants.kDisplayFormat)
                
                let selectedDateString = AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kDisplayFormat)
                
                let startFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
                let endFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
                
                if self.isStartDate ?? true {
                    self.startDateLabel.text = selectedDateString
                    self.startDateLabel.textColor = UIColor.afDateOrange
                    
                    self.getLoanHistryList(startDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat),
                                           endDate: AFUserSession.shared.dateFormatter.createString(from:endFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat))
                    
                } else {
                    self.endDateLabel.text = selectedDateString
                    self.endDateLabel.textColor = UIColor.afDateOrange
                    
                    self.getLoanHistryList(startDate: AFUserSession.shared.dateFormatter.createString(from: startFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat),
                                           endDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat))
                    
                }
            }
        }
        
    //MARK: - API Calls
    /// Call 'getLoanHistryList' API .
    ///
    /// - returns: LoanHistryList
    func getLoanHistryList(startDate:String?, endDate:String?) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.getLoanHistory(StartDate: startDate ?? "", EndDate: endDate ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let loanHistryList = Mapper<LoanHistoryListHandler>().map(JSONObject: resultData){
                    
                    self.loanHistoryResponse = loanHistryList
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Reload tableView data after API response
            self.requestsTableView.reloadUserData(self.loanHistoryResponse?.loanHistory)
        })
    }
}

//MARK: - TableView Delegates
extension LoanRequestVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return loanHistoryResponse?.loanHistory?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView :LoanRequestHeaderView = tableView.dequeueReusableHeaderFooterView() {
            
            headerView.setItemInfo(aItem: (loanHistoryResponse?.loanHistory?[section]))
            
            return headerView
        }
        return AFAccordionTableViewHeaderView()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :LoanRequestCell = tableView.dequeueReusableCell() {
            
            cell.setItemInfo(aItem: (loanHistoryResponse?.loanHistory?[indexPath.section]))
            
            return cell
        }
        
        return UITableViewCell()
    }
}

extension LoanRequestVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        loanHistoryResponse?.loanHistory?[section].isSectionExpanded = true
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        loanHistoryResponse?.loanHistory?[section].isSectionExpanded = false
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}
