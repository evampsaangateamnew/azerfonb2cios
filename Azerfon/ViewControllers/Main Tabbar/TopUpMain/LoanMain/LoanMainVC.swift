//
//  LoanVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/3/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class LoanMainVC: BaseVC {
    
    //MARK: - Properties
    let loanTypesList : [String] = [Localized("Btn_Title_loan"), Localized("Btn_Title_request"), Localized("Btn_Title_payment")]
    var selectedIndex :IndexPath = IndexPath(item: 0, section: 0)
    
    var selectedType  : Constants.AFLoanScreenType = .loan
    var loanInnerVC   : LoanInnerVC?
    var loanRequestVC : LoanRequestVC?
    var loanPaymentVC : LoanPaymentVC?
    
    //MARK: - IBOutlet
    @IBOutlet var mainContentView    : AFView!
    
    @IBOutlet var loanOptionsCollectionView   : UICollectionView!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loanOptionsCollectionView.delegate = self
        loanOptionsCollectionView.dataSource = self
        loanOptionsCollectionView.allowsMultipleSelection = false
        
        NormalTypeCell.registerReusableCell(with: loanOptionsCollectionView)
        self.loanOptionsCollectionView.reloadData()
        self.loanOptionsCollectionView.selectItem(at: self.selectedIndex, animated: true, scrollPosition: .left)
        
        self.reDirectUserToSelectedScreen(type: selectedType)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_Loan")
    }
    
    /**
     Add child view controller to parent view.
     
     - Conditions:
     
     1. if storeLocatorData != nil
     1. StoreMapVC: if selectedType == StoreLocator
     2. StoreListVC: if selectedType == StoreList
     2. GetStoresList
     
     
     - returns: void
     */
    func reDirectUserToSelectedScreen(type :Constants.AFLoanScreenType) {
        
        //removing all childs viewscontrollers from mainView
        self.removeChildViewController(childController: LoanInnerVC())
        self.removeChildViewController(childController: LoanRequestVC())
        self.removeChildViewController(childController: LoanPaymentVC())
        
        //check each tabs one by one and perform operations accordingly
        if type == .loan {
            
            if (loanInnerVC == nil) {
                loanInnerVC = LoanInnerVC.instantiateViewControllerFromStoryboard()
            }
            
            self.addChildViewController(childController: loanInnerVC ?? LoanInnerVC(), onView: mainContentView)
            
        } else if type == .request {
            
            if (loanRequestVC == nil) {
                loanRequestVC = LoanRequestVC.instantiateViewControllerFromStoryboard()
            }
            
            self.addChildViewController(childController: loanRequestVC ?? LoanRequestVC(), onView: mainContentView)
            
        } else if type == .payment {
            
            if (loanPaymentVC == nil) {
                loanPaymentVC = LoanPaymentVC.instantiateViewControllerFromStoryboard()
            }
            
            self.addChildViewController(childController: loanPaymentVC ?? LoanPaymentVC(), onView: mainContentView)
        }
    }
    
    //MARK: - API's Calls
}


extension LoanMainVC :UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let str: String = loanTypesList[indexPath.item]

        let cellSize : CGSize = CGSize(width: ((str as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.afSans(fontSize: 14)]).width + 50), height: 36)
        
        if cellSize.width > ((UIScreen.main.bounds.width - 34 - 20) / 3) {
            return cellSize
        } else {
            return CGSize(width: ((UIScreen.main.bounds.width - 34 - 20) / 3), height: 36)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return loanTypesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell :NormalTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier: NormalTypeCell.cellIdentifier(), for: indexPath) as? NormalTypeCell {
            
            cell.setLayout(title: loanTypesList[indexPath.item],
                           isSelected: selectedIndex == indexPath ? true : false,
                           isSpecial: false )
            
            
            
            return cell
        }
        
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath
        if let cell :NormalTypeCell = collectionView.cellForItem(at: indexPath) as? NormalTypeCell {
            cell.setLayout(title: loanTypesList[indexPath.item],
                           isSelected: true,
                           isSpecial: false )
            
            switch indexPath.row {
            case 0:
                selectedType = Constants.AFLoanScreenType.loan
            case 1:
                selectedType = Constants.AFLoanScreenType.request
            case 2:
                selectedType = Constants.AFLoanScreenType.payment
            default:
                selectedType = Constants.AFLoanScreenType.loan
            }
            
            self.reDirectUserToSelectedScreen(type: selectedType)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let cell :NormalTypeCell = collectionView.cellForItem(at: indexPath) as? NormalTypeCell {
            cell.setLayout(title: loanTypesList[indexPath.item],
                           isSelected: false,
                           isSpecial: false )
        }
    }
}
