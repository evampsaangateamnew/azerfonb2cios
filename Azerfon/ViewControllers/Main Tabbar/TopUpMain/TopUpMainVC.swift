//
//  TopUpVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class TopUpMainVC: BaseVC {

    //MARK: - Property
    var topupList : [Items] = []
    var topupList2 : [SubItems] = []
    var titleValue :String = Localized("Title_TopUp")
    var showBackButton: Bool?
    
    //MARK: - IBOutlet
    @IBOutlet var topupTableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        
        //getting services from session and adding into local array and reloading data
        if showBackButton == true {
            for aHorizontalMenu in AFUserSession.shared.appMenu?.menuHorizontal ?? [MenuHorizontal]() {
                if aHorizontalMenu.identifier == Constants.HorizontalMenus.services.rawValue {
                    if let submenu: [Items] = aHorizontalMenu.items {
                        for submenItem in submenu {
                            if submenItem.identifier == "service_topup" {
                                topupList2 = submenItem.subitems ?? []
                                self.titleLabel?.text = submenItem.title.isBlank ? Localized("service_topup") : submenItem.title
                                break
                            }
                        }
                    }
                }
            }
        } else {
            //getting services from session and adding into local array and reloading data
            AFUserSession.shared.appMenu?.menuHorizontal?.forEach({ (aHorizontalMenu) in
                if aHorizontalMenu.identifier == Constants.HorizontalMenus.topup.rawValue {
                    topupList = aHorizontalMenu.items ?? []
                    self.titleLabel?.text = aHorizontalMenu.title.isBlank ? Localized("Title_TopUp") : aHorizontalMenu.title
                }
            })
        }
        if showBackButton == true {
            topupList2.sort{ $0.sortOrder.toInt < $1.sortOrder.toInt }
            loadViewContent()
            topupTableView.reloadUserData(topupList2)
        } else {
            topupList.sort{ $0.sortOrder.toInt < $1.sortOrder.toInt }
            loadViewContent()
            topupTableView.reloadUserData(topupList)
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if showBackButton == true {
            self.menuButton?.setImage(UIImage(named: "backImg"), for: .normal)
        } else {
            self.menuButton?.setImage(UIImage(named: "menu_icon"), for: .normal)
        }
    }
    
    //MARK: - IBAction
    override func menuPressed(_ sender: AnyObject) {
        if showBackButton == true {
            self.backPressed(sender)
        } else {
            super.menuPressed(sender)
        }
    }
    
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        ServicesCell.registerReusableCell(with: topupTableView)
        //self.titleLabel?.text = titleValue
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier : String) {
        
        switch identifier {
            
        case "topup_topup":
            // Load TopUp ViewController
            if let topUpVC :TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(topUpVC, animated: true)
            }
            break
            
        case "topup_fasttopup":
            // Load TopUp ViewController
            if let fastTopupVC :FastTopupVC = FastTopupVC.instantiateViewControllerFromStoryboard2() {
                self.navigationController?.pushViewController(fastTopupVC, animated: true)
            }
            break
            
        case "topup_autopayment":
            // Load TopUp ViewController
            if let autoPaymentVC :AutoPaymentVC = AutoPaymentVC.instantiateViewControllerFromStoryboard2() {
                self.navigationController?.pushViewController(autoPaymentVC, animated: true)
            }
            break
            
        case "topup_money_transfer":
            // Load MoneyTransfer ViewController
            if let moneyTransferVC :MoneyTransferVC = MoneyTransferVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(moneyTransferVC, animated: true)
            }
            break
            
        case "topup_money_request":
            // Load MoneyTransfer ViewController
            if let moneyRequestVC :MoneyRequestVC = MoneyRequestVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(moneyRequestVC, animated: true)
            }
            break
            
        case "topup_loan":
            // Load MoneyTransfer ViewController
            if let loanMainVC :LoanMainVC = LoanMainVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(loanMainVC, animated: true)
            }
            break
            
        default:
            break
        }
    }
}

//MARK: - TableView Delegates
extension TopUpMainVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showBackButton == true {
            return topupList2.count;
        } else {
            return topupList.count;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let myCell: ServicesCell = tableView.dequeueReusableCell() {
            if showBackButton == true {
                myCell.setItemInfo2(aItem: topupList2[indexPath.row])
            } else {
                myCell.setItemInfo(aItem: topupList[indexPath.row])
            }
            return myCell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Redirect user to selected screen
        if showBackButton == true {
            redirectUserToSelectedController(Identifier: topupList2[indexPath.row].identifier)
        } else {
            redirectUserToSelectedController(Identifier: topupList[indexPath.row].identifier)
        }
    }
}
