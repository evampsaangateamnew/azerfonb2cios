//
//  TopUpVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/1/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper


class TopUpVC: BaseVC {
    
    
    //MARK: - IBOutlet
    @IBOutlet var descriptionTextLabel       : AFLabel!
    @IBOutlet var selectOptionTitleLabel     : AFLabel!
    @IBOutlet var scratchCardLabel           : AFLabel!
    @IBOutlet var plasticCardLabel           : AFLabel!
    
    @IBOutlet var receiverNumberTextField    : AFTextField!
    @IBOutlet var cardTextField              : AFCardTextField!
    
    @IBOutlet var payButton                  : AFButton!
    @IBOutlet var topUpByScratchCardButton   : AFButton!
    @IBOutlet var topUpByPlasticCardButton   : AFButton!
    
    @IBOutlet var amountTextField    : AFTextField!
    
    @IBOutlet var mainStackView: UIStackView!
    @IBOutlet var cardTypeStackView: UIStackView!
    @IBOutlet var cardType_lbl: AFLabel!
    @IBOutlet var cardType_image : UIImageView!
    @IBOutlet var cardTypeDropDown: UIDropDown!
    
    
    
    
    
    @IBOutlet var savedCardStackView: UIStackView!
    @IBOutlet var selectCardLbl: AFLabel!
    @IBOutlet var cardsTableView: UITableView!
    
    @IBOutlet var storeCardStackView: UIStackView!
    @IBOutlet weak var storeCardForPayment_btn : UIButton!
    @IBOutlet weak var storeCardForPaymentDescription_lbl : UILabel!
    
    
    
    //MARK: Properties
    
    
    var isScratchCardSelected: Bool? = false // true
    var selectedTopUpType : Constants.AFTopUpType = .PlasticCard
    
    
    var cardTypes : [String] = ["master","visa"]

    var selectedCardType : String = ""
    var selectedSaveCardIndex : Int = -1
    var isSaveCard : Bool? = true
    var selectedCard : PlasticCard?
    
    
    var savedCards : [SavedCards]?
    

    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AFUserSession.shared.predefineData?.plasticCards?.count ?? 0 > 0 {
            cardTypes.removeAll()
            for cardObject in AFUserSession.shared.predefineData?.plasticCards ?? [PlasticCard]() {
                cardTypes.append(cardObject.cardVale ?? "")
            }
        }
        
        
        
        cardsTableView.tableHeaderView = UIView()
        receiverNumberTextField.delegate = self
        receiverNumberTextField.text = Constants.numberPrefix + AFUserSession.shared.msisdn
        cardTextField.delegate = self
        cardTextField.formatting = .scratchCard
        amountTextField.delegate  = self
        
        setUpDropdown()
        cardsTableView.register(UINib(nibName: CardCell.identifier, bundle: nil), forCellReuseIdentifier: CardCell.identifier)
        cardsTableView.register(UINib(nibName: AddCardPaymentCell.ID, bundle: nil), forCellReuseIdentifier: AddCardPaymentCell.ID)
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "TopUpScreenViews")+1, forKey: "TopUpScreenViews")
        UserDefaults.standard.synchronize()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
//        selectTopUpOptionLayout(topUpType: selectedTopUpType)
        loadViewContent()
        self.getSavedCards()
    }
    
    
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text                = Localized("Title_TopUp")
        self.descriptionTextLabel?.text      = Localized("Title_TopUpTypesDescription")
        self.selectOptionTitleLabel?.text    = Localized("Title_SelectOption")
        self.scratchCardLabel?.text          = Localized("Title_OptionScratchCard")
        self.plasticCardLabel?.text          = Localized("Title_OptionPlasticCard")
        
        receiverNumberTextField.placeholder  = Localized("Placeholder_MobileNumber")
        cardTextField.placeholder            = Localized("PlaceHolder_CardNumber")
        
        
        self.amountTextField.placeholder     = Localized("Placeholder_EnterAmount")
        self.cardType_lbl.text               = Localized("Title_CardType")
        
        //saved card....
        self.selectCardLbl.text               = Localized("Title_SelectCard")
        
        //store payment
        self.storeCardForPaymentDescription_lbl.text = Localized("Title_StoreCardDescription")
        
        
        payButton.setTitle((selectedTopUpType == .PlasticCard) ? Localized("BtnTitle_CONTINUE") : Localized("BtnTitle_Pay"), for: UIControl.State.normal)
        
        
        if isScratchCardSelected ?? true{
            self.selectTopUpOptionLayout(topUpType: .ScratchCard)
        }else{
            self.selectTopUpOptionLayout(topUpType: .PlasticCard)
        }
//        receiverNumberTextField.text = AFUserSession.shared.msisdn
    }
    
    /**
     Dropdown setup
     */
    func setUpDropdown(){
//            var titles : [String] = []
//
//            selectedAmountList?.forEach({ (aPrepaidAmount) in
//                titles.append(aPrepaidAmount.amount ?? "")
//            })
            
        self.cardTypeDropDown.dataSource = cardTypes
        self.cardTypeDropDown.placeholder = AFUserSession.shared.predefineData?.plasticCards?.first?.cardVale ?? "master"
        
        self.selectedCardType = String(AFUserSession.shared.predefineData?.plasticCards?.first?.cardKey  ??  1)
            self.cardTypeDropDown.didSelectOption { (index, option) in
                self.selectedCard = AFUserSession.shared.predefineData?.plasticCards?[index]
                self.selectedCardType = String(self.selectedCard?.cardKey ?? 1)
            }
        
    }
    
    
    func selectTopUpOptionLayout(topUpType: Constants.AFTopUpType) {
        
        topUpByScratchCardButton.setImage(#imageLiteral(resourceName: "radioUnSelected"), for: UIControl.State.normal)
        topUpByPlasticCardButton.setImage(#imageLiteral(resourceName: "radioUnSelected"), for: UIControl.State.normal)
        
        switch topUpType {
            
        case .ScratchCard:
            self.mainStackView.isHidden = true
            cardTextField.isHidden = false
            topUpByScratchCardButton.setImage(#imageLiteral(resourceName: "radioSelected"), for: UIControl.State.normal)
            self.selectedTopUpType = Constants.AFTopUpType.ScratchCard
            payButton.setTitle(Localized("BtnTitle_Pay"), for: UIControl.State.normal)
            
        case .PlasticCard:
            self.mainStackView.isHidden = false
            cardTextField.isHidden = true
            topUpByPlasticCardButton.setImage(#imageLiteral(resourceName: "radioSelected"), for: UIControl.State.normal)
            self.selectedTopUpType = Constants.AFTopUpType.PlasticCard
            payButton.setTitle(Localized("BtnTitle_CONTINUE"), for: UIControl.State.normal)
            
        }
    }
    
    
    
    
    func checkTopupSurvey(message: String) {
        if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "topup" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "-1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "TopUpScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
                    inAppSurveyVC.toptupTitleText = Localized("Title_Alert")
                    inAppSurveyVC.attributedMessage = NSAttributedString.init(string: message)
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            } else {
                self.showSuccessAlertWithMessage(message: message) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            self.showSuccessAlertWithMessage(message: message) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    

}
//MARK: - IBAction
extension TopUpVC{
    
  
    @IBAction func topUpByScratchCardPressed(_ sender: Any) {
        selectTopUpOptionLayout(topUpType: .ScratchCard)
    }
    
    @IBAction func topUpByPlasticCardPressed(_ sender: UIButton) {
        
        selectTopUpOptionLayout(topUpType: .PlasticCard)
    }
    
    @IBAction func payButtonPressed(_ sender: UIButton) {
        
        var reciverMSISDN = ""
        if let msisdnText = receiverNumberTextField.text?.formatedMSISDN(),
            msisdnText.isValidMSISDN() {
            
            reciverMSISDN = msisdnText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
            return
        }
        
        if selectedTopUpType == .PlasticCard {
            if self.amountTextField.text?.isBlank ?? true{
                showErrorAlertWithMessage(message: Localized("Lbl_EnterAmount"))
                return
            }
            if self.selectedCardType.isBlank{
                self.showErrorAlertWithMessage(message: Localized("Lbl_SelectCard"))
                return
            }
            
            
            if selectedSaveCardIndex == -1 {
                self.isSaveCard = false
                showCardTypesAlert()
                return
                
            } else {
                let amount = amountTextField.text ?? "0"
                let number = receiverNumberTextField.text ?? "0"
                var messageString = String(format: Localized("Message_LoadConfirmation"), amount, number)
                if AFLanguageManager.userSelectedLanguageShortCode() == "az-Cyrl" {
                    messageString = String(format: Localized("Message_LoadConfirmation"), number, amount)
                }
//                let message = String(format: Localized("Message_LoadConfirmation"), amountTextField.text!, receiverNumberTextField.text!)
                self.showConfirmationAlert(message: messageString,  {
                    self.makepayment(topUpNumber: reciverMSISDN)
                })
                
            }
        } else {
            
            var cardPinNumber = ""
            if cardTextField.text?.isValidCardNumber() ?? false {
                
                cardPinNumber = cardTextField.text ?? ""
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidCardNumber"))
                return
            }
            
            _ = receiverNumberTextField.resignFirstResponder()
            _ = cardTextField.resignFirstResponder()
            
            /* SHOW Alert and perform operation accordinglly */
            self.showConfirmationAlert(message: String(format: Localized("Message_TopUpConfirmation"), reciverMSISDN), {
                /* User confirmed to toup */
                self.requestTopUp(SubscriberType: AFUserSession.shared.subscriberType().rawValue, CardNumber: cardPinNumber, TopUp: reciverMSISDN)
            })
        }
    }
    
    //Checkbox tapped...
    @IBAction  func checkBoxTapped(_ sender: UIButton){
        if sender.currentImage == UIImage(named: "check_on") {
            sender.setImage(UIImage(named: "check_off"), for: .normal)
            self.isSaveCard = false
        }else{
            sender.setImage(UIImage(named: "check_on"), for: .normal)
            self.isSaveCard = true
        }
    }
}


//MARK: - Table delegates & datasource

extension TopUpVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.savedCards?.count ?? 0) + 1
        
//        return  3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54.0
    }
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return Localized("DeleteButtonTitle")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.savedCards?.count ?? 0{
//        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: AddCardPaymentCell.ID, for: indexPath) as! AddCardPaymentCell
            cell.lblTitle.text = Localized("Lbl_AddAnotherCard")
            if self.savedCards?.count == 0 {
                cell.lblTitle.text = Localized("Lbl_AddACard")
                cell.imgIcon.image = #imageLiteral(resourceName: "fastPaymentAddCard")
            }
            cell.imgarrow.isHidden = true
            return cell
        }else{
            
            let cell = cardsTableView.dequeueReusableCell(withIdentifier: CardCell.identifier, for: indexPath) as! CardCell
            cell.setUp(withData: self.savedCards?[indexPath.row])
            if indexPath.row == self.selectedSaveCardIndex{
                cell.cardNumber.textColor = UIColor.afSuperPink
                cell.radioSelect.image = #imageLiteral(resourceName: "radio active")
            }else{
                cell.cardNumber.textColor = #colorLiteral(red: 0.3019607843, green: 0.3019607843, blue: 0.3098039216, alpha: 1)
                cell.radioSelect.image = #imageLiteral(resourceName: "radioUnSelected")
            }
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == self.savedCards?.count ?? 0{
//        if indexPath.row == 2{
            var reciverMSISDN =  ""
            if let msisdnText = receiverNumberTextField.text?.formatedMSISDN(),
                msisdnText.isValidMSISDN() {
                
                reciverMSISDN = msisdnText
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
                return
            }
            /*if self.amountTextField.text?.isBlank ?? true{
                self.showErrorAlertWithMessage(message: Localized("Lbl_EnterAmount"))
                return
            }
            let plastic = self.myStoryBoard.instantiateViewController(withIdentifier:  CardTypeSelectionVC.identifier) as! CardTypeSelectionVC
            plastic.selectedAmount = self.amountTextField.text ?? "0"
            plastic.selectedMsisdn = reciverMSISDN
            self.navigationController?.pushViewController(plastic, animated: true)*/
            self.isSaveCard = true
            showCardTypesAlert()
            
            
        }else{
            if indexPath.row == self.selectedSaveCardIndex {
                self.selectedSaveCardIndex = -1
            } else {
                self.selectedSaveCardIndex = indexPath.row
            }
            
            self.selectedCardType = self.savedCards?[indexPath.row].cardType ?? ""
            self.cardTextField.text = self.savedCards?[indexPath.row].number ?? ""
        }
        self.cardsTableView.reloadData()
    }
    
    
    //Swipe to Delete
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return  true
        if self.savedCards?.count ?? 0 > 0 {
            if indexPath.row < self.savedCards?.count ?? 0 {
                return true
            }
        }
        return false
    }
    
    /*func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if  editingStyle == .delete{
            self.deleteSavedCard(savedCardId: self.savedCards?[indexPath.row].id ?? "") { (isDeleted) in
                if isDeleted{
                    self.savedCards?.remove(at: indexPath.row)
                    self.cardsTableView.reloadData()

                }
            }
        }
    }*/
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: Localized("DeleteButtonTitle")) { (action, indexPath) in
                self.deleteSavedCard(savedCardId: self.savedCards?[indexPath.row].id ?? "") { (isDeleted) in
                    if isDeleted{
                        self.savedCards?.remove(at: indexPath.row)
                        self.cardsTableView.reloadData()

                    }
                }
            }
        delete.backgroundColor = UIColor.afSuperPink//UIColor.init(hexString: "A51837")
        return[delete]
    }
    
}

//MARK: - UITextFieldDelegate
extension TopUpVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if let char = string.cString(using: String.Encoding.utf8) {
//                let isBackSpace = strcmp(char, "\\b")
//                if (isBackSpace == -92) {
//                    return true
//                }
//        }
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == receiverNumberTextField ||  textField == cardTextField {
            
            /* Check prefix limit and disabling editing in prefix */
            let protectedRange = NSMakeRange(0, (textField == receiverNumberTextField) ? receiverNumberTextField.prefixLength : cardTextField.prefixLength)
            let intersection = NSIntersectionRange(protectedRange, range)
            
            if intersection.length > 0 ||
                range.location < (protectedRange.location + protectedRange.length) {
                
                return false
            }
            
            /* Restricting user to enter only number */
            let allowedCharactors = Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            if textField == receiverNumberTextField {
                /* Restriction for MSISDN length */
                return newLength <= (Constants.MSISDNLength + receiverNumberTextField.prefixLength)
            } else if textField == cardTextField {
                
                /* Restriction for 13 digit card number length */
                return newLength <= (Constants.ValidCardNumberLength + cardTextField.prefixLength + 3)
            }
            
        }
        else if textField == amountTextField{
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let arrayOfString = newString.components(separatedBy: ".")
            if amountTextField.text?.length == 0 && string.toInt < 1 {
                return false
            } else if  arrayOfString.count > 1{
                if arrayOfString.last?.count ?? 0 > 2 {
                    return false
                }
            }
            return true

        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == receiverNumberTextField {
            
            if selectedTopUpType == .PlasticCard {
                payButtonPressed(UIButton())
                return true
            } else {
                cardTextField.becomeFirstResponder()
                return false
            }
            
        } else if textField == cardTextField {
            payButtonPressed(UIButton())
            return false
        }
        return true
    }
}



//MARK: API calls...
extension TopUpVC{
    
    
    
    /// Call 'initiatePayment' API.
    ///
    /// - returns: Void
    func  initiatePayment(topUpNumber: String){
        self.showActivityIndicator()
        var topUpamount = "0"
        if isSaveCard == false {
            topUpamount = self.amountTextField.text  ?? "0"
        }
        _ = AFAPIClient.shared.initiatePayment(CardType: self.selectedCardType, Amount: topUpamount, IsSaved: self.isSaveCard ?? false, TopUpNumber: topUpNumber){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<SavedCards>().map(JSONObject: resultData){
                        if resp.paymentKey.isEmpty || resp.paymentKey == "" {
                            //do nothing
                        } else {
                            if let plasticCardVC = self.myStoryBoard.instantiateViewController(withIdentifier: "PlasticCardVC") as? PlasticCardVC {
                                plasticCardVC.topUpToMSISDN = self.receiverNumberTextField.text ?? ""
                                plasticCardVC.redirectionURL = resp.paymentKey
                                if topUpamount.toInt > 0 {
                                    plasticCardVC.isSaveCard = self.isSaveCard ?? false
                                }
                                self.navigationController?.pushViewController(plasticCardVC, animated: true)
                            }
                        }
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
    
    
    /// Call 'makePayment' API.
    ///
    /// - returns: Void
    func  makepayment(topUpNumber: String){
        self.showActivityIndicator()
        _ = AFAPIClient.shared.makepayment(PaymentKey: self.savedCards?[selectedSaveCardIndex].paymentKey ?? "", Amount: amountTextField.text ?? "", TopupNumber: topUpNumber){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                        self.checkTopupSurvey(message: resultDesc ?? "")
                    }
                    AFUserSession.shared.shouldReloadDashboardData = true
                    
//
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
    
    /// Call 'deletesavedcard' API.
    ///
    /// - returns: Void
    func  deleteSavedCard(savedCardId: String, onCompletion : @escaping (Bool) -> ()){
        self.showActivityIndicator()
        _ = AFAPIClient.shared.deleteSavedcard(savedCardId: savedCardId){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    self.showSuccessAlertWithMessage(message: resultDesc) {
                        onCompletion(true)
//                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
    
    
    /// Call 'getsavedcards' API.
    ///
    /// - returns: Void
    func  getSavedCards(){
        self.showActivityIndicator()
        _ = AFAPIClient.shared.getSavedCards({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<SavedCardsObj>().map(JSONObject:resultData){
                        self.savedCards = resp.cardDetails
                        if (resp.lastAmount ?? "").toFloat >= 1.0 {
                            self.amountTextField.text = resp.lastAmount ?? ""
                        } else {
                            self.amountTextField.text = ""
                        }
                        
                        //FIXME: - Change the check to ">" for  new/first time case check... otherwise use "<="
                        if self.savedCards?.count ?? 0 <= 0{
                            self.selectedSaveCardIndex = -1
                            self.cardTypeStackView.isHidden = true
                            self.savedCardStackView.isHidden = false
                            self.storeCardStackView.isHidden = true
                            self.selectCardLbl.isHidden = true
                        }else{
                            self.selectedSaveCardIndex = 0
                            self.cardTypeStackView.isHidden = true
                            self.savedCardStackView.isHidden = false
                            self.storeCardStackView.isHidden = true
                            self.selectCardLbl.isHidden = true
                        }
                        self.cardsTableView.reloadData()
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    
    /// Call 'TopUp' API.
    ///
    /// - returns: Void
    
    func requestTopUp( SubscriberType subscriberType: String, CardNumber cardPinNumber : String, TopUp topUpNum : String) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.requestTopUp(SubscriberType: subscriberType, CardPin: cardPinNumber , TopupNumber: topUpNum , { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "top_up", contentType:"nar_card" , successStatus:"0" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let _ = Mapper<TopUp>().map(JSONObject:resultData) {
                    
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "top_up", contentType:"nar_card" , successStatus:"1" )
                    
                    /*
                     Set check to true if user topup on it's own number
                     so that dashboad data will be reloaded once user redirected to dashboad
                     */
                    if topUpNum.isEqual(AFUserSession.shared.userInfo?.msisdn, ignorCase: true) {
                        
                        AFUserSession.shared.shouldReloadDashboardData = true
                    }
                    
                    self.showSuccessAlertWithMessage(message: resultDesc)
                    
                    self.cardTextField.text = ""
                    
                    
                    
                } else {
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "top_up", contentType:"nar_card" , successStatus:"0" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
        
    }
    
}


extension TopUpVC {
    func showCardTypesAlert() {
        if AFUserSession.shared.predefineData?.plasticCards?.count ?? 0 > 0 {
            if let alert = self.myStoryBoard2.instantiateViewController(withIdentifier: "CardTypeAlertVC") as? CardTypeAlertVC {
                alert.setAlertWith { (stringToCheck) in
                    self.selectedCardType = stringToCheck
                    self.initiatePayment(topUpNumber: self.receiverNumberTextField.text?.formatedMSISDN() ?? "")
                }
                self.presentPOPUP(alert, animated: true, modalTransitionStyle:.crossDissolve, completion: nil)
            }
        }
    }
    
    func openHisabPortal() {
        if let plasticCardVC = self.myStoryBoard.instantiateViewController(withIdentifier: "PlasticCardVC") as? PlasticCardVC {
            plasticCardVC.topUpToMSISDN = self.amountTextField.text ?? ""
            plasticCardVC.redirectionURL = "https://hesab.az/unregistered/#/mobile/bakcell/parameters?lang=\(AFLanguageManager.userSelectedLanguageShortCode())"
            self.navigationController?.pushViewController(plasticCardVC, animated: true)
        }
    }
}
