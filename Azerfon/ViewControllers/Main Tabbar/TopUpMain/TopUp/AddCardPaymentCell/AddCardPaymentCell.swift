//
//  AddCardPaymentCell.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 04/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class AddCardPaymentCell: UITableViewCell {
    
    static let ID = "AddCardPaymentCell"
    
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblTitle: AFMarqueeLabel!
    @IBOutlet var imgarrow: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
