//
//  CardTypesCell.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 06/05/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit

class CardTypesCell: UITableViewCell {
    
    static let identifier = "CardTypesCell"
    
    @IBOutlet var radioBtn: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
