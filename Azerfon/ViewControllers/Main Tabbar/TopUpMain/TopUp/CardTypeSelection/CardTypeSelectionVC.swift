//
//  CardTypeSelectionVC.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 04/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class CardTypeSelectionVC: BaseVC {
    
    static let identifier = "CardTypeSelectionVC"
    
    @IBOutlet var topUpTitle_lbl: UILabel!
    
    @IBOutlet var message_lbl: UILabel!
    
    @IBOutlet weak var cardType_lbl: UILabel!
    @IBOutlet weak var cardType_Img: UIImageView!
    @IBOutlet weak var carTypeDropDown: UIDropDown!
    
    @IBOutlet var btnContinue: UIButton!
    
    @IBOutlet weak var storeCardForPayment_btn : UIButton!
    @IBOutlet weak var storeCardForPaymentDescription_lbl : UILabel!
    
    
    //MARK: - Properties..
    
    var cardTypes : [String] = ["master","visa"]
    var selectedCardType : String = ""
    var selectedAmount : String?
    var selectedMsisdn : String?
    var isSaveCard : Bool = true
    var selectedCard : PlasticCard?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if AFUserSession.shared.predefineData?.plasticCards?.count ?? 0 > 0 {
            cardTypes.removeAll()
            for cardObject in AFUserSession.shared.predefineData?.plasticCards ?? [PlasticCard]() {
                cardTypes.append(cardObject.cardVale ?? "")
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.interactivePop(true)
        layoutViews()
    }
    
    func layoutViews(){
        self.titleLabel?.text = Localized("Title_TopUp")
        self.btnContinue.setTitle(Localized("BtnTitle_Pay"), for: .normal)
        self.message_lbl.text = Localized("Label_Massage")
        self.cardType_lbl.text = Localized("Title_CardType")
        self.storeCardForPaymentDescription_lbl.text = Localized("Title_StoreCardDescription")

//        setting up dropdown.
        setUpDropdown()
    }
    
    
    @IBAction func payBtnPressed(_ sender: UIButton) {
        if selectedCardType.isEmpty || selectedCardType == "" {
            self.showErrorAlertWithMessage(message: Localized("Lbl_SelectCard"))
        } else {
//            let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
//
//
//            self.presentPOPUP(alert, animated: true, completion: nil)
//
//            alert.setConfirmationAlertForTopUp(title: Localized("Title_Confirmation"), reciver: Localized("Message_ConfirmationSaveCard"), showBalnce: false)
//
//            alert.setYesButtonCompletionHandler { (aString) in
//                self.saveCard = true
//                self.initiatePayment()
//            }
//            alert.setNoButtonCompletionHandler { (aString) in
//                self.initiatePayment()
//            }
            
            let message = String(format: Localized("Message_LoadConfirmation"), selectedAmount!, selectedMsisdn!)
            self.showConfirmationAlert(message: message,  {
                self.initiatePayment()
            })
            
        }
    }
  
    
    
    //Checkbox tapped...
    @IBAction  func checkBoxTapped(_ sender: UIButton){
        if sender.currentImage == UIImage(named: "check_on") {
            sender.setImage(UIImage(named: "check_off"), for: .normal)
            self.isSaveCard = false
        }else{
            sender.setImage(UIImage(named: "check_on"), for: .normal)
            self.isSaveCard = true
        }
    }
}

//MARK: - Dropdown Functionality..
extension CardTypeSelectionVC{
    /**
     Dropdown setup
     */
    func setUpDropdown(){
            
        self.carTypeDropDown.dataSource = cardTypes
        self.carTypeDropDown.placeholder = AFUserSession.shared.predefineData?.plasticCards?.first?.cardVale ?? "master"
        
        self.selectedCardType = String(AFUserSession.shared.predefineData?.plasticCards?.first?.cardKey  ??  1)
            self.carTypeDropDown.didSelectOption { (index, option) in
                self.selectedCard = AFUserSession.shared.predefineData?.plasticCards?[index]
                self.selectedCardType = String(self.selectedCard?.cardKey ?? 1)
            }
        
    }
    
    
}


//MARK: _ API calls


extension CardTypeSelectionVC {
    
    func  initiatePayment(){
        self.showActivityIndicator()
        _ = AFAPIClient.shared.initiatePayment(CardType: self.selectedCardType, Amount: selectedAmount ?? "", IsSaved: isSaveCard, TopUpNumber: selectedMsisdn ?? ""){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<SavedCards>().map(JSONObject: resultData){
                        if resp.paymentKey.isEmpty || resp.paymentKey == "" {
                            //do nothing
                        } else {
                            if let plasticCardVC = self.myStoryBoard.instantiateViewController(withIdentifier: "PlasticCardVC") as? PlasticCardVC {
                                plasticCardVC.topUpToMSISDN = self.selectedMsisdn ?? ""
                                plasticCardVC.redirectionURL = resp.paymentKey
                                self.navigationController?.pushViewController(plasticCardVC, animated: true)
                            }
                        }
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
}
