//
//  PlasticCardVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/29/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import WebKit
import ObjectMapper

class PlasticCardVC: BaseVC {
    
    //MARK: - Properties
    var topUpToMSISDN = ""
    var redirectionURL = ""
    var isSaveCard = false
    
    //MARK: - IBOutlet
    @IBOutlet var webContainerView: UIView!
    var myWebView: WKWebView!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        loadWebViewContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_TopUp")
    }
    
    func loadWebViewContent() {
        /*
        var cardURL : String?
        
        switch  AFLanguageManager.userSelectedLanguage() {
            
        case .russian:
            cardURL = AFUserSession.shared.predefineData?.redirectionLinks?.onlinePaymentGatewayURL_RU
        case .english:
            cardURL = AFUserSession.shared.predefineData?.redirectionLinks?.onlinePaymentGatewayURL_EN
        case .azeri:
            cardURL = AFUserSession.shared.predefineData?.redirectionLinks?.onlinePaymentGatewayURL_AZ
        }
        
        var failedToOpenURL :Bool = false
        
        if var baseURLString = cardURL,
            baseURLString.isBlank == false {
            
             baseURLString.append("&prefix=\(String(topUpToMSISDN.prefix(2)))&number=\(String(topUpToMSISDN.suffix(7)))&payment_type=phone")
            
            if let url = URL(string: baseURLString),
                UIApplication.shared.canOpenURL(url) {
                
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "top_up", contentType:"plastic_card_redirection" , successStatus:"1" )
                
                webView.allowsInlineMediaPlayback = true
                webView.mediaPlaybackRequiresUserAction = false
                webView.delegate = self
                
                failedToOpenURL = true
                
                webView.loadRequest(URLRequest(url: url, timeoutInterval: Constants.AFAPIClientDefaultTimeOut ))
                
                #if DEBUG
                print("\n\nPlastic Card URL is: \(url.absoluteString)\n\n")
                #endif
                
            }
        }
        
        if failedToOpenURL == false {
            //EventLog
            AFAnalyticsManager.logEvent(screenName: "top_up", contentType:"plastic_card_redirection" , successStatus:"0" )
            
            clearWebView()
        }*/
        
        
        
        
        
        
        /*let config = WKWebViewConfiguration.init()
        config.preferences.javaScriptEnabled = true
        //config.preferences.javaEnabled = true
        config.preferences.javaScriptCanOpenWindowsAutomatically = true
        var failedToOpenURL: Bool = true
        
        if #available(iOS 11.0, *) {
            config.setURLSchemeHandler(self, forURLScheme: "com.bakcell.carrierapp.portal")
        
        } else {
            // Fallback on earlier versions
        }
        myWebView = WKWebView.init(frame: webContainerView.frame, configuration:config )
//        myWebView.contentScaleFactor = 2.0
        myWebView.scrollView.zoomScale = 1.0
        myWebView.backgroundColor = .clear
        myWebView.navigationDelegate = self
        webContainerView.addSubview(myWebView)
        myWebView.snp.makeConstraints { make in
            make.top.equalTo(webContainerView.snp.top).offset(0.0)
            make.right.equalTo(webContainerView.snp.right).offset(0.0)
            make.left.equalTo(webContainerView.snp.left).offset(0.0)
            make.bottom.equalTo(webContainerView.snp.bottom).offset(0.0)
        }
        
        WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
        
        
        
        if let url = URL(string: redirectionURL) ,
            UIApplication.shared.canOpenURL(url) {
            
            let request = URLRequest(url: url)
            myWebView.load(request)
            failedToOpenURL = false
        }
        
        if failedToOpenURL == true {
            
            
            self.showErrorAlertWithMessage(message: Localized("Message_FailurePlasticCard")) {
                self.navigationController?.popViewController(animated: true)
            }
            clearWebView()
        }
        if let url = URL(string: redirectionURL) ,
            UIApplication.shared.canOpenURL(url) {
            
            let request = URLRequest(url: url)
            myWebView.load(request)
            failedToOpenURL = false
        }
        
        if failedToOpenURL == true {
            
            
            
            self.showErrorAlertWithMessage(message: Localized("Message_FailurePlasticCard")) {
                self.navigationController?.popViewController(animated: true)
            }
            clearWebView()
        }*/
        
        let config = WKWebViewConfiguration.init()
        config.preferences.javaScriptEnabled = true
        //config.preferences.javaEnabled = true
        config.preferences.javaScriptCanOpenWindowsAutomatically = true
     
            
        
        
        if #available(iOS 11.0, *) {
            config.setURLSchemeHandler(self, forURLScheme: "com.bakcell.carrierapp.portal")
        
        } else {
            // Fallback on earlier versions
        }
        myWebView = WKWebView.init(frame: webContainerView.frame, configuration:config )
        myWebView.backgroundColor = .clear
        myWebView.navigationDelegate = self
        webContainerView.addSubview(myWebView)
        myWebView.snp.makeConstraints { make in
            make.top.equalTo(webContainerView.snp.top).offset(0.0)
            make.right.equalTo(webContainerView.snp.right).offset(0.0)
            make.left.equalTo(webContainerView.snp.left).offset(0.0)
            make.bottom.equalTo(webContainerView.snp.bottom).offset(0.0)
        }
        
        WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
        
        var failedToOpenURL: Bool = true
        if let url = URL(string: redirectionURL) ,
            UIApplication.shared.canOpenURL(url) {
            
            let request = URLRequest(url: url)
            myWebView.load(request)
            failedToOpenURL = false
        }
        
        if failedToOpenURL == true {
            
            self.showErrorAlertWithMessage(message: Localized("Message_FailurePlasticCard")) {
                self.navigationController?.popViewController(animated: true)
            }
            clearWebView()
        }
    }
    
    func clearWebView() {
        
        if let clearURL = URL(string: "about:blank") {
            myWebView.load(URLRequest(url: clearURL))
        }
        
        self.showErrorAlertWithMessage(message: Localized("Message_FailurePlasticCard"))
        self.myWebView.showDescriptionViewWithImage(description: Localized("Message_FailurePlasticCard"))
        
    }
    
    //in-App-Survery handling
    func checkTopupSurvey(message: String) {
        if isSaveCard == true {
            self.showSuccessAlertWithMessage(message: message) {
                self.popTotopupVC()
                }
            return
        }
        if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "topup" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "-1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "TopUpScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
                    inAppSurveyVC.toptupTitleText = Localized("Title_Alert")
                    inAppSurveyVC.attributedMessage = NSAttributedString.init(string: message)
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            } else {
                self.showSuccessAlertWithMessage(message: message) {
                    self.popTotopupVC()
                    }
            }
        } else {
            self.showSuccessAlertWithMessage(message: message) {
                self.popTotopupVC()
                }
        }
    }
}

extension PlasticCardVC :WKNavigationDelegate, WKUIDelegate, WKURLSchemeHandler {
    
    

    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(webView.url?.absoluteURL ?? "Not found")
        if (error as NSError).code !=  NSURLErrorCancelled {
            
            self.clearWebView()
        }
        self.removeAllActivityIndicator()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.showActivityIndicator()
        print(webView.url?.absoluteURL ?? "Not found")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            self.removeAllActivityIndicator()
        print(webView.url?.absoluteURL ?? "Not found")
        if let urlString = webView.url?.absoluteString, urlString.containsSubString(subString: "error.jsp") == true {
                // show error
                print("success ")
            self.showErrorAlertWithMessage(message: Localized("Message_FailurePlasticCard")) {
                self.popTotopupVC()
                }
            } else if let urlString = webView.url?.absoluteString, urlString.containsSubString(subString: "ok.jsp") == true {
                //show success popup
                print("success ")
                self.showErrorAlertWithMessage(message: Localized("Message_FailurePlasticCard")) {
                    self.popTotopupVC()
                }
            }
        
        }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if let urlString = webView.url?.absoluteString, urlString.containsSubString(subString: "/user/account/topup?result=false") == true {
                // show error
            self.removeAllActivityIndicator()
            myWebView.alpha = 0.0
            let messageToShow = getRequiredFailuerMessage(urlString: urlString)
            self.showErrorAlertWithMessage(message: messageToShow) {
                self.popTotopupVC()
                }
            } else if let urlString = webView.url?.absoluteString, urlString.containsSubString(subString: "/user/account/topup?result=true") == true {
                //show success popup
                self.removeAllActivityIndicator()
                myWebView.alpha = 0.0
                let messageToShow = getRequiredSuccessMessage(urlString: urlString)
                AFUserSession.shared.shouldReloadDashboardData = true
                if urlString.containsSubString(subString: "=200") {
                    self.checkTopupSurvey(message: messageToShow)
                } else {
                    self.showSuccessAlertWithMessage(message: messageToShow) {
                        self.popTotopupVC()
                    }
                }
            }
        
        decisionHandler(.allow)
    }
        
    @available(iOS 11.0, *)
    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
        
    }
    
    @available(iOS 11.0, *)
    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {
        
    }
    
    func popTotopupVC() {
        
        self.navigationController?.viewControllers.forEach({ (aViewController) in
            if aViewController.isKind(of: TopUpVC().classForCoder) {
                self.navigationController?.popToViewController(aViewController, animated: true)
            }

        })
    }
    
    
    func getRequiredSuccessMessage(urlString: String) -> String {
        if urlString.containsSubString(subString: "error=200") {
            if AFLanguageManager.userSelectedLanguageShortCode() == "en" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200en ?? Localized("Message_CardAddedSuccessfully")
            } else if AFLanguageManager.userSelectedLanguageShortCode() == "ru" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200ru ?? Localized("Message_CardAddedSuccessfully")
            } else {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200az ?? Localized("Message_CardAddedSuccessfully")
            }
        } else if urlString.containsSubString(subString: "error=116") {
            if AFLanguageManager.userSelectedLanguageShortCode() == "en" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116en ?? Localized("Message_CardAddedSuccessfully")
            } else if AFLanguageManager.userSelectedLanguageShortCode() == "ru" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116ru ?? Localized("Message_CardAddedSuccessfully")
            } else {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116az ?? Localized("Message_CardAddedSuccessfully")
            }
        } else if urlString.containsSubString(subString: "error=500") {
            if AFLanguageManager.userSelectedLanguageShortCode() == "en" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500en ?? Localized("Message_CardAddedSuccessfully")
            } else if AFLanguageManager.userSelectedLanguageShortCode() == "ru" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500ru ?? Localized("Message_CardAddedSuccessfully")
            } else {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500az ?? Localized("Message_CardAddedSuccessfully")
            }
        }
        return Localized("Message_CardAddedSuccessfully")
    }
    
    
    func getRequiredFailuerMessage(urlString: String) -> String {
        if urlString.containsSubString(subString: "error=200") {
            if AFLanguageManager.userSelectedLanguageShortCode() == "en" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200en ?? Localized("Message_GenralError")
            } else if AFLanguageManager.userSelectedLanguageShortCode() == "ru" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200ru ?? Localized("Message_GenralError")
            } else {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200az ?? Localized("Message_GenralError")
            }
        } else if urlString.containsSubString(subString: "/user/account/topup?result=false&error=116") {
            if AFLanguageManager.userSelectedLanguageShortCode() == "en" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116en ?? Localized("Message_GenralError")
            } else if AFLanguageManager.userSelectedLanguageShortCode() == "ru" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116ru ?? Localized("Message_GenralError")
            } else {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116az ?? Localized("Message_GenralError")
            }
        } else if urlString.containsSubString(subString: "/user/account/topup?result=false&error=500") {
            if AFLanguageManager.userSelectedLanguageShortCode() == "en" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500en ?? Localized("Message_GenralError")
            } else if AFLanguageManager.userSelectedLanguageShortCode() == "ru" {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500ru ?? Localized("Message_GenralError")
            } else {
                return AFUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500az ?? Localized("Message_GenralError")
            }
        }
        return Localized("Message_GenralError")
    }
    
    
    
    
}
