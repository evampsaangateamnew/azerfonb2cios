//
//  FastTopupVC.swift
//  Azerfon
//
//  Created by Muhammad Irfan Awan on 05/04/2021.
//  Copyright © 2021 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class FastTopupVC: BaseVC {

   
    //View Controller Identifier.
    static let identifier = "FastTopupVC"
    
    
    
    // MARK: - IBOutlet
    @IBOutlet var tableView: UITableView!
    
    
    //MARK: -Properties
    var fastPayments : [FastPayment]?
    var selectedCard:FastPayment?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: FastTopupCell.ID, bundle: nil), forCellReuseIdentifier: FastTopupCell.ID)
        tableView.register(UINib(nibName: AddCardPaymentCell.ID, bundle: nil), forCellReuseIdentifier: AddCardPaymentCell.ID)
        tableView.tableFooterView = UIView()
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "TopUpScreenViews")+1, forKey: "TopUpScreenViews")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        
        loadViewLayout()
        self.getFastPayments()
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        titleLabel?.text = Localized("fastTopup_Title")
    }
    
    
    func checkTopupSurvey(message: String) {
        if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "topup" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "-1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "TopUpScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
                    inAppSurveyVC.toptupTitleText = Localized("Title_Alert")
                    inAppSurveyVC.attributedMessage = NSAttributedString.init(string: message)
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            } else {
                self.showSuccessAlertWithMessage(message: message) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            self.showSuccessAlertWithMessage(message: message) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}


//MARK: TableView Datasource and delegates
extension FastTopupVC: UITableViewDataSource, UITableViewDelegate{
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.fastPayments?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return Localized("DeleteButtonTitle")
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//       return true
        if self.fastPayments?.count ?? 0 > 0 {
            if indexPath.row < self.fastPayments?.count ?? 0 {
                return true
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.fastPayments?.count ?? 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: AddCardPaymentCell.ID, for: indexPath) as! AddCardPaymentCell
            cell.lblTitle.text = Localized("Lbl_DoMorePayments")
            if self.fastPayments?.count ?? 0 == 0 {
                cell.lblTitle.text = Localized("lbl_AddFastPayment")
            }
            cell.lblTitle.superview?.backgroundColor = .white
            cell.imgIcon.image = #imageLiteral(resourceName: "fastTopup")
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: FastTopupCell.ID, for: indexPath) as! FastTopupCell
            cell.setUp(withData: self.fastPayments?[indexPath.row])
            cell.payBtn.tag =  indexPath.row
            cell.payBtn.addTarget(self, action: #selector(payButtonTapped(_:)), for: .touchUpInside)
            cell.payBtn.setMarqueeButtonLayout()
            cell.payBtn.setTitle(Localized("BtnTitle_FastPay"), for: .normal)
            cell.payBtn.layer.borderColor = UIColor.white.cgColor
            cell.payBtn.setTitleColor(.white, for: .normal)
            cell.cardNumber.textColor = UIColor.afGunmetal
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: Localized("DeleteButtonTitle")) { (action, indexPath) in
            self.deleteFastPayment(fastPaymentId: self.fastPayments?[indexPath.row].id ?? ""){ isDeleted  in
                if isDeleted{
                    self.fastPayments?.remove(at: indexPath.row)
                    self.tableView.reloadData()
                }
                
            }
        }
        delete.backgroundColor = UIColor.init(hexString: "A51837")
        return[delete]
    }
    
    
    //MARK: Pay Button  Action
    @objc func payButtonTapped(_ sender: UIButton){
        selectedCard = fastPayments?[sender.tag]
        showPaymentPrompt()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == fastPayments?.count ?? 0{
            let topUp:TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard() as! TopUpVC
            topUp.isScratchCardSelected  = false
            self.navigationController?.pushViewController(topUp, animated: true)
        }else{
            selectedCard = fastPayments?[indexPath.row]
            showPaymentPrompt()
        }
        
    }

    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if  editingStyle == .delete{
            
            //FIXME: if  "id" is needed pass id as param, otherwise "paymentKey"
//            let id = String(self.fastPayments?[indexPath.row].id ?? 0)
            
            
        }
    }
    
    
    func showPaymentPrompt() {
//        let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
//
//        self.presentPOPUP(alert, animated: true, completion: nil)
//        let amount = selectedCard?.amount ?? "0"
//        let number = selectedCard?.topupNumber ?? "0"
//        let messageString = String(format: Localized("Message_LoadConfirmation"), amount, number)
//
//        alert.setConfirmationAlertForTopUp(title: Localized("Title_Confirmation"), reciver: messageString, showBalnce: false)
//
//        alert.setYesButtonCompletionHandler { (aString) in
//            self.makepayment()
//        }
        let amount = selectedCard?.amount ?? "0"
        let number = selectedCard?.topupNumber ?? AFUserSession.shared.msisdn
        var messageString = String(format: Localized("Message_LoadConfirmation"), amount, number)
        if AFLanguageManager.userSelectedLanguageShortCode() == "az-Cyrl" {
            messageString = String(format: Localized("Message_LoadConfirmation"), number, amount)
        }
        self.showConfirmationAlert(message: messageString,  {
            self.makepayment()
        })
        
        self.showConfirmationAlert(message: Localized("Message_LoadConfirmation"), {
            
            /* OK BUTTON PRESSED */
            
            
            
        })
        
    }
    
    
    
}


//MARK: - API Calls
extension FastTopupVC{
    
    
    /// Call 'makePayment' API.
    ///
    /// - returns: Void
    func  makepayment(){
        self.showActivityIndicator()
        _ = AFAPIClient.shared.makepayment(PaymentKey: selectedCard?.paymentKey ?? "", Amount: selectedCard?.amount ?? "", TopupNumber: selectedCard?.topupNumber ?? ""){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {

//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                        self.checkTopupSurvey(message: resultDesc ?? "")
                    }
                    AFUserSession.shared.shouldReloadDashboardData = true


                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )

                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
    
    
    
    /// Call 'getfastpayments' API.
    ///
    /// - returns: Void
    
    
    func  getFastPayments(){
        self.showActivityIndicator()
        _ = AFAPIClient.shared.getFastPayments({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<FastPaymentsResponse>().map(JSONObject: resultData){
                        self.fastPayments = resp.fastPaymentDetails
                        self.tableView.reloadData()
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    /// Call 'deletepayment' API.
    ///
    /// - returns: Void
    
    func  deleteFastPayment(fastPaymentId: String, onCompletion : @escaping (Bool) -> ()){
        self.showActivityIndicator()
        _ = AFAPIClient.shared.deletePayment(fastPaymentId: fastPaymentId){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    self.showSuccessAlertWithMessage(message: resultDesc) {
                        onCompletion(true)
//                        self.navigationController?.popViewController(animated: true)
                    }
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
}
