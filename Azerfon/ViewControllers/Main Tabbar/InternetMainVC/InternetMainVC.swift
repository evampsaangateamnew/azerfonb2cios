//
//  InternetMainVC.swift
//  Azerfon
//
//  Created by Muhammad Irfan Awan on 19/07/2021.
//  Copyright © 2021 Evamp&Saanga. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout
import ObjectMapper

class InternetMainVC: BaseVC {
    
    //MARK: - Properties
    var showCardView            : Bool = true
    var isUserSearch            : Bool = true
    
    var redirectToSupplementory : (isRedireted :Bool, offeringID :String?) = (isRedireted :false, offeringID :nil)
    var tabMenuArray            : [Constants.AFInternetTabType] = [.all]
    var selectedTabType         : Constants.AFInternetTabType = .all
    var supplementaryOffers     : SupplementaryResponse?
    var selectedOfferGroupInfo  : OffersModel?
    var selectedInternetFilters : [FilterContent] = []
    var currentLanguageInternetInfo   : Constants.AFLanguage?
    
    // USED to toggle
    var selectedSectionViewType : Constants.AFSectionType = .offerTitleView
    
    let cardLayout              : UPCarouselFlowLayout = UPCarouselFlowLayout()
    
    var collectenViewCurrentIndex:Int = 0
    
    //MARK: - IBOutlet
    @IBOutlet var searchTextField   : UITextField!
    @IBOutlet var searchButton      : UIButton!
    @IBOutlet var switchButton      : UIButton!
    @IBOutlet var filterButton      : UIButton!
    
    @IBOutlet var mainView          : UIView!
    
    @IBOutlet var tabBarCollectionView: UICollectionView!
    @IBOutlet var tabBarCollectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet var supplementoryContentCollectionView: UICollectionView!
    @IBOutlet var leftArrowButton      : UIButton!
    @IBOutlet var rightArrowButton      : UIButton!
    @IBOutlet var upArrowButton      : UIButton!
    @IBOutlet var downArrowButton      : UIButton!
    
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarCollectionView.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        
        NormalTypeCell.registerReusableCell(with: tabBarCollectionView)
        tabBarCollectionView.delegate = self
        tabBarCollectionView.dataSource = self
        tabBarCollectionView.allowsMultipleSelection = false
        
        
        supplementoryContentCollectionView.delegate = self
        supplementoryContentCollectionView.dataSource = self
        
        SupplementaryCell.registerReusableCell(with: supplementoryContentCollectionView)
        
        
        self.supplementoryContentCollectionView.layoutIfNeeded()
        self.cardLayout.itemSize = CGSize(width: self.supplementoryContentCollectionView.bounds.width - 54 , height:  self.supplementoryContentCollectionView.bounds.height)
        self.cardLayout.scrollDirection = .horizontal
        self.cardLayout.sideItemScale = 0.8
        self.cardLayout.sideItemAlpha = 1
        self.cardLayout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 22)
        self.supplementoryContentCollectionView.collectionViewLayout = self.cardLayout
        
        self.leftArrowButton.isHidden = true
        self.rightArrowButton.isHidden = true
        self.upArrowButton.isHidden = true
        self.downArrowButton.isHidden = true
        
        tabBarCollectionViewHeight.constant = 39
        
        /* set search textField layout */
        self.searchTextField.delegate = self
        self.searchTextField.attributedPlaceholder = NSAttributedString(string: Localized("PlaceHolder_Search"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.afWhite])
        
        let searchSepratorView = UIView()
        searchSepratorView.backgroundColor = UIColor.afWhite
        self.searchTextField.addSubview(searchSepratorView)
        
        searchSepratorView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.searchTextField.snp.left)
            maker.right.equalTo(self.searchTextField.snp.right)
            maker.bottom.equalTo(self.searchTextField.snp.bottom)
            maker.height.equalTo(1)
        }
        /* Reload card layout and images of switch button */
        self.updateCollectionViewLayout(isCardLayout: showCardView)
        
        /* Select menu option and highlight */
        let menuIndex = tabMenuArray.index(of: selectedTabType)
        tabBarCollectionView.selectItem(at: IndexPath(item: menuIndex ?? 0, section:0) , animated: true, scrollPosition: .centeredHorizontally)
        tabBarCollectionView.delegate?.collectionView?(tabBarCollectionView, didSelectItemAt: IndexPath(item: menuIndex ?? 0, section:0))
        
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        // Layout view lables
        loadViewLayout()
        
        /* Clear user tariff info from current objects if he switch to any other number */
        if currentLanguageInternetInfo != AFLanguageManager.userSelectedLanguage() {
            
            tabMenuArray = [.all]
            supplementaryOffers = nil
            tabBarCollectionView.reloadData()
            supplementoryContentCollectionView.reloadData()
            currentLanguageInternetInfo = AFLanguageManager.userSelectedLanguage()
            getInternetOfferings(showIndicator: true)
        }
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarCollectionView.reloadInputViews()
    }
    
    
    //MARK: - IBActions
    @IBAction func switchView (_ sender: Any) {
        /* Revese showCardView value */
        showCardView = !showCardView
        /* Update layout*/
        self.updateCollectionViewLayout(isCardLayout: showCardView)
    }
    
    @IBAction func searchPressed(_ sender: UIButton) {
        layoutSearchTextField(isUserSearch ? false : true)
    }
    
    @IBAction func filterPressed(_ sender: UIButton){
        
        // Reset search
        layoutSearchTextField(false, reloadData: false)
        
        if let filterVC :PackagesFilterVC = PackagesFilterVC.instantiateViewControllerFromStoryboard() {
            
            filterVC.filtersData = supplementaryOffers?.internet?.filters?.app
            filterVC.selectedFilters = self.selectedInternetFilters
            
            filterVC.setCompletionHandler { (selectedFilters) in
                
                self.selectedInternetFilters = selectedFilters
                
                self.selectedOfferGroupInfo?.offers = self.supplementaryOffers?.internet?.filterOfferByGroupValue(selectedFilters: selectedFilters)
                self.reloadSupplementoryOffer(itemIndex: 0)
            }
            self.presentPOPUP(filterVC, animated: true)
        }
    }
    
    @IBAction func subscribePressed(_ sender: AFButton) {
        
        if sender.tag >= 0,
            let selectedOffer = selectedOfferGroupInfo?.offers?[sender.tag] {
            
            var isTypeRenewValue: Bool = false
            
            switch sender.buttonContentType {
            case .topup:
                if let topUpVC :TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard() {
                    self.navigationController?.pushViewController(topUpVC, animated: true)
                }
                return
            case .renew:
                isTypeRenewValue = true
                break
            default:
                break
            }
            
            self.showConformationAlert(selectedOffer: selectedOffer, isRenew: isTypeRenewValue)
        }
    }
    
    @IBAction func moveToOffer(_ sender: AFButton) {
        
        //print("current offer index while taping nex: \(collectenViewCurrentIndex)")
        if sender == rightArrowButton || sender == downArrowButton {
            if collectenViewCurrentIndex <= (selectedOfferGroupInfo?.offers?.count ?? 0) - 2 {
                collectenViewCurrentIndex  = collectenViewCurrentIndex + 1
                if showCardView {
                    self.supplementoryContentCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredHorizontally, animated: true)
                } else {
                    self.supplementoryContentCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredVertically, animated: true)
                }
                
            }
        } else {
            if collectenViewCurrentIndex >= 1 {
                collectenViewCurrentIndex = collectenViewCurrentIndex - 1
                if showCardView {
                    self.supplementoryContentCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredHorizontally, animated: true)
                } else {
                    self.supplementoryContentCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredVertically, animated: true)
                }
            }
        }
        /*if (selectedOfferGroupInfo?.offers?.count ?? 0) > 1 {
            if collectenViewCurrentIndex == 0 {
                if !showCardView {
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = false
                } else {
                    self.rightArrowButton.isHidden = false
                    self.leftArrowButton.isHidden = true
                }
            } else if collectenViewCurrentIndex == (selectedOfferGroupInfo?.offers?.count ?? 0) - 1 {
                if !showCardView {
                    self.upArrowButton.isHidden = false
                    self.downArrowButton.isHidden = true
                } else {
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = false
                }
            } else {
                if !showCardView {
                    self.upArrowButton.isHidden = false
                    self.downArrowButton.isHidden = false
                } else {
                    self.rightArrowButton.isHidden = false
                    self.leftArrowButton.isHidden = false
                }
            }
        } else {
            if !showCardView {
                self.upArrowButton.isHidden = true
                self.downArrowButton.isHidden = true
            } else {
                self.rightArrowButton.isHidden = true
                self.leftArrowButton.isHidden = true
            }
        }*/
    }
    
    //MARK: - Functions
    
    func loadViewLayout(){
        
        titleLabel?.text = Localized("Title_Internet");
    }
    
    func showConformationAlert(selectedOffer :SupplementaryOfferItem, isRenew : Bool) {
        
        var activationMessage = ""
        
        if isRenew {
            activationMessage = Localized("Message_RenewOffer")
        } else {
            activationMessage = Localized("Message_subscribeOffer")
        }
        self.showConfirmationAlert(message: activationMessage, {
            
            /* OK BUTTON PRESSED */
            
            self.changeSupplementaryOffering(selectedOffer: selectedOffer, actionType: true)
            
        })
    }
    
    func updateCollectionViewLayout(isCardLayout :Bool, reloadData :Bool = true) {
        
        if isCardLayout {
            
            switchButton.setImage(UIImage (named: "card_icon"), for: UIControl.State.normal)
            DispatchQueue.main.async {
                self.cardLayout.scrollDirection = .horizontal
                self.cardLayout.itemSize = CGSize(width: self.supplementoryContentCollectionView.bounds.width - 54 , height:  self.supplementoryContentCollectionView.bounds.height)
            }
            
        } else {
            switchButton.setImage(UIImage (named: "list_icon"), for: UIControl.State.normal)
            DispatchQueue.main.async {
                self.cardLayout.scrollDirection = .vertical
                
                self.cardLayout.itemSize = CGSize(width: self.supplementoryContentCollectionView.bounds.width - 54 , height:  self.supplementoryContentCollectionView.bounds.height - 44)
            }
            
        }
        // Reset search
        layoutSearchTextField(false, reloadData: reloadData)
    }
    
    func layoutSearchTextField(_ showTextField :Bool, reloadData :Bool = true) {
        
        isUserSearch = showTextField
        
        if showTextField {
            titleLabel?.isHidden = true
            searchTextField.isHidden = false
            
            searchTextField.text = ""
            _ = searchTextField.becomeFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search_cross") , for: .normal)
            
        } else {
            titleLabel?.isHidden = false
            searchTextField.isHidden = true
            
            searchTextField.text = ""
            _ = searchTextField.resignFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search") , for: .normal)
            
            if reloadData {
                selectedOfferGroupInfo = getOffersInfoFor(selectedType: selectedTabType)
                self.reloadSupplementoryOffer(itemIndex: 0)
            }
        }
        
    }
    
    func reloadSupplementoryOffer(itemIndex :Int, errorMessage : String = Localized("Message_NoData")) {
        
        /* Reload tariff items */
        self.leftArrowButton.isHidden = true
        self.rightArrowButton.isHidden = true
        self.upArrowButton.isHidden = true
        self.downArrowButton.isHidden = true
        collectenViewCurrentIndex = 0
        
        if (selectedOfferGroupInfo?.offers?.count ?? 0) > 0 {
            
            if (selectedOfferGroupInfo?.offers?.count ?? 0) > 1 {
                if !showCardView {
                    //self.downArrowButton.isHidden = false
                } else {
                    //self.rightArrowButton.isHidden = false
                }
            }
            
            self.supplementoryContentCollectionView.hideDescriptionView()
            
            self.supplementoryContentCollectionView.reloadData()
            self.supplementoryContentCollectionView.performBatchUpdates(nil) { (isCompleted) in
                if isCompleted {
                    
                    self.supplementoryContentCollectionView.scrollToItem(at: IndexPath(item: itemIndex, section: 0), at: .centeredHorizontally, animated: true)
                }
            }
            
        } else {
            self.supplementoryContentCollectionView.showDescriptionViewWithImage(description: errorMessage,
                                                                                 descriptionColor: UIColor.afWhite)
            self.supplementoryContentCollectionView.reloadData()
        }
    }
    
    func reDirectUserToSelectedScreens() {
        
        /* reset filters in case of user switch tab */
        self.selectedInternetFilters = []
        
        var offerIndex : Int = 0
        
        // Check if user is redirected from notification
        if redirectToSupplementory.isRedireted ,
            let supplementaryOffersObject = supplementaryOffers {
            // Filter and find offer by offering Id
            
            let (type, index) = supplementaryOffersObject.typeAndIndexOfIntentnetOfferingId(offeringId: redirectToSupplementory.offeringID)
            
            // Higlight selected collectionView cell color.
            
            let deSelectedItemIndexPath = IndexPath(item: tabMenuArray.index(of: self.selectedTabType) ?? 0, section:0)
            tabBarCollectionView.deselectItem(at: deSelectedItemIndexPath, animated: true)
            tabBarCollectionView.delegate?.collectionView?(tabBarCollectionView, didDeselectItemAt: deSelectedItemIndexPath)
            
            
            let menuIndex = tabMenuArray.index(of: type)
            tabBarCollectionView.selectItem(at: IndexPath(item: menuIndex ?? 0, section:0) , animated: true, scrollPosition: .centeredHorizontally)
            tabBarCollectionView.delegate?.collectionView?(tabBarCollectionView, didSelectItemAt: IndexPath(item: menuIndex ?? 0, section:0))
            
            offerIndex = index
            
            //  Set to false so user redirected to offer only once
            redirectToSupplementory = (false, nil)
        }
        
        self.mainView.isHidden = false
//        self.searchButton.isHidden = false
//        self.switchButton.isHidden = false
        
        /*if selectedTabType == .daily {
            self.filterButton.isHidden = false
        } else {
            self.filterButton.isHidden = true
        }*/
        self.filterButton.isHidden = true
        self.switchButton.isHidden = true
        self.searchButton.isHidden = true
        
        selectedOfferGroupInfo = getOffersInfoFor(selectedType: selectedTabType)
        
        self.reloadSupplementoryOffer(itemIndex: offerIndex)
    }
    
    func loadSupplementaryOfferingsData() {
        
        
        /* Load data from UserDefaults */
        if let supplementaryOffersHandler :SupplementaryResponse = SupplementaryResponse.loadFromUserDefaults(key: APIsType.internetOffers.selectedLocalizedAPIKey()) {
            
            // Set information in local object
            self.supplementaryOffers = supplementaryOffersHandler
            
            if Connectivity.isConnectedToInternet == true && redirectToSupplementory.isRedireted {
                /* Load new data */
                self.getInternetOfferings()
                
            } else {
                // Redirect user to selected screen
                self.reDirectUserToSelectedScreens()
                
                /* Load new data in background*/
                self.getInternetOfferings(showIndicator: false)
            }
            
        } else {
            self.getInternetOfferings()
        }
    }
    
    func getOffersInfoFor( selectedType : Constants.AFInternetTabType) -> OffersModel? {
        
        var offersGroupInfo : OffersModel?
        
        switch selectedType {
            
        case .all:
            offersGroupInfo = supplementaryOffers?.all
            
        case .daily:
            offersGroupInfo = supplementaryOffers?.daily
            
        case .weekly:
            offersGroupInfo = supplementaryOffers?.weekly
            
        case .monthly:
            offersGroupInfo = supplementaryOffers?.monthly
            
        case .hourly:
            offersGroupInfo = supplementaryOffers?.hourly
            
        }
        
        return offersGroupInfo
    }
    
    /**
     USED to toggle section.
     
     - returns: Void.
     */
    
    func openSelectedSection() {
        
        let visbleIndexPath = supplementoryContentCollectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = supplementoryContentCollectionView.cellForItem(at: aIndexPath) as? SupplementaryCell {
                
                var openSectionIndex : Int? = 0
                
                if let aTariffObject = selectedOfferGroupInfo?.offers?[cell.tableView.tag] {
                    
                    let type = typeOfDataInSection(aTariffObject)
                    
                    if selectedSectionViewType == .offerTitleView && type.contains(.offerTitleView) {
                        
                        openSectionIndex = 0
                        
                    } else if selectedSectionViewType == .detail && type.contains(.detail) {
                        
                        openSectionIndex = type.index(of:.detail)
                        
                    }
                }
                
                cell.tableView.closeAllSectionsExcept(openSectionIndex ?? 0, shouldCallDelegate: false)
                
                openSectionAt(index: openSectionIndex ?? 0, tableView: cell.tableView)
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : AFAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
    
    
    
    //MARK: - API Calls
    
    /// Call 'getSupplementaryOffering' API .
    ///
    /// - returns: void
    func getInternetOfferings(showIndicator :Bool = true) {
        
        if (showIndicator == true) {
            self.showActivityIndicator()
        }
        
        tabMenuArray = [.all] //, .daily, .monthly, .hourly
        
        _ = AFAPIClient.shared.getInternetOfferings({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if (showIndicator == true) {
                self.hideActivityIndicator()
            }
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let supplementaryOffersHandler = Mapper<SupplementaryResponse>().map(JSONObject: resultData){
                        self.tabMenuArray.removeAll()
                        self.tabMenuArray = [.all]
                        
                        if let monthly = resultData?["monthlyBundles"], monthly != nil {
                            self.tabMenuArray.append(.monthly)
                        }
                        if let weekly = resultData?["weeklyBundles"], weekly != nil {
                            self.tabMenuArray.append(.weekly)
                        }
                        if let daily = resultData?["dailyBundles"], daily != nil {
                            self.tabMenuArray.append(.daily)
                        }
                        if let hourly = resultData?["hourlyBundles"], hourly != nil {
                            self.tabMenuArray.append(.hourly)
                        }
                        
                        // Saving Supplementary Offers data in user defaults
                        supplementaryOffersHandler.saveInUserDefaults(key: APIsType.internetOffers.selectedLocalizedAPIKey())
                        
                        
                        // Set information in local object
                        self.supplementaryOffers = supplementaryOffersHandler
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            // Redirect user to selected screen
            self.tabBarCollectionView.reloadData()
            self.reDirectUserToSelectedScreens()
        })
    }
    
    
    /**
     Call 'changeSupplementaryOffering' API .
     - parameter  selectedOffer : offer user selected to renew or deactivate
     - parameter  actionType : if user want to renew offer then actionType will be true and for deactivation it will be false.
     - returns: Void
     */
    func changeSupplementaryOffering(selectedOffer: SupplementaryOfferItem, actionType :Bool) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.changeSupplementaryOffering(actionType: actionType, offeringId: selectedOffer.header?.offeringId ?? "", offerName: selectedOffer.header?.offerName ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "internet_subscription", contentType:"subscription" , successStatus:"0" )
                
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let response = Mapper<MySubscriptionModel>().map(JSONObject: resultData) {
                    
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "package_subscription", contentType:"subscription" , successStatus:"1" )
                    
                    self.checkAndShowInAppSurvey(selectedOffer: selectedOffer, message: response.message ?? "")
                    /* Show successfull message */
//                    self.showSuccessAlertWithMessage(message: response.message)
                    
                    // Redirect user to selected screen
//                    self.reDirectUserToSelectedScreens()
                    
                } else {
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "package_subscription", contentType:"subscription" , successStatus:"0" )
                    
                    /* Show error alert to user */
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    //
    func checkAndShowInAppSurvey(selectedOffer: SupplementaryOfferItem,message: String){
        
        if let survey = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.bundle.rawValue}) {
            
            /*if  survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0, survey.visitLimit != "-1",
                survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "SupplementaryScreenViews") {
                
                
                
            }else{
                self.showSuccessAlertWithMessage(message: message){
                    self.reDirectUserToSelectedScreens()
                }
            }*/
            if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                /*if let  answerIndex = survey.questions?.first?.answers?.firstIndex(where: { $0.answerId ==  userSurvey?.answerId }),let answer = survey.questions?.first?.answers?[answerIndex] {
                    inAppSurveyVC.selectedAnswer = answer
                }*/
                inAppSurveyVC.currentSurvay = survey
                inAppSurveyVC.showTopUpStackView = true
                //inAppSurveyVC.survayComment = userSurvey?.comments
                inAppSurveyVC.offeringId = ""//selectedOffer.header?.offeringId ?? ""
                inAppSurveyVC.offeringType = "2"
                inAppSurveyVC.attributedMessage = message.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h3GreyRight)
                inAppSurveyVC.callBack = {
                    self.reDirectUserToSelectedScreens()
                }
                self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
            }
            UserDefaults.standard.set(0, forKey: "SupplementaryScreenViews")
            UserDefaults.standard.synchronize()
            
        }else{
            // Show Error alert
            self.showSuccessAlertWithMessage(message: message) {
                self.reDirectUserToSelectedScreens()
            }
        }
    }
    
}
//MARK: - COLLECTION VIEW Delegates
extension InternetMainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if supplementoryContentCollectionView == collectionView {
            
            if showCardView {
                return CGSize(width: self.supplementoryContentCollectionView.bounds.width - 54 , height:  self.supplementoryContentCollectionView.bounds.height)
            } else {
                return CGSize(width: self.supplementoryContentCollectionView.bounds.width - 54 , height:  self.supplementoryContentCollectionView.bounds.height - 44)
            }
            
        } else {
            let str: String = tabMenuArray[indexPath.item].localizedString()
            return CGSize(width: ((str as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.h5GreyCenter]).width + 30), height: 35)//return CGSize(width: self.tabBarCollectionView.frame.size.width/3 + 4, height: 35)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if supplementoryContentCollectionView == collectionView {
            
            return selectedOfferGroupInfo?.offers?.count ?? 0
        } else {
            return self.tabMenuArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if supplementoryContentCollectionView == collectionView {
            
            if let cell :SupplementaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: SupplementaryCell.cellIdentifier(), for: indexPath) as? SupplementaryCell,
                let aOfferObject = selectedOfferGroupInfo?.offers?[indexPath.item] {
                cell.delegate = self
                cell.setTitleLayout(header: aOfferObject.header)
                
                cell.tableView.delegate = self
                cell.tableView.dataSource = self
                
                cell.tableView.tag = indexPath.item
                cell.subscribeButton.tag = indexPath.item
                cell.subscribeButton.addTarget(self, action: #selector(subscribePressed(_:)), for: .touchUpInside)
                
                cell.tableView.reloadData()
                
                return cell
            }
            
        } else {
            
            if let cell :NormalTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier: NormalTypeCell.cellIdentifier(), for: indexPath) as? NormalTypeCell {
                
                cell.setLayout(title: tabMenuArray[indexPath.item].localizedString(),
                               isSelected: selectedTabType == tabMenuArray[indexPath.item] ? true : false,
                               isSpecial: false )
                return cell
            }
        }
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if supplementoryContentCollectionView == collectionView {
            return
        }
        
        if let cell :NormalTypeCell = collectionView.cellForItem(at: indexPath) as? NormalTypeCell {
            cell.setLayout(title: tabMenuArray[indexPath.item].localizedString(),
                           isSelected: true,
                           isSpecial: false )
            
        }
        
        
        selectedTabType = tabMenuArray[indexPath.item]
        
        // Reset search
        layoutSearchTextField(false, reloadData: false)
        
        /* get packages info in case it is empty */
        if supplementaryOffers == nil {
            
            self.loadSupplementaryOfferingsData()
        }
        tabBarCollectionView.reloadData()
        // Redirect user to selected screen
        reDirectUserToSelectedScreens()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if supplementoryContentCollectionView == collectionView {
            return
        }
        
        if let cell :NormalTypeCell = collectionView.cellForItem(at: indexPath) as? NormalTypeCell {
            cell.setLayout(title: tabMenuArray[indexPath.item].localizedString(),
                           isSelected: false,
                           isSpecial: false )
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("scroll did end")
        let visibleRect = CGRect(origin: supplementoryContentCollectionView.contentOffset, size: supplementoryContentCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = supplementoryContentCollectionView.indexPathForItem(at: visiblePoint)
        let currentPage = visibleIndexPath?.item
        /*let pageWidth = scrollView.frame.size.width
        let pageheight = scrollView.frame.size.height
        if showCardView {
            currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        } else {
            currentPage = Int((scrollView.contentOffset.y + pageheight/4) / pageheight)
        }*/
        //print("page index\(currentPage)")
        print("array count\(selectedOfferGroupInfo?.offers?.count ?? 0)")
        /*if (selectedOfferGroupInfo?.offers?.count ?? 0) > 1 {
            if currentPage == 0 {
                if !showCardView {
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = false
                } else {
                    self.rightArrowButton.isHidden = false
                    self.leftArrowButton.isHidden = true
                }
            } else if currentPage == (selectedOfferGroupInfo?.offers?.count ?? 0) - 1 {
                if !showCardView {
                    self.upArrowButton.isHidden = false
                    self.downArrowButton.isHidden = true
                } else {
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = false
                }
            } else {
                if !showCardView {
                    self.upArrowButton.isHidden = false
                    self.downArrowButton.isHidden = false
                } else {
                    self.rightArrowButton.isHidden = false
                    self.leftArrowButton.isHidden = false
                }
            }
        } else {
            if !showCardView {
                self.upArrowButton.isHidden = true
                self.downArrowButton.isHidden = true
            } else {
                self.rightArrowButton.isHidden = true
                self.leftArrowButton.isHidden = true
            }
        }*/
        collectenViewCurrentIndex = currentPage ?? 0
    }
    
}


//MARK: - UITableViewDelegate
extension InternetMainVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let aOfferObject = selectedOfferGroupInfo?.offers?[tableView.tag]{
            
            return typeOfDataInSection(aOfferObject).count
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 70
        } else {
            return 40
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let aOfferObject = selectedOfferGroupInfo?.offers?[tableView.tag] {
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[section]{
            case .offerTitleView:
                if let headerView :SupplementaryTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    headerView.setTitleLayout(header: aOfferObject.header, headerType: .offerTitleView, checkZeroPrice : true)
                    return headerView
                }
                break
            case .detail:
                if let headerView :ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    headerView.setViewWithTitle(title: Localized("Title_Detail"),
                                                isSectionSelected: selectedSectionViewType == .detail ? true : false,
                                                headerType: .detail)
                    return headerView
                }
                break
                
            default:
                break
            }
        }
        
        return AFAccordionTableViewHeaderView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let aOfferObject = selectedOfferGroupInfo?.offers?[tableView.tag]{
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[section]{
            case .offerTitleView:
                return aOfferObject.header?.attributeList?.count ?? 0
                
            case .detail:
                
                return typeOfDataInDetailSection(aOfferObject.details).count
                
            default:
                break
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let aOfferObject = selectedOfferGroupInfo?.offers?[tableView.tag] {
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[indexPath.section]{
            case .offerTitleView:
                
                if let cell : SingleAttributeCell = tableView.dequeueReusableCell() {
                    cell.setAttributeHeader(aOfferObject.header?.attributeList?[indexPath.row],
                                            showSeprator: indexPath.row < ((aOfferObject.header?.attributeList?.count ?? 0) - 1))
                    
                    return cell
                }
                
                break
            case .detail:
                
                let detailsData = typeOfDataInDetailSection(aOfferObject.details)
                switch detailsData[indexPath.row] {
                    
                case .price:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setPriceLayoutValues(price: aOfferObject.details?.price?[indexPath.row],
                                                  showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                    break
                case .rounding:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setRoundingLayoutValues(rounding: aOfferObject.details?.rounding,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .textWithTitle:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithTitleLayoutValues(textWithTitle: aOfferObject.details?.textWithTitle,
                                                          showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithOutTitle:
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aOfferObject.details?.textWithOutTitle,
                                                             showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithPoints:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithPointsLayoutValues(textWithPoint: aOfferObject.details?.textWithPoints,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .date:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aOfferObject.details?.date,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .time:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aOfferObject.details?.time,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .roamingDetails:
                    
                    if let cell:RoamingDetailsCell = tableView.dequeueReusableCell() {
                        cell.setRoamingDetailsLayout(roamingDetails: aOfferObject.details?.roamingDetails,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .freeResourceValidity:
                    
                    if let cell:FreeResourceValidityCell = tableView.dequeueReusableCell() {
                        cell.setFreeResourceValidityValues(freeResourceValidity: aOfferObject.details?.freeResourceValidity,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .titleSubTitleValueAndDesc:
                    
                    if let cell:TitleSubTitleValueAndDescCell = tableView.dequeueReusableCell() {
                        cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aOfferObject.details?.titleSubTitleValueAndDesc,
                                                                      showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                default:
                    break
                }
                break
                
            default:
                break
            }
        }
        
        return UITableViewCell()
    }
}

// MARK: - AFAccordionTableViewDelegate

extension InternetMainVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: true)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
       
        if let headerView = header as? ExpandableTitleHeaderView,
            selectedSectionViewType != headerView.viewType {

            selectedSectionViewType = headerView.viewType
            
        } else if let headerView = header as? SupplementaryTitleHeaderView,
            selectedSectionViewType != headerView.viewType {
            
            selectedSectionViewType = headerView.viewType
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: false)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView,
            selectedSectionViewType == headerView.viewType {
            
            selectedSectionViewType = .offerTitleView
            tableView.toggleSection(0)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

//MARK: TariffsCellDelegate
extension InternetMainVC: SupplementaryCellDelegate {
    func starTapped(offerId: String) {
        
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            if  let  userSurvey = AFUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offerId}){
                if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == SurveyScreenName.bundle_subscribed.rawValue }){
                    inAppSurveyVC.currentSurvay = survey
                    if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                        inAppSurveyVC.survayQuestion = question
                        if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                            inAppSurveyVC.selectedAnswer = question.answers?[answer]
                        }
                    }
                }
                inAppSurveyVC.survayComment = userSurvey.comments
                inAppSurveyVC.offeringId = offerId
                inAppSurveyVC.offeringType = "2"
            } else {
                if let survey = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.bundle_subscribed.rawValue}) {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.offeringId = offerId
                    inAppSurveyVC.offeringType = "2"
                }
            }
            inAppSurveyVC.callBack =  {
                DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                    self.supplementoryContentCollectionView.reloadData()
                }
            }
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
        
    }
    
    
}

//MARK: - Helping functions
extension InternetMainVC {
    
    func typeOfDataInSection(_ aOfferItem : SupplementaryOfferItem?) -> [Constants.AFSectionType] {
        
        var dataTypes : [Constants.AFSectionType] = []
        
        // 1
        /* Show offer title view in all cases */
        dataTypes.append(.offerTitleView)
        
        // 2
        if aOfferItem?.details != nil {
            dataTypes.append(.detail)
        }
        
        return dataTypes
    }
    
    func typeOfDataInDetailSection(_ aDetail : Details?) -> [Constants.AFOfferType] {
        
        var dataTypes : [Constants.AFOfferType] = []
        
        if let aDetailObject =  aDetail {
            
            // 1
            if let count = aDetailObject.price?.count {
                
                for _ in 0..<count {
                    dataTypes.append(.price)
                }
            }
            // 2
            if aDetailObject.rounding != nil  {
                
                dataTypes.append(.rounding)
            }
            // 3
            if aDetailObject.textWithTitle != nil {
                
                dataTypes.append(.textWithTitle)
            }
            // 4
            if aDetailObject.textWithOutTitle != nil {
                
                dataTypes.append(.textWithOutTitle)
            }
            // 5
            if aDetailObject.textWithPoints != nil {
                
                dataTypes.append(.textWithPoints)
            }
            // 6
            if aDetailObject.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(.titleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailObject.date != nil {
                
                dataTypes.append(.date)
            }
            // 8
            if aDetailObject.time != nil {
                
                dataTypes.append(.time)
            }
            // 9
            if aDetailObject.roamingDetails != nil {
                
                dataTypes.append(.roamingDetails)
            }
            // 10
            if aDetailObject.freeResourceValidity != nil {
                
                dataTypes.append(.freeResourceValidity)
            }
        }
        return dataTypes
    }
}

//MARK:- UITextFieldDelegate

extension InternetMainVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == searchTextField {
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            // Filter offers with new name
            selectedOfferGroupInfo?.offers = getOffersInfoFor(selectedType: selectedTabType)?.filterOfferBySearchString(searchString: newString)
            /* Reload user content */
            self.reloadSupplementoryOffer(itemIndex: 0, errorMessage: Localized("Message_NothingFoundSearch"))
            
            if newString.length > 0 {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "search", contentType:"packages_screen" , searchTerm: newString )
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
}

//MARK: - UIScrollViewDelegate
extension InternetMainVC : UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == supplementoryContentCollectionView {
            openSelectedSection()
        }
    }
}



