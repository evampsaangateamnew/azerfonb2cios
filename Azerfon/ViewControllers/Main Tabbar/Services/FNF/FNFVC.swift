//
//  FNFVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/17/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class FNFVC: BaseVC {
    
    //MARK: - Properties
    var fnfList             : [FNFList]?  = []
    var updateFnfNumber     : FNFList?
    var fnfMaxCount         : Int = 0
    
    //MARK: - IBOutlet
    @IBOutlet var topDescriptionLabel     : UILabel!
    @IBOutlet var middleDescriptionLabel  : UILabel!
    @IBOutlet var bottomDescriptionLabel  : UILabel!
    @IBOutlet var numberCountLabel        : UILabel!
    
    @IBOutlet var addNumberField          : AFTextField!
    
    @IBOutlet var addNumbersButton        : UIButton!
    @IBOutlet var clearNumbersButton      : AFButton!
    
    @IBOutlet var numbersTableView        : UITableView!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        addNumberField.delegate = self
        addNumberField.text = Constants.numberPrefix
        
        loadViewContent()
        
        // Get FNF API call
        getFNFList()
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "FNFScreenViews")+1, forKey: "FNFScreenViews")
        UserDefaults.standard.synchronize()
    }
    
    //MARK: - IBAction
    @IBAction func clearAllNumbersButtonPressed(_ sender: UIButton) {
        
        /* SHOW Alert and perform operation accordinglly */
        self.showConfirmationAlert(message: Localized("Message_DeleteFNFConfirmation"), {
            
            /* User confirmed to delete number's list */
            self.deleteFNFList()
        })
    }
    
    @IBAction func addNumbersButtonPressed(_ sender: UIButton) {
        
        if fnfList?.count ?? 0 >= fnfMaxCount &&
            self.updateFnfNumber == nil {
            
            self.showErrorAlertWithMessage(message: Localized("Message_FNFMaxLimit"))
            return
        }
        
        var newMSISDN = ""
        
        if let userMSISDNText = addNumberField.text?.formatedMSISDN(),
            userMSISDNText.isValidMSISDN() {
            
            if userMSISDNText.isEqual(AFUserSession.shared.msisdn, ignorCase: true) == false {
                
                newMSISDN = userMSISDNText
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_FavoriteNumber_itSelf"))
                return
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
            return
        }
        
        addNumberField.resignFirstResponder()
        
        /* check either user want to update or add new fnf number  */
        if self.updateFnfNumber != nil {
            /* SHOW Alert and perform operation accordinglly */
            self.showConfirmationAlert(message: Localized("Message_updateFNFConfirmation"), {
                
                /* User confirmed to update fnf number */
                self.updateFNFNumber(oldMsisdn: self.updateFnfNumber?.msisdn ?? "", newMsisdn: newMSISDN)
            })
        } else {
            /* SHOW Alert and perform operation accordinglly */
            self.showConfirmationAlert(message: Localized("Message_addFNFConfirmation", arguments: newMSISDN), {
                
                /* User confirmed to add fnf number */
                self.addIntoFNFList(msisdn: newMSISDN)
            })
        }
    }
    
    @IBAction func editNumberButtonPressed(_ sender: UIButton) {
        
        if let fnfNumber : FNFList = fnfList?[sender.tag] {
            
            updateFnfNumber = fnfNumber
            
            let formatedMSISDN = fnfNumber.msisdn?.formatedMSISDN()
            
            self.addNumberField.text = String.init(format: "%@%@", Constants.numberPrefix, (formatedMSISDN ?? "") )
            
            if addNumberField.isUserInteractionEnabled == false {
                addNumberField.isUserInteractionEnabled = true
            }
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_FNF")
        self.topDescriptionLabel?.text = Localized("Title_AddFriendDescription")
        self.middleDescriptionLabel?.text = Localized("Title_FriendAndFamilyDescription")
        self.bottomDescriptionLabel?.text = String(format: Localized("Title_FnfBottomDescription"), "0")
        self.numberCountLabel?.text = "0/0"
        
        self.addNumberField.placeholder = Localized("Placeholder_AddNewNumber")
        self.addNumberField.text = Constants.numberPrefix
        
        self.clearNumbersButton.setTitle(Localized("BtnTitle_ClearAllNumbers"), for: .normal)
    }
    
    func reloadTableViewDataAndCount() {
        
        self.numberCountLabel.text = "\(fnfList?.count ?? 0)/\(fnfMaxCount)"
        self.bottomDescriptionLabel?.text = String(format: Localized("Title_FnfBottomDescription"), "\(fnfMaxCount)")
        
        if fnfList?.count ?? 0 < fnfMaxCount  {
            //  addNumbersButton.isEnabled = true
            addNumberField.isUserInteractionEnabled = true
        } else {
            //  addNumbersButton.isEnabled = false
            // addNumberField.isUserInteractionEnabled = false
        }
        
        if (fnfList?.count ?? 0) <= 0 {
            clearNumbersButton.isEnabled = false
            clearNumbersButton.setButtonLayoutType(.superPinkText_SameBorder_WhitBG_Rounded_Disabled)
        } else {
            clearNumbersButton.isEnabled = true
            clearNumbersButton.setButtonLayoutType(.superPinkText_SameBorder_WhitBG_Rounded)
        }
        
        self.numbersTableView.reloadUserData(fnfList)
    }
    
    //MARK: - APIs call
    /// Call 'getFNF' API.
    ///
    /// - returns: Void
    func getFNFList() {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.getFNF({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if (error != nil && (error?.code == AFAPIClientHandlerErrorCode.noNetwork.rawValue || error?.code == AFAPIClientHandlerErrorCode.timeOut.rawValue)) {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling and parsing data from API response.
                if let FNFResponse = Mapper<FNFHandler>().map(JSONObject:resultData) {
                    
                    /* Saving user fnf max count*/
                    self.fnfMaxCount = FNFResponse.fnfLimit?.toInt ?? 0
                    
                    if let fnfList = FNFResponse.fnfList {
                        self.fnfList = fnfList
                    } else {
                        self.fnfList = []
                    }
                }
            }
            self.reloadTableViewDataAndCount()
        })
    }
    
    /// Call 'addIntoFNFList' API.
    ///
    ///msisdn : MSISDN need to be add
    /// - returns: Void
    func addIntoFNFList(msisdn : String) {
        
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.addFNF(MSISDN: msisdn, fnfCount: self.fnfList?.count ?? 0, { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let FNFResponse = Mapper<FNFHandler>().map(JSONObject:resultData) {
                    
                    self.addNumberField.text = Constants.numberPrefix
                    
                    if let fnfList = FNFResponse.fnfList {
                        self.fnfList = fnfList
                    } else {
                        self.fnfList = []
                    }
                    
//                    self.showSuccessAlertWithMessage(message: resultDesc)
                    self.showInAppSurvey(message: resultDesc ?? "")
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadTableViewDataAndCount()
        })
    }
    
    /// Call 'deleteFNFList' API.
    ///
    /// - returns: Void
    func deleteFNFList() {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.deleteFNF(completeFNFList: fnfList, { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    self.fnfList = []
                    self.updateFnfNumber = nil
                    self.showInAppSurvey(message: resultDesc ?? "")
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadTableViewDataAndCount()
        })
    }
    
    /// Call 'updateFNFNumber' API.
    ///
    ///msisdn : MSISDN need to be add
    /// - returns: Void
    func updateFNFNumber(oldMsisdn : String, newMsisdn : String) {
        
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.updateFNF(oldMSISDN: oldMsisdn, newMSISDN: newMsisdn,{ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let FNFResponse = Mapper<FNFHandler>().map(JSONObject:resultData) {
                    
                    self.updateFnfNumber = nil
                    
                    self.addNumberField.text = Constants.numberPrefix
                    
                    if let fnfList = FNFResponse.fnfList {
                        self.fnfList = fnfList
                    } else {
                        self.fnfList = []
                    }
                    
                    //self.showSuccessAlertWithMessage(message: resultDesc)
                    self.showInAppSurvey(message: resultDesc ?? "")
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadTableViewDataAndCount()
        })
    }
    
    
    func showInAppSurvey(message: String) {
        if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "fnf" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "-1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "FNFScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
                    inAppSurveyVC.toptupTitleText = Localized("Title_Alert")
                    inAppSurveyVC.attributedMessage = NSAttributedString.init(string: message)
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            } else {
                self.showSuccessAlertWithMessage(message: message)
            }
        } else {
            self.showSuccessAlertWithMessage(message: message)
        }
    }
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension FNFVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fnfList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :FNFCell = tableView.dequeueReusableCell() {
            
            //updating cell info
            cell.setItemLayout(aItem: fnfList?[indexPath.row], indexPath: indexPath)
            
            //adding target to edit number
            cell.editButton.addTarget(self, action: #selector(editNumberButtonPressed(_:)), for: .touchUpInside)
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Deselect user selected row
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK: - UITextFieldDelegate
extension FNFVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == addNumberField {
            
            /* Check prefix limit and disabling editing in prefix */
            let protectedRange = NSMakeRange(0, addNumberField.prefixLength)
            let intersection = NSIntersectionRange(protectedRange, range)
            
            if intersection.length > 0 ||
                range.location < (protectedRange.location + protectedRange.length) {
                
                return false
            }
            
            /* Restricting user to enter only number */
            let allowedCharactors = Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            /* Restriction for MSISDN length */
            return newLength <= (Constants.MSISDNLength + addNumberField.prefixLength)
            
        }
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addNumbersButtonPressed(UIButton())
        return true
    }
}

