//
//  FNFCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/17/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class FNFCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var numberLabel   : AFMarqueeLabel!
    @IBOutlet var dateTimeLabel : UILabel!
    @IBOutlet var editButton    : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setItemLayout(aItem : FNFList?, indexPath : IndexPath) {
        self.numberLabel.text = aItem?.msisdn?.formatedMSISDNInAzeriFormate()
        self.dateTimeLabel.text = aItem?.createdDate
        self.editButton.tag = indexPath.row
    }
    
}
