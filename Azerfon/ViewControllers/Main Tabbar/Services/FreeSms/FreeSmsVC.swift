//
//  FreeSmsVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/16/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class FreeSmsVC: BaseVC {
    
    //MARK: - Properties
    var remainingSmsCount : String = "0"
    
    //MARK: - IBOutlet
    @IBOutlet var smsDescriptionTitleLabel          : AFLabel!
    @IBOutlet var characterLengthCountTitleLabel    : AFLabel!
    @IBOutlet var smsCountLabel                     : AFLabel!
    @IBOutlet var smsLeftLabel                      : AFLabel!
    
    @IBOutlet var sendButton                        : AFButton!
    
    @IBOutlet var receiversNumberField              : AFTextField!
    
    @IBOutlet var smsTextView                       : UITextView!
    
    @IBOutlet var textViewBottomContraint   : NSLayoutConstraint!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        receiversNumberField.delegate = self
        
        //calling API to get remaining free sms count
        self.getRemainingFreeSMSCount()
        
        //setting prefix
        receiversNumberField.text = Constants.numberPrefix
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        //add observers
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillAppear),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillDisappear),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //removing observors
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    //MARK: - IBAction
    @IBAction func sendButtonPressed(_ sender: UIButton){
        
        var reciverMSISDN = ""
        if let msisdnText = receiversNumberField.text?.formatedMSISDN(),
            msisdnText.isValidMSISDN() {
            
            if msisdnText.isEqual(AFUserSession.shared.msisdn, ignorCase: true) == false {
                
                reciverMSISDN = msisdnText
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_FreeSMS_itSelf"))
                return
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
            return
        }
        
        if smsTextView.text.isBlank{
            self.showErrorAlertWithMessage(message: Localized("Ttitle_MessageEmpty"))
            return
        }
        _ = smsTextView.resignFirstResponder()
        
        //        self.textViewBottomContraint.constant = 141
        
        //calling api to send sms
        self.sendFreeSMS(recieverMsisdns: reciverMSISDN, textMessage: self.smsTextView.text)
    }
    
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_FreeSms")
        
        smsTextView.layer.borderColor = UIColor.afDatePickerGrey.cgColor
        
        smsDescriptionTitleLabel.text = Localized("Title_FreeSmsDesc")
        receiversNumberField.placeholder = Localized("PlaceHolder_ReceiverNumber")
        smsTextView.placeholder = Localized("Placeholder_TypeMessage")
        characterLengthCountTitleLabel.text = Localized("Title_CharacterLength") + "0"
        smsCountLabel.text = Localized("Title_SMSCount") + "0"
        smsLeftLabel.text = Localized("Title_SmsLeft") + remainingSmsCount
        sendButton.setTitle(Localized("BtnTitle_Send") , for: .normal)
        textViewDidChange(smsTextView)
        
        
    }
    
    func resetTextView() {
        smsTextView.text = ""
        textViewDidChange(smsTextView)
        smsTextView.placeholder = Localized("Placeholder_TypeMessage")
        characterLengthCountTitleLabel.text = Localized("Title_CharacterLength") + "0"
        smsCountLabel.text = Localized("Title_SMSCount") + "0"
        smsLeftLabel.text = Localized("Title_SmsLeft") + remainingSmsCount
    }
    
    @objc
    func keyboardWillAppear(notification: NSNotification?) {
        
        guard let keyboardFrame = notification?.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        
        let keyboardHeight: CGFloat
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
        } else {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }
        
        print("keyboard height : \(keyboardHeight)")
        
        if receiversNumberField.isFirstResponder == false {
            self.textViewBottomContraint.constant = keyboardHeight + 15
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    @objc
    func keyboardWillDisappear(notification: NSNotification?) {
        
        self.resetTextViewHeight()
    }
    
    func resetTextViewHeight() {
        
        if self.textViewBottomContraint.constant != 141 {
            self.textViewBottomContraint.constant = 141
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    //MARK: - API Calls
    // Call 'getFreeSMSCount' API .
    ///
    /// - returns: Void
    func getRemainingFreeSMSCount() {
        
        self.showActivityIndicator()
        
        /*API call tpo get data*/
        _ = AFAPIClient.shared.getFreeSMSCount({(response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parsing response data
                    if let remainingSMSHandler = Mapper<FreeSMS>().map(JSONObject: resultData){
                        
                        self.remainingSmsCount = remainingSMSHandler.onNetSMS
                        self.resetTextView()
                    }
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    /// Call 'getFreeSMSStatus' API .
    ///
    /// - returns: Void
    func sendFreeSMS(recieverMsisdns: String, textMessage: String) {
        
        self.showActivityIndicator()
        
        /*API call tpo get data*/
        _ = AFAPIClient.shared.sendFreeSMS(recieverMsisdns: recieverMsisdns, textMessage: textMessage,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let freeSMSHandler = Mapper<FreeSMS>().map(JSONObject: resultData){
                        
                        self.showSuccessAlertWithMessage(message: freeSMSHandler.responseMsg)
                        self.remainingSmsCount = freeSMSHandler.onNetSMS
                        self.resetTextView()
                    }
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - TextView delagates
extension FreeSmsVC : UITextViewDelegate {
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        //start here
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        self.resetTextViewHeight()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard let textViewValue = textView.text else { return false } // Check that if text is nill then return false
        
        let newTextAfterChange = (textViewValue as NSString).replacingCharacters(in: range, with: text)
        
        var smsAllowedTextLength = 0
        if newTextAfterChange.containsOtherThenAllowedCharactersForFreeSMSInEnglish() {
            smsAllowedTextLength = Constants.AzriAndRussianFreeSMSLength
        } else {
            smsAllowedTextLength = Constants.EnglishFreeSMSLength
        }
        
        let newTextLength = newTextAfterChange.endIndex.utf16Offset(in: newTextAfterChange)
        
        if newTextLength <= smsAllowedTextLength {
            
            return true
            
        } else if newTextLength <= (smsAllowedTextLength * 2) {
            
            return true
        } else {
            
            if newTextLength > (smsAllowedTextLength * 2) {
                self.showErrorAlertWithMessage(message: String(format: Localized("Message_SMSLimit"), (smsAllowedTextLength * 2)))
            }
            return false
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        
        if textView == self.smsTextView {
            
            if let placeholderLabel = self.smsTextView.viewWithTag(100) as? UILabel {
                placeholderLabel.isHidden = self.smsTextView.text.count > 0
            }
            
            if let newText = textView.text {
                
                var smsAllowedTextLength = 0
                if newText.containsOtherThenAllowedCharactersForFreeSMSInEnglish() {
                    smsAllowedTextLength = Constants.AzriAndRussianFreeSMSLength
                } else {
                    smsAllowedTextLength = Constants.EnglishFreeSMSLength
                }
                
                let newTextLength = newText.endIndex.utf16Offset(in: newText)
                
                characterLengthCountTitleLabel.text  = "\(Localized("Title_CharacterLength"))\(newTextLength)"
                
                if newTextLength <= 0 {
                    smsCountLabel.text = "\(Localized("Title_SMSCount"))\(0)"
                    
                } else if newTextLength <= smsAllowedTextLength {
                    smsCountLabel.text = "\(Localized("Title_SMSCount"))\(1)"
                    
                } else if newTextLength <= (smsAllowedTextLength * 2) {
                    smsCountLabel.text = "\(Localized("Title_SMSCount"))\(2)"
                }
                
                if newTextLength > (smsAllowedTextLength * 2) {
                    self.showErrorAlertWithMessage(message: String(format: Localized("Message_SMSLimit"), (smsAllowedTextLength * 2)))
                }
                
            } else {
                characterLengthCountTitleLabel.text  = "\(Localized("Title_CharacterLength"))\(0)"
                smsCountLabel.text = "\(Localized("Title_SMSCount"))\(0)"
            }
        }
    }
}

//MARK: - UITextFieldDelegate
extension FreeSmsVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == receiversNumberField {
            
            /* Check prefix limit and disabling editing in prefix */
            let protectedRange = NSMakeRange(0, receiversNumberField.prefixLength)
            let intersection = NSIntersectionRange(protectedRange, range)
            
            if intersection.length > 0 ||
                range.location < (protectedRange.location + protectedRange.length) {
                
                return false
            }
            
            /* Restricting user to enter only number */
            let allowedCharactors = Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            /* Restriction for MSISDN length */
            return newLength <= (Constants.MSISDNLength + receiversNumberField.prefixLength)
            
        }
        return false
    }
    
}
