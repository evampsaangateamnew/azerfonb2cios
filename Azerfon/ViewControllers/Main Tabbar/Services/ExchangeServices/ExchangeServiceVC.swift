//
//  ExchangeServiceVC.swift
//  Azerfon
//
//  Created by Waqas Umar on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ExchangeServiceVC: BaseVC {
    
    enum ExchangeTypes {
        case dataToVoice
        case voiceToData
    }
    //MARK: - Properties
    var exchangeServiceRsp : ExchangeServiceModel?
    var currentValuesArray = [ExchangeServiceValueModel]()
    var selectedType : ExchangeTypes = .voiceToData
    
    //MARK: - IBOutlet
    
    @IBOutlet var btnVoiceToData    : AFButton!
    @IBOutlet var btnDataToVoice    : AFButton!
    @IBOutlet var containerView     : AFView!
    @IBOutlet var steppedProgress   : MWSnapableProgressView!
    @IBOutlet var lblTopText        : AFLabel!
    @IBOutlet var lblBelowText      : UITextView!
    @IBOutlet var btnExchange       : AFButton!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchExchangeServiceDataVoiceValues()
        self.btnExchange.setButtonLayoutType(.whiteText_ClearBG_Rounded_Disabled)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    
    @IBAction func voiceToDataPressed(_ sender: AFButton) {
        
        selectedType = .voiceToData
        
        /* Reload data */
        self.loadExchnageServiceInfo()
    }
    
    @IBAction func dataTovoicePressed(_ sender: AFButton) {
        
        selectedType = .dataToVoice
        
        /* Reload data */
        self.loadExchnageServiceInfo()
    }
    
    @IBAction func subscribePressed(_ sender: AFButton) {
        
        let currentValueIndex = steppedProgress.currentIndex
        
        if currentValueIndex > 0, currentValuesArray.count > 1 {
            
            var terifName = ""
                            if let offeringName = AFUserSession.shared.homePageData?.offeringNameDisplay,
                                offeringName.isBlank == false {
                                terifName = offeringName
                            } else {
                                terifName = AFUserSession.shared.userInfo?.offeringNameDisplay ?? ""
                            }
            
            var terifItemMatched = false
            for itemToCheck in AFUserSession.shared.predefineData?.cevirEligibleTariffs ?? [String]() {
                if itemToCheck .isEqual(terifName) == true {
                    terifItemMatched = true
                    break
                }
            }
            
            if terifItemMatched == true {
                self.showConfirmationAlert(message: Localized("Message_ExchangeConfirmation"), {
                    self.callApiForExchangeService()
                })
                
            } else {
                self.showErrorAlertWithMessage(message:AFUserSession.shared.predefineData?.cevirErrorMessage)
            }
            
            
            
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
    }
    
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        self.titleLabel?.text = Localized("title_Exchange_Service")
        self.btnVoiceToData?.setTitle(Localized("voice_To_Data"), for: .normal)
        self.btnDataToVoice?.setTitle(Localized("data_To_Voice"), for: .normal)
        self.btnExchange?.setTitle(Localized("btn_Title_Exchange"), for: .normal)
        
        self.voiceToDataPressed(AFButton())
        
        setupProgressBar()
    }
    
    func loadExchnageServiceInfo() {
        
        if selectedType == .dataToVoice {
            
            btnVoiceToData.setButtonLayoutType(.whiteText_DarkPinkBG_Rounded)
            btnDataToVoice.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
            
        } else {
            
            btnDataToVoice.setButtonLayoutType(.whiteText_DarkPinkBG_Rounded)
            btnVoiceToData.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
        }
        
        if exchangeServiceRsp != nil {
            
            self.containerView.hideDescriptionView()
            
            steppedProgress.isHidden = false
            self.lblTopText.isHidden = false
            self.lblBelowText.isHidden = false
            
            if selectedType == .dataToVoice {
                /*set header and footer text*/
                self.lblTopText.text = AFUserSession.shared.predefineData?.exchangeService?.dov?.header
                self.lblBelowText.text = AFUserSession.shared.predefineData?.exchangeService?.dov?.footer
                
            } else {
                /*set header and footer text*/
                self.lblTopText.text = AFUserSession.shared.predefineData?.exchangeService?.vod?.header
                self.lblBelowText.text = AFUserSession.shared.predefineData?.exchangeService?.vod?.footer
                
            }
            
            self.setupProgressBar()
            
            /*if self.exchangeServiceRsp?.isServiceEnabled?.isTrue() ?? false {
                self.btnExchange.isEnabled = true
                self.btnExchange.setButtonLayoutType(.whiteText_ClearBG_Rounded)
            } else {
                self.btnExchange.isEnabled = false
                self.btnExchange.setButtonLayoutType(.whiteText_ClearBG_Rounded_Disabled)
            }*/
            self.btnExchange.isEnabled = true
            self.btnExchange.setButtonLayoutType(.whiteText_ClearBG_Rounded)
            
        } else {
            
            self.containerView.showDescriptionViewWithImage(description: Localized("Message_NoData"))
            
            steppedProgress.isHidden = true
            self.lblTopText.isHidden = true
            self.lblBelowText.isHidden = true
            
        }
        
        
    }
    
    func setupProgressBar() {
        
        currentValuesArray = []
        
        if selectedType == .dataToVoice {
            
            let firstValue = ExchangeServiceValueModel()
            firstValue.data = Localized("labal_ExchangeFor")
            firstValue.voice = Localized("labal_ExchangeYouGet")
            
            currentValuesArray.append(firstValue)
            
            if let dataToVoice = exchangeServiceRsp?.datavalues {
                currentValuesArray.append(contentsOf: dataToVoice)
            }
            
        } else {
            
            let firstValue = ExchangeServiceValueModel()
            firstValue.data = Localized("labal_ExchangeYouGet")
            firstValue.voice = Localized("labal_ExchangeFor")
            
            currentValuesArray.append(firstValue)
            
            if let voiceToData = exchangeServiceRsp?.voicevalues {
                currentValuesArray.append(contentsOf: voiceToData)
            }
        }
        
        steppedProgress.lineHeight = 2
        steppedProgress.radius = 4
        steppedProgress.progressLineHeight = 4
        steppedProgress.selectedRadius = 10
        
        steppedProgress.selectedDotColor = UIColor(hexString: "#d43458")
        steppedProgress.dimLineColor = UIColor(hexString: "#d3d6e4")
        
        steppedProgress.textColor = UIColor.afGreyishBrown
        steppedProgress.lightTextColor = UIColor.afGunmetal
        
        steppedProgress.normalTextFont = UIFont.afSansBold(fontSize: 12)
       steppedProgress.lightTextFont = UIFont.afSans(fontSize: 12)
        
        steppedProgress.translatesAutoresizingMaskIntoConstraints = false
        steppedProgress.numberOfPoints = currentValuesArray.count
        
        if currentValuesArray.count > 1 {
            steppedProgress.delegate = self
            steppedProgress.currentIndex = 1
        }
        
    }
    
    
    //MARK: - API's Calls
    
    func fetchExchangeServiceDataVoiceValues()  {
        
        
        
        if let responseHandler :ExchangeServiceModel = ExchangeServiceModel.loadFromUserDefaults(key: APIsType.exchangeService.selectedLocalizedAPIKey()) {
            
            /* Load infomation */
            self.exchangeServiceRsp = responseHandler
            
        } else {
            self.showActivityIndicator()
        }
        
        _ = AFAPIClient.shared.getExchangeServiceValues ({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let exchangeResp = Mapper<ExchangeServiceModel>().map(JSONObject: resultData) {
                    
                    self.exchangeServiceRsp = exchangeResp
                    exchangeResp.saveInUserDefaults(key: APIsType.exchangeService.selectedLocalizedAPIKey())
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            /* load information */
            self.loadExchnageServiceInfo()
        })
    }
    
    func callApiForExchangeService()  {
        
        self.showActivityIndicator()
        
        let selectedValue = currentValuesArray[steppedProgress.currentIndex]
        
        let dataValue = selectedValue.data ?? ""
        let voiceValue = selectedValue.voice ?? ""
        
        _ = AFAPIClient.shared.exchangeServiceApiCall(isDataToVoice: selectedType == .dataToVoice, dataVal: dataValue, voiceVal:voiceValue, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                //EventLog
                if self.selectedType == .dataToVoice {
                    //Log event for DataToVoice
                    AFAnalyticsManager.logEvent(screenName: "exchange_offer", contentType:"data_to_voice" , successStatus:"0" )
                } else {
                    //Log event for VoiceToData
                    AFAnalyticsManager.logEvent(screenName: "exchange_offer", contentType:"voice_to_data" , successStatus:"0" )
                }
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let response = Mapper<MessageResponse>().map(JSONObject:resultData) {
                    
                    //EventLog
                    if self.selectedType == .dataToVoice {
                        //Log event for DataToVoice
                        AFAnalyticsManager.logEvent(screenName: "exchange_offer", contentType:"data_to_voice" , successStatus:"1" )
                    } else {
                        //Log event for VoiceToData
                        AFAnalyticsManager.logEvent(screenName: "exchange_offer", contentType:"voice_to_data" , successStatus:"1" )
                    }
                    
                    self.showSuccessAlertWithMessage(message: response.message)
                    
                } else {
                    //EventLog
                    if self.selectedType == .dataToVoice {
                        //Log event for DataToVoice
                        AFAnalyticsManager.logEvent(screenName: "exchange_offer", contentType:"data_to_voice" , successStatus:"0" )
                    } else {
                        //Log event for VoiceToData
                        AFAnalyticsManager.logEvent(screenName: "exchange_offer", contentType:"voice_to_data" , successStatus:"0" )
                    }
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK:- MWSnapableProgressViewDelegate
extension ExchangeServiceVC : MWSnapableProgressViewDelegate {
    
    func progressBar(_ progressVw: MWSnapableProgressView,
                     didSelectItemAtIndex index: Int) {
        
        steppedProgress.currentIndex = index
        
    }
    
    func progressBar(_ progressVw: MWSnapableProgressView,
                     canSelectItemAtIndex index: Int) -> Bool {
        if index > 0 {
            return true
        }
        return false
    }
    
    func progressBar(_ progressVw: MWSnapableProgressView,
                     textAtIndex index: Int, position: MWSnapableProgressViewTextLocation) -> String {
        
        var posit : MWSnapableProgressViewTextLocation = position
        
        if selectedType == .dataToVoice {
            posit = position == .top ? .bottom:.top
        }
        
        let value = currentValuesArray[index]
        
        if posit == .top {
            var returnStr = (value.voice ?? "")
            if let unit = value.voiceUnit {
                returnStr = "\(returnStr)\n\(unit)"
            }
            return returnStr
        }
        else if posit == .bottom {
            var returnStr = (value.data ?? "")
            if let unit = value.dataUnit {
                returnStr = "\(returnStr)\n\(unit)"
            }
            return returnStr
        }
        
        return ""
    }
    
}
