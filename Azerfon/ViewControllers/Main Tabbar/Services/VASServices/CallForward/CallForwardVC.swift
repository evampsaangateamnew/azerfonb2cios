//
//  CallForwardVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/24/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class CallForwardVC: BaseVC {
    
    //MARK: - Properties
    var selectedCoreServiceListItem : CoreServicesList?
    
    fileprivate var didChangeNumberBlock : AFButtonWithParamCompletionHandler?
    
    //MARK: - IBOutlets
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var mobileNumberTextField: AFTextField!
    @IBOutlet var saveButton: UIButton!
    
    //MARK: - ViewControllers Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mobileNumberTextField.delegate = self
        
        if let number = selectedCoreServiceListItem?.forwardNumber.formatedMSISDN(),
            number.isBlank == false {
            
            mobileNumberTextField.text = number
        } else {
            //setting prsefix
            mobileNumberTextField.text = Constants.numberPrefix
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        // Layout with localized string
        layoutViewController()
    }
    
    //MARK: - Functions
    func layoutViewController() {
        self.titleLabel?.text               = Localized("Title_ForwardNumber")
        descriptionLabel.text               = Localized("Info_FNDescription")
        mobileNumberTextField.placeholder   =  Localized("Placeholder_MobileNumber")
        saveButton.setTitle(Localized("BtnTitle_SAVE"), for: UIControl.State.normal)
    }
    
    // Yes and No button completion handler
    func setCompletionHandler(completionBlock : @escaping AFButtonWithParamCompletionHandler ) {
        self.didChangeNumberBlock = completionBlock
    }
    
    //MARK:- IBACTIONS
    @IBAction func savePressed(_ sender: Any) {
        
        var msisdn : String = ""
        // MSISDN validation
        if mobileNumberTextField.text?.isBlank == true {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
            return
        }
        
        if let mobileNumberText = mobileNumberTextField.text?.formatedMSISDN(),
            mobileNumberText.isValidMSISDN(),
            mobileNumberText.firstCharacter?.isEqual("0", ignorCase: true) == false {
            
            if mobileNumberText.isEqual(AFUserSession.shared.msisdn, ignorCase: true) == false {
                
                msisdn = mobileNumberText
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_CantAddSameMSISDN"))
                return
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidMSISDN"))
            return
        }
        
        // TextField resignFirstResponder
        _ = mobileNumberTextField.resignFirstResponder()
        
        // Calling APi to add number
        processCoreServices(forwardNumber: msisdn)
    }
    
    
    //MARK: - API Calls
    /// Call 'getCoreServices' API .
    ///
    /// - returns: Void
    func processCoreServices(forwardNumber: String) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.processCoreServices(actionType: true, offeringId: selectedCoreServiceListItem?.offeringId ?? "", number: forwardNumber, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    //show success alert
                    self.showSuccessAlertWithMessage(message: resultDesc, {
                        self.didChangeNumberBlock?(forwardNumber)
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                } else {
                    self.showErrorAlertWithMessage(message: resultDesc ?? "")
                }
            }
        })
    }
}

//MARK: - Textfield delagates

extension CallForwardVC : UITextFieldDelegate {
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == mobileNumberTextField{
            
            /* Check prefix limit and disabling editing in prefix */
            let protectedRange = NSMakeRange(0, mobileNumberTextField.prefixLength)
            let intersection = NSIntersectionRange(protectedRange, range)
            
            if intersection.length > 0 ||
                range.location < (protectedRange.location + protectedRange.length) {
                
                return false
            }
            
            /* Restricting user to enter only number */
            let allowedCharactors = Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            /* Restriction for MSISDN length */
            return newLength <= (Constants.MSISDNLength + mobileNumberTextField.prefixLength)
            
        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        savePressed(UIButton())
        return true
    }
}
