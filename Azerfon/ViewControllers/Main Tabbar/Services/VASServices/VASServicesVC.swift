//
//  VASServicesVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class VASServicesVC: BaseVC {
    
    //MARK: - Properties
    var coreServicesData :  [CoreService]?
    
    //MARK: - IBOutlet
    @IBOutlet var servicesTableView        : UITableView!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Get Coreservices API call
        getCoreServices()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        //reloading data
        self.servicesTableView.reloadUserData(self.coreServicesData)
    }
    
    //MARK: - IBAction
    
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_VasServices")
    }
    
    func typeOfDataInCoreService(aCoreService: CoreServicesList) -> Constants.AFCoreServiceType {
        
        var dataType : Constants.AFCoreServiceType = Constants.AFCoreServiceType.coreService
        
        if aCoreService.status.length > 0 && aCoreService.status.isActive() && aCoreService.offeringId.isEqual(Constants.K_Call_Forward, ignorCase: true) == false && (aCoreService.effectiveDate != "" && aCoreService.expireDate != "") {
            dataType = Constants.AFCoreServiceType.coreServiceWithBar
        }
        
        return dataType
    }
    
    func toActiveOrInActiveString(string: String?, isActive : Bool = false) -> String {
        
        if string?.isBlank  ?? true {
            return Localized("Info_Inactive")
        }
        
        if isActive {
            return Localized("Info_Active")
        } else {
            return Localized("Info_Inactive")
        }
        
    }
    
    func navigateToFrwarCalls(forSection : Int,forRow: Int, selectedCoreServiceItem :CoreServicesList) {
        
        if let forwardNumberVC :CallForwardVC = CallForwardVC.instantiateViewControllerFromStoryboard() {
            forwardNumberVC.selectedCoreServiceListItem = selectedCoreServiceItem
            
            forwardNumberVC.setCompletionHandler(completionBlock: { (newNumber) in
                self.updateCoreServiceForwordNumber(section: forSection, row: forRow, newForwardNumber: newNumber)
            })
            
            self.navigationController?.pushViewController(forwardNumberVC, animated: true)
        }
    }
    
    /* func createTagForSwitch(section: Int, row: Int) -> Int {
     let tagString = "\(section+1)909\(row)"
     return tagString.toInt
     }
     func getSectionAndRowForTag(tag: Int) -> (Int,Int) {
     let tagString = "\(tag)"
     let tagArray = tagString.components(separatedBy: "909")
     return ((tagArray.first?.toInt ?? 0) - 1, tagArray.last?.toInt ?? 0)
     }*/
    
    func updateCoreServiceStatus(section : Int, row : Int, isActive : Bool = false, isCallForword : Bool = false) {
        
        if coreServicesData?.indices.contains(section) ?? false {
            
            let aCatagoryType = coreServicesData?[section]
            
            if aCatagoryType?.coreServicesList?.indices.contains(row) ?? false {
                
                if let aCoreService = aCatagoryType?.coreServicesList?[row] {
                    
                    aCoreService.status = self.toActiveOrInActiveString(string: aCoreService.status, isActive: isActive)
                    
                    if isCallForword {
                       aCoreService.forwardNumber = ""
                    }
                    
                    aCatagoryType?.coreServicesList?[row] = aCoreService
                    
                    coreServicesData?[section] = aCatagoryType!
                    
                    // Save updated core services information
                    //                    MBUtilities.saveJSONStringInToUserDefaults(jsonString: coreServicesData?.toJSONString(), key: Localized("API_CoreServices"))
                }
            }
        }
        
        // Reload TableView Data
        self.servicesTableView.reloadUserData(self.coreServicesData)
    }
    
    func updateCoreServiceForwordNumber(section : Int, row : Int, newForwardNumber : String = "") {
        
        if coreServicesData?.indices.contains(section) ?? false {
            
            let aCatagoryType = coreServicesData?[section]
            
            if aCatagoryType?.coreServicesList?.indices.contains(row) ?? false {
                
                if let aCoreService = aCatagoryType?.coreServicesList?[row] {
                    aCoreService.forwardNumber = newForwardNumber
                    aCoreService.status = self.toActiveOrInActiveString(string: aCoreService.status, isActive: true)
                    
                    aCatagoryType?.coreServicesList?[row] = aCoreService
                    
                    coreServicesData?[section] = aCatagoryType!
                    
                    // Save updated core services information
                    //                    MBUtilities.saveJSONStringInToUserDefaults(jsonString: coreServicesData?.toJSONString(), key: Localized("API_CoreServices"))
                }
            }
        }
        // Reload TableView Data
        self.servicesTableView.reloadUserData(self.coreServicesData)
    }
    
    //MARK: - API's Calls
    /// Call 'getCoreServices' API .
    ///
    /// - returns: Void
    func getCoreServices() {
        
        if let responseHandler :CoreServices = CoreServices.loadFromUserDefaults(key: APIsType.vasServices.selectedLocalizedAPIKey()) {
            
            // Setting Data in ViewController
            self.coreServicesData = responseHandler.coreServices
            
            self.servicesTableView.reloadUserData(self.coreServicesData)
            
        } else {
            self.showActivityIndicator()
        }
        
        _ = AFAPIClient.shared.getCoreServices({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    if let coreServicesResponseHandler = Mapper<CoreServices>().map(JSONObject: resultData) {
                        
                        // Setting Data in ViewController
                        self.coreServicesData = coreServicesResponseHandler.coreServices
                        
                        //caching coreservices response
                        coreServicesResponseHandler.saveInUserDefaults(key: APIsType.vasServices.selectedLocalizedAPIKey())
                        
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.servicesTableView.reloadUserData(self.coreServicesData)
        })
    }
    
    @objc func switchChanged(mySwitch: AFSwitch) {
        
        let section = mySwitch.sectionTag
        let row = mySwitch.rowTag
        
        if self.coreServicesData?.indices.contains(section) ?? false {
            
            let aCatagoryType = self.coreServicesData?[section]
            
            if aCatagoryType?.coreServicesList?.indices.contains(row) ?? false {
                
                if let aCoreService = aCatagoryType?.coreServicesList?[row] {
                    
                    if aCoreService.offeringId.isEqualToStringIgnoreCase(otherString: Constants.K_Call_Forward) &&
                        mySwitch.isOn == false {
                        
                        self.showActivityIndicator()
                        
                        _ = AFAPIClient.shared.changeSupplementaryOffering(actionType: false, offeringId: aCoreService.offeringId, offerName: aCatagoryType?.coreServiceCategory ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                            
                            self.hideActivityIndicator()
                            
                            if error != nil {
                                error?.showServerErrorInViewController(self)
                                
                                // changing back to first value
                                mySwitch.setOn(!mySwitch.isOn, animated: true)
                            } else {
                                
                                // handling data from API response.
                                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                                    
                                    // Update current status in local storage
                                    self.updateCoreServiceStatus(section: section, row: row, isActive: mySwitch.isOn, isCallForword: true)
                                    
                                    // Success Alert // No Result Message
                                    //self.showSuccessAlertWithMessage(message: resultDesc)
                                    
                                } else {
                                    
                                    // Error Alert
                                    self.showErrorAlertWithMessage(message: resultDesc)
                                    
                                    // changing back to first value
                                    mySwitch.setOn(!mySwitch.isOn, animated: true)
                                }
                            }
                        })
                        
                    } else {
                        
                        /* SHOW Alert and perform operation accordinglly */
                        self.showConfirmationAlert(message: Localized("Message_VasActivationConfirmation"), {
                            self.showActivityIndicator()
                            
                            // Number will be empty in this case
                            _ = AFAPIClient.shared.processCoreServices(actionType: mySwitch.isOn, offeringId: aCoreService.offeringId, number: "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                                
                                self.hideActivityIndicator()
                                
                                if error != nil {
                                    error?.showServerErrorInViewController(self)
                                    
                                    // changing back to first value
                                    mySwitch.setOn(!mySwitch.isOn, animated: true)
                                } else {
                                    
                                    // handling data from API response.
                                    if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                                        
                                        // Update current status in local storage
                                        self.updateCoreServiceStatus(section: section, row: row, isActive: mySwitch.isOn)
                                        
                                    } else {
                                        
                                        // Error Alert
                                        self.showErrorAlertWithMessage(message: resultDesc)
                                        
                                        // changing back to first value
                                        mySwitch.setOn(!mySwitch.isOn, animated: true)
                                    }
                                }
                            })
                            
                        }, {
                            // changing back to first value
                            mySwitch.setOn(!mySwitch.isOn, animated: true)
                            
                        })
                        
                    }
                }
            }
        }
    }
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension VASServicesVC : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return coreServicesData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return coreServicesData?[section].coreServicesList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  40;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let coreServicesHeader :VASServicesHeaderCell = tableView.dequeueReusableHeaderFooterView() {
            coreServicesHeader.titleLabel.text = (coreServicesData?[section].coreServiceCategory ?? "")
            return coreServicesHeader
        }
        
        return AFAccordionTableViewHeaderView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let aCoreService = coreServicesData?[indexPath.section].coreServicesList?[indexPath.row] {
            
            //getting correct type
            let dataType : Constants.AFCoreServiceType = self.typeOfDataInCoreService(aCoreService: aCoreService)
            
            //checking type and returning back corrent cell
            switch (dataType) {
                
            case .coreService:
                
                //checking for CallForwarding service
                if aCoreService.offeringId.isEqual(Constants.K_Call_Forward, ignorCase: true),
                    let callForwardCell :CallForwardCell = tableView.dequeueReusableCell() {
                    
                    //set cell layout
                    callForwardCell.layoutView(aCoreService: aCoreService, indexPath: indexPath)
                    
                    //adding target on button
                    callForwardCell.callForwardSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                    
                    return callForwardCell
                    
                } else {
                    if let cell :VASServicesCell = tableView.dequeueReusableCell() {
                        
                        //set cell layout
                        cell.layoutView(aCoreService: aCoreService, indexPath: indexPath)
                        
                        //adding target on button
                        cell.callForwardSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                        
                        //hide/show separator view
                        if indexPath.row == ((coreServicesData?[indexPath.section].coreServicesList?.count) ?? 0) - 1 {
                            cell.separatorView.isHidden = true
                        } else {
                            cell.separatorView.isHidden = false
                        }
                        
                        return cell
                    }
                    
                }
                
            case .coreServiceWithBar:
                if let cell :VASServicesWithBarCell = tableView.dequeueReusableCell() {
                    
                    //set cell layout
                    cell.layoutView(aCoreService: aCoreService, indexPath: indexPath)
                    
                    //adding target on button
                    cell.callForwardSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                    
                    //hide/show separator view
                    if indexPath.row == ((coreServicesData?[indexPath.section].coreServicesList?.count) ?? 0) - 1 {
                        cell.separatorView.isHidden = true
                    } else {
                        cell.separatorView.isHidden = false
                    }
                    
                    return cell
                }
                
            default:
                break
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let aCoreService = coreServicesData?[indexPath.section].coreServicesList?[indexPath.row] {
            
            //getting correct type
            let dataType : Constants.AFCoreServiceType = self.typeOfDataInCoreService(aCoreService: aCoreService)
            
            //checking type and returning back corrent cell
            switch (dataType) {
            case .coreService:
                if let aCoreService = coreServicesData?[indexPath.section].coreServicesList?[indexPath.row] {
                    
                    /* Check if selected offer is Call Forward and is not active then redirect to forward number setting page */
                    if aCoreService.offeringId.isEqual(Constants.K_Call_Forward, ignorCase: true) {
                        
                        if aCoreService.status.isActive() == false {
                            navigateToFrwarCalls(forSection: indexPath.section, forRow: indexPath.row, selectedCoreServiceItem: aCoreService)
                        }
                        
                    }
                }
            default:
                break
            }
        }
        
    }
}
