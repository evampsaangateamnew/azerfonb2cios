//
//  CallForwardCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class CallForwardCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var forwardNumberTitleLabel        : AFMarqueeLabel!
    @IBOutlet var descriptionLabel               : AFLabel!
    
    @IBOutlet var callForwardSwitch              : AFSwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func layoutView(aCoreService: CoreServicesList, indexPath : IndexPath) {
        
        /*check if forward number service is active than turn on the switch and enable user interaction else turn off the switch and disable user interaction*/
        if (aCoreService.status).isActive() {
            callForwardSwitch.setOn(true , animated: false)
            callForwardSwitch.isEnabled = true
        } else {
            callForwardSwitch.setOn(false , animated: false)
            callForwardSwitch.isEnabled = false
        }
        
        var forwardNumberString = Localized("Title_ForwadTo") + ": "
        
        if aCoreService.forwardNumber.isBlank == false {
            forwardNumberString += aCoreService.forwardNumber.formatedMSISDNInAzeriFormate()
            
        } else {
            forwardNumberString += Localized("Info_None")
        }
        
        forwardNumberTitleLabel.text = forwardNumberString
        self.descriptionLabel.text = aCoreService.description
        
        //set section and row index
        self.callForwardSwitch.sectionTag = indexPath.section
        self.callForwardSwitch.rowTag = indexPath.row
    }
    
}
