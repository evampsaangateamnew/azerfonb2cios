//
//  VASInternetCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class VASInternetCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var internetSettingsTitleLabel     : UILabel!
    @IBOutlet var descriptionLabel               : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
