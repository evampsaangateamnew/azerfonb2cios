//
//  VASServicesWithBarCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class VASServicesWithBarCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var TitleLabel                : UILabel!
    @IBOutlet var priceLabel                : UILabel!
    @IBOutlet var descriptionLabel          : UILabel!
    @IBOutlet var renewTitleLabel           : UILabel!
    @IBOutlet var daysLeftLabel             : UILabel!
    @IBOutlet var renewDateLabel            : UILabel!
    
    @IBOutlet var callForwardSwitch         : AFSwitch!
    
    @IBOutlet var serviceProgressView       : AFProgressView!
    
    @IBOutlet var separatorView          : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func layoutView(aCoreService: CoreServicesList, indexPath : IndexPath) {
        self.TitleLabel.text = aCoreService.name
        
        if aCoreService.price.isBlank {
            self.priceLabel.text = ""
            
        } else if aCoreService.price.isEqual("0", ignorCase: true) ||
        aCoreService.price.isEqual("0.0", ignorCase: true) {
            
            self.priceLabel.attributedText = Localized("Title_FREE").createAttributedString(mainStringColor: UIColor.afDarkPink, mainStringFont: UIFont.h3GreyRight)
        } else {
            self.priceLabel.attributedText = aCoreService.price.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
        }
        
        self.descriptionLabel.text = aCoreService.description
        self.renewTitleLabel.text = aCoreService.progressTitle
        
        //switch On/Off 
        callForwardSwitch.setOn((aCoreService.status).isActive() , animated: false)
        
        //set date
        let computedValuesFromDates = AFProgressUtilities.calculateProgressValues(from: aCoreService.effectiveDate, to: aCoreService.expireDate, AdditionalValueInEndDate: -1)
        self.daysLeftLabel.text = computedValuesFromDates.daysLeftDisplayValue
        self.renewDateLabel.text = String.init(format: "%@: %@", aCoreService.progressDateLabel, computedValuesFromDates.endDate)
        self.serviceProgressView.setProgress(computedValuesFromDates.progressValue, animated: true)
        
        //set section and row index
        self.callForwardSwitch.sectionTag = indexPath.section
        self.callForwardSwitch.rowTag = indexPath.row
    }
    
}
