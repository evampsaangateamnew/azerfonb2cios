//
//  VASBalanceCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class VASServicesCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var TitleLabel             : UILabel!
    @IBOutlet var priceLabel             : UILabel!
    @IBOutlet var descriptionLabel       : UILabel!
    
    @IBOutlet var callForwardSwitch      : AFSwitch!
    
    @IBOutlet var separatorView          : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func layoutView(aCoreService: CoreServicesList, indexPath : IndexPath) {
        
        self.TitleLabel.text = aCoreService.name
        
        if aCoreService.price.isBlank {
            self.priceLabel.text = ""
        } else if aCoreService.price.isEqual("0", ignorCase: true) ||
            aCoreService.price.isEqual("0.0", ignorCase: true) {
            
            self.priceLabel.attributedText = Localized("Title_FREE").createAttributedString(mainStringColor: UIColor.afDarkPink, mainStringFont: UIFont.h3GreyRight)
        } else {
            self.priceLabel.attributedText = aCoreService.price.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
        }
        
        //set validity labels text in case of invalid offer
        if (aCoreService.status).isActive() == false,
            aCoreService.validity.removeNullValues().isBlank == false {
            self.descriptionLabel.text = "\(aCoreService.description)\n\n\(aCoreService.validityLabel): \(aCoreService.validity)"
        } else {
            self.descriptionLabel.text = aCoreService.description
        }
        
        callForwardSwitch.setOn((aCoreService.status).isActive() , animated: false)
        
        //set section and row index
        self.callForwardSwitch.sectionTag = indexPath.section
        self.callForwardSwitch.rowTag = indexPath.row
    }
}
