//
//  VASServicesHeaderCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class VASServicesHeaderCell: UITableViewHeaderFooterView {

    //MARK: - IBOutlet
    @IBOutlet var titleLabel        : UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
