//
//  NarTVTableViewCell.swift
//  Azerfon
//
//  Created by Mian Waqas Umar on 23/04/2019.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class NarTVTableViewCell: UITableViewCell {
    
    var planModelObject : NarTvPlanModel?
    
    //MARKL: - IBOutlet
    @IBOutlet var planNameLabel : AFLabel!
    @IBOutlet var planPriceLabel : AFLabel!
    @IBOutlet var descriptionLabel : AFLabel!
    @IBOutlet var toRenewLabel : AFLabel!
    @IBOutlet var remainingDaysLabel : AFLabel!
    @IBOutlet var renewalDateLabel : AFLabel!
    @IBOutlet var roundedView : AFView?
    @IBOutlet var progressBar : AFProgressView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    


    override func setSelected(_ selected: Bool, animated: Bool) {
        
        selectedBackgroundView?.backgroundColor = UIColor.clear
        super.setSelected(selected, animated: animated)
        selectedBackgroundView?.backgroundColor = UIColor.clear
        roundedView?.backgroundColor = selected ? UIColor.afVeryLightPinkTwo : UIColor.white
        /*
        if let plan = planModelObject, plan.activationDetails == nil {
            
            
            
            if let bgVw = roundedView {
                
            }

        }
        */
    }
    
    func configureCell(withNarTvPlan model: NarTvPlanModel) {
        
        self.selectionStyle = .none
        
        self.planModelObject = model
        self.planNameLabel.text = model.title
        
        self.planPriceLabel.attributedText = (model.price ?? "0").createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.b3GreyRight, imageSize: CGSize(width: 10, height: 6))
        
        self.descriptionLabel.text = model.description
        
        if model.status?.count ?? 0 > 0 &&  model.status == "true" {
            self.toRenewLabel.text = model.labelToRenew
            
            //set days left
            let computedValuesFromDates = AFProgressUtilities.calculateProgressValues(from: model.startDate ?? "", to: model.endDate ?? "")
            
            self.renewalDateLabel.text = String.init(format: "%@ %@", model.labelRenewalDate ?? "", computedValuesFromDates.endDate)
            self.remainingDaysLabel.text = computedValuesFromDates.daysLeftDisplayValue
        }
    }
}
