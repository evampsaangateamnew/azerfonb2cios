//
//  NarTvVC.swift
//  Azerfon
//
//  Created by Mian Waqas Umar on 23/04/2019.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class NarTvVC: BaseVC {
    //MARK: - Properties
    var narTvPlansResponse : NarTvResponseModel?
    var subscriptions = [NarTvPlanModel]()
    var newSelectedPlan : NarTvPlanModel?
//    var alreadySubscribedPlan : NarTvPlanModel?
    
    //MARK: - IBOutlet
    @IBOutlet var btnSubscribe : AFButton!
    @IBOutlet var packagesTableView : UITableView!
    @IBOutlet var tableViewHeightConstra : NSLayoutConstraint!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Call get Nar TV data*/
        getNarTvPlans()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    
    @IBAction func subscribePressed(_ sender: AFButton) {
        
        if subscriptions.count <= 0 {
            //open store URL
            openURLInSafari(urlString: AFUserSession.shared.predefineData?.narTv?.narTvRedirectionLinkIos ?? "")
            
        } else if self.newSelectedPlan != nil {
            let alertText = String(format: Localized("Message_SubscribePlan"), (newSelectedPlan?.title ?? ""))
            
            self.showConfirmationAlert(message: alertText, {
                self.narTvMigeratePlanApi(subscriberID: self.narTvPlansResponse?.subscriberId ?? "", planID: self.newSelectedPlan?.planId ?? "")
            })
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        self.btnSubscribe?.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: .normal)
        
    }
    
    
    //MARK: - API's Calls
    
    func getNarTvPlans()  {
        
        newSelectedPlan = nil
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.getNarTvSubscriptions(isFrom: "") { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let narTvResp = Mapper<NarTvResponseModel>().map(JSONObject: resultData) {
                    
                    self.narTvPlansResponse = narTvResp
                    self.subscriptions = narTvResp.subscriptions ?? []
                    
                } else {
                    // Show error alert to user
                    // self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.packagesTableView.reloadData()
        }
    }
    
    func narTvMigeratePlanApi(subscriberID : String, planID : String) -> Void {
        
        guard let newPlan = newSelectedPlan else { return }
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.narTvMigrationApi(subscriberId: subscriberID, planId: planID,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let resp = Mapper<MessageResponse>().map(JSONObject: resultData) {
                    
                    self.showSuccessAlertWithMessage(message: resp.message, {
                        self.getNarTvPlans()
                    })
                    
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension NarTvVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if subscriptions.count <= 0  {
            
            tableViewHeightConstra.constant = 330
            self.btnSubscribe?.setTitle(Localized("BtnTitle_NarTV"), for: .normal)
            return 2
            
        } else {
            
            tableViewHeightConstra.constant = 700
            self.btnSubscribe?.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: .normal)
            return subscriptions.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "description") as? NarTVTableViewCell {
                cell.descriptionLabel.text = Localized("info_NARDescription")
                return cell
            }
        }
        else if subscriptions.count <= 0  {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "notSubscriber") as? NarTVTableViewCell {
                cell.descriptionLabel.text = Localized("label_YouAreNotA")
                return cell
            }
        }
        else {
            var identifier = "unsubscribed"
            let plan = subscriptions[indexPath.row - 1]
            
            if plan.status?.count ?? 0 > 0 &&  plan.status == "true" {
                identifier = "subscribed"
            }
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? NarTVTableViewCell {
                cell.configureCell(withNarTvPlan: plan)
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row > 0 && !(subscriptions.count <= 0)  {
            newSelectedPlan = subscriptions[indexPath.row - 1]
            
        } else {
            newSelectedPlan = nil
        }
        
    }
}

