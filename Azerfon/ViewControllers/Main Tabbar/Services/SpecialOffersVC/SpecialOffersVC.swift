//
//  SpecialOffersVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/24/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout
import ObjectMapper

class SpecialOffersVC: BaseVC {
    
    //MARK: - Properties
    var showCardView            : Bool = true
    var isUserSearch            : Bool = true
    
    var secialOffers            : [SupplementaryOfferItem]?
    var filterdOffers           : [SupplementaryOfferItem]?
    var redirectToSpecial       : (isRedireted :Bool, offeringID :String?) = (isRedireted :false, offeringID :nil)
    
    // USED to toggle
    var selectedSectionViewType : Constants.AFSectionType = .offerTitleView
    let cardLayout              : UPCarouselFlowLayout = UPCarouselFlowLayout()
    
    //MARK: - IBOutlet
    @IBOutlet var searchTextField   : UITextField!
    @IBOutlet var searchButton      : UIButton!
    @IBOutlet var SpecialOfferCollectionView: UICollectionView!
    
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SpecialOfferCollectionView.delegate = self
        SpecialOfferCollectionView.dataSource = self
        
        SupplementaryCell.registerReusableCell(with: SpecialOfferCollectionView)
        
        
        self.SpecialOfferCollectionView.layoutIfNeeded()
        self.cardLayout.scrollDirection = .horizontal
        self.cardLayout.sideItemScale = 0.8
        self.cardLayout.sideItemAlpha = 1
        self.cardLayout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 10)
        self.SpecialOfferCollectionView.collectionViewLayout = self.cardLayout
        
        /* set search textField layout */
        self.searchTextField.delegate = self
        self.searchTextField.attributedPlaceholder = NSAttributedString(string: Localized("PlaceHolder_Search"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.afWhite])
        
        let searchSepratorView = UIView()
        searchSepratorView.backgroundColor = UIColor.afWhite
        self.searchTextField.addSubview(searchSepratorView)
        
        searchSepratorView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.searchTextField.snp.left)
            maker.right.equalTo(self.searchTextField.snp.right)
            maker.bottom.equalTo(self.searchTextField.snp.bottom)
            maker.height.equalTo(1)
        }
        /* Reload search textField and button */
        layoutSearchTextField(false, reloadData: false)
        
        /* load special offers and load collection view data */
        self.loadSpecialOffersData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        // Layout view lables
        loadViewLayout()
        
        DispatchQueue.main.async {
            self.cardLayout.itemSize = CGSize(width: self.SpecialOfferCollectionView.bounds.width - 54 , height:  self.SpecialOfferCollectionView.bounds.height)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: - Functions
    
    func loadViewLayout(){
        
        titleLabel?.text = Localized("Title_SpecialOffer");
    }
    
    func layoutSearchTextField(_ showTextField :Bool, reloadData :Bool = true) {
        
        isUserSearch = showTextField
        
        if showTextField {
            titleLabel?.isHidden = true
            searchTextField.isHidden = false
            
            searchTextField.text = ""
            _ = searchTextField.becomeFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search_cross") , for: .normal)
            
        } else {
            titleLabel?.isHidden = false
            searchTextField.isHidden = true
            
            searchTextField.text = ""
            _ = searchTextField.resignFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search") , for: .normal)
            
            if reloadData {
                //selectedOfferGroupInfo = getOffersInfoFor(selectedType: selectedTabType)
                self.reloadCollectionView(itemIndex: 0)
            }
        }
        
    }
    
    func reloadCollectionView(itemIndex :Int, errorMessage : String = Localized("Message_NoData")) {
        
        /* Reload tariff items */
        if (filterdOffers?.count ?? 0) > 0 {
            
            self.SpecialOfferCollectionView.hideDescriptionView()
            
            self.SpecialOfferCollectionView.reloadData()
            self.SpecialOfferCollectionView.performBatchUpdates(nil) { (isCompleted) in
                if isCompleted {
                    
                    self.SpecialOfferCollectionView.scrollToItem(at: IndexPath(item: itemIndex, section: 0), at: .centeredHorizontally, animated: true)
                }
            }
            
        } else {
            self.SpecialOfferCollectionView.showDescriptionViewWithImage(description: errorMessage,
                                                                         descriptionColor: UIColor.afWhite)
            self.SpecialOfferCollectionView.reloadData()
        }
    }
    
    func loadSpecialOffersData() {
        
        
        /* Load data from UserDefaults */
        if let specialOffersHandler :SpecialOffersModel = SpecialOffersModel.loadFromUserDefaults(key: "\(APIsType.specialOffers.selectedLocalizedAPIKey())") {
            
            /* Set information in local object */
            self.secialOffers = specialOffersHandler.special?.offers
            self.secialOffers?.append(contentsOf: specialOffersHandler.tm?.offers ?? [])
            
            self.filterdOffers = self.secialOffers
            
            
            if Connectivity.isConnectedToInternet == true && redirectToSpecial.isRedireted {
                
                /* Load new data */
                self.getSpecialOffers(showIndicator: true)
                
            } else {
                /* Reload collection view data */
                self.reloadCollectionView(itemIndex: 0)
                
                /* Load new data in background*/
                self.getSpecialOffers(showIndicator: false)
            }
            
            
        } else {
            self.getSpecialOffers()
        }
    }
    
    func reloadSpecialOffersData() {
        if redirectToSpecial.isRedireted {
            let (containsOffer, offerIndex) = SupplementaryResponse.containsOfferByOfferingId(offeringId: redirectToSpecial.offeringID, offerArray: self.filterdOffers)
            
            if containsOffer {
                /* Reload collection view data */
                self.reloadCollectionView(itemIndex: offerIndex)
            } else {
                /* Reload collection view data */
                self.reloadCollectionView(itemIndex: 0)
            }
            
            redirectToSpecial = (false, nil)
            
        } else {
            /* Reload collection view data */
            self.reloadCollectionView(itemIndex: 0)
        }
    }
    
    /**
     USED to toggle section.
     
     - returns: Void.
     */
    
    func openSelectedSection() {
        
        let visbleIndexPath = SpecialOfferCollectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = SpecialOfferCollectionView.cellForItem(at: aIndexPath) as? SupplementaryCell {
                
                var openSectionIndex : Int? = 0
                
                if let aTariffObject = filterdOffers?[cell.tableView.tag] {
                    
                    let type = typeOfDataInSection(aTariffObject)
                    
                    if selectedSectionViewType == .offerTitleView && type.contains(.offerTitleView) {
                        
                        openSectionIndex = 0
                        
                    } else if selectedSectionViewType == .detail && type.contains(.detail) {
                        
                        openSectionIndex = type.index(of:.detail)
                        
                    }
                }
                
                cell.tableView.closeAllSectionsExcept(openSectionIndex ?? 0, shouldCallDelegate: false)
                
                openSectionAt(index: openSectionIndex ?? 0, tableView: cell.tableView)
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : AFAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
    
    
    //MARK: - IBActions
    
    @IBAction func searchPressed(_ sender: UIButton) {
        layoutSearchTextField(isUserSearch ? false : true)
    }
    
    @IBAction func subscribePressed(_ sender: AFButton) {
        if sender.tag >= 0,
            let selectedOffer = filterdOffers?[sender.tag]{
            
            var isTypeRenewValue: Bool = false
            
            switch sender.buttonContentType {
            case .topup:
                if let topUpVC :TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard() {
                    self.navigationController?.pushViewController(topUpVC, animated: true)
                }
                return
            case .renew:
                isTypeRenewValue = true
                break
            default:
                break
            }
            
            self.showConformationAlert(selectedOffer: selectedOffer,
                                       isRenew: isTypeRenewValue)
        }
    }
    
    func showConformationAlert(selectedOffer :SupplementaryOfferItem, isRenew : Bool) {
        
        var activationMessage = ""
        
        if isRenew {
            activationMessage = Localized("Message_RenewOffer")
        } else {
            activationMessage = Localized("Message_subscribeOffer")
        }
        self.showConfirmationAlert(message: activationMessage, {
            
            /* OK BUTTON PRESSED */
            
            self.changeSupplementaryOffering(selectedOffer: selectedOffer, actionType: true)
            
        })
    }
    
    
    
    //MARK: - API Calls
    
    /**
     Call 'getSupplementaryOffering' API .
     - returns: void
     */
    
    func getSpecialOffers(showIndicator :Bool = true) {
        
        if (showIndicator == true) {
            self.showActivityIndicator()
        }
        
        _ = AFAPIClient.shared.getSpecialOffers({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if (showIndicator == true) {
                self.hideActivityIndicator()
            }
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let specialOffersHandler = Mapper<SpecialOffersModel>().map(JSONObject: resultData){
                        
                        // Saving Supplementary Offers data in user defaults
                        specialOffersHandler.saveInUserDefaults(key: APIsType.specialOffers.selectedLocalizedAPIKey())
                        
                        /* Set information in local object */
                        self.secialOffers = specialOffersHandler.special?.offers
                        self.secialOffers?.append(contentsOf: specialOffersHandler.tm?.offers ?? [])
                        
                        self.filterdOffers = self.secialOffers
                        
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            /* Reload collection view data */
            self.reloadSpecialOffersData()
        })
    }
    
    /**
     Call 'changeSupplementaryOffering' API .
     - parameter  selectedOffer : offer user selected to renew or deactivate
     - parameter  actionType : if user want to renew offer then actionType will be true and for deactivation it will be false.
     - returns: Void
     */
    func changeSupplementaryOffering(selectedOffer: SupplementaryOfferItem, actionType :Bool) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.changeSupplementaryOffering(actionType: actionType, offeringId: selectedOffer.header?.offeringId ?? "", offerName: selectedOffer.header?.offerName ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "special_offer_subscription", contentType:"subscription" , successStatus:"0" )
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let response = Mapper<MySubscriptionModel>().map(JSONObject: resultData) {
                    
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "special_offer_subscription", contentType:"subscription" , successStatus:"1" )
                    
                    /* Show successfull message */
                    self.showSuccessAlertWithMessage(message: response.message)
                    
                    /* Reload collection view data */
                    self.reloadCollectionView(itemIndex: 0)
                    
                } else {
                    //EventLog
                    AFAnalyticsManager.logEvent(screenName: "special_offer_subscription", contentType:"subscription" , successStatus:"0" )
                    
                    /* Show error alert to user */
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
}
//MARK: - COLLECTION VIEW Delegates
extension SpecialOffersVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.SpecialOfferCollectionView.bounds.width - 54 , height:  self.SpecialOfferCollectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterdOffers?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell :SupplementaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: SupplementaryCell.cellIdentifier(), for: indexPath) as? SupplementaryCell,
            let aOfferObject = filterdOffers?[indexPath.item] {
            
            cell.setTitleLayout(header: aOfferObject.header)
            
            cell.tableView.delegate = self
            cell.tableView.dataSource = self
            
            cell.tableView.tag = indexPath.item
            cell.subscribeButton.tag = indexPath.item
            
            cell.subscribeButton.addTarget(self, action: #selector(subscribePressed(_:)), for: .touchUpInside)
            
            cell.tableView.reloadData()
            
            return cell
        }
        return UICollectionViewCell()
        
    }
}


//MARK: - UITableViewDelegate
extension SpecialOffersVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let aOfferObject = filterdOffers?[tableView.tag]{
            
            return typeOfDataInSection(aOfferObject).count
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 70
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let aOfferObject = filterdOffers?[tableView.tag] {
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[section]{
            case .offerTitleView:
                if let headerView :SupplementaryTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    
                    headerView.setTitleLayout(header: aOfferObject.header,
                                              headerType: .offerTitleView)
                    return headerView
                }
                break
            case .detail:
                if let headerView :ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    headerView.setViewWithTitle(title: Localized("Title_Detail"),
                                                isSectionSelected: selectedSectionViewType == .detail ? true : false,
                                                headerType: .detail)
                    return headerView
                }
                break
                
            default:
                break
            }
            
        }
        
        return AFAccordionTableViewHeaderView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let aOfferObject = filterdOffers?[tableView.tag]{
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[section]{
            case .offerTitleView:
                return aOfferObject.header?.attributeList?.count ?? 0
                
            case .detail:
                
                return typeOfDataInDetailSection(aOfferObject.details).count
                
            default:
                break
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let aOfferObject = filterdOffers?[tableView.tag] {
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[indexPath.section]{
            case .offerTitleView:
                
                if let cell : SingleAttributeCell = tableView.dequeueReusableCell() {
                    cell.setAttributeHeader(aOfferObject.header?.attributeList?[indexPath.row],
                                            showSeprator: indexPath.row < ((aOfferObject.header?.attributeList?.count ?? 0) - 1))
                    
                    return cell
                }
                
                break
            case .detail:
                
                let detailsData = typeOfDataInDetailSection(aOfferObject.details)
                
                switch detailsData[indexPath.row] {
                    
                case .price:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setPriceLayoutValues(price: aOfferObject.details?.price?[indexPath.row],
                                                  showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                    break
                case .rounding:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setRoundingLayoutValues(rounding: aOfferObject.details?.rounding,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .textWithTitle:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithTitleLayoutValues(textWithTitle: aOfferObject.details?.textWithTitle,
                                                          showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithOutTitle:
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aOfferObject.details?.textWithOutTitle,
                                                             showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithPoints:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithPointsLayoutValues(textWithPoint: aOfferObject.details?.textWithPoints,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .date:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aOfferObject.details?.date,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .time:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aOfferObject.details?.time,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .roamingDetails:
                    
                    if let cell:RoamingDetailsCell = tableView.dequeueReusableCell() {
                        cell.setRoamingDetailsLayout(roamingDetails: aOfferObject.details?.roamingDetails,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .freeResourceValidity:
                    
                    if let cell:FreeResourceValidityCell = tableView.dequeueReusableCell() {
                        cell.setFreeResourceValidityValues(freeResourceValidity: aOfferObject.details?.freeResourceValidity,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .titleSubTitleValueAndDesc:
                    
                    if let cell:TitleSubTitleValueAndDescCell = tableView.dequeueReusableCell() {
                        cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aOfferObject.details?.titleSubTitleValueAndDesc,
                                                                      showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                default:
                    break
                }
                break
                
            default:
                break
            }
        }
        
        return UITableViewCell()
    }
}

// MARK: - AFAccordionTableViewDelegate

extension SpecialOffersVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: true)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView,
            selectedSectionViewType != headerView.viewType {
            
            selectedSectionViewType = headerView.viewType
            
        } else if let headerView = header as? SupplementaryTitleHeaderView,
            selectedSectionViewType != headerView.viewType {
            
            selectedSectionViewType = headerView.viewType
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: false)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView,
            selectedSectionViewType == headerView.viewType {
            
            selectedSectionViewType = .offerTitleView
            tableView.toggleSection(0)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

//MARK: - Helping functions
extension SpecialOffersVC {
    
    func typeOfDataInSection(_ aOfferItem : SupplementaryOfferItem?) -> [Constants.AFSectionType] {
        
        var dataTypes : [Constants.AFSectionType] = []
        
        // 1
        /* Show offer title view in all cases */
        dataTypes.append(.offerTitleView)
        
        // 2
        if aOfferItem?.details != nil {
            dataTypes.append(.detail)
        }
        
        return dataTypes
    }
    
    func typeOfDataInDetailSection(_ aDetail : Details?) -> [Constants.AFOfferType] {
        
        var dataTypes : [Constants.AFOfferType] = []
        
        if let aDetailObject =  aDetail {
            
            // 1
            if let count = aDetailObject.price?.count {
                
                for _ in 0..<count {
                    dataTypes.append(.price)
                }
            }
            // 2
            if aDetailObject.rounding != nil  {
                
                dataTypes.append(.rounding)
            }
            // 3
            if aDetailObject.textWithTitle != nil {
                
                dataTypes.append(.textWithTitle)
            }
            // 4
            if aDetailObject.textWithOutTitle != nil {
                
                dataTypes.append(.textWithOutTitle)
            }
            // 5
            if aDetailObject.textWithPoints != nil {
                
                dataTypes.append(.textWithPoints)
            }
            // 6
            if aDetailObject.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(.titleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailObject.date != nil {
                
                dataTypes.append(.date)
            }
            // 8
            if aDetailObject.time != nil {
                
                dataTypes.append(.time)
            }
            // 9
            if aDetailObject.roamingDetails != nil {
                
                dataTypes.append(.roamingDetails)
            }
            // 10
            if aDetailObject.freeResourceValidity != nil {
                
                dataTypes.append(.freeResourceValidity)
            }
        }
        return dataTypes
    }
}

//MARK:- UITextFieldDelegate

extension SpecialOffersVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == searchTextField {
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            // Filter offers with new name
            filterdOffers = SpecialOffersModel.filterOfferBySearchString(searchString: newString, offers: secialOffers)
            /* Reload user content */
            self.reloadCollectionView(itemIndex: 0, errorMessage: Localized("Message_NothingFoundSearch"))
            
            if newString.length > 0 {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "search", contentType:"special_offers_screen" , searchTerm: newString )
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
}

//MARK: - UIScrollViewDelegate
extension SpecialOffersVC : UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == SpecialOfferCollectionView {
            openSelectedSection()
        }
    }
}


