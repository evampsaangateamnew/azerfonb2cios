//
//  ServicesVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class ServicesVC: BaseVC {
    
    //MARK: - Property
    var servicesList : [Items] = []
    var titleValue :String = Localized("Title_Services")
    
    //MARK: - IBOutlet
    @IBOutlet var servicesTableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        //getting services from session and adding into local array and reloading data
        AFUserSession.shared.appMenu?.menuHorizontal?.forEach({ (aHorizontalMenu) in
            if aHorizontalMenu.identifier == Constants.HorizontalMenus.services.rawValue {
                servicesList = aHorizontalMenu.items ?? []
                
                self.titleValue = aHorizontalMenu.title.isBlank ? Localized("Title_Services") : aHorizontalMenu.title
            }
        })
        
        servicesList.sort{ $0.sortOrder.toInt < $1.sortOrder.toInt }
        
        /* Remove Special offer menu if specialOffersMenu is not true */
        if (AFUserSession.shared.userInfo?.specialOffersTariffData?.specialOffersMenu?.isEqual("hide", ignorCase: true) ?? false),
            servicesList.contains(where: { $0.identifier.isEqual("services_special_offers", ignorCase: true)}),
            let specialOfferIndex = servicesList.index(where: { $0.identifier.isEqual("services_special_offers", ignorCase: true)}) {
            
            servicesList.remove(at: specialOfferIndex)
        }
        
        /*
        /* Remove Exchange offer from Menu if user is not prepaid full */
        if AFUserSession.shared.userType != .prepaidFull,
            servicesList.contains(where: { $0.identifier.isEqual("services_exchange_service", ignorCase: true)}),
            let exchangeOfferIndex = servicesList.index(where: { $0.identifier.isEqual("services_exchange_service", ignorCase: true)}) {
            
            servicesList.remove(at: exchangeOfferIndex)
        }
         */
        
        loadViewContent()
        
        servicesTableView.reloadUserData(servicesList)
    }
    
    //MARK: - IBAction
    
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        ServicesCell.registerReusableCell(with: servicesTableView)
        
        self.titleLabel?.text = titleValue
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier : String) {
        
        switch identifier {
            
        case "services_special_offers":
            if let freeSmsVC :SpecialOffersVC = SpecialOffersVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(freeSmsVC, animated: true)
            }
            break
            
        case "services_supplementary_offers":
            if let freeSmsVC :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(freeSmsVC, animated: true)
            }
            break
            
        case "services_free_sms":
            // Load SendFreeSms VC
            if let freeSmsVC :FreeSmsVC = FreeSmsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(freeSmsVC, animated: true)
            }
            break
            
        case "services_fnf":
            // Load FNF VC
            if let fNFVC :FNFVC = FNFVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(fNFVC, animated: true)
            }
            break
            
        case "services_vas_services":
            // Load VASServices VC
            if let vasServicesVC :VASServicesVC = VASServicesVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(vasServicesVC, animated: true)
            }
            break
            
        case "services_exchange_service":
            
            if let exchangeServcVC : ExchangeServiceVC = ExchangeServiceVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(exchangeServcVC, animated: true)
            }
            
            break
            
        case "services_nar_tv":
            
            if let narTvVC : NarTvVC = NarTvVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(narTvVC, animated: true)
            }
            
            break
        case "service_topup":
            
            if let narTvVC : TopUpMainVC = TopUpMainVC.instantiateViewControllerFromStoryboard() {
                narTvVC.showBackButton = true
                self.navigationController?.pushViewController(narTvVC, animated: true)
            }
            
            
            break
            
        case "dataUsage_withoutPack":
            
            if let paygVC : PaygVC = PaygVC.instantiateViewControllerFromStoryboard2() {
                paygVC.titleSter = getTitleWith(idenifier: "dataUsage_withoutPack")
                self.navigationController?.pushViewController(paygVC, animated: true)
            }
            
            break
            
        default:
            break
        }
    }
    
    func getTitleWith(idenifier:String) -> String {
        var titleFound = ""
        for itemToCheck in  servicesList {
            if itemToCheck.identifier.lowercased() == idenifier.lowercased() {
                titleFound = itemToCheck.title
                break
            }
        }
        return titleFound
    }
}

//MARK: - TableView Delegates
extension ServicesVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicesList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let myCell: ServicesCell = tableView.dequeueReusableCell() {
            myCell.setItemInfo(aItem: servicesList[indexPath.row])
            return myCell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Redirect user to selected screen
        redirectUserToSelectedController(Identifier: servicesList[indexPath.row].identifier)
    }
}
