//
//  SupplementaryTitleHeaderView.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 5/10/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class SupplementaryTitleHeaderView: AFAccordionTableViewHeaderView {
    
    var viewType : Constants.AFSectionType = Constants.AFSectionType.unSpecified
    
    @IBOutlet var myContentView: UIView!
    @IBOutlet var nameLabel: AFMarqueeLabel!
    @IBOutlet var priceLabel: AFLabel!
    @IBOutlet var validityTitleLabel: AFLabel!
    @IBOutlet var validityValueLabel: AFLabel!
    
    func setTitleLayout(header : SupplementaryHeader?, headerType :Constants.AFSectionType, checkZeroPrice :Bool = false) {
        
        viewType = headerType
        
        nameLabel.text = header?.offerName
        
        /* Check if layout is set for special, if true then don't show amount if it is less then equal to 0 */
        if checkZeroPrice == true {
            
            if let offerPrice = header?.price?.trimmWhiteSpace,
                offerPrice.toDouble > 0  {
                
                priceLabel.attributedText = offerPrice
                    .createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown,
                                                              mainStringFont: UIFont.b3GreyRight,
                                                              imageSize: CGSize(width: 10, height: 6))
            } else {
                priceLabel.text = ""
            }
           
        } else {
            priceLabel.attributedText = header?.price?
                .createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown,
                                                          mainStringFont: UIFont.b3GreyRight,
                                                          imageSize: CGSize(width: 10, height: 6))
        }
        
        if let validityTitle = header?.validityTitle,
            validityTitle.isBlank == false {
            
            validityTitleLabel.text = validityTitle
        } else {
            validityTitleLabel.text = ""
        }
        
        if let info = header?.validityInformation,
            info.isBlank == false {
            
            if let validityText = validityTitleLabel.text,
                validityText.isBlank == false {
                
                validityTitleLabel.text = "\(validityText): \(info)"
            } else {
                validityTitleLabel.text = info
            }
        }
        
        validityValueLabel.attributedText = header?.validityValue?
            .createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey,
                                                      mainStringFont: UIFont.b4LightGreyRight,
                                                      imageSize: CGSize(width: 10, height: 6))
    }
}
