//
//  OfferContentCell.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/1/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit


protocol SupplementaryCellDelegate {
    func starTapped(offerId :  String)
}

class SupplementaryCell: UICollectionViewCell {
    
    @IBOutlet var myContentView: AFView!
    @IBOutlet var tableView: AFAccordionTableView!
    @IBOutlet var buttonsContainorView: UIView!
    @IBOutlet var subscribeButton: AFButton!
    @IBOutlet var subscribedOfferButton: AFButton!
    
    //Stars outlets....
    @IBOutlet weak var btnsStackView: UIStackView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    
    var delegate : SupplementaryCellDelegate? = nil
    var offer: SupplementaryHeader?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ExpandableTitleHeaderView.registerReusableHeaderView(with: tableView)
        
        tableView.allowMultipleSectionsOpen = false
        tableView.keepOneSectionOpen = true;
        tableView.allowsSelection = false
        
        tableView.estimatedSectionHeaderHeight = 40
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
        tableView.separatorColor = UIColor.clear
        tableView.initialOpenSections = [0]
        self.btnsStackView.isHidden = true
    }
    
    func setTitleLayout(header : SupplementaryHeader?) {
        
        self.offer = header
        /* Setting buttons layout */
        subscribedOfferButton.removeFromSuperview()
        buttonsContainorView.addSubview(subscribedOfferButton)
        
        subscribeButton.removeFromSuperview()
        buttonsContainorView.addSubview(subscribeButton)
        
        subscribedOfferButton.removeConstraints(subscribedOfferButton?.constraints ?? [])
        subscribeButton.removeConstraints(subscribeButton?.constraints ?? [])
        
        /* Setting button content type */
        if header?.isTopUp?.isButtonEnabled() ?? false {
            
            subscribedOfferButton.isHidden = true
            self.btnsStackView.isHidden = true
            
            subscribeButton.isEnabled = true
            subscribeButton.isHidden = false
            
            subscribeButton.snp.makeConstraints { (maker) in
                maker.width.greaterThanOrEqualTo(180)
                maker.height.equalTo(40)
                maker.centerX.equalTo(buttonsContainorView.snp.centerX)
                maker.centerY.equalTo(buttonsContainorView.snp.centerY)
            }
            
            subscribeButton.buttonContentType = .topup
            subscribeButton.setTitle(Localized("BtnTitle_Topup"), for: .normal)
            subscribeButton.setButtonLayoutType(.whiteText_ClearBG_Rounded)
            
            
        } else if header?.isAlreadySusbcribed?.isTrue() ?? false {
            
            if header?.btnRenew?.isButtonEnabled() ?? false {
                
                subscribeButton.isEnabled = true
                subscribeButton.isHidden = false
                
                subscribeButton.snp.makeConstraints { (maker) in
                    maker.width.greaterThanOrEqualTo(100).priority(999)
                    maker.height.equalTo(40)
                    maker.centerX.equalTo(buttonsContainorView.snp.centerX).multipliedBy(0.5)
                    maker.centerY.equalTo(buttonsContainorView.snp.centerY)
                    maker.left.greaterThanOrEqualTo(buttonsContainorView.snp.left).offset(8)
                }
                
                subscribeButton.buttonContentType = .renew
                subscribeButton.setTitle(Localized("BtnTitle_RENEW"), for: .normal)
                subscribeButton.setButtonLayoutType(.superPinkText_WhitBG_Rounded)
                
                
                subscribedOfferButton.isEnabled = false
                subscribedOfferButton.isHidden = false
                self.btnsStackView.isHidden = false
                self.setRating()
                
                subscribedOfferButton.snp.makeConstraints { (maker) in
                    maker.width.equalTo(subscribeButton.snp.width).priority(999)
                    maker.height.equalTo(40)
                    maker.centerX.equalTo(buttonsContainorView.snp.centerX).multipliedBy(1.5)
                    maker.centerY.equalTo(buttonsContainorView.snp.centerY)
                    maker.left.greaterThanOrEqualTo(subscribeButton.snp.right).offset(8)
                    maker.right.greaterThanOrEqualTo(buttonsContainorView.snp.right).offset(-8)
                }
                
                subscribedOfferButton.buttonContentType = .subscribed
                subscribedOfferButton.setTitle(Localized("BtnTitle_SUBSCRIBED"), for: .normal)
                subscribedOfferButton.setButtonLayoutType(.whiteText_ClearBG_Rounded_Disabled)
                
            } else {
                
                subscribeButton.isHidden = true
                
                subscribedOfferButton.isEnabled = false
                subscribedOfferButton.isHidden = false
                self.btnsStackView.isHidden = false
                self.setRating()
                
                subscribedOfferButton.snp.makeConstraints { (maker) in
                    maker.width.greaterThanOrEqualTo(180)
                    maker.height.equalTo(40)
                    maker.centerX.equalTo(buttonsContainorView.snp.centerX)
                    maker.centerY.equalTo(buttonsContainorView.snp.centerY)
                }
                
                subscribedOfferButton.buttonContentType = .subscribed
                subscribedOfferButton.setTitle(Localized("BtnTitle_SUBSCRIBED"), for: .normal)
                subscribedOfferButton.setButtonLayoutType(.whiteText_ClearBG_Rounded_Disabled)
            }
            
        } else {
            
            subscribedOfferButton.isHidden = true
            self.btnsStackView.isHidden = true
            
            subscribeButton.isEnabled = true
            subscribeButton.isHidden = false
            
            subscribeButton.snp.makeConstraints { (maker) in
                maker.width.greaterThanOrEqualTo(180)
                maker.height.equalTo(40)
                maker.centerX.equalTo(buttonsContainorView.snp.centerX)
                maker.centerY.equalTo(buttonsContainorView.snp.centerY)
            }
            
            subscribeButton.buttonContentType = .subscribe
            subscribeButton.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: .normal)
            subscribeButton.setButtonLayoutType(.whiteText_ClearBG_Rounded)
        }
    }
    
    // stars for survey
    func setRating(){
        
        if let surveyToDisplay  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == SurveyScreenName.bundle_subscribed.rawValue }) {
            if  let  userSurvey = AFUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == self.offer?.offeringId ?? ""}) {
                if let  question =  surveyToDisplay.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                    if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                        switch answer {
                        case 0:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 1:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 2:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 3:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 4:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                        default:
                            showUnselectedStars()
                            break
                        }
                        
                    }
                } else {
                    showUnselectedStars()
            }
                
            
            } else {
               showUnselectedStars()
            }
        } else {
            self.btnsStackView?.isHidden = true
        }
    }
    
    func showUnselectedStars() {
        btn1.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
    }
    
    
    @IBAction func starTapped(_ sender: UIButton){
        if  let ofr  = self.offer?.offeringId{
            self.delegate?.starTapped(offerId: ofr )
        }
    }
}
