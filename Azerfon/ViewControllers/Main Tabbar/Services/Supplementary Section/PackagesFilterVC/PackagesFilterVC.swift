//
//  PackagesFilterVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/24/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class PackagesFilterVC: BaseVC {
    typealias AFFilterCompletionHandler = (_ selectedFilterOptions : [FilterContent]) -> Void
    
    //MARK: - Properties
    var filtersData         : [FilterContent]?
    var selectedFilters     : [FilterContent] = []
    var applyCompletionBlock     : AFFilterCompletionHandler? = { _ in }
    
    //MARK: - IBOutlet
    @IBOutlet var resetButton   : UIButton?
    @IBOutlet var applyButton   : UIButton?
    @IBOutlet var tableView     : UITableView?
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.afBlackTransparent
        tableView?.hideDefaultSeprator()
        
        
        tableView?.delegate = self
        tableView?.dataSource = self
        
        FilterCell.registerReusableCell(with: tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        resetButton?.setTitle(Localized("BtnTitle_Reset"), for: .normal)
        applyButton?.setTitle(Localized("BtnTitle_APPLY"), for: .normal)
    }
    
    //MARK: - IBAction
    @IBAction func resetPressed(_ sender: UIButton) {
        selectedFilters = []
        tableView?.reloadData()
    }
    
    @IBAction func applyPressed(_ sender: UIButton) {
        if applyCompletionBlock != nil {
            applyCompletionBlock?(selectedFilters)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Functions
    func setCompletionHandler(completionBlock : @escaping AFFilterCompletionHandler ) {
        self.applyCompletionBlock = completionBlock
    }
    //MARK: - API's Calls
    
}

extension PackagesFilterVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtersData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell : FilterCell = tableView.dequeueReusableCell(),
            let aFilterInfo = filtersData?[indexPath.row] {
            cell.titleLabel.text = aFilterInfo.value
            
            if selectedFilters.contains(where: {$0.key == aFilterInfo.key}) {
                cell.imageView?.image = UIImage(named: "check_on")
            } else {
                cell.imageView?.image = UIImage(named: "check_off")
            }
            
            let clearView = UIView()
            clearView.backgroundColor = UIColor.clear
            
            cell.selectedBackgroundView = clearView
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as? FilterCell,
            let selectedFilter = filtersData?[indexPath.row] {
            
            if selectedFilters.contains(where: {$0.key == selectedFilter.key}) {
                
                selectedFilters.removeAll(where: {$0.key == selectedFilter.key})
                cell.imageView?.image = UIImage(named: "check_off")
                
            } else {
                
                selectedFilters.append(selectedFilter)
                cell.imageView?.image = UIImage(named: "check_on")
            }
        }
    }
}

extension PackagesFilterVC {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.view {
                applyCompletionBlock = nil
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}


