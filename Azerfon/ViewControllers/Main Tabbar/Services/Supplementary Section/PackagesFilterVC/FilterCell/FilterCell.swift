//
//  FilterCell.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/24/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
