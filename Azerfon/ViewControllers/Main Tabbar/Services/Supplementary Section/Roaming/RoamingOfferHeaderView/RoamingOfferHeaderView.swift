//
//  RoamingOfferHeaderView.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 5/13/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class RoamingOfferHeaderView: AFAccordionTableViewHeaderView {
    
    var viewType : Constants.AFSectionType = Constants.AFSectionType.unSpecified
    
    @IBOutlet var countryNameLabel: AFLabel!
    @IBOutlet var countryFlagImageView: UIImageView!
    @IBOutlet var nameLabel: AFMarqueeLabel!
    @IBOutlet var priceLabel: AFLabel!
    @IBOutlet var validityTitleLabel: AFLabel!
    @IBOutlet var validityValueLabel: AFLabel!
  
    func setTitleLayout(header : SupplementaryHeader?, selectedCountry :Countries?, headerType :Constants.AFSectionType, checkZeroPrice :Bool = false) {
        
        viewType = headerType
        
        countryNameLabel.text = selectedCountry?.name ?? ""
        if let flagName =  selectedCountry?.flag,
            flagName.isBlank == false,
            let flagImage = UIImage(named: flagName) {
            
            countryFlagImageView.image = flagImage
        } else {
            countryFlagImageView.image = UIImage(named: "dummy_flag")
        }
        
        nameLabel.text = header?.offerName
        
        /* Check if layout is set for special, if true then don't show amount if it is less then equal to 0 */
        if checkZeroPrice == true {
            if let offerPrice = header?.price?.trimmWhiteSpace,
                offerPrice.toDouble > 0  {
                
                priceLabel.attributedText = offerPrice
                    .createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown,
                                                              mainStringFont: UIFont.b3GreyRight,
                                                              imageSize: CGSize(width: 10, height: 6))
            } else {
                priceLabel.text = ""
            }
        } else {
            priceLabel.attributedText = header?.price?
                .createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown,
                                                          mainStringFont: UIFont.b3GreyRight,
                                                          imageSize: CGSize(width: 10, height: 6))
        }
        
        validityTitleLabel.text = ""
        
        if let validityTitle = header?.validityTitle,
            validityTitle.isBlank == false {
            validityTitleLabel.text = validityTitle
        }
        
        if let info = header?.validityInformation,
            info.isBlank == false {
            
            if let validityText = validityTitleLabel.text,
                validityText.isBlank == false {
                
                validityTitleLabel.text = "\(validityText): \(info)"
            } else {
                validityTitleLabel.text = info
            }   
        }
        
        validityValueLabel.attributedText = header?.validityValue?
            .createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey,
                                                      mainStringFont: UIFont.b4LightGreyRight,
                                                      imageSize: CGSize(width: 10, height: 6))
        
    }

}
