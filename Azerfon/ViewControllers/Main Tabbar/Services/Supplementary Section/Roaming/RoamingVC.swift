//
//  RoamingVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/18/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout
import ObjectMapper

class RoamingVC: BaseVC {
    
    //MARK: - Properties
    var showCardView : Bool     = true
    var prepareVCForRoaming     : Bool = false
    var selectedFilterOptions   : [Constants.AFFilterType : [KeyValue]] = [:]
    
    var selectedRoamingOffers   : Roaming?
    var filterdOffers           : [SupplementaryOfferItem]?
    var isUserSearch            : Bool = true
    let cardLayout              : UPCarouselFlowLayout = UPCarouselFlowLayout()
    
    var selectedRoamingCountryName  : Countries?
    var selectedInternetFilters     : [FilterContent] = []
    
    // USED to toggle
    var selectedSectionViewType : Constants.AFSectionType = .offerTitleView
    
    var collectenViewCurrentIndex:Int = 0
    
    //MARK: - IBOutlet
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var switchButton: UIButton!
    @IBOutlet var filterButton: UIButton!
    
    @IBOutlet var roamingCollectionView: UICollectionView!
    @IBOutlet var leftArrowButton      : UIButton!
    @IBOutlet var rightArrowButton      : UIButton!
    @IBOutlet var upArrowButton      : UIButton!
    @IBOutlet var downArrowButton      : UIButton!
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filterdOffers = selectedRoamingOffers?.offers
        
        roamingCollectionView.delegate = self
        roamingCollectionView.dataSource = self
        
        RoamingOfferCell.registerReusableCell(with: roamingCollectionView)
        
        self.roamingCollectionView.layoutIfNeeded()
        self.cardLayout.itemSize = CGSize(width: self.roamingCollectionView.bounds.width - 54 , height:  self.roamingCollectionView.bounds.height)
        self.cardLayout.scrollDirection = .horizontal
        self.cardLayout.sideItemScale = 0.8
        self.cardLayout.sideItemAlpha = 1
        self.cardLayout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 22)
        self.roamingCollectionView.collectionViewLayout = self.cardLayout
        
        
        self.searchTextField.delegate = self
        self.searchTextField.attributedPlaceholder = NSAttributedString(string: Localized("PlaceHolder_Search"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.afWhite])
        
        let searchSepratorView = UIView()
        searchSepratorView.backgroundColor = UIColor.afWhite
        self.searchTextField.addSubview(searchSepratorView)
        
        searchSepratorView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.searchTextField.snp.left)
            maker.right.equalTo(self.searchTextField.snp.right)
            maker.bottom.equalTo(self.searchTextField.snp.bottom)
            maker.height.equalTo(1)
        }
        
        /* Reload card layout and images of switch button */
        updateCollectionViewLayout(isCardLayout: showCardView, reloadData: false)
        self.reloadRoamingOffer(itemIndex: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        // Layout view lables
        loadViewLayout()
        
    }
    
    
    //MARK: - IBActions
    @IBAction func switchView(_ sender: Any) {
        /* Revese showCardView value */
        showCardView = !showCardView
        /* Update layout*/
        self.updateCollectionViewLayout(isCardLayout: showCardView)
    }
    
    @IBAction func searchPressed(_ sender: UIButton) {
        layoutSearchTextField(isUserSearch ? false : true)
    }
    
    @IBAction func filterPressed(_ sender: UIButton){
        
        // Reset search
        layoutSearchTextField(false, reloadData: false)
        
        if let filterVC :PackagesFilterVC = PackagesFilterVC.instantiateViewControllerFromStoryboard() {
            
            filterVC.filtersData = selectedRoamingOffers?.filters?.app
            filterVC.selectedFilters = self.selectedInternetFilters
            
            filterVC.setCompletionHandler { (selectedFilters) in
                
                self.selectedInternetFilters = selectedFilters
                
                self.filterdOffers = self.selectedRoamingOffers?.filterOfferByGroupValue(selectedFilters: selectedFilters)
                
                self.reloadRoamingOffer(itemIndex: 0)
            }
            self.presentPOPUP(filterVC, animated: true)
        }
    }
    
    @IBAction func subscribePressed(_ sender: AFButton) {
        if sender.tag >= 0,
            let selectedOffer = filterdOffers?[sender.tag]{
            
            var isTypeRenewValue: Bool = false
            
            switch sender.buttonContentType {
            case .topup:
                if let topUpVC :TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard() {
                    self.navigationController?.pushViewController(topUpVC, animated: true)
                }
                return
            case .renew:
                isTypeRenewValue = true
                break
            default:
                break
            }
            
            showConformationAlert(selectedOffer: selectedOffer, isRenew: isTypeRenewValue)
        }
    }
    
    @IBAction func moveToOffer(_ sender: AFButton) {
        
        //print("current offer index while taping nex: \(collectenViewCurrentIndex)")
        if sender == rightArrowButton || sender == downArrowButton {
            if collectenViewCurrentIndex <= (filterdOffers?.count ?? 0) - 2 {
                collectenViewCurrentIndex  = collectenViewCurrentIndex + 1
                if showCardView {
                    self.roamingCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredHorizontally, animated: true)
                } else {
                    self.roamingCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredVertically, animated: true)
                }
                
            }
        } else {
            if collectenViewCurrentIndex >= 1 {
                collectenViewCurrentIndex = collectenViewCurrentIndex - 1
                if showCardView {
                    self.roamingCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredHorizontally, animated: true)
                } else {
                    self.roamingCollectionView.scrollToItem(at: IndexPath(item: collectenViewCurrentIndex, section: 0), at: .centeredVertically, animated: true)
                }
            }
        }
        if (filterdOffers?.count ?? 0) > 1 {
            if collectenViewCurrentIndex == 0 {
                if !showCardView {
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = false
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = true
                } else {
                    self.rightArrowButton.isHidden = false
                    self.leftArrowButton.isHidden = true
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = true
                }
            } else if collectenViewCurrentIndex == (filterdOffers?.count ?? 0) - 1 {
                if !showCardView {
                    self.upArrowButton.isHidden = false
                    self.downArrowButton.isHidden = true
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = true
                } else {
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = false
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = true
                }
            } else {
                if !showCardView {
                    self.upArrowButton.isHidden = false
                    self.downArrowButton.isHidden = false
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = true
                } else {
                    self.rightArrowButton.isHidden = false
                    self.leftArrowButton.isHidden = false
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = true
                }
            }
        } else {
            if !showCardView {
                self.upArrowButton.isHidden = true
                self.downArrowButton.isHidden = true
            } else {
                self.rightArrowButton.isHidden = true
                self.leftArrowButton.isHidden = true
            }
        }
    }
    
    //MARK: - Functions
    
    func loadViewLayout(){
        
        titleLabel?.text = Localized("Title_Roaming");
    }
    
    func showConformationAlert(selectedOffer :SupplementaryOfferItem, isRenew : Bool) {
        
        var activationMessage = ""
        
        if isRenew {
            activationMessage = Localized("Message_RenewOffer")
        } else {
            activationMessage = Localized("Message_subscribeOffer")
        }
        self.showConfirmationAlert(message: activationMessage, {
            
            /* OK BUTTON PRESSED */
            
            self.changeSupplementaryOffering(selectedOffer: selectedOffer, actionType: true)
            
        })
    }
    
    func updateCollectionViewLayout(isCardLayout :Bool, reloadData :Bool = true) {
        
        if isCardLayout {
            
            switchButton.setImage(UIImage (named: "card_icon"), for: UIControl.State.normal)
            
            DispatchQueue.main.async {
                self.cardLayout.scrollDirection = .horizontal
                self.cardLayout.itemSize = CGSize(width: self.roamingCollectionView.bounds.width - 54 , height:  self.roamingCollectionView.bounds.height)
            }
            
        } else {
            switchButton.setImage(UIImage (named: "list_icon"), for: UIControl.State.normal)
            
            DispatchQueue.main.async {
                self.cardLayout.scrollDirection = .vertical
                self.cardLayout.itemSize = CGSize(width: self.roamingCollectionView.bounds.width - 54 , height:  self.roamingCollectionView.bounds.height - 44)
            }
            
        }
        
        // Resetting selected filter
        selectedFilterOptions = [:]
        
        // Reset search
        layoutSearchTextField(false, reloadData: reloadData)
    }
    
    func layoutSearchTextField(_ showTextField :Bool, reloadData :Bool = true) {
        
        isUserSearch = showTextField
        
        if showTextField {
            titleLabel?.isHidden = true
            searchTextField.isHidden = false
            
            searchTextField.text = ""
            _ = searchTextField.becomeFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search_cross") , for: .normal)
            
        } else {
            titleLabel?.isHidden = false
            searchTextField.isHidden = true
            
            searchTextField.text = ""
            _ = searchTextField.resignFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search") , for: .normal)
            
            if reloadData {
                filterdOffers = selectedRoamingOffers?.offers
                self.reloadRoamingOffer(itemIndex: 0)
            }
        }
        
    }
    
    func reloadRoamingOffer(itemIndex :Int, errorMessage : String = Localized("Message_NoData")) {
        
        
        /*hidding arrows*/
        self.leftArrowButton.isHidden = true
        self.rightArrowButton.isHidden = true
        self.upArrowButton.isHidden = true
        self.downArrowButton.isHidden = true
        collectenViewCurrentIndex = 0
        
        /* Reload tariff items */
        if (filterdOffers?.count ?? 0) > 0 {
            
            if (filterdOffers?.count ?? 0) > 1 {
                if !showCardView {
                    self.downArrowButton.isHidden = false
                } else {
                    self.rightArrowButton.isHidden = false
                }
            }
            
            self.roamingCollectionView.hideDescriptionView()
            
            self.roamingCollectionView.reloadData()
            self.roamingCollectionView.performBatchUpdates(nil) { (isCompleted) in
                if isCompleted {
                    DispatchQueue.main.async {
                        self.roamingCollectionView.scrollToItem(at: IndexPath(item: itemIndex, section: 0), at: self.showCardView ? .centeredHorizontally : .centeredVertically, animated: true)
                    }
                }
            }
            
        } else {
            self.roamingCollectionView.showDescriptionViewWithImage(description: errorMessage,
                                                                    descriptionColor: UIColor.afWhite)
            self.roamingCollectionView.reloadData()
        }
    }
    
    /**
     USED to toggle section.
     
     - returns: Void.
     */
    
    func openSelectedSection() {
        
        let visbleIndexPath = roamingCollectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = roamingCollectionView.cellForItem(at: aIndexPath) as? RoamingOfferCell {
                
                var openSectionIndex : Int? = 0
                
                if let aTariffObject = filterdOffers?[cell.tableView.tag] {
                    
                    let type = typeOfDataInSection(aTariffObject)
                    
                    if selectedSectionViewType == .offerTitleView && type.contains(.offerTitleView) {
                        
                        openSectionIndex = 0
                        
                    } else if selectedSectionViewType == .detail && type.contains(.detail) {
                        
                        openSectionIndex = type.index(of:.detail)
                        
                    }
                }
                
                cell.tableView.closeAllSectionsExcept(openSectionIndex ?? 0, shouldCallDelegate: false)
                
                openSectionAt(index: openSectionIndex ?? 0, tableView: cell.tableView)
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : AFAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
    
    /**
     Call 'changeSupplementaryOffering' API .
     - parameter  selectedOffer : offer user selected to renew or deactivate
     - parameter  actionType : if user want to renew offer then actionType will be true and for deactivation it will be false.
     - returns: Void
     */
    func changeSupplementaryOffering(selectedOffer: SupplementaryOfferItem, actionType :Bool) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.changeSupplementaryOffering(actionType: actionType, offeringId: selectedOffer.header?.offeringId ?? "", offerName: selectedOffer.header?.offerName ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let response = Mapper<MySubscriptionModel>().map(JSONObject: resultData) {
                    
                    /* Show successfull message */
                    self.showSuccessAlertWithMessage(message: response.message)
                    
                    /* Reload data */
                    self.reloadRoamingOffer(itemIndex: 0)
                    
                } else {
                    /* Show error alert to user */
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
}
//MARK: - COLLECTION VIEW Delegates
extension RoamingVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if showCardView {
            return CGSize(width: self.roamingCollectionView.bounds.width - 54 , height:  self.roamingCollectionView.bounds.height)
        } else {
            return CGSize(width: self.roamingCollectionView.bounds.width - 54 , height:  self.roamingCollectionView.bounds.height - 44)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return filterdOffers?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell :RoamingOfferCell = collectionView.dequeueReusableCell(withReuseIdentifier: RoamingOfferCell.cellIdentifier(), for: indexPath) as? RoamingOfferCell,
            let aOfferObject = filterdOffers?[indexPath.item] {
            
            cell.delegate = self
            cell.setTitleLayout(header: aOfferObject.header)
            
            cell.tableView.delegate = self
            cell.tableView.dataSource = self
            
            cell.tableView.tag = indexPath.item
            cell.subscribeButton.tag = indexPath.item
            cell.subscribeButton.addTarget(self, action: #selector(subscribePressed(_:)), for: .touchUpInside)
            
            cell.tableView.reloadData()
            
            return cell
        }
        
        return UICollectionViewCell()
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("scroll did end")
        let visibleRect = CGRect(origin: roamingCollectionView.contentOffset, size:roamingCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = roamingCollectionView.indexPathForItem(at: visiblePoint)
        let currentPage = visibleIndexPath?.item
        /*let pageWidth = scrollView.frame.size.width
        let pageheight = scrollView.frame.size.height
        if showCardView {
            currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        } else {
            currentPage = Int((scrollView.contentOffset.y + pageheight/4) / pageheight)
        }*/
        //print("page index\(currentPage)")
        print("array count\(filterdOffers?.count ?? 0)")
        if (filterdOffers?.count ?? 0) > 1 {
            if currentPage == 0 {
                if !showCardView {
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = false
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = true
                } else {
                    self.rightArrowButton.isHidden = false
                    self.leftArrowButton.isHidden = true
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = true
                }
            } else if currentPage == (filterdOffers?.count ?? 0) - 1 {
                if !showCardView {
                    self.upArrowButton.isHidden = false
                    self.downArrowButton.isHidden = true
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = true
                } else {
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = false
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = true
                }
            } else {
                if !showCardView {
                    self.upArrowButton.isHidden = false
                    self.downArrowButton.isHidden = false
                    self.rightArrowButton.isHidden = true
                    self.leftArrowButton.isHidden = true
                } else {
                    self.rightArrowButton.isHidden = false
                    self.leftArrowButton.isHidden = false
                    self.upArrowButton.isHidden = true
                    self.downArrowButton.isHidden = true
                }
            }
        } else {
            if !showCardView {
                self.upArrowButton.isHidden = true
                self.downArrowButton.isHidden = true
            } else {
                self.rightArrowButton.isHidden = true
                self.leftArrowButton.isHidden = true
            }
        }
        collectenViewCurrentIndex = currentPage ?? 0
    }
}


//MARK: - UITableViewDelegate
extension RoamingVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let aOfferObject = filterdOffers?[tableView.tag]{
            
            return typeOfDataInSection(aOfferObject).count
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 92
        } else {
            return 40
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let aOfferObject = filterdOffers?[tableView.tag]{
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[section]{
            case .offerTitleView:
                if let headerView :RoamingOfferHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    
                    headerView.setTitleLayout(header: aOfferObject.header, selectedCountry: selectedRoamingCountryName, headerType: .offerTitleView, checkZeroPrice: true)
                    
                    return headerView
                }
                break
            case .detail:
                if let headerView :ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    headerView.setViewWithTitle(title: Localized("Title_Detail"),
                                                isSectionSelected: selectedSectionViewType == .detail ? true : false,
                                                headerType: .detail)
                    return headerView
                }
                break
                
            default:
                break
            }
            
        }
        
        return AFAccordionTableViewHeaderView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let aOfferObject = filterdOffers?[tableView.tag]{
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[section]{
            case .offerTitleView:
                return aOfferObject.header?.attributeList?.count ?? 0
                
            case .detail:
                
                return typeOfDataInDetailSection(aOfferObject.details).count
                
            default:
                break
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let aOfferObject = filterdOffers?[tableView.tag] {
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[indexPath.section]{
            case .offerTitleView:
                
                if let cell : SingleAttributeCell = tableView.dequeueReusableCell() {
                    cell.setAttributeHeader(aOfferObject.header?.attributeList?[indexPath.row],
                                            showSeprator: indexPath.row < ((aOfferObject.header?.attributeList?.count ?? 0) - 1))
                    
                    return cell
                }
                
                break
            case .detail:
                let detailsData = typeOfDataInDetailSection(aOfferObject.details)
                switch detailsData[indexPath.row] {
                    
                case .price:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setPriceLayoutValues(price: aOfferObject.details?.price?[indexPath.row],
                                                  showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                    break
                case .rounding:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setRoundingLayoutValues(rounding: aOfferObject.details?.rounding,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .textWithTitle:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithTitleLayoutValues(textWithTitle: aOfferObject.details?.textWithTitle,
                                                          showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithOutTitle:
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aOfferObject.details?.textWithOutTitle,
                                                             showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithPoints:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithPointsLayoutValues(textWithPoint: aOfferObject.details?.textWithPoints,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .date:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aOfferObject.details?.date,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .time:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aOfferObject.details?.time,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .roamingDetails:
                    
                    if let cell:RoamingDetailsCell = tableView.dequeueReusableCell() {
                        cell.setRoamingDetailsLayout(roamingDetails: aOfferObject.details?.roamingDetails,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .freeResourceValidity:
                    
                    if let cell:FreeResourceValidityCell = tableView.dequeueReusableCell() {
                        cell.setFreeResourceValidityValues(freeResourceValidity: aOfferObject.details?.freeResourceValidity,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .titleSubTitleValueAndDesc:
                    
                    if let cell:TitleSubTitleValueAndDescCell = tableView.dequeueReusableCell() {
                        cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aOfferObject.details?.titleSubTitleValueAndDesc,
                                                                      showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                default:
                    break
                }
                break
                
            default:
                break
            }
        }
        
        return UITableViewCell()
    }
}

// MARK: - AFAccordionTableViewDelegate

extension RoamingVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: true)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView,
            selectedSectionViewType != headerView.viewType {
            
            selectedSectionViewType = headerView.viewType
            
        } else if let headerView = header as? RoamingOfferHeaderView,
            selectedSectionViewType != headerView.viewType {
            
            selectedSectionViewType = headerView.viewType
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: false)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            if selectedSectionViewType == headerView.viewType {
                selectedSectionViewType = .offerTitleView
                tableView.toggleSection(0)
                
            }
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}


//MARK: RoamingCellDelegate
extension RoamingVC: RoamingCellDelegate {
    func starTapped(offerId: String) {
        
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            if  let  userSurvey = AFUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offerId}){
                if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == SurveyScreenName.bundle_subscribed.rawValue }){
                    inAppSurveyVC.currentSurvay = survey
                    if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                        inAppSurveyVC.survayQuestion = question
                        if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                            inAppSurveyVC.selectedAnswer = question.answers?[answer]
                        }
                    }
                }
                inAppSurveyVC.survayComment = userSurvey.comments
                inAppSurveyVC.offeringId = offerId
                inAppSurveyVC.offeringType = "2"
            } else {
                if let survey = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.bundle_subscribed.rawValue}) {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.offeringId = offerId
                    inAppSurveyVC.offeringType = "2"
                }
            }
            inAppSurveyVC.callBack =  {
                DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                    self.roamingCollectionView.reloadData()
                }
            }
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
        
    }
    
    
}

//MARK: - Helping functions
extension RoamingVC {
    
    func typeOfDataInSection(_ aOfferItem : SupplementaryOfferItem?) -> [Constants.AFSectionType] {
        
        var dataTypes : [Constants.AFSectionType] = []
        
        // 1
        /* Show offer title view in all cases */
        dataTypes.append(.offerTitleView)
        
        // 2
        if aOfferItem?.details != nil {
            dataTypes.append(.detail)
        }
        
        return dataTypes
    }
    
    func typeOfDataInDetailSection(_ aDetail : Details?) -> [Constants.AFOfferType] {
        
        var dataTypes : [Constants.AFOfferType] = []
        
        if let aDetailObject =  aDetail {
            
            // 1
            if let count = aDetailObject.price?.count {
                
                for _ in 0..<count {
                    dataTypes.append(.price)
                }
            }
            // 2
            if aDetailObject.rounding != nil  {
                
                dataTypes.append(.rounding)
            }
            // 3
            if aDetailObject.textWithTitle != nil {
                
                dataTypes.append(.textWithTitle)
            }
            // 4
            if aDetailObject.textWithOutTitle != nil {
                
                dataTypes.append(.textWithOutTitle)
            }
            // 5
            if aDetailObject.textWithPoints != nil {
                
                dataTypes.append(.textWithPoints)
            }
            // 6
            if aDetailObject.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(.titleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailObject.date != nil {
                
                dataTypes.append(.date)
            }
            // 8
            if aDetailObject.time != nil {
                
                dataTypes.append(.time)
            }
            // 9
            if aDetailObject.roamingDetails != nil {
                
                dataTypes.append(.roamingDetails)
            }
            // 10
            if aDetailObject.freeResourceValidity != nil {
                
                dataTypes.append(.freeResourceValidity)
            }
        }
        return dataTypes
    }
}

//MARK:- UITextFieldDelegate

extension RoamingVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == searchTextField {
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            
            // Filter offers with new name
            filterdOffers = selectedRoamingOffers?.filterOfferBySearchString(searchString: newString)
            
            /* Reload user content */
            self.reloadRoamingOffer(itemIndex: 0, errorMessage: Localized("Message_NothingFoundSearch"))
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
}

//MARK: - UIScrollViewDelegate
extension RoamingVC : UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == roamingCollectionView {
            openSelectedSection()
        }
    }
}



