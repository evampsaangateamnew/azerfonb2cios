//
//  PaygVC.swift
//  Azerfon
//
//  Created by Muhammad Irfan Awan on 29/07/2021.
//  Copyright © 2021 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class PaygVC: BaseVC {
    
    
    @IBOutlet var lblTitle          : UILabel!
    @IBOutlet var descriptionLabel  : UILabel!
    @IBOutlet var paygSwitch        : AFSwitch!
    
    var offeringId: String = ""
    var titleSter: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setViewLayout()
    }
    
    func setViewLayout() {
        titleLabel?.text = titleSter
        lblTitle.text = Localized("Title_PAYG")
        descriptionLabel.text = Localized("Description_PAYG")
        getPAYGstatus()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func switchChanged(mySwitch: AFSwitch) {
        changePaygStatus()
    }
    
    
    
    //MARK: - API's Calls
    /// Call 'getCoreServices' API .
    ///
    /// - returns: Void
    func getPAYGstatus() {
        
        if let responseHandler :PAYGservice = PAYGservice.loadFromUserDefaults(key: APIsType.paygServices.selectedLocalizedAPIKey()) {
            
            // Setting Data in ViewController
            if responseHandler.paygStatus?.lowercased() == "active" {
                self.paygSwitch.setOn(true, animated: true)
            } else {
                self.paygSwitch.setOn(false, animated: true)
            }
            self.offeringId = responseHandler.offeringId
        }

        self.showActivityIndicator()

        _ = AFAPIClient.shared.getPAYGstatus({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    if let responseHandler = Mapper<PAYGservice>().map(JSONObject: resultData) {
                        
                        if responseHandler.paygStatus?.lowercased() == "active" {
                            self.paygSwitch.setOn(true, animated: true)
                        } else {
                            self.paygSwitch.setOn(false, animated: true)
                        }
                        self.offeringId = responseHandler.offeringId
                        
                        //caching coreservices response
                        responseHandler.saveInUserDefaults(key: APIsType.paygServices.selectedLocalizedAPIKey())
                        
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    func changePaygStatus() {
        
        self.showActivityIndicator()
        
        // Number will be empty in this case
        _ = AFAPIClient.shared.processCoreServices(actionType: paygSwitch.isOn, offeringId: self.offeringId, number: "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
                // changing back to first value
                self.paygSwitch.setOn(!self.paygSwitch.isOn, animated: true)
            } else {
                
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Update current status in local storage
                    //offer changed successfully
                    if self.paygSwitch.isOn == true {
                        self.showSuccessAlertWithMessage(message: resultDesc)
                    } else {
                        self.showSuccessAlertWithMessage(message: resultDesc)
                    }
                    
                } else {
                    
                    // Error Alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                    
                    // changing back to first value
                    self.paygSwitch.setOn(!self.paygSwitch.isOn, animated: true)
                }
            }
        })
    }

}

