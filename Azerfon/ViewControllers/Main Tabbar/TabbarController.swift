//
//  TabbarController.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/14/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class TabbarController: UITabBarController {
    
    //MARK: - Properties
    var isLoggedInNow :Bool = false
    var isAppResumeAPICallInProcess :Bool = false
    
    //MARK: - IBOutlet
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadViewContent()
        
        self.tabBar.barTintColor = UIColor.afPurplishBrown
        
        UITabBarItem.appearance().setTitleTextAttributes(
            [NSAttributedString.Key.font :UIFont.b5LightGreyCenter,
             NSAttributedString.Key.foregroundColor :UIColor.afWhite],
            for: .normal)
        
        /*
        UITabBarItem.appearance().setTitleTextAttributes(
            [NSAttributedString.Key.font :UIFont.b5LightGreyCenter,
             NSAttributedString.Key.foregroundColor :UIColor.afSuperPink],
            for: .selected)
         */
        
        
        /* Check if user just loggedIn then don't call AppResume api */
        if isLoggedInNow {
            
            /* Calling AppResume */
            loadCustomerInfo(callResumeAPI: false)
            /* reset check */
            isLoggedInNow = false
            
        } else { /* Check if user is not loggedIn then call AppResume api */
            
            /* Calling AppMenu in case user just loggedIn/ Forgot/ SignUp */
            loadCustomerInfo(callResumeAPI: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /* shouldCallAndUpdateAppResumeData will be true in case use change his current language or migrate tariff */
        if AFUserSession.shared.shouldCallAndUpdateAppResumeData == true {
            
            /* Calling AppResume and load information of new selected language */
            getAppResume()
            
            /* Reset changed language check*/
            AFUserSession.shared.shouldCallAndUpdateAppResumeData = false
            
            /* Update translated content */
            loadViewContent()
        }
    }
    
    //MARK: - IBAction
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.viewControllers?.forEach({ (aController) in
            
            if aController.isKind(of: DashboardVC.classForCoder()){
                aController.tabBarItem.title = Localized("Title_Dashboard")
            } else if aController.isKind(of: TariffVC.classForCoder()) {
                aController.tabBarItem.title = Localized("Title_Tariffs")
            } else if aController.isKind(of: ServicesVC.classForCoder()) {
                aController.tabBarItem.title = Localized("Title_Services")
            } else if aController.isKind(of: TopUpMainVC.classForCoder()) {
                aController.tabBarItem.title = Localized("Title_TopUp")
            } else if aController.isKind(of: InternetMainVC.classForCoder()) {
                aController.tabBarItem.title = Localized("Title_Internet")
            } else if aController.isKind(of: MyAccountVC.classForCoder()) {
                aController.tabBarItem.title = Localized("Title_MyAccount")
            }
        })
    }
    
    func loadCustomerInfo(callResumeAPI :Bool) {
        
        if callResumeAPI {
            /* Calling AppResume */
            getAppResume()
            
        } else {
            /* Calling AppMenu in case user just loggedIn/ Forgot/ SignUp */
            self.getAppMenu()
        }
    }
    
    /**
     Sort AppMenu data and update PIC session and load tabbar options.
     
     - parameter appMenuData: information of AppMenu.
     
     - returns: void.
     */
    func sortAndLoadAppMenu(_ appMenuData: AppMenusModel?, shouldLoadDashboardInfo :Bool) {
        
        /* Setting appMenu information */
        AFUserSession.shared.appMenu = appMenuData
        
        /* Sort AppMenus */
        AFUserSession.shared.appMenu?.menuHorizontal?.sort { $0.sortOrder.toInt < $1.sortOrder.toInt }
        AFUserSession.shared.appMenu?.menuVertical?.sort { $0.sortOrder.toInt < $1.sortOrder.toInt }
        
        /* Load options accourding to response */
        self.setTabBarMenuLayout()
        
        /* Call Dashboard information loading function. */
        if shouldLoadDashboardInfo,
            let dashboardObject = self.getObjectOfViewControllerOfType(Identifier: .dashboard, title: nil) as? DashboardVC {
            
            let hasInfo = dashboardObject.loadDashboardInformation()
            dashboardObject.getDashboardInformation(sender: nil, shouldShowLoader: (hasInfo ? false : true))
        }
    }
    
    /**
     update Tabbar layout setting accourding to new Data.
     
     - returns: void
     */
    func setTabBarMenuLayout() {
        
        var viewControllers: [UIViewController] = []
        
        // Loop throught each item in new tabbar data
        AFUserSession.shared.appMenu?.menuHorizontal?.forEach { (aMenuListItem) in
            
            // Getting object for View controller in tabBar against identifier
            if let aIdentifier = Constants.HorizontalMenus(rawValue: aMenuListItem.identifier),
                let aViewControllerObject = self.getObjectOfViewControllerOfType(Identifier: aIdentifier, title: aMenuListItem.title) {
                
                viewControllers.append(aViewControllerObject)
            }
        }
        
        /* IN CASE there is no information exist create Dashboard object. */
        if viewControllers.count <= 0 {
            
            // Get instance first add into tabbar
            if let aViewController : UIViewController = getObjectOfViewControllerOfType(Identifier: .dashboard, title: nil) {
                
                viewControllers.append(aViewController)
            } else {
                
                // if no instance found then create and add Dashboard object
                if let aViewController :DashboardVC = DashboardVC.instantiateViewControllerFromStoryboard() {
                    aViewController.tabBarItem = UITabBarItem(title: Localized("Title_Dashboard"), image: UIImage.imageFor(name:  "dashboardUnSelected_icon"), selectedImage: UIImage.imageFor(name:  "dashboardSelected_icon"))
                    viewControllers.append(aViewController)
                }
            }
        }
        // Replace tabbar controllers with new controller.
        self.setViewControllers(viewControllers, animated: true)
    }
    
    /**
     Get or Create object against identifier and return UIViewController object.
     
     - parameter identifier: ViewController identifier.
     
     - returns: UIViewController object againt identifier.
     */
    func getObjectOfViewControllerOfType(Identifier identifier: Constants.HorizontalMenus, title :String?) -> UIViewController? {
        
        var aViewController : UIViewController?
        
        // Find viewController against identifier from tabbar and
        self.viewControllers?.forEach({ (aController) in
            
            if aController.isKind(of: DashboardVC.classForCoder()) && identifier == .dashboard {
                aViewController = aController
                return
            } else if aController.isKind(of: TariffVC.classForCoder()) && identifier == .tariffs {
                aViewController = aController
                return
            } else if aController.isKind(of: ServicesVC.classForCoder()) && identifier == .services {
                aViewController = aController
                return
            } else if aController.isKind(of: TopUpMainVC.classForCoder()) && identifier == .topup {
                aViewController = aController
                return
            } else if aController.isKind(of: InternetMainVC.classForCoder()) && identifier == .internet {
                aViewController = aController
                return
            } else if aController.isKind(of: MyAccountVC.classForCoder()) && identifier == .myaccount {
                aViewController = aController
                return
            }
        })
        
        /* If not able to find a viewController and can Initialize then create a new ViewController object and add */
        if aViewController == nil {
            
            aViewController = self.createInstanceOfViewControllerFor(Identifier: identifier, title: title)
        
        } else {
           aViewController?.tabBarItem = createTabBarItem(forIdentifier: identifier, title: title)
        }
        
        return aViewController
    }
    
    /**
     Create object against identifier and return UIViewController object.
     
     - parameter identifier: ViewController identifier.
     
     - returns: create UIViewController object againt identifier.
     */
    func createInstanceOfViewControllerFor(Identifier identifier : Constants.HorizontalMenus, title :String?) -> UIViewController? {
        
        if identifier == .dashboard,
            let aViewController :DashboardVC = DashboardVC.instantiateViewControllerFromStoryboard() {
            
            aViewController.tabBarItem = createTabBarItem(forIdentifier: identifier, title: title)
            return aViewController
            
        } else if identifier == .tariffs,
            let aViewController :TariffVC = TariffVC.instantiateViewControllerFromStoryboard() {
            
            aViewController.tabBarItem = createTabBarItem(forIdentifier: identifier, title: title)
            return aViewController
            
        } else if identifier == .services,
            let aViewController :ServicesVC = ServicesVC.instantiateViewControllerFromStoryboard() {
            
            aViewController.tabBarItem = createTabBarItem(forIdentifier: identifier, title: title)
            return aViewController
            
        } else if identifier == .topup,
            let aViewController :TopUpMainVC = TopUpMainVC.instantiateViewControllerFromStoryboard() {
            aViewController.tabBarItem = createTabBarItem(forIdentifier: identifier, title: title)
            return aViewController
            
        } else if identifier == .internet,
                  let aViewController :InternetMainVC = InternetMainVC.instantiateViewControllerFromStoryboard() {
                  aViewController.tabBarItem = createTabBarItem(forIdentifier: identifier, title: title)
                  return aViewController
                  
              } else if identifier == .myaccount,
            let aViewController :MyAccountVC = MyAccountVC.instantiateViewControllerFromStoryboard() {
            
            aViewController.tabBarItem = createTabBarItem(forIdentifier: identifier, title: title)
            return aViewController
        }
        
        return nil
    }
    
    
    func createTabBarItem(forIdentifier identifier:Constants.HorizontalMenus, title :String?) -> UITabBarItem {
        
        var itemTitle = ""
        var itemIconName = ""
        var selectedItemIcon = ""
        
        switch identifier {
        case .myaccount:
            itemTitle = title ?? Localized("Title_MyAccount")
            itemIconName = "myaccountUnSelected_icon"
            selectedItemIcon = "myaccountSelected_icon"
            break
            
        case .services:
            itemTitle = title ?? Localized("Title_Services")
            
            let showSpecialIconForServices = AFUserSession.shared.userInfo?.specialOffersTariffData?.specialOffersMenu?.isEqual("show", ignorCase: true) ?? false
            
            itemIconName = showSpecialIconForServices ? "sevicesNewOffer":"servicesUnSelected_icon"
            selectedItemIcon = showSpecialIconForServices ? "servicesNewOfferSelected":"servicesSelected_icon"
            break
            
        case .tariffs:
            
            itemTitle = title ?? Localized("Title_Tariffs")
            
            let showSpecialIconForTariff = AFUserSession.shared.userInfo?.specialOffersTariffData?.specialTariffMenu?.isEqual("show", ignorCase: true) ?? false
            
            itemIconName = showSpecialIconForTariff ? "tariffsNewOffer":"tariffsUnSelected_icon"
            selectedItemIcon = showSpecialIconForTariff ? "tariffsNewOfferSelected":"tariffsSelected_icon"
            break
            
        case .topup:
            itemTitle = title ?? Localized("Title_TopUp")
            itemIconName = "topupUnSelected_icon"
            selectedItemIcon = "topupSelected_icon"
            break
            
        case .internet:
            itemTitle = title ?? Localized("Title_Internet")
            itemIconName = "internet_unselected"
            selectedItemIcon = "internet_selected"
            break
            
        default:
            
            itemTitle = title ?? Localized("Title_Dashboard")
            itemIconName = "dashboardUnSelected_icon"
            selectedItemIcon = "dashboardSelected_icon"
            break
        }
        
        let item = UITabBarItem(title: itemTitle,
                                image: UIImage.imageFor(name: itemIconName),
                                selectedImage: UIImage.imageFor(name: selectedItemIcon))
        
        return item
        
    }
    
    
    //MARK: - API's Calls
    /**
     call to get appResume data.
     
     - returns: void.
     */
    func getAppResume() {
        
        self.isAppResumeAPICallInProcess = true
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.appResume({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.isAppResumeAPICallInProcess = false
            
            self.hideActivityIndicator()
            
            if error != nil {
                
                if AFUserSession.shared.loadUserInfomation() {
                    // getting AppMenu Detail
                    self.getAppMenu()
                    
                } else {
                    error?.showServerErrorInViewController(self)
                }
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let appResumeHandler = Mapper<LoginModel>().map(JSONObject: resultData) {
                        
                        /* Update AppResume information in User Defaults */
                        AFUserInfoUtilities.updateLoggedInUserInfo(loggedInUserMSISDN: AFUserSession.shared.msisdn,
                                                                   loginInfo: appResumeHandler,
                                                                   appConfig: AFUserSession.shared.appConfig)
                    }
                    
                    // getting AppMenu Detail
                    self.getAppMenu()
                    
                } else if resultCode != Constants.AFAPIStatusCode.sessionExpired.rawValue &&
                    AFUserSession.shared.loadUserInfomation() == true {
                    // getting AppMenu Detail
                    self.getAppMenu()
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
        })
    }
    
    /**
     call to get AppMenu data.
     
     - returns: void.
     */
    func getAppMenu() {
        
        var foundAppMenuData : Bool = false
        
        /* Loading initial data of menu from UserDefaults */
        if let appMenuHandler :AppMenusResponseModel = AppMenusResponseModel.loadFromUserDefaults(key: APIsType.appMenu.selectedAPIKey()),
            let currentMenu = appMenuHandler.getMenuForCurrentLanguage(AFLanguageManager.userSelectedLanguage()){
            
            /* If found information in userDefaults then hide loader and call in background and load. */
            if (currentMenu.menuVertical?.count ?? 0) <= 0 &&
                (currentMenu.menuHorizontal?.count ?? 0) <= 0 {
                
                foundAppMenuData = false
            } else {
                foundAppMenuData = true
                
                /* load appMenu information */
                self.sortAndLoadAppMenu(currentMenu, shouldLoadDashboardInfo: true)
            }
        }
        
        /* If found info then don't show loader for menu API*/
        if foundAppMenuData == false {
            self.showActivityIndicator()
        }
        
        /* API call to get Menu data */
        _ = AFAPIClient.shared.getAppMenu({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if foundAppMenuData != true {
                self.hideActivityIndicator()
            }
            
            if error != nil  {
                /* If error occur Durring AppMenu API call then show error */
                error?.showServerErrorInViewController(self)
                
                if foundAppMenuData == false {
                    /* clear App Menu and load dashboard info */
                    self.sortAndLoadAppMenu(nil, shouldLoadDashboardInfo: true)
                }
                
            } else if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                let appMenuHandler = Mapper<AppMenusResponseModel>().map(JSONObject: resultData) ,
                let currentMenu = appMenuHandler.getMenuForCurrentLanguage(AFLanguageManager.userSelectedLanguage()) { /* handling data from API response. */
                
                /*Save data into user defaults*/
                appMenuHandler.saveInUserDefaults(key: APIsType.appMenu.selectedAPIKey())
                
                /* load appMenu information and call Load Dashboard, if Menu data loaded from defaults then don't call loadDashboard API again */
                self.sortAndLoadAppMenu(currentMenu, shouldLoadDashboardInfo: foundAppMenuData ? false : true)
                
            } else { /* If error occur Durring AppMenu API call then show error. */
                self.showErrorAlertWithMessage(message: resultDesc)
                
                if foundAppMenuData == false {
                    /* clear App Menu and load dashboard info */
                    self.sortAndLoadAppMenu(nil, shouldLoadDashboardInfo: true)
                }
            }
            self.getSurveys()
        })
    }
 
    
    
    /**
     call to get Survey data.
     
     - returns: void.
     */
    
    func getSurveys() {
        
        self.showActivityIndicator()
        _ = AFAPIClient.shared.getSurveys({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue ,
                    let homePageResponse = Mapper<InAppSurveyMapper>().map(JSONObject:resultData) {
                    AFUserSession.shared.appSurvays = homePageResponse
                } else {
                    //EventLog
//                    AFAnalyticsManager.logEvent(screenName: "Login Screen", contentType:"User failed to Login" , status:"Failed" )
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}
