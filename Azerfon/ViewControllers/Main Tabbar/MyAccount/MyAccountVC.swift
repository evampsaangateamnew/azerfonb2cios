//
//  MyAccountVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class MyAccountVC: BaseVC {

    //MARK: - Property
    var myAccountOptionsList : [Items] = []
    var titleValue :String = Localized("Title_MyAccount")
    
    //MARK: - IBOutlet
    @IBOutlet var myAccountTableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        //getting services from session and adding into local array and reloading data
        AFUserSession.shared.appMenu?.menuHorizontal?.forEach({ (aHorizontalMenu) in
            if aHorizontalMenu.identifier == Constants.HorizontalMenus.myaccount.rawValue {
                myAccountOptionsList = aHorizontalMenu.items ?? []
                
                self.titleValue = aHorizontalMenu.title.isBlank ? Localized("Title_MyAccount") : aHorizontalMenu.title
            }
        })
        myAccountOptionsList.sort{ $0.sortOrder.toInt < $1.sortOrder.toInt }
        
        loadViewContent()
        
        myAccountTableView.reloadUserData(myAccountOptionsList)
    }
    
    //MARK: - IBAction
    
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        ServicesCell.registerReusableCell(with: myAccountTableView)
        
        self.titleLabel?.text = titleValue
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier : String) {
        
        switch identifier {
            
        case "myaccount_usage_history":
            // Load UsageHistoryMainVC VC
            if let usageHistoryMainVC :UsageHistoryMainVC = UsageHistoryMainVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(usageHistoryMainVC, animated: true)
            }
            break
            
        case "myaccount_operations_history":
            // Load OperationsHistory VC
            if let operationHistoryVC :OperationHistoryVC = OperationHistoryVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(operationHistoryVC, animated: true)
            }
            break
            
        case "myaccount_my_subscriptions":
            // Load mySubscriptions VC
            if let mySubscriptionsVC :MySubscriptionsVC = MySubscriptionsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(mySubscriptionsVC, animated: true)
            }
            break
            
        default:
            break
        }
    }

}

//MARK: - TableView Delegates
extension MyAccountVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myAccountOptionsList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let myCell: ServicesCell = tableView.dequeueReusableCell() {
            myCell.setItemInfo(aItem: myAccountOptionsList[indexPath.row])
            return myCell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Redirect user to selected screen
        redirectUserToSelectedController(Identifier: myAccountOptionsList[indexPath.row].identifier)
    }
}
