//
//  OperationHistoryCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class OperationHistoryCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var endingBalanceTitleLabel    : UILabel!
    @IBOutlet var endingBalanceLabel         : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.endingBalanceTitleLabel.text = Localized("Title_EndingBalance")
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /**
     Sets Item labels values.
     
     - parameter aItem: requests data.
     
     - returns: void.
     */
    func setItemInfo(aItem: OperationsRecord) {
        endingBalanceLabel.attributedText = aItem.endingBalance.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey, mainStringFont: UIFont.b4LightGrey, imageSize: CGSize(width: 8, height: 5))
    }
}
