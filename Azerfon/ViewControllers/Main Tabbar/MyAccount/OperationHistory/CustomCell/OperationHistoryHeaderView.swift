//
//  OperationHistoryHeaderView.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class OperationHistoryHeaderView: AFAccordionTableViewHeaderView {

    //MARK: - IBOutlet
    @IBOutlet var dateTimeLabel              : UILabel!
    @IBOutlet var typeLabel                  : UILabel!
    @IBOutlet var descriptionLabel           : UILabel!
    @IBOutlet var amountLabel                : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /**
     Sets Item labels values.
     
     - parameter aItem: requests data.
     
     - returns: void.
     */
    func setItemInfo(aItem: OperationsRecord) {
        dateTimeLabel.text = aItem.date
        typeLabel.text = aItem.transactionType
        descriptionLabel.text = aItem.description
        amountLabel.attributedText = aItem.amount.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey, mainStringFont: UIFont.b4LightGrey, imageSize: CGSize(width: 8, height: 5))
    }
    
}
