//
//  OperationHistoryVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class OperationHistoryVC: BaseVC {
    
    //MARK: - Properties
    var isStartDate : Bool?
    var operationsHistoryData : [OperationsRecord]?
    var filteredOperationsData : [OperationsRecord]?
    var loanHistoryResponse : LoanHistoryListHandler?
    var storeLocator : StoreLocatorModel?
    var transactionSelectedType : String? = Localized("DropDown_All")
    
    //MARK: - IBOutlet
    @IBOutlet var dateTimeTitleLabel              : AFMarqueeLabel!
    @IBOutlet var typeTitleLabel                  : AFMarqueeLabel!
    @IBOutlet var descriptionTitleLabel           : AFMarqueeLabel!
    @IBOutlet var amountTitleLabel                : AFMarqueeLabel!
    @IBOutlet var periodDropDownTitleLabel        : AFLabel!
    @IBOutlet var categoryDropDownTitleLabel      : AFLabel!
    @IBOutlet var startDateLabel                  : AFLabel!
    @IBOutlet var endDateLabel                    : AFLabel!
    
    @IBOutlet var historyTableView      : AFAccordionTableView!
    
    @IBOutlet var constDatePickerViewHeight : NSLayoutConstraint!
    
    @IBOutlet var categoryDropDown: UIDropDown!
    @IBOutlet var periodDropDown: UIDropDown!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.constDatePickerViewHeight.constant = 0
        self.dateDropDownChanged(transacitonIndex: 0)
        
        //init tableview
        historyTableView.hideDefaultSeprator()
        historyTableView.allowMultipleSectionsOpen = true
        historyTableView.allowsSelection = false
        historyTableView.delegate = self
        historyTableView.dataSource = self
        historyTableView.estimatedRowHeight = 100
        
        self.historyTableView.reloadUserData(filteredOperationsData)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        //init dropdown
        setDropDown()
    }
    
    //MARK: - IBAction
    @IBAction func startButtonAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        let threeMonthsAgos = Date().todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLabel.text ,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: AFUserSession.shared.dateFormatter.createDate(from: endDateString, dateFormate: Constants.kDisplayFormat) ?? todayDate,
                                currentDate: currentDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate, currentDate: currentDate)
        }
        
    }
    
    @IBAction func endButtonAction(_ sender: UIButton) {
        
        isStartDate = false;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        self.showDatePicker(minDate: AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate,
                            maxDate: todayDate,
                            currentDate: currentDate)
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        self.titleLabel?.text = Localized("Title_OperationsHistory")
        self.dateTimeTitleLabel?.text = Localized("Title_DateTime")
        self.typeTitleLabel?.text = Localized("Title_Type")
        self.descriptionTitleLabel?.text = Localized("Title_Description")
        self.amountTitleLabel?.text = Localized("Title_Amount")
        self.periodDropDownTitleLabel?.text = Localized("Title_SelectPeriod")
        self.categoryDropDownTitleLabel?.text = Localized("Title_SelectCategory")
    }
    
    /*Initialze - Setup dropdown */
    func setDropDown(){
        
        //Set Period Drop Down
        periodDropDown.placeholder = Constants.dateDropDownOptions.first
        periodDropDown.dataSource = Constants.dateDropDownOptions
        periodDropDown.didSelectOption { (index, option) in
            self.dateDropDownChanged( transacitonIndex: index)
        }
        
        //Set category Drop Down
        let categoriesTitles : [String] = [Localized("DropDown_All"),Localized("DropDown_DataBundles"), Localized("DropDown_SMSBundles"), Localized("DropDown_Duplicate"), Localized("DropDown_MRC"), Localized("DropDown_Payment"),Localized("DropDown_Adjustment"),Localized("DropDown_Content(CP)"),Localized("DropDown_Installment"),Localized("DropDown_MoneyTransfer"),Localized("DropDown_ChangeOwnership"),Localized("DropDown_ChangeCustomerType"),Localized("DropDown_Others")]
        categoryDropDown.text = Localized("DropDown_All")
        
        categoryDropDown.dataSource = categoriesTitles
        
        categoryDropDown.didSelectOption { (index, option) in
            self.transactionSelectedType = option
            
            self.filteredOperationsData = self.filterHistoryBy(type: self.transactionSelectedType, operationRecords: self.operationsHistoryData, allTypes: self.categoryDropDown.dataSource)
            
            // Reload tableview data
            self.historyTableView.reloadUserData(self.filteredOperationsData)
        }
    }
    
    /**
     Date dropdown changed.
     
     - parameter transacitonIndex: Selected drpodown index.
     
     - returns: void
     */
    func dateDropDownChanged( transacitonIndex: Int? , showNote: Bool = false)  {
        
        let todayDateString = Date().todayDateString(dateFormate: Constants.kNewAPIFormat)
        switch transacitonIndex{
        case 0:
            print("default")
            self.setDatePickerHeight(constantValue: 0)
            
            self.operationsHistoryApiCall(StartDate: todayDateString, EndDate: todayDateString) { (canShow) in
                
                if canShow &&
                    DisclaimerMessageVC.canShowDisclaimerOfType(.operationHistory) == true,
                    let disclaimerMessageVC :DisclaimerMessageVC = DisclaimerMessageVC.instantiateViewControllerFromStoryboard() {
                    
                    disclaimerMessageVC.setDisclaimerAlertWith(description: Localized("Disclaimer_Description"), okBtnClickedBlock: { (dontShowAgain) in
                        
                        if dontShowAgain == true {
                            DisclaimerMessageVC.setDisclaimerStatusForType(.operationHistory, canShow: false)
                        }
                    })
                    
                    self.presentPOPUP(disclaimerMessageVC, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
                    
                }
            }
        case 1:
            print("Last 7 days")
            self.setDatePickerHeight(constantValue: 0)
            
            // Get data of previous seven days
            let sevenDaysAgo = Date().todayDateString(withAdditionalValue: -7)
            self.operationsHistoryApiCall(StartDate: sevenDaysAgo, EndDate: todayDateString)
            
        case 2:
            print("Last 30 days")
            self.setDatePickerHeight(constantValue: 0)
            
            // Get data of previous thirty days
            let thirtyDaysAgo = Date().todayDateString(withAdditionalValue: -30)
            self.operationsHistoryApiCall(StartDate: thirtyDaysAgo, EndDate: todayDateString)
            
        case 3:
            print("Previous month")
            self.setDatePickerHeight(constantValue: 0)
            
            // Getting previous month start and end date
            let startOfPreviousMonth = Date().todayDate().getPreviousMonth()?.startOfMonth() ?? Date().todayDate()
            let endOfPreviousMonth = Date().todayDate().getPreviousMonth()?.endOfMonth() ?? Date().todayDate()
            
            let startDateString = AFUserSession.shared.dateFormatter.createString(from: startOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
            let endDateString = AFUserSession.shared.dateFormatter.createString(from: endOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
            
            self.operationsHistoryApiCall(StartDate: startDateString, EndDate: endDateString)
            
        case 4:
            self.setDatePickerHeight(constantValue: 47)
            
            //For date formate
            self.startDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            self.endDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            
            self.startDateLabel.textColor = UIColor.afBrownGrey
            self.endDateLabel.textColor = UIColor.afBrownGrey
            
            self.operationsHistoryApiCall(StartDate: todayDateString, EndDate: todayDateString)
            
            print("Selected Date")
            
        default:
            print("destructive")
            
        }
    }
    
    func setDatePickerHeight(constantValue : CGFloat) {
        
        if self.constDatePickerViewHeight.constant !=  constantValue {
            self.constDatePickerViewHeight.constant = constantValue
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    /**
     Show date picker.
     
     - parameter minDate: minimum date of picker.
     - parameter maxDate: maximum date of picker.
     - parameter currentDate: current date of picker.
     
     - returns: void
     */
    func showDatePicker(minDate : Date , maxDate : Date, currentDate : Date){
        self.showNewDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate) { (selectedDate) in
            let todayDate = Date().todayDate(dateFormate: Constants.kDisplayFormat)
            
            let selectedDateString = AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kDisplayFormat)
            
            let startFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            let endFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            
            
            if self.isStartDate ?? true {
                self.startDateLabel.text = selectedDateString
                self.startDateLabel.textColor = UIColor.afDateOrange
                self.operationsHistoryApiCall(StartDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat),
                                              EndDate: AFUserSession.shared.dateFormatter.createString(from:endFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat))
            } else {
                self.endDateLabel.text = selectedDateString
                self.endDateLabel.textColor = UIColor.afDateOrange
                self.operationsHistoryApiCall(StartDate: AFUserSession.shared.dateFormatter.createString(from: startFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat),
                                              EndDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat))
            }
        }
    }
    
    /**
     Filters Operational History.
     
     - parameter type: Type for which you want to filter.
     - parameter operationRecords: All operationalhistory record.
     - parameter allTypes: All Types.Arrary of strings.
     
     - returns: Array of OperationsRecord.
     */
    func filterHistoryBy(type: String?, operationRecords: [OperationsRecord]?, allTypes: [String]? ) -> [OperationsRecord]? {
        
        guard let myType = type else {
            return nil
        }
        
        if myType == Localized("DropDown_All"){
            return operationRecords
            
        } else if myType == Localized("DropDown_Others"){
            
            // Get all recodes which does not mach any type
            let  filterdHistoryDetailsWhichDoesNotMatchType = operationRecords?.filter() {
                
                let aRecordType = ($0 as OperationsRecord).transactionType.lowercased()
                
                if (allTypes?.contains(where: { $0.lowercased() == aRecordType })) == false {
                    return true
                } else {
                    return false
                }
            }
            
            // Get Data of selected type "others"
            let  filterdHistoryDetailsForOthers = operationRecords?.filter() {
                
                if ($0 as OperationsRecord).transactionType.lowercased() == myType.lowercased() {
                    
                    return true
                } else {
                    return false
                }
            }
            
            // Combine both filtered data of "others" type and type does not match
            var filteredData = filterdHistoryDetailsForOthers
            filteredData?.append(contentsOf: filterdHistoryDetailsWhichDoesNotMatchType ?? [])
            
            return filteredData
            
        } else {
            
            let  filterdHistoryDetails = operationRecords?.filter() {
                
                if ($0 as OperationsRecord).transactionType.lowercased() == myType.lowercased() {
                    
                    return true
                } else {
                    return false
                }
            }
            return filterdHistoryDetails
        }
        
    }
    
    //MARK: - API Calls
    /// Call 'operationsHistoryApiCall' API.
    ///
    /// - returns: Void
    func operationsHistoryApiCall(StartDate startDate : String, EndDate endDate: String, completionHandler: @escaping (Bool) -> Void = {_ in }) {
        
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.getOperationsHistory(StartDate: startDate, EndDate: endDate, { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                completionHandler(false)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let opsHistoryResponse = Mapper<OperationsHistoryHandler>().map(JSONObject:resultData) {
                    
                    self.operationsHistoryData = opsHistoryResponse.records
                    self.filteredOperationsData = self.filterHistoryBy(type: self.transactionSelectedType, operationRecords: self.operationsHistoryData, allTypes: self.categoryDropDown.dataSource)
                    completionHandler(true)
                    
                } else {
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                    completionHandler(false)
                }
            }
            
            // Reload tableview data
            self.historyTableView.reloadUserData(self.filteredOperationsData)
        })
        
    }
}

//MARK: - TableView Delegates
extension OperationHistoryVC : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.filteredOperationsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView :OperationHistoryHeaderView = tableView.dequeueReusableHeaderFooterView() {
            
            headerView.setItemInfo(aItem: (filteredOperationsData?[section])!)
            
            return headerView
        }
        return AFAccordionTableViewHeaderView()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :OperationHistoryCell = tableView.dequeueReusableCell() {
            
            cell.setItemInfo(aItem: (filteredOperationsData?[indexPath.section])!)
            
            return cell
        }
        
        return UITableViewCell()
    }
}

extension OperationHistoryVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        filteredOperationsData?[section].isSectionExpanded = true
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        filteredOperationsData?[section].isSectionExpanded = false
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}
