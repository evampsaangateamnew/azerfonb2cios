//
//  UsageHistorySummaryVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class UsageHistorySummaryVC: BaseVC {
    
    //MARK: - Properties
    var isStartDate          : Bool?
    var usageSummaryResponse : UsageSummaryHistoryHandler?
    
    //MARK: - IBOutlet
    @IBOutlet var dropDown: UIDropDown!
    
    @IBOutlet var serviceTitleLabel      : AFMarqueeLabel!
    @IBOutlet var usageTitleLabel        : AFMarqueeLabel!
    @IBOutlet var chargedTitleLabel      : AFMarqueeLabel!
    @IBOutlet var dropdownTitleLabel     : AFLabel!
    @IBOutlet var startDateLabel         : AFLabel!
    @IBOutlet var endDateLabel           : AFLabel!
    
    @IBOutlet var summaryTableView : UITableView!
    
    @IBOutlet var constDatePickerViewHeight : NSLayoutConstraint!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.summaryTableView.reloadData()
        
        self.constDatePickerViewHeight.constant = 0
        self.dateDropDownChanged(transacitonIndex: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        //init dropdown
        setDropDown()
    }
    
    //MARK: - IBAction
    @IBAction func startButtonAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        let threeMonthsAgos = Date().todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLabel.text ,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: AFUserSession.shared.dateFormatter.createDate(from: endDateString, dateFormate: Constants.kDisplayFormat) ?? todayDate,
                                currentDate: currentDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate, currentDate: currentDate)
        }
        
    }
    
    @IBAction func endButtonAction(_ sender: UIButton) {
        
        isStartDate = false;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        self.showDatePicker(minDate: AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate,
                            maxDate: todayDate,
                            currentDate: currentDate)
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        self.serviceTitleLabel?.text = Localized("Title_Service")
        self.usageTitleLabel?.text = Localized("Title_Usage")
        self.chargedTitleLabel?.text = Localized("Title_Charged")
        self.dropdownTitleLabel?.text = Localized("Title_SelectPeriod")
    }
    
    /*Initialze - Setup dropdown */
    func setDropDown(){
        
        //Date Drop Down
        self.dropDown.placeholder = Constants.dateDropDownOptions.first
        
        self.dropDown.dataSource = Constants.dateDropDownOptions
        self.dropDown.didSelectOption { (index, option) in
            self.dateDropDownChanged( transacitonIndex: index)
        }
    }
    
    /**
     Date dropdown changed.
     
     - parameter transacitonIndex: Selected drpodown index.
     
     - returns: void
     */
    func dateDropDownChanged( transacitonIndex: Int?)  {
        
        let todayDateString = Date().todayDateString(dateFormate: Constants.kNewAPIFormat)
        switch transacitonIndex{
        case 0:
            print("default")
            self.setDatePickerHeight(constantValue: 0)
            
            self.getUsageSummaryHistory(startDate: todayDateString, endDate: todayDateString) { (canShow) in
                
                if canShow &&
                    DisclaimerMessageVC.canShowDisclaimerOfType(.usageHistory) == true,
                    let disclaimerMessageVC :DisclaimerMessageVC = DisclaimerMessageVC.instantiateViewControllerFromStoryboard() {
                    
                    disclaimerMessageVC.setDisclaimerAlertWith(description: Localized("Disclaimer_Description"), okBtnClickedBlock: { (dontShowAgain) in
                        
                        if dontShowAgain == true {
                            DisclaimerMessageVC.setDisclaimerStatusForType(.usageHistory, canShow: false)
                        }
                    })
                    
                    self.presentPOPUP(disclaimerMessageVC, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
                    
                }
            }
        case 1:
            print("Last 7 days")
            self.setDatePickerHeight(constantValue: 0)
            
            // Get data of previous seven days
            let sevenDaysAgo = Date().todayDateString(withAdditionalValue: -7)
            self.getUsageSummaryHistory(startDate: sevenDaysAgo, endDate: todayDateString)
            
        case 2:
            print("Last 30 days")
            self.setDatePickerHeight(constantValue: 0)
            
            // Get data of previous thirty days
            let thirtyDaysAgo = Date().todayDateString(withAdditionalValue: -30)
            self.getUsageSummaryHistory(startDate: thirtyDaysAgo, endDate: todayDateString)
            
        case 3:
            print("Previous month")
            self.setDatePickerHeight(constantValue: 0)
            
            // Getting previous month start and end date
            let startOfPreviousMonth = Date().todayDate().getPreviousMonth()?.startOfMonth() ?? Date().todayDate()
            let endOfPreviousMonth = Date().todayDate().getPreviousMonth()?.endOfMonth() ?? Date().todayDate()
            
            let startDateString = AFUserSession.shared.dateFormatter.createString(from: startOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
            let endDateString = AFUserSession.shared.dateFormatter.createString(from: endOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
            
            self.getUsageSummaryHistory(startDate: startDateString, endDate: endDateString)
            
        case 4:
            self.setDatePickerHeight(constantValue: 47)
            
            //For date formate
            self.startDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            self.endDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            
            self.startDateLabel.textColor = UIColor.afBrownGrey
            self.endDateLabel.textColor = UIColor.afBrownGrey
            
            self.getUsageSummaryHistory(startDate: todayDateString, endDate: todayDateString)
            
            print("Selected Date")
            
        default:
            print("destructive")
            
        }
    }
    
    func setDatePickerHeight(constantValue : CGFloat) {
        
        if self.constDatePickerViewHeight.constant !=  constantValue {
            self.constDatePickerViewHeight.constant = constantValue
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    /**
     Show date picker.
     
     - parameter minDate: minimum date of picker.
     - parameter maxDate: maximum date of picker.
     - parameter currentDate: current date of picker.
     
     - returns: void
     */
    func showDatePicker(minDate : Date , maxDate : Date, currentDate : Date){
        self.showNewDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate) { (selectedDate) in
            let todayDate = Date().todayDate(dateFormate: Constants.kDisplayFormat)
            
            let selectedDateString = AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kDisplayFormat)
            
            let startFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            let endFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            
            
            if self.isStartDate ?? true {
                self.startDateLabel.text = selectedDateString
                self.startDateLabel.textColor = UIColor.afDateOrange
                self.getUsageSummaryHistory(startDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat),
                                            endDate: AFUserSession.shared.dateFormatter.createString(from:endFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat))
            } else {
                self.endDateLabel.text = selectedDateString
                self.endDateLabel.textColor = UIColor.afDateOrange
                self.getUsageSummaryHistory(startDate: AFUserSession.shared.dateFormatter.createString(from: startFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat),
                                            endDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat))
            }
        }
    }
    
    //MARK: - API Calls
    
    /**
     Call 'getUsageSummaryHistory' API .
     
     - parameter startDate: Start date.
     - parameter startDate: End date.
     
     
     - returns: SummaryHistory
     */
    func getUsageSummaryHistory(startDate:String?, endDate:String?, completionHandler: @escaping (Bool) -> Void = {_ in }) {
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.getUsageSummaryHistory(StartDate: startDate ?? "", EndDate: endDate ?? "",{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                completionHandler(false)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let usageHistoryList = Mapper<UsageSummaryHistoryHandler>().map(JSONObject: resultData) {
                    
                    self.usageSummaryResponse = usageHistoryList
                    completionHandler(true)
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                    completionHandler(false)
                }
            }
            
            self.summaryTableView.reloadUserData(self.usageSummaryResponse?.summaryList)
        })
    }
    
}


// MARK: - <UITableViewDataSource> / <UITableViewDelegate>
extension UsageHistorySummaryVC :UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.usageSummaryResponse?.summaryList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView :UsageSummaryHeaderView = tableView.dequeueReusableHeaderFooterView() {
            
            headerView.setTileLabelText(self.usageSummaryResponse?.summaryList?[section])
            
            return headerView
        }
        return AFAccordionTableViewHeaderView()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usageSummaryResponse?.summaryList?[section].records?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :UsageSummaryCell = tableView.dequeueReusableCell() {
            
            cell.setLayoutFor(self.usageSummaryResponse?.summaryList?[indexPath.section].records?[indexPath.row])
            
            return cell
        }
        
        return UITableViewCell()
    }
}


// MARK: - <AFAccordionTableViewDelegate>

extension UsageHistorySummaryVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? UsageSummaryHeaderView {
            headerView.setExpandStatus(true)
            
            self.usageSummaryResponse?.summaryList?[section].isSectionExpanded = true
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? UsageSummaryHeaderView {
            
            headerView.setExpandStatus(false)
            
            self.usageSummaryResponse?.summaryList?[section].isSectionExpanded = false
            
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}
