//
//  UsageSummaryHeaderView.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class UsageSummaryHeaderView: AFAccordionTableViewHeaderView {

    //MARK: IBOutlet
    @IBOutlet var serviceLabel      : AFLabel!
    @IBOutlet var usageLabel        : AFLabel!
    @IBOutlet var chargedLabel      : AFLabel!
    
    @IBOutlet var expandCellImage   : UIImageView!
    
    //MARK: - Functions
    func setTileLabelText(_ aItem: SummaryList?) {
        if let aSummaryItem = aItem {
            
            serviceLabel.text = aSummaryItem.name
            usageLabel.text = aSummaryItem.totalUsage
            chargedLabel.attributedText = aSummaryItem.totalCharge.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey, mainStringFont: UIFont.b4LightGrey, imageSize: CGSize(width: 8, height: 5))
            
            setExpandStatus(aSummaryItem.isSectionExpanded )
            
        } else {
            serviceLabel.text =  ""
            usageLabel.text = ""
            chargedLabel.text = ""
            setExpandStatus(false )
        }
        
    }
    
    func setExpandStatus(_ isExpanded : Bool?) {
        
        if isExpanded == true {
            self.expandCellImage.image = UIImage.imageFor(name: "pinkMinusIcon")
        } else {
            self.expandCellImage.image  = UIImage.imageFor(name: "pinkPlusIcon")
            
        }
    }
    
}
