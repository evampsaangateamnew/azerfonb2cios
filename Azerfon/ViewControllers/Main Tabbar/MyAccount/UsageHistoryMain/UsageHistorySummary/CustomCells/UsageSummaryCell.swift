//
//  UsageSummaryCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class UsageSummaryCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet var serviceTypeLabel      : AFMarqueeLabel!
    @IBOutlet var usageLabel            : AFLabel!
    @IBOutlet var chargedAmountLabel    : AFLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Functions
    func setLayoutFor(_ aItem: Records?) {
        if let aSummaryItem = aItem {
            
            serviceTypeLabel.text = aSummaryItem.service_type
            usageLabel.text = aSummaryItem.total_usage
            chargedAmountLabel.attributedText = aSummaryItem.chargeable_amount.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey, mainStringFont: UIFont.b4LightGrey, imageSize: CGSize(width: 8, height: 5))
            
        } else {
            serviceTypeLabel.text =  ""
            usageLabel.text = ""
            chargedAmountLabel.text = ""
        }
        
    }
    
}
