//
//  UsageHistoryPINVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class UsageHistoryPINVC: BaseVC {
    
    //MARK: - Properties
    var isStartDate : Bool?
    var passportNumber : String?
    var transactionSelectedType : String? = Localized("DropDown_All")
    
    var selectedType                    : Constants.AFResendType = .UsageHistory
    
    //MARK: - IBOutlet
    @IBOutlet var descriptionLabel                : AFLabel!
    
    @IBOutlet var smsPinField                     : AFTextField!
    
    @IBOutlet var submitButton                    : AFButton!
    @IBOutlet var resendButton                    : UIButton!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.smsPinField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        
        var otpValue : String = ""
        // OTP validation
        if let otpText = smsPinField.text,
            otpText.trimmWhiteSpace.count == Constants.OTPLength {
            
            otpValue = otpText.trimmWhiteSpace
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidOTP"))
            return
        }
        
        _ = smsPinField.resignFirstResponder()
        
        // API call for user authentication
        verifyOTP(AFUserSession.shared.msisdn, Cause: selectedType, Pin: otpValue)
    }
    
    @IBAction func resendButtonAction(_ sender: UIButton) {
        
        _ = smsPinField.resignFirstResponder()
        smsPinField.text = ""
        
        self.resendOTP(AFUserSession.shared.msisdn, Cause: selectedType)
        
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.descriptionLabel?.text = Localized("Title_UsageSmsPinDescription")
        
        self.smsPinField.placeholder = Localized("Title_EnterPin")
        self.submitButton.setTitle(Localized("Btn_Title_Submit"), for: .normal)
        
        let resendButtonTitle = String(format: Localized("BtnTitle_ResendOTP"), Localized("BtnTitle_RESEND"))
            .createAttributedString(mainStringColor: UIColor.afBrownGrey,
                                    mainStringFont: UIFont.b4LightGreyRight,
                                    highlightedStringColor: UIColor.afDarkPink,
                                    highlightedStringFont: UIFont.b4BoldLightGrey,
                                    highlighStrings: [Localized("BtnTitle_RESEND")])
        
        resendButton.setAttributedTitle(resendButtonTitle, for: .normal)
    }
    //MARK: - API's Calls
    
    /**
     Call 'verifyOTP' API with to verify OTP for signup and forgot.
     
     - parameter MSISDN: MSISDN of current user.
     - parameter cause: type of current screen
     - parameter Pin: pin to be verified.
     
     - returns: void
     */
    func verifyOTP(_ msisdn : String , Cause cause : Constants.AFResendType, Pin pin :String) {
        
        self.showActivityIndicator()
        _ = AFAPIClient.shared.verifyOTP(msisdn, Cause: cause, Pin: pin,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue ,
                    let _ = Mapper<RegistrationVerifyOTPModel>().map(JSONObject: resultData) {
                    
                    //set verified OTP flag to true
                    AFUserSession.shared.isOTPVerified = true
                    
                    if let usageHistoryMainVC = self.parent as? UsageHistoryMainVC {
                        
                        if let usageHistoryDetailsObj :UsageHistoryDetailsVC = UsageHistoryDetailsVC.instantiateViewControllerFromStoryboard() {
                            
                            
                            UIView.transition(with: usageHistoryMainVC.mainContentView, duration: 0.5, options: .transitionFlipFromRight, animations: {
                                usageHistoryMainVC.addChildViewController(childController: usageHistoryDetailsObj, onView: usageHistoryMainVC.mainContentView)
                                usageHistoryMainVC.removeChildViewController(childController: UsageHistoryPassportVC())
                                
                                
                            }, completion:{ _ in
                                usageHistoryMainVC.usageDetailsVC = usageHistoryDetailsObj
                            })
                        }
                    }
                    
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    /**
     Call 'verifyOTP' API with to verify OTP for signup and forgot.
     
     - parameter MSISDN: MSISDN of current user.
     - parameter cause: type of current screen
     
     - returns: void
     */
    func resendOTP(_ msisdn : String , Cause cause : Constants.AFResendType){
        
        self.showActivityIndicator()
        _ = AFAPIClient.shared.resendPin(msisdn, Cause: cause,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    //self.showSuccessAlertWithMessage(message: resultDesc)
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - UITextFieldDelegate
extension UsageHistoryPINVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /* Check that if text is nil then return false */
        guard let text = textField.text else { return false }
        
        /* Length of text in UITextField */
        let newLength = text.count + string.count - range.length
        
        if textField == smsPinField {
            
            return newLength <= Constants.OTPLength /* Restriction for OTP length */
            
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == smsPinField {
            _ = textField.resignFirstResponder()
            submitButtonPressed(AFButton())
            return true
        } else {
            return true
        }
    }
}
