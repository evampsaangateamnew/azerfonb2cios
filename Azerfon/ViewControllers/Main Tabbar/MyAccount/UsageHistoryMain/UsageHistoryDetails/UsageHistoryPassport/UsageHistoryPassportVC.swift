//
//  UsageHistoryPassportVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class UsageHistoryPassportVC: BaseVC {
    
    //MARK: - Properties
    
    //MARK: - IBOutlet

    @IBOutlet var descriptionLabel                : AFLabel!
    @IBOutlet var submitButton                    : AFButton!
    
    @IBOutlet var sevenDigitPinField              : AFTextField!
    
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sevenDigitPinField.delegate = self;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
    }
    
    //MARK: - IBAction

    @IBAction func submitButtonPressed(_ sender: UIButton) {
        
        var sevenDigitPIN : String = ""
        // 7 digit PIN validation
        if let pinText = sevenDigitPinField.text,
            pinText.count == 7 {
            sevenDigitPIN = pinText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterPassportNumber"))
            return
        }
        
        // TextField resignFirstResponder
        sevenDigitPinField.resignFirstResponder()
        
        //calling API
        self.verifyAccountDetails(passportNumber: sevenDigitPIN)
        
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.descriptionLabel?.text = Localized("Title_UsagePinDescription")
        
        self.sevenDigitPinField.placeholder = Localized("Title_SevenDigitCode")
        self.submitButton.setTitle(Localized("Btn_Title_Submit"), for: .normal)
    }
    
    //MARK: - API Calls
    /// Call 'verifyAccountDetails' API .
    ///
    /// - returns: pin
    func verifyAccountDetails(passportNumber:String?) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.verifyAccountDetails(PassportNumber: passportNumber ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let _ = Mapper<UsagePin>().map(JSONObject: resultData) {
                        
                        if let usageSummaryVC = self.parent as? UsageHistoryMainVC {
                            
                            if let usageHistoryPINObj :UsageHistoryPINVC = UsageHistoryPINVC.instantiateViewControllerFromStoryboard() {
                                usageHistoryPINObj.passportNumber = passportNumber ?? ""
                                
                                UIView.transition(with: usageSummaryVC.mainContentView, duration: 0.5, options: .transitionFlipFromRight, animations: {
                                    usageSummaryVC.addChildViewController(childController: usageHistoryPINObj, onView: usageSummaryVC.mainContentView)
                                    usageSummaryVC.removeChildViewController(childController: UsageHistoryPassportVC())
                                    
                                    
                                }, completion:nil)
                            }
                        }
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        });
    }
}

extension UsageHistoryPassportVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        
        //Set max Limit for textfields
        let newLength = text.count + string.count - range.length
        
        //  OTP textfield
        if textField == sevenDigitPinField {
            
            // limiting Passport lenght to 7 digits
            let allowedCharacters = CharacterSet(charactersIn: "\(Constants.allowedAlphbeats)\(Constants.allowedNumbers)")
            let characterSet = CharacterSet(charactersIn: string)
            
            if newLength <= 7 && allowedCharacters.isSuperset(of: characterSet) {
                
                // Returning number input
                return  true
            } else {
                return  false
            }
        } else  {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == sevenDigitPinField {
            submitButtonPressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
