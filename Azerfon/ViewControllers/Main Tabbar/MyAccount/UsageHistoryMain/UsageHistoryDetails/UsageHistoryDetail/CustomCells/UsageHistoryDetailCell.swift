//
//  UsageHistoryDetailCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/15/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class UsageHistoryDetailCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet var chargeTitleLabel          : AFLabel!
    @IBOutlet var peakTitleLabel            : AFLabel!
    @IBOutlet var zoneTitleLabel            : AFLabel!
    @IBOutlet var destinationTitleLabel     : AFLabel!
    
    @IBOutlet var chargeLabel               : AFLabel!
    @IBOutlet var peakLabel                 : AFLabel!
    @IBOutlet var zoneLabel                 : AFLabel!
    @IBOutlet var destinationLabel          : AFLabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Functions
    func setLayoutFor(_ aItem: DetailsHistory?) {
        
        chargeTitleLabel.text =  Localized("Title_Charge")
        peakTitleLabel.text = Localized("Title_Peak")
        zoneTitleLabel.text = Localized("Title_Zone")
        destinationTitleLabel.text = Localized("Title_Destination")
        
        if let aHistoryItem = aItem {
            chargeLabel.attributedText = aHistoryItem.chargedAmount.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afBrownGrey, mainStringFont: UIFont.b4LightGrey, imageSize: CGSize(width: 8, height: 5))
            peakLabel.text = aHistoryItem.period
            zoneLabel.text = aHistoryItem.zone
            destinationLabel.text = aHistoryItem.destination
            
        } else {
            chargeLabel.text = ""
            peakLabel.text = ""
            zoneLabel.text = ""
            destinationLabel.text = ""
        }
        
    }
    
    
}
