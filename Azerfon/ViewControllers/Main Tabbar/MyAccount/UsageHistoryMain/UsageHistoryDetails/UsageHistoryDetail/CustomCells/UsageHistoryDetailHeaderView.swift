//
//  UsageHistoryDetailHeaderView.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/16/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class UsageHistoryDetailHeaderView: AFAccordionTableViewHeaderView {
    
    //MARK: - IBOutlet
    @IBOutlet var dateTimeLabel          : UILabel!
    @IBOutlet var serviceLabel           : UILabel!
    @IBOutlet var numberLabel            : UILabel!
    @IBOutlet var usagesLabel            : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /**
     Sets Item labels values.
     
     - parameter aItem: requests data.
     
     - returns: void.
     */
    func setItemInfo(aItem: DetailsHistory) {
        dateTimeLabel.text = aItem.startDateTime
        serviceLabel.text = aItem.service
        numberLabel.text = aItem.number
        usagesLabel.text = aItem.usage
    }
    
}
