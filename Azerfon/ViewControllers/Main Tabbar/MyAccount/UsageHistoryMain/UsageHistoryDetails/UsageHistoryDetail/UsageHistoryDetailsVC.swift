//
//  UsageHistoryDetailsVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class UsageHistoryDetailsVC: BaseVC {
    
    //MARK: - Properties
    var isStartDate : Bool?
    var usagehistoryCompleteResponse : UsageDetailsHistoryHandler? = UsageDetailsHistoryHandler()
    var usagehistoryFilteredData : UsageDetailsHistoryHandler? = UsageDetailsHistoryHandler()
    var loanHistoryResponse : LoanHistoryListHandler?
    var storeLocator : StoreLocatorModel?
    var transactionSelectedType : String? = "All"
    
    //MARK: - IBOutlet
    @IBOutlet var dateTimeTitleLabel              : AFMarqueeLabel!
    @IBOutlet var serviceTitleLabel               : AFMarqueeLabel!
    @IBOutlet var numberTitleLabel                : AFMarqueeLabel!
    @IBOutlet var usageTitleLabel                 : AFMarqueeLabel!
    @IBOutlet var periodDropDownTitleLabel        : AFLabel!
    @IBOutlet var categoryDropDownTitleLabel      : AFLabel!
    @IBOutlet var startDateLabel                  : AFLabel!
    @IBOutlet var endDateLabel                    : AFLabel!
    
    @IBOutlet var historyTableView      : AFAccordionTableView!
    
    @IBOutlet var constDatePickerViewHeight : NSLayoutConstraint!
    
    @IBOutlet var categoryDropDown: UIDropDown!
    @IBOutlet var periodDropDown: UIDropDown!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.constDatePickerViewHeight.constant = 0
        self.dateDropDownChanged(transacitonIndex: 0)
        
        //init tableview
        historyTableView.hideDefaultSeprator()
        historyTableView.allowMultipleSectionsOpen = true
        historyTableView.allowsSelection = false
        historyTableView.delegate = self
        historyTableView.dataSource = self
        historyTableView.estimatedRowHeight = 100
        
        self.historyTableView.reloadUserData(usagehistoryFilteredData?.records)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewContent()
        
        //init dropdown
        setDropDown()
    }
    
    //MARK: - IBAction
    @IBAction func startButtonAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        let threeMonthsAgos = Date().todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLabel.text ,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: AFUserSession.shared.dateFormatter.createDate(from: endDateString, dateFormate: Constants.kDisplayFormat) ?? todayDate,
                                currentDate: currentDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate, currentDate: currentDate)
        }
        
    }
    
    @IBAction func endButtonAction(_ sender: UIButton) {
        
        isStartDate = false;
        
        let todayDate = Date().todayDate()
        
        let currentDate = AFUserSession.shared.dateFormatter.createDate(from: endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        self.showDatePicker(minDate: AFUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate,
                            maxDate: todayDate,
                            currentDate: currentDate)
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        self.dateTimeTitleLabel?.text = Localized("Title_DateTime")
        self.serviceTitleLabel?.text = Localized("Title_Service")
        self.numberTitleLabel?.text = Localized("Title_Number")
        self.usageTitleLabel?.text = Localized("Title_Usage")
        self.periodDropDown?.text = Localized("Title_SelectPeriod")
        self.categoryDropDownTitleLabel?.text = Localized("Title_SelectCategory")
        periodDropDownTitleLabel.text = Localized("Title_SelectPeriod")
    }
    
    /*Initialze - Setup dropdown */
    func setDropDown(){
        
        //Set Period Drop Down
        periodDropDown.placeholder = Constants.dateDropDownOptions.first
        periodDropDown.dataSource = Constants.dateDropDownOptions
        periodDropDown.didSelectOption { (index, option) in
            self.dateDropDownChanged( transacitonIndex: index)
        }
        
        //Set category Drop Down
        let categoriesTitles : [String] = [Localized("DropDown_All"),Localized("DropDown_Internet"), Localized("DropDown_Voice"), Localized("DropDown_SMS"),Localized("DropDown_Others")]
        categoryDropDown.text = Localized("DropDown_All")
        
        categoryDropDown.dataSource = categoriesTitles
        
        categoryDropDown.didSelectOption { (index, option) in
            self.setSelectedCategory(category: option)
        }
    }
    
    /**
     Filter usage history.
     
     - parameter category: Selected category.
     
     - returns: void
     */
    func setSelectedCategory(category: String) {
        
        switch category {
        case Localized("DropDown_All"):
            self.transactionSelectedType = "All"
            
        case Localized("DropDown_Internet"):
            self.transactionSelectedType = "data"
            
        case Localized("DropDown_Voice"):
            self.transactionSelectedType = "voice"
            
        case Localized("DropDown_SMS"):
            self.transactionSelectedType = "sms"
            
        case Localized("DropDown_Others"):
            self.transactionSelectedType = "others"
            
        default:
            break
        }
        
        let records : [DetailsHistory]? = self.filterUsageDetailBy(type: self.transactionSelectedType, usageDetails: self.usagehistoryCompleteResponse?.records)
        
        self.usagehistoryFilteredData?.records = records
        
        //reloading table view data
        self.historyTableView.reloadUserData(self.usagehistoryFilteredData?.records)
    }
    
    /**
     Date dropdown changed.
     
     - parameter transacitonIndex: Selected drpodown index.
     
     - returns: void
     */
    func dateDropDownChanged( transacitonIndex: Int?)  {
        
        let todayDateString = Date().todayDateString(dateFormate: Constants.kNewAPIFormat)
        switch transacitonIndex{
        case 0:
            print("default")
            self.setDatePickerHeight(constantValue: 0)
            
            self.getUsageHistoryDetailAPICall(startDate: todayDateString, endDate: todayDateString)
        case 1:
            print("Last 7 days")
            self.setDatePickerHeight(constantValue: 0)
            
            // Get data of previous seven days
            let sevenDaysAgo = Date().todayDateString(withAdditionalValue: -7)
            self.getUsageHistoryDetailAPICall(startDate: sevenDaysAgo, endDate: todayDateString)
            
        case 2:
            print("Last 30 days")
            self.setDatePickerHeight(constantValue: 0)
            
            // Get data of previous thirty days
            let thirtyDaysAgo = Date().todayDateString(withAdditionalValue: -30)
            self.getUsageHistoryDetailAPICall(startDate: thirtyDaysAgo, endDate: todayDateString)
            
        case 3:
            print("Previous month")
            self.setDatePickerHeight(constantValue: 0)
            
            // Getting previous month start and end date
            let startOfPreviousMonth = Date().todayDate().getPreviousMonth()?.startOfMonth() ?? Date().todayDate()
            let endOfPreviousMonth = Date().todayDate().getPreviousMonth()?.endOfMonth() ?? Date().todayDate()
            
            let startDateString = AFUserSession.shared.dateFormatter.createString(from: startOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
            let endDateString = AFUserSession.shared.dateFormatter.createString(from: endOfPreviousMonth, dateFormate: Constants.kNewAPIFormat)
            
            self.getUsageHistoryDetailAPICall(startDate: startDateString, endDate: endDateString)
            
        case 4:
            self.setDatePickerHeight(constantValue: 47)
            
            //For date formate
            self.startDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            self.endDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            
            self.startDateLabel.textColor = UIColor.afBrownGrey
            self.endDateLabel.textColor = UIColor.afBrownGrey
            
            self.getUsageHistoryDetailAPICall(startDate: todayDateString, endDate: todayDateString)
            
            print("Selected Date")
            
        default:
            print("destructive")
            
        }
    }
    
    func setDatePickerHeight(constantValue : CGFloat) {
        
        if self.constDatePickerViewHeight.constant !=  constantValue {
            self.constDatePickerViewHeight.constant = constantValue
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    /**
     Show date picker.
     
     - parameter minDate: minimum date of picker.
     - parameter maxDate: maximum date of picker.
     - parameter currentDate: current date of picker.
     
     - returns: void
     */
    func showDatePicker(minDate : Date , maxDate : Date, currentDate : Date){
        self.showNewDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate) { (selectedDate) in
            let todayDate = Date().todayDate(dateFormate: Constants.kDisplayFormat)
            
            let selectedDateString = AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kDisplayFormat)
            
            let startFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            let endFieldDate = AFUserSession.shared.dateFormatter.createDate(from: self.endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            
            
            if self.isStartDate ?? true {
                self.startDateLabel.text = selectedDateString
                self.startDateLabel.textColor = UIColor.afDateOrange
                self.getUsageHistoryDetailAPICall(startDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat),
                                                  endDate: AFUserSession.shared.dateFormatter.createString(from:endFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat))
            } else {
                self.endDateLabel.text = selectedDateString
                self.endDateLabel.textColor = UIColor.afDateOrange
                self.getUsageHistoryDetailAPICall(startDate: AFUserSession.shared.dateFormatter.createString(from: startFieldDate ?? todayDate, dateFormate: Constants.kNewAPIFormat),
                                                  endDate: AFUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kNewAPIFormat))
            }
        }
    }
    
    func filterUsageDetailBy(type:String?, usageDetails : [DetailsHistory]? ) -> [DetailsHistory]? {
        
        guard let myType = type else {
            return []
        }
        
        if myType.isEqualToStringIgnoreCase(otherString: "All") {
            return usageDetails
        }
        
        // Filtering offers array
        var filterdUsageDetails : [DetailsHistory] = []
        
        if let offers = usageDetails {
            
            for aDetailHistory in offers {
                
                if let historyDataType = aDetailHistory.type {
                    
                    if historyDataType.lowercased() == myType.lowercased() {
                        
                        filterdUsageDetails.append(aDetailHistory)
                    }
                }
            }
            
        } else {
            return []
        }
        
        return filterdUsageDetails
    }
    
    //MARK: - API Calls
    
    /**
     Call 'getUsageSummaryHistory' API .
     
     - parameter startDate: Start date.
     - parameter startDate: End date.
     
     
     - returns: SummaryHistory
     */
    func getUsageHistoryDetailAPICall(startDate:String?, endDate:String?) {
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.getUsageHistoryDetail(StartDate: startDate ?? "", EndDate: endDate ?? "",{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let usageHistoryList = Mapper<UsageDetailsHistoryHandler>().map(JSONObject: resultData){
                        
                        self.usagehistoryCompleteResponse = usageHistoryList
                        
                        self.usagehistoryFilteredData?.records = self.filterUsageDetailBy(type: self.transactionSelectedType, usageDetails: usageHistoryList.records)
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.historyTableView.reloadUserData(self.usagehistoryFilteredData?.records)
        })
    }
}

//MARK: - TableView Delegates
extension UsageHistoryDetailsVC : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return usagehistoryFilteredData?.records?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView :UsageHistoryDetailHeaderView = tableView.dequeueReusableHeaderFooterView() {
            
            headerView.setItemInfo(aItem: (usagehistoryFilteredData?.records?[section])!)
            
            return headerView
        }
        return AFAccordionTableViewHeaderView()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :UsageHistoryDetailCell = tableView.dequeueReusableCell() {
            
            cell.setLayoutFor((usagehistoryFilteredData?.records![indexPath.section])!)
            
            return cell
        }
        
        return UITableViewCell()
    }
}

extension UsageHistoryDetailsVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        usagehistoryFilteredData?.records![section].isSectionExpanded = true
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        usagehistoryFilteredData?.records![section].isSectionExpanded = false
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}
