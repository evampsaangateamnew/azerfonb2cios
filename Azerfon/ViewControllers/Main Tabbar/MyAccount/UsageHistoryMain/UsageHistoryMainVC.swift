//
//  UsageHistoryVC.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 4/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class UsageHistoryMainVC: BaseVC {
    
    //MARK: - Properties
    var selectedType        : Constants.AFUsageHistoryType = .summary
    var usageSummaryVC      :  UsageHistorySummaryVC?
    var usageDetailsVC      :  UsageHistoryDetailsVC?
    var usagePassportVC     :  UsageHistoryPassportVC?
    var usagePINVC          :  UsageHistoryPINVC?
    
    //MARK: - IBOutlet
    @IBOutlet var topLargeNavigationView    : UIView!
    @IBOutlet var mainContentView           : AFView!
    @IBOutlet var summaryButton             : AFButton!
    @IBOutlet var detailsButton             : AFButton!
    
    
    @IBOutlet var buttonView                : UIView!
    @IBOutlet var navViewHeightConstraint   : NSLayoutConstraint!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.reDirectUserToSelectedScreen(type: selectedType)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        /* Hide top summy detail buttons for corporate user */
        if AFUserSession.shared.customerType() == .corporateCustomer ||
            AFUserSession.shared.customerType() == .dataSimPostpaidCorporate {
            buttonView.isHidden = true
            topLargeNavigationView.removeConstraints(buttonView.constraints)
            navViewHeightConstraint.constant = 44
        }
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func summaryPressed(_ sender: AFButton) {
        
        selectedType = .summary
        
        self.reDirectUserToSelectedScreen(type: selectedType)
    }
    
    @IBAction func detailsPressed(_ sender: AFButton) {
        
        selectedType = .details
        
        self.reDirectUserToSelectedScreen(type: selectedType)
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        self.titleLabel?.text = Localized("Title_UsageHistory")
        
        self.summaryButton.setTitle(Localized("TitleBtn_Summary"), for: .normal)
        self.detailsButton.setTitle(Localized("Title_Btn_UsageHistory_Details"), for: .normal)
    }
    
    /**
     Add child view controller to parent view.
     - returns: void
     */
    func reDirectUserToSelectedScreen(type :Constants.AFUsageHistoryType) {
        if type == .summary {
            
            summaryButton.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
            detailsButton.setButtonLayoutType(.whiteText_DarkPinkBG_Rounded)
        } else {
            
            summaryButton.setButtonLayoutType(.whiteText_DarkPinkBG_Rounded)
            detailsButton.setButtonLayoutType(.purplishBrownText_WhiteBG_Rounded)
        }
        
        //check each tabs one by one and perform operations accordingly
        if type == .summary {
            
            if (usageSummaryVC == nil) {
                usageSummaryVC = UsageHistorySummaryVC.instantiateViewControllerFromStoryboard()
            }
            
            self.addChildViewController(childController: usageSummaryVC ?? UsageHistorySummaryVC(), onView: mainContentView)
            
        } else if type == .details {
            
            /*checking if user already verified PIN than simply show details screen else go through verify PIN process*/
            if AFUserSession.shared.isOTPVerified == true {
                if (usageDetailsVC == nil) {
                    usageDetailsVC = UsageHistoryDetailsVC.instantiateViewControllerFromStoryboard()
                }
                
                self.addChildViewController(childController: usageDetailsVC ?? UsageHistoryDetailsVC(), onView: mainContentView)
            } else {
                if (usagePassportVC == nil) {
                    usagePassportVC = UsageHistoryPassportVC.instantiateViewControllerFromStoryboard()
                }
                
                self.addChildViewController(childController: usagePassportVC ?? UsageHistoryPassportVC(), onView: mainContentView)
            }
            
        }
    }
    
    //MARK: - API's Calls
}

