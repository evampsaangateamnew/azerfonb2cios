//
//  MySubscriptionsVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/25/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
import UPCarouselFlowLayout

class MySubscriptionsVC: BaseVC {
    
    //MARK: - Properties
    var showCardView            : Bool = true
    var isUserSearch            : Bool = true
    
    var redirectedFromHomeScreen    : Bool = false
    var tabMenuArray            : [Constants.AFOfferTabType] = [.call, .internet,.sms, .hybrid, .roaming ]
    var selectedTabType         : Constants.AFOfferTabType = .internet
    var subscriptionsOffers     : SupplementaryResponse?
    var selectedOfferGroupInfo  : [SupplementaryOfferItem]?
    
    // USED to toggle
    var selectedSectionViewType : Constants.AFSectionType = .offerTitleView
    
    let cardLayout  : UPCarouselFlowLayout = UPCarouselFlowLayout()
    
    //MARK: - IBOutlet
    @IBOutlet var searchTextField   : UITextField!
    @IBOutlet var searchButton      : UIButton!
    
    @IBOutlet var tabBarCollectionView: UICollectionView!
    
    @IBOutlet var subscriptionCollectionView: UICollectionView!
    
    @IBOutlet  var btnReview: AFButton!
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarCollectionView.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        
        NormalTypeCell.registerReusableCell(with: tabBarCollectionView)
        tabBarCollectionView.delegate = self
        tabBarCollectionView.dataSource = self
        tabBarCollectionView.allowsMultipleSelection = false
        
        subscriptionCollectionView.delegate = self
        subscriptionCollectionView.dataSource = self
        SubscriptionCell.registerReusableCell(with: subscriptionCollectionView)
        
        self.subscriptionCollectionView.layoutIfNeeded()
        self.cardLayout.itemSize = CGSize(width: self.subscriptionCollectionView.bounds.width - 54 , height:  self.subscriptionCollectionView.bounds.height)
        self.cardLayout.scrollDirection = .horizontal
        self.cardLayout.sideItemScale = 0.8
        self.cardLayout.sideItemAlpha = 1
        self.cardLayout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 10)
        self.subscriptionCollectionView.collectionViewLayout = self.cardLayout
        
        /* set search textField layout */
        self.searchTextField.delegate = self
        self.searchTextField.attributedPlaceholder = NSAttributedString(string: Localized("PlaceHolder_Search"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.afWhite])
        
        let searchSepratorView = UIView()
        searchSepratorView.backgroundColor = UIColor.afWhite
        self.searchTextField.addSubview(searchSepratorView)
        
        searchSepratorView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.searchTextField.snp.left)
            maker.right.equalTo(self.searchTextField.snp.right)
            maker.bottom.equalTo(self.searchTextField.snp.bottom)
            maker.height.equalTo(1)
        }
        
        layoutSearchTextField(false, reloadData: false)
        
        self.btnReview.setTitle(Localized("Title_Review"), for: .normal)
        
        /* load Subscriptions data */
        loadSubscriptionsData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        // Layout view lables
        loadViewLayout()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    //MARK: - IBActions
    
    @IBAction func searchPressed(_ sender: UIButton) {
        layoutSearchTextField(isUserSearch ? false : true)
    }
    
    @IBAction func renewPressed(_ sender: AFButton) {
        
        if sender.tag >= 0,
            let selectedOffer = selectedOfferGroupInfo?[sender.tag] {
            
            switch sender.buttonContentType {
            case .topup:
                if let topUpVC :TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard() {
                    self.navigationController?.pushViewController(topUpVC, animated: true)
                }
                return
            case .renew:
                self.showConfirmationAlert(message: Localized("Message_RenewOffer"), {
                    /* Call change supplementory on user ok button */
                    self.changeSupplementaryOffering(selectedOffer: selectedOffer, actionType: true)
                })
                break
            default:
                break
            }
            
            
        }
    }
    
    @IBAction func deactivatePressed(_ sender: AFButton) {
        
        if sender.tag >= 0,
            let selectedOffer = selectedOfferGroupInfo?[sender.tag] {
            
            self.showConfirmationAlert(message: Localized("Message_deactivateOffer"), {
                /* Call change supplementory on user ok button */
                self.changeSupplementaryOffering(selectedOffer: selectedOffer, actionType: false)
            })
        }
    }
    
    @IBAction func reviewPressed(_ sender: AFButton) {
        if let supplementaryVC :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard() {
            
        /*var newSelectedTabType = selectedTabType
            switch selectedTabType {
                
            case .internet:
                supplementaryVC.selectedTabType = .internet
            case .sms:
                supplementaryVC.selectedTabType = .sms
            case .call:
                supplementaryVC.selectedTabType = .call
            }*/
           supplementaryVC.selectedTabType = selectedTabType
            self.navigationController?.pushViewController(supplementaryVC, animated: true)
        }
    }
    
    //MARK: - Functions
    
    func loadViewLayout(){
        
        titleLabel?.text = Localized("Title_Subscriptions");
    }
    
    func layoutSearchTextField(_ showTextField :Bool, reloadData :Bool = true) {
        
        isUserSearch = showTextField
        
        if showTextField {
            titleLabel?.isHidden = true
            searchTextField.isHidden = false
            
            searchTextField.text = ""
            _ = searchTextField.becomeFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search_cross") , for: .normal)
            
        } else {
            titleLabel?.isHidden = false
            searchTextField.isHidden = true
            
            searchTextField.text = ""
            _ = searchTextField.resignFirstResponder()
            
            searchButton.setImage(UIImage.imageFor(name: "search") , for: .normal)
            
            if reloadData {
                selectedOfferGroupInfo = getOffersInfoFor(selectedType: selectedTabType)
                self.reloadSubscriptionsOffer(itemIndex: 0)
            }
        }
        
    }
    
    func reloadSubscriptionsOffer(itemIndex :Int, errorMessage : String = Localized("Message_NoData")) {
        
        /* Reload tariff items */
        if (selectedOfferGroupInfo?.count ?? 0) > 0 {
            
            self.subscriptionCollectionView.hideDescriptionView()
            self.btnReview.isHidden = true;
            
            self.subscriptionCollectionView.reloadData()
            self.subscriptionCollectionView.performBatchUpdates(nil) { (isCompleted) in
                if isCompleted {
                    
                    self.subscriptionCollectionView.scrollToItem(at: IndexPath(item: itemIndex, section: 0), at: .centeredHorizontally, animated: true)
                }
            }
            
        } else {
            self.subscriptionCollectionView.showDescriptionViewWithImage(description: errorMessage,
                                                                         descriptionColor: UIColor.afWhite)
            self.subscriptionCollectionView.reloadData()
            self.btnReview.isHidden = false;
        }
    }
    
    func reDirectUserToSelectedScreens() {
        
        var newSelectedTabType = selectedTabType
        
        /* Check if user is redirected form home screen */
        if redirectedFromHomeScreen &&
            ( newSelectedTabType == .call || newSelectedTabType == .sms || newSelectedTabType == .internet) {
            
            if newSelectedTabType == .call {
                
                if let selectedOffers = getOffersInfoFor(selectedType: .allInclusiveCall),
                    selectedOffers.count > 0 {
                    
                    newSelectedTabType = .allInclusiveCall
                    selectedOfferGroupInfo = selectedOffers
                } else {
                    newSelectedTabType = .call
                    selectedOfferGroupInfo = getOffersInfoFor(selectedType: .call)
                }
                
            } else if newSelectedTabType == .sms {
                if let selectedOffers = getOffersInfoFor(selectedType: .allInclusiveSMS),
                    selectedOffers.count > 0 {
                    
                    newSelectedTabType = .allInclusiveSMS
                    selectedOfferGroupInfo = selectedOffers
                } else {
                    newSelectedTabType = .sms
                    selectedOfferGroupInfo = getOffersInfoFor(selectedType: .sms)
                }
                
            } else if newSelectedTabType == .internet {
                
                if let selectedOffers = getOffersInfoFor(selectedType: .allInclusiveInternet),
                    selectedOffers.count > 0 {
                    
                    newSelectedTabType = .allInclusiveInternet
                    selectedOfferGroupInfo = selectedOffers
                } else {
                    newSelectedTabType = .internet
                    selectedOfferGroupInfo = getOffersInfoFor(selectedType: .internet)
                }
            }
        } else {
            
            selectedOfferGroupInfo = getOffersInfoFor(selectedType: newSelectedTabType)
        }
        
        
        if newSelectedTabType == .allInclusiveCall ||
            newSelectedTabType == .allInclusiveSMS ||
            newSelectedTabType == .allInclusiveInternet {
            
            tabMenuArray = [.call, .internet,.sms, .hybrid, .roaming, newSelectedTabType]
            
        } else {
            tabMenuArray = [.call, .internet,.sms, .hybrid, .roaming]
        }
        
        /* if offers are empty then load hybrid offers */
        if redirectedFromHomeScreen && (selectedOfferGroupInfo?.count ?? 0) <= 0 {
            newSelectedTabType = .hybrid
            selectedOfferGroupInfo = getOffersInfoFor(selectedType: .hybrid)
        }
        
        /* Reset user redirection check */
        redirectedFromHomeScreen = false
        
        /* update current selected tab */
        self.selectedTabType = newSelectedTabType
        
        /* Reload top tabbar offers */
        tabBarCollectionView.reloadData()
        tabBarCollectionView.performBatchUpdates(nil) { (isCompleted) in
            if isCompleted {
                // Higlight selected collectionView cell color.
                let index = self.tabMenuArray.index(of: self.selectedTabType)
                self.tabBarCollectionView.selectItem(at: IndexPath(item:index ?? 0, section:0) , animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
            }
        }
        
        /* Reload Subscriptions data */
        self.reloadSubscriptionsOffer(itemIndex: 0)
    }
    
    func getOffersInfoFor( selectedType : Constants.AFOfferTabType) -> [SupplementaryOfferItem]? {
        
        var offersGroupInfo : [SupplementaryOfferItem]?
        
        switch selectedType {
            
        case .call:
            offersGroupInfo = subscriptionsOffers?.call?.offers
            
        case .internet:
            offersGroupInfo = subscriptionsOffers?.internet?.offers
            
        case .sms:
            offersGroupInfo = subscriptionsOffers?.sms?.offers
            
        case .campaign:
            offersGroupInfo = subscriptionsOffers?.campaign?.offers
            
        case .tm:
            offersGroupInfo = subscriptionsOffers?.tm?.offers
            
        case .hybrid:
            offersGroupInfo = subscriptionsOffers?.hybrid?.offers
            
        case .roaming:
            offersGroupInfo = subscriptionsOffers?.roaming?.offers
            
        case .allInclusiveCall:
            offersGroupInfo = subscriptionsOffers?.voiceInclusiveOffers?.offers
        
        case .allInclusiveInternet:
            offersGroupInfo = subscriptionsOffers?.internetInclusiveOffers?.offers
        
        case .allInclusiveSMS:
            offersGroupInfo = subscriptionsOffers?.smsInclusiveOffers?.offers
            
        default:
            offersGroupInfo = nil
            break
        }
        
        return offersGroupInfo
    }
    
    func loadSubscriptionsData() {
        /* Load data from UserDefaults */
        
        self.btnReview.isHidden = true;
        if Connectivity.isConnectedToInternet == false,
            let subscriptionsOffersHandler :SupplementaryResponse = SupplementaryResponse.loadFromUserDefaults(key: "\(APIsType.mySubscriptions.selectedLocalizedAPIKey())") {
            
            // Set information in local object
            self.subscriptionsOffers = subscriptionsOffersHandler
            // Redirect user to selected screen
            self.reDirectUserToSelectedScreens()
            
        }
        
        MySubscriptionsVC.getSubscriptions(showError: true, parentViewController: self) { (subscribedOffers) in
            
            if subscribedOffers == nil {
                if Connectivity.isConnectedToInternet == true {
                    /* Save subscribed offers */
                    self.subscriptionsOffers = subscribedOffers
                }
                
            } else {
                /* Save subscribed offers */
                self.subscriptionsOffers = subscribedOffers
            }
            // Redirect user to selected screen
            self.reDirectUserToSelectedScreens()
        }
    }
    
    /**
     USED to toggle section.
     
     - returns: Void.
     */
    
    func openSelectedSection() {
        
        let visbleIndexPath = subscriptionCollectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = subscriptionCollectionView.cellForItem(at: aIndexPath) as? SubscriptionCell {
                
                var openSectionIndex : Int? = 0
                
                if let aTariffObject = selectedOfferGroupInfo?[cell.tableView.tag] {
                    
                    let type = typeOfDataInSection(aTariffObject)
                    
                    if selectedSectionViewType == .offerTitleView && type.contains(.offerTitleView) {
                        
                        openSectionIndex = 0
                        
                    } else if selectedSectionViewType == .detail && type.contains(.detail) {
                        
                        openSectionIndex = type.index(of:.detail)
                        
                    }
                }
                
                cell.tableView.closeAllSectionsExcept(openSectionIndex ?? 0, shouldCallDelegate: false)
                
                openSectionAt(index: openSectionIndex ?? 0, tableView: cell.tableView)
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : AFAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
    
    
    //MARK: - API Calls
    
    /**
     Call 'getSubscriptions' API .
     - returns: void
     */
    class func getSubscriptions(showError : Bool,
                                parentViewController : UIViewController,
                                completionBlock: @escaping (_ subscribedOffer : SupplementaryResponse?) -> Void ) {
        
        parentViewController.showActivityIndicator()
        
        _ = AFAPIClient.shared.getSubscriptions({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            parentViewController.hideActivityIndicator()
            
            if error != nil {
                if showError {
                    error?.showServerErrorInViewController(parentViewController)
                }
                /* execute callback */
                completionBlock(nil)
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let subscriptionsOffersHandler = Mapper<SupplementaryResponse>().map(JSONObject: resultData) {
                    
                    subscriptionsOffersHandler.saveInUserDefaults(key: APIsType.mySubscriptions.selectedLocalizedAPIKey())
                    
                    /* execute callback */
                    completionBlock(subscriptionsOffersHandler)
                    
                    
                } else {
                    if showError {
                        // Show error alert to user
                        parentViewController.showErrorAlertWithMessage(message: resultDesc)
                    }
                    
                    /* execute callback */
                    completionBlock(nil)
                }
            }
        })
    }
    
    
    /**
     Call 'changeSupplementaryOffering' API .
     - parameter  selectedOffer : offer user selected to renew or deactivate
     - parameter  actionType : if user want to renew offer then actionType will be true and for deactivation it will be false.
     - returns: Void
     */
    func changeSupplementaryOffering(selectedOffer: SupplementaryOfferItem, actionType :Bool) {
        
        self.showActivityIndicator()
        
        _ = AFAPIClient.shared.changeSupplementaryOffering(actionType: actionType, offeringId: selectedOffer.header?.offeringId ?? "", offerName: selectedOffer.header?.offerName ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                //EventLog
                if actionType {
                    //Log event for subscription renewal
                    AFAnalyticsManager.logEvent(screenName: "my_subscription", contentType:"renewal" , successStatus:"0" )
                } else {
                    //Log event for subscription deactivation
                    AFAnalyticsManager.logEvent(screenName: "my_subscription", contentType:"deactivation" , successStatus:"0" )
                }
                
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                    let response = Mapper<MySubscriptionModel>().map(JSONObject: resultData) {
                    
                    //EventLog
                    if actionType {
                        //Log event for subscription renewal
                        AFAnalyticsManager.logEvent(screenName: "my_subscription", contentType:"renewal" , successStatus:"1" )
                    } else {
                        //Log event for subscription deactivation
                        AFAnalyticsManager.logEvent(screenName: "my_subscription", contentType:"deactivation" , successStatus:"1" )
                    }
                    
                    /* Save subscribed offers */
                    self.subscriptionsOffers = response.mySubscriptionsData
                    
                    /* Show successfull message */
                    if  actionType == true {
                        self.checkAndShowInAppSurvey(message: response.message ?? "", offeringId: selectedOffer.header?.offeringId ?? "")
                    } else {
                        self.showSuccessAlertWithMessage(message: response.message)
                    }
                    
                    //
                    
                    // reload data
                    self.reloadSubscriptionsOffer(itemIndex: 0)
                    
                } else {
                    //EventLog
                    if actionType {
                        //Log event for subscription renewal
                        AFAnalyticsManager.logEvent(screenName: "my_subscription", contentType:"renewal" , successStatus:"0" )
                    } else {
                        //Log event for subscription deactivation
                        AFAnalyticsManager.logEvent(screenName: "my_subscription", contentType:"deactivation" , successStatus:"0" )
                    }
                    
                    /* Show error alert to user */
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    
    
    //Show  Survey
    
    func checkAndShowInAppSurvey(message: String, offeringId:String){
        
        if let survey = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.bundle_subscribed.rawValue}) {
            
            if  survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0, survey.visitLimit != "-1",
                survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "SupplementaryScreenViews") {
                
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    /*if let  answerIndex = survey.questions?.first?.answers?.firstIndex(where: { $0.answerId ==  userSurvey?.answerId }),let answer = survey.questions?.first?.answers?[answerIndex] {
                        inAppSurveyVC.selectedAnswer = answer
                    }*/
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
                    //inAppSurveyVC.survayComment = userSurvey?.comments
                    inAppSurveyVC.offeringId = offeringId
                    inAppSurveyVC.offeringType = "2"
                    inAppSurveyVC.attributedMessage = message.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h3GreyRight)
                    inAppSurveyVC.callBack = {
                        self.reDirectUserToSelectedScreens()
                    }
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
                UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "SupplementaryScreenViews")+1, forKey: "SupplementaryScreenViews")
                UserDefaults.standard.synchronize()
                
            }else{
                self.showSuccessAlertWithMessage(message: message){
                    self.reDirectUserToSelectedScreens()
                }
            }
            
        }else{
            // Show Error alert
            self.showSuccessAlertWithMessage(message: message) {
                self.reDirectUserToSelectedScreens()
            }
        }
    }
    
}
//MARK: - COLLECTION VIEW Delegates
extension MySubscriptionsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if subscriptionCollectionView == collectionView {
            
            if showCardView {
                return CGSize(width: self.subscriptionCollectionView.bounds.width - 54 , height:  self.subscriptionCollectionView.bounds.height)
            } else {
                return CGSize(width: self.subscriptionCollectionView.bounds.width - 54 , height:  self.subscriptionCollectionView.bounds.height - 44)
            }
            
        } else {
            let str: String = tabMenuArray[indexPath.item].localizedString()
            
            return CGSize(width: ((str as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.h5GreyCenter]).width + 30), height: 35)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if subscriptionCollectionView == collectionView {
            
            return selectedOfferGroupInfo?.count ?? 0
        } else {
            return self.tabMenuArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if subscriptionCollectionView == collectionView {
            
            if let cell :SubscriptionCell = collectionView.dequeueReusableCell(withReuseIdentifier: SubscriptionCell.cellIdentifier(), for: indexPath) as? SubscriptionCell,
                let aOfferObject = selectedOfferGroupInfo?[indexPath.item] {
                cell.delegate = self
                cell.setTitleLayout(header: aOfferObject.header)
                
                cell.tableView.delegate = self
                cell.tableView.dataSource = self
                
                cell.tableView.allowsSelection = false
                cell.tableView.tag = indexPath.item
                
                cell.renewButton.tag = indexPath.item
                cell.renewButton.addTarget(self, action: #selector(renewPressed(_:)), for: .touchUpInside)
                
                cell.deactivateButton.tag = indexPath.item
                cell.deactivateButton.addTarget(self, action: #selector(deactivatePressed(_:)), for: .touchUpInside)
                
                cell.tableView.reloadData()
                
                return cell
            }
            
        } else {
            
            if let cell :NormalTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier: NormalTypeCell.cellIdentifier(), for: indexPath) as? NormalTypeCell {
                
                cell.setLayout(title: tabMenuArray[indexPath.item].localizedString(),
                               isSelected: selectedTabType == tabMenuArray[indexPath.item] ? true : false,
                               isSpecial: false )
                return cell
            }
        }
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if subscriptionCollectionView == collectionView {
            return
        }
        
        // Check if user select same selected tab again if true then do nothing.
        if selectedTabType == tabMenuArray[indexPath.item] {
            return
        } else {
            // update selected tab information
            selectedTabType = tabMenuArray[indexPath.item]
        }
        
        
        if let cell :NormalTypeCell = collectionView.cellForItem(at: indexPath) as? NormalTypeCell {
            cell.setLayout(title: tabMenuArray[indexPath.item].localizedString(),
                           isSelected: true,
                           isSpecial: false )
            
        }
        
        // Reset search
        layoutSearchTextField(false, reloadData: false)
        
        /* get packages info in case it is empty */
        if subscriptionsOffers == nil {
            
            self.loadSubscriptionsData()
        }
        
        /* load info of selected tab */
        selectedOfferGroupInfo = getOffersInfoFor(selectedType: self.selectedTabType)
        self.reloadSubscriptionsOffer(itemIndex: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if subscriptionCollectionView == collectionView {
            return
        }
        
        if let cell :NormalTypeCell = collectionView.cellForItem(at: indexPath) as? NormalTypeCell {
            cell.setLayout(title: tabMenuArray[indexPath.item].localizedString(),
                           isSelected: false,
                           isSpecial: false )
        }
    }
}


//MARK: - UITableViewDelegate
extension MySubscriptionsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let aOfferObject = selectedOfferGroupInfo?[tableView.tag]{
            
            return typeOfDataInSection(aOfferObject).count
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 50
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let aOfferObject = selectedOfferGroupInfo?[tableView.tag] {
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[section]{
            case .offerTitleView:
                if let headerView :SubscriptionHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    headerView.setTitleLayout(header: aOfferObject.header, headerType: .offerTitleView)
                    return headerView
                }
                break
            case .detail:
                if let headerView :ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    headerView.setViewWithTitle(title: Localized("Title_Detail"),
                                                isSectionSelected: selectedSectionViewType == .detail ? true : false,
                                                headerType: .detail)
                    return headerView
                }
                break
                
            default:
                break
            }
        }
        
        return AFAccordionTableViewHeaderView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let aOfferObject = selectedOfferGroupInfo?[tableView.tag]{
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[section]{
            case .offerTitleView:
                return aOfferObject.header?.usage?.count ?? 0
                
            case .detail:
                
                return typeOfDataInDetailSection(aOfferObject.details).count
                
            default:
                break
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let aOfferObject = selectedOfferGroupInfo?[tableView.tag] {
            
            let types = typeOfDataInSection(aOfferObject)
            
            switch types[indexPath.section]{
            case .offerTitleView:
                
                if let cell : OfferUsageCell = tableView.dequeueReusableCell() {
                    cell.setUsageLayout(aUsage: aOfferObject.header?.usage?[indexPath.item],
                                        isRenewButtonEnabled: aOfferObject.header?.btnRenew?.isButtonEnabled(),
                                        isDaily: aOfferObject.header?.isDaily?.isTrue() ?? false,
                                        showSeparator: indexPath.row < ((aOfferObject.header?.usage?.count ?? 0) - 1))
                    
                    return cell
                }
                
                break
            case .detail:
                let detailsData = typeOfDataInDetailSection(aOfferObject.details)
                
                switch detailsData[indexPath.row] {
                    
                case .price:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setPriceLayoutValues(price: aOfferObject.details?.price?[indexPath.row],
                                                  showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                    break
                case .rounding:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setRoundingLayoutValues(rounding: aOfferObject.details?.rounding,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .textWithTitle:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithTitleLayoutValues(textWithTitle: aOfferObject.details?.textWithTitle,
                                                          showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithOutTitle:
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aOfferObject.details?.textWithOutTitle,
                                                             showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .textWithPoints:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithPointsLayoutValues(textWithPoint: aOfferObject.details?.textWithPoints,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .date:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aOfferObject.details?.date,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                case .time:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aOfferObject.details?.time,
                                                        showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .roamingDetails:
                    
                    if let cell:RoamingDetailsCell = tableView.dequeueReusableCell() {
                        cell.setRoamingDetailsLayout(roamingDetails: aOfferObject.details?.roamingDetails,
                                                     showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .freeResourceValidity:
                    
                    if let cell:FreeResourceValidityCell = tableView.dequeueReusableCell() {
                        cell.setFreeResourceValidityValues(freeResourceValidity: aOfferObject.details?.freeResourceValidity,
                                                           showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    break
                    
                case .titleSubTitleValueAndDesc:
                    
                    if let cell:TitleSubTitleValueAndDescCell = tableView.dequeueReusableCell() {
                        cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aOfferObject.details?.titleSubTitleValueAndDesc,
                                                                      showSeprator: indexPath.row < (detailsData.count - 1))
                        return cell
                    }
                    
                default:
                    break
                }
                break
                
            default:
                break
            }
        }
        
        return UITableViewCell()
    }
}

// MARK: - AFAccordionTableViewDelegate

extension MySubscriptionsVC : AFAccordionTableViewDelegate {
    
    func tableView(_ tableView: AFAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: true)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        if let headerView = header as? ExpandableTitleHeaderView,
            selectedSectionViewType != headerView.viewType {
            
            selectedSectionViewType = headerView.viewType
            
        } else if let headerView = header as? SubscriptionHeaderView,
            selectedSectionViewType != headerView.viewType {
            
            selectedSectionViewType = headerView.viewType
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.updateStateIcon(isSelected: false)
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            if selectedSectionViewType == headerView.viewType {
                selectedSectionViewType = .offerTitleView
                tableView.toggleSection(0)
                
            }
        }
    }
    
    func tableView(_ tableView: AFAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}


//MARK: TariffsCellDelegate
extension MySubscriptionsVC: SubsctiptionsCellDelegate {
    func starTapped(offerId: String) {
        
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            if  let  userSurvey = AFUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offerId}){
                if let survey  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.survayId  == userSurvey.surveryId }){
                    inAppSurveyVC.currentSurvay = survey
                    if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                        inAppSurveyVC.survayQuestion = question
                        if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                            inAppSurveyVC.selectedAnswer = question.answers?[answer]
                        }
                    }
                }
                inAppSurveyVC.survayComment = userSurvey.comments
                inAppSurveyVC.offeringId = offerId
                inAppSurveyVC.offeringType = "2"
            } else {
                if let survey = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.bundle.rawValue}) {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.offeringId = offerId
                    inAppSurveyVC.offeringType = "2"
                }
            }
            inAppSurveyVC.callBack =  {
                DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                    self.subscriptionCollectionView.reloadData()
                }
            }
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
        
    }
    
    
}

//MARK: - Helping functions
extension MySubscriptionsVC {
    
    func typeOfDataInSection(_ aOfferItem : SupplementaryOfferItem?) -> [Constants.AFSectionType] {
        
        var dataTypes : [Constants.AFSectionType] = []
        
        //1
        dataTypes.append(.offerTitleView)
        
        //2
        if aOfferItem?.details != nil {
            
            dataTypes.append(.detail)
        }
        
        return dataTypes
    }
    
    func typeOfDataInDetailSection(_ aDetail : Details?) -> [Constants.AFOfferType] {
        
        var dataTypes : [Constants.AFOfferType] = []
        
        if let aDetailObject =  aDetail {
            
            // 1
            if let count = aDetailObject.price?.count {
                
                for _ in 0..<count {
                    dataTypes.append(.price)
                }
            }
            // 2
            if aDetailObject.rounding != nil  {
                
                dataTypes.append(.rounding)
            }
            // 3
            if aDetailObject.textWithTitle != nil {
                
                dataTypes.append(.textWithTitle)
            }
            // 4
            if aDetailObject.textWithOutTitle != nil {
                
                dataTypes.append(.textWithOutTitle)
            }
            // 5
            if aDetailObject.textWithPoints != nil {
                
                dataTypes.append(.textWithPoints)
            }
            // 6
            if aDetailObject.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(.titleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailObject.date != nil {
                
                dataTypes.append(.date)
            }
            // 8
            if aDetailObject.time != nil {
                
                dataTypes.append(.time)
            }
            // 9
            if aDetailObject.roamingDetails != nil {
                
                dataTypes.append(.roamingDetails)
            }
            // 10
            if aDetailObject.freeResourceValidity != nil {
                
                dataTypes.append(.freeResourceValidity)
            }
        }
        return dataTypes
    }
}

//MARK:- UITextFieldDelegate

extension MySubscriptionsVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == searchTextField {
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            // Filter offers with new name
            selectedOfferGroupInfo = OffersModel.filterOfferBySearchString(searchString: newString, offersToFilter: getOffersInfoFor(selectedType: selectedTabType))
            
            /* Reload user content */
            self.reloadSubscriptionsOffer(itemIndex: 0, errorMessage: Localized("Message_NothingFoundSearch"))
            
            if newString.length > 0 {
                //EventLog
                AFAnalyticsManager.logEvent(screenName: "search", contentType:"my_subscription_screen" , searchTerm:newString )
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

//MARK: - UIScrollViewDelegate
extension MySubscriptionsVC : UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == subscriptionCollectionView {
            openSelectedSection()
        }
    }
}

