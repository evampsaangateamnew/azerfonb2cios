//
//  SubscriptionHeaderView.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 5/13/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class SubscriptionHeaderView: AFAccordionTableViewHeaderView {
    
    //MARK:- Properties
    var viewType : Constants.AFSectionType = Constants.AFSectionType.unSpecified
    
    @IBOutlet var nameLabel: AFMarqueeLabel!
    @IBOutlet var priceLabel: AFLabel!
    
    func setTitleLayout(header : SupplementaryHeader?, headerType :Constants.AFSectionType ) {
        
        viewType = headerType
        
        nameLabel.text = header?.offerName
        priceLabel.attributedText = header?.price?
            .createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown,
                                                      mainStringFont: UIFont.b3GreyRight,
                                                      imageSize: CGSize(width: 10, height: 6))
    }

}
