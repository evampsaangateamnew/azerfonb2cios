//
//  OfferUsageCell.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/26/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class OfferUsageCell: UITableViewCell {
    
    @IBOutlet var remainingView         : UIView!
    @IBOutlet var iconImageView         : UIImageView!
    @IBOutlet var remainingTitleLabel   : AFMarqueeLabel!
    @IBOutlet var remainingValueLabel   : AFLabel!
    @IBOutlet var remainingProgressView : AFProgressView!
    
    
    @IBOutlet var renewalView           : UIView!
    @IBOutlet var renewalTitleLabel     : AFMarqueeLabel!
    @IBOutlet var renewalDaysLabel      : AFLabel!
    @IBOutlet var renewalProgressView   : AFProgressView!
    @IBOutlet var activatedDateLabel    : AFMarqueeLabel!
    @IBOutlet var renewalDateLabel      : AFMarqueeLabel!
    @IBOutlet var separatorView         : UIView!
    
    @IBOutlet var remainingViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var renewalViewHeightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUsageLayout(aUsage :UsageModel?, isRenewButtonEnabled: Bool? = false, isDaily: Bool = false, showSeparator: Bool) {
        
        if showSeparator {
            separatorView.backgroundColor = UIColor.afSeparator
        } else {
            separatorView.backgroundColor = UIColor.clear
        }
        
        
        // If remainingUsage or totalUsage is empty then hide usage progress view
        if (aUsage?.remainingUsage?.isBlank ?? true) ||
            (aUsage?.totalUsage?.isBlank ?? true) ||
            aUsage?.totalUsage?.isEqual("0", ignorCase: true) ?? false {
            
            remainingView.isHidden = true
            remainingViewHeightConstraint.constant = 0
            
        } else {
            
            remainingView.isHidden = false
            remainingViewHeightConstraint.constant = 42
            
            //Icon setting
            if aUsage?.iconName?.isBlank == false {
                
                iconImageView.isHidden = false
                iconImageView.image = UIImage.imageFor(key: aUsage?.iconName)
            } else {
                iconImageView.isHidden = true
            }
            
            remainingTitleLabel.text = aUsage?.remainingTitle
            
            
            if aUsage?.remainingUsage?.isHasFreeText()  ?? false ||
                aUsage?.totalUsage?.isHasFreeText()  ?? false {
                /* Check If remainingUsage or totalUsage value is 'free' ten set progress color green */
                
                remainingValueLabel.text = Localized("Title_FREE")
                
                // remainingProgressView.changeGradientLayerColors(newGradientColors: [UIColor.afGreen.cgColor,UIColor.afGreen.cgColor], newBackgroundColor: UIColor.afLightBlueGrey)
                
                remainingProgressView.setProgress(1, animated: true)
                
            } else { /* Setting usage progress*/
                
                remainingValueLabel.text = "\(aUsage?.remainingUsage ?? "") \(aUsage?.unit ?? "") / \(aUsage?.totalUsage ?? "")"
                
                remainingProgressView.changeGradientLayerColors(newGradientColors: [UIColor.afSuperPink.cgColor,UIColor.afSuperPink.cgColor], newBackgroundColor: UIColor.afLightBlueGrey)
                
                let progressValue : Double = ((aUsage?.totalUsage?.toDouble ?? 0) - (aUsage?.remainingUsage?.toDouble ?? 0)) / (aUsage?.totalUsage?.toDouble ?? 0)
                
                remainingProgressView.setProgress(Float(progressValue), animated: true)
            }
            
        }
        
        
        
        
        /* Setting activationDate and renewalDate progress */
        if aUsage?.activationDate == nil ||
            aUsage?.renewalDate  == nil {
            
            renewalView.isHidden = true
            renewalViewHeightConstraint.constant = 0
            
            
        } else {
            
            renewalView.isHidden = false
            renewalViewHeightConstraint.constant = 56
            
            if aUsage?.activationDate?.isBlank == true ||
                aUsage?.renewalDate?.isBlank == true {
                
                renewalTitleLabel.text = aUsage?.renewalTitle
                
                // set progress view value
                renewalProgressView.setProgress(1, animated: true)
                
                renewalDaysLabel.text = "0 \(Localized("Info_days"))"
                activatedDateLabel.text = ""
                renewalDateLabel.text = Localized("Info_Expired")
                
            } else {
                
                
                // Sending empty type to calculate daliy progress
                let computedValuesFromDates = AFProgressUtilities.calculateProgressValuesForMRC(from: aUsage?.activationDate ?? "",
                                                                                                to: aUsage?.renewalDate ?? "",
                                                                                                type: isDaily ? "daily" : "monthly")
                
                // set progress view value
                if computedValuesFromDates.daysLeft <= 0 {
                    renewalProgressView.setProgress(1, animated: true)
                } else {
                    
                    renewalProgressView.setProgress(computedValuesFromDates.progressValue, animated: true)
                }
                
                renewalDaysLabel.text = computedValuesFromDates.daysLeftDisplayValue
                
                // activationDate setting
                activatedDateLabel.text = "\(Localized("Info_Activated")): \(computedValuesFromDates.startDate)"
                
                if isRenewButtonEnabled == false {
                    // Bottom Renewal View
                    renewalTitleLabel.text = Localized("Title_ToExpiration")
                    
                    renewalDateLabel.text = "\(Localized("Info_Validity")): \(computedValuesFromDates.endDate)"
                } else {
                    
                    // Bottom Renewal View
                    renewalTitleLabel.text = aUsage?.renewalTitle
                    
                    // renewalDate setting
                    renewalDateLabel.text = "\(Localized("Info_RenewDate")): \(computedValuesFromDates.endDate)"
                }
            }
        }
    }
    
}
