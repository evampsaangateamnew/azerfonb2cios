//
//  OfferContentCell.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/1/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit


protocol SubsctiptionsCellDelegate {
    func starTapped(offerId :  String)
}

class SubscriptionCell: UICollectionViewCell {
    
    @IBOutlet var myContentView: AFView!
    
    @IBOutlet var tableView: AFAccordionTableView!
    @IBOutlet var buttonsContainorView: UIView!
    @IBOutlet var renewButton: AFButton!
    @IBOutlet var deactivateButton: AFButton!
    
    //Stars outlets....
    @IBOutlet weak var btnsStackView: UIStackView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    
    var delegate : SubsctiptionsCellDelegate? = nil
    var offer: SupplementaryHeader?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ExpandableTitleHeaderView.registerReusableHeaderView(with: tableView)
        
        tableView.allowMultipleSectionsOpen = false
        tableView.keepOneSectionOpen = true;
        tableView.allowsSelection = false
        
        tableView.estimatedSectionHeaderHeight = 40
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
        tableView.separatorColor = UIColor.clear
        tableView.initialOpenSections = [0]
        self.btnsStackView.isHidden = true
    }
    
    func setTitleLayout(header : SupplementaryHeader?) {
        
        self.offer = header
        
        renewButton.removeFromSuperview()
        buttonsContainorView.addSubview(renewButton)
        
        deactivateButton.removeFromSuperview()
        buttonsContainorView.addSubview(deactivateButton)
        
        renewButton.removeConstraints(renewButton?.constraints ?? [])
        deactivateButton.removeConstraints(deactivateButton?.constraints ?? [])
        
        
        if header?.isTopUp?.isButtonEnabled() ?? false {
            
            deactivateButton.isHidden = true
            
            renewButton.buttonContentType = .topup
            renewButton.setTitle(Localized("BtnTitle_Topup"), for: .normal)
            
            renewButton.isHidden = false
            
            renewButton.snp.makeConstraints { (maker) in
                maker.width.equalTo(180).priority(999)
                maker.height.equalTo(40)
                maker.centerX.equalTo(buttonsContainorView.snp.centerX)
                maker.centerY.equalTo(buttonsContainorView.snp.centerY)
            }
            renewButton.setButtonLayoutType(.superPinkText_WhitBG_Rounded)
            
        } else {
            
            renewButton.buttonContentType = .renew
            renewButton.setTitle(Localized("BtnTitle_RENEW"), for: .normal)
            
            deactivateButton.buttonContentType = .deactivate
            deactivateButton.setTitle(Localized("BtnTitle_DEACTIVATE"), for: .normal)
            
            
            if (header?.btnRenew?.isButtonEnabled() ?? false) == true &&
                (header?.btnDeactivate?.isButtonEnabled() ?? false) == true {
                
                renewButton.isHidden = false
                
                renewButton.snp.makeConstraints { (maker) in
                    maker.width.greaterThanOrEqualTo(100).priority(999)
                    maker.height.equalTo(40)
                    maker.centerX.equalTo(buttonsContainorView.snp.centerX).multipliedBy(0.5)
                    maker.centerY.equalTo(buttonsContainorView.snp.centerY)
                    maker.left.greaterThanOrEqualTo(buttonsContainorView.snp.left).offset(8)
                }
                renewButton.setButtonLayoutType(.superPinkText_WhitBG_Rounded)
                
                deactivateButton.isHidden = false
                
                deactivateButton.snp.makeConstraints { (maker) in
                    maker.width.equalTo(renewButton.snp.width).priority(999)
                    maker.height.equalTo(40)
                    maker.centerX.equalTo(buttonsContainorView.snp.centerX).multipliedBy(1.5)
                    maker.centerY.equalTo(buttonsContainorView.snp.centerY)
                    maker.left.greaterThanOrEqualTo(renewButton.snp.right).offset(8)
                    maker.right.greaterThanOrEqualTo(buttonsContainorView.snp.right).offset(-8)
                }
                
                deactivateButton.setButtonLayoutType(.whiteText_ClearBG_Rounded)
                
            } else if (header?.btnDeactivate?.isButtonEnabled() ?? false) == true {
                
                renewButton.isHidden = true
                
                deactivateButton.isHidden = false
                
                deactivateButton.snp.makeConstraints { (maker) in
                    maker.width.equalTo(180).priority(999)
                    maker.height.equalTo(40)
                    maker.centerX.equalTo(buttonsContainorView.snp.centerX)
                    maker.centerY.equalTo(buttonsContainorView.snp.centerY)
                }
                
                deactivateButton.setButtonLayoutType(.whiteText_ClearBG_Rounded)
                
            } else if (header?.btnRenew?.isButtonEnabled() ?? false) == true {
                
                deactivateButton.isHidden = true
                
                renewButton.isHidden = false
                
                renewButton.snp.makeConstraints { (maker) in
                    maker.width.equalTo(180).priority(999)
                    maker.height.equalTo(40)
                    maker.centerX.equalTo(buttonsContainorView.snp.centerX)
                    maker.centerY.equalTo(buttonsContainorView.snp.centerY)
                }
                renewButton.setButtonLayoutType(.superPinkText_WhitBG_Rounded)
                
            } else {
                deactivateButton.isHidden = true
                renewButton.isHidden = true
                
            }
            
        }
        
        
        self.btnsStackView.isHidden = false
        self.setRating()
        
    }
    
    // stars for survey
    func setRating(){
        
        if let surveyToDisplay  = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == SurveyScreenName.bundle_subscribed.rawValue }) {
            if  let  userSurvey = AFUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == self.offer?.offeringId ?? ""}) {
                if let  question =  surveyToDisplay.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                    if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                        switch answer {
                        case 0:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 1:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 2:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 3:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 4:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                        default:
                            showUnselectedStars()
                            break
                        }
                        
                    }
                } else {
                    showUnselectedStars()
            }
                
            
            } else {
               showUnselectedStars()
            }
        } else {
            self.btnsStackView?.isHidden = true
        }
    }
    
    func showUnselectedStars() {
        btn1.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
    }
    
    
    @IBAction func starTapped(_ sender: UIButton){
        if  let ofr  = self.offer?.offeringId{
            self.delegate?.starTapped(offerId: ofr )
        }
    }
}
