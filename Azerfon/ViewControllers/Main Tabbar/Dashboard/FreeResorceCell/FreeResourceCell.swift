//
//  FreeResorceCell.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/22/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class FreeResourceCell: UICollectionViewCell {
    
    var viewType : Constants.AFFreeResourceType?
    
    @IBOutlet var mainContentView       : UIView!
    @IBOutlet var titleLabel            : AFLabel!
    @IBOutlet var addButton             : UIButton!
    @IBOutlet var valueLabel            : AFLabel!
    @IBOutlet var progressView          : AFProgressView!
    @IBOutlet var unitLabel             : AFLabel!
    
    @IBOutlet var leftSepratorView      : UIView!
    @IBOutlet var rightSepratorView     : UIView!
    
    func setLayoutValues(freeResourceData : FreeResourceAndRoaming?, type : Constants.AFFreeResourceType, isRoaming: Bool, itemIndex :Int) {
        // mainContentView.backgroundColor = .afWhite
        
        if let freeResorce = freeResourceData {
            
            titleLabel.text = freeResorce.resourcesTitleLabel
            valueLabel.text = freeResorce.remainingFormatted
            unitLabel.text = freeResorce.resourceUnitName
            
            if freeResorce.resourceInitialUnits.toFloat <= 0 {
                progressView.setProgress(0, animated: true)
            
            } else {
                
                let value = (freeResorce.resourceRemainingUnits.toFloat / freeResorce.resourceInitialUnits.toFloat)
                progressView.setProgress(value, animated: true)
            }
        } else {
            titleLabel.text = type.rawValue
            valueLabel.text = "0"
            unitLabel.text = type.unitString()
            progressView.setProgress(0, animated: true)
        }
        
        rightSepratorView.backgroundColor = .clear
        
        if itemIndex >= 1 {
            leftSepratorView.backgroundColor = .afVeryLightPinkTwo
        } else {
            leftSepratorView.backgroundColor = .clear
        }
        
        /*
         if type == .internet {
         leftSepratorView.backgroundColor = .afVeryLightPinkTwo
         rightSepratorView.backgroundColor = .afVeryLightPinkTwo
         } else {
         leftSepratorView.backgroundColor = .clear
         rightSepratorView.backgroundColor = .clear
         }
         */
    }
}
