//
//  DashboardVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/13/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
import KYDrawerController

class DashboardVC: BaseVC {
    
    //MARK: - Properties
    var currentUserType :Constants.AFUserDashboardType = .prepaid
    var homepageData :HomePageModel?
    var isRoamingSelected :Bool = false
    
    //MARK: - IBOutlet
    @IBOutlet var parentScrollView                  : UIScrollView!
    @IBOutlet var notificartionCountLabel           : AFLabel?
    @IBOutlet var balanceTitleLabel                 : AFLabel!
    @IBOutlet var balanceValueLabel                 : AFMarqueeLabel!
    @IBOutlet var bonusLabel                        : AFLabel!
    
    @IBOutlet var roamingContentView                : AFView!
    @IBOutlet var roamingButton                     : UIButton!
    @IBOutlet var exchangeButton                    : UIButton!
    @IBOutlet var roamingNoDataLabel                : AFLabel!
    @IBOutlet var freeResourceCollectionView        : UICollectionView!
    
    @IBOutlet var postpaidAmountView                : UIView!
    @IBOutlet var creditLimitView                   : UIView!
    @IBOutlet var creditLimitTitleLabel             : AFLabel!
    @IBOutlet var creditLimitValueLabel             : AFMarqueeLabel!
    
    @IBOutlet var totalPaymentView                  : UIView!
    @IBOutlet var totalPaymentTitleLabel            : AFLabel!
    @IBOutlet var totalPaymentValueLabel            : AFMarqueeLabel!
    
    @IBOutlet var outstandingDeptView               : UIView!
    @IBOutlet var outstandingDeptTitleLabel         : AFLabel!
    @IBOutlet var outstandingDeptValueLabel         : AFMarqueeLabel!
    
    @IBOutlet var postpaidAmountHeightConstraint    :NSLayoutConstraint!
    @IBOutlet var creditLimitHeightConstraint       :NSLayoutConstraint!
    @IBOutlet var totalPaymentHeightConstraint      :NSLayoutConstraint!
    @IBOutlet var outstandingDeptHeightConstraint   :NSLayoutConstraint!
    
    @IBOutlet var currentPackageLabel               : AFMarqueeLabel!
    @IBOutlet var currentStatusLabel                : AFLabel!
    
    @IBOutlet var creditView                        : UIView!
    @IBOutlet var creditTitleLabel                  : AFLabel!
    @IBOutlet var creditValueLabel                  : AFLabel!
    @IBOutlet var creditProgressView                :AFProgressView!
    @IBOutlet var creditProgressValueLabel          : AFLabel!
    @IBOutlet var creditProgressDateLabel           : AFLabel!
    @IBOutlet var creditProgressDaysLabel           : AFLabel!
    
    
    @IBOutlet var mrcView                           : UIView!
    @IBOutlet var mrcAmountView                     : UIView!
    @IBOutlet var mrcTitleLabel                     : AFMarqueeLabel!
    @IBOutlet var mrcValueLabel                     : AFLabel!
    @IBOutlet var mrcProgressView                   : AFProgressView!
    @IBOutlet var mrcProgressValueLabel             : AFLabel!
    @IBOutlet var mrcProgressDateLabel              : AFLabel!
    @IBOutlet var mrcProgressDaysLabel              : AFLabel!
    
    
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        notificartionCountLabel?.roundAllCorners(radius: 9)
        creditProgressView.changeGradientLayerColors(newGradientColors: [UIColor.afWhite.cgColor,UIColor.afWhite.cgColor], newBackgroundColor: UIColor.afBerry)
        mrcProgressView.changeGradientLayerColors(newGradientColors: [UIColor.afWhite.cgColor,UIColor.afWhite.cgColor], newBackgroundColor: UIColor.afBerry)
        
        creditProgressView.setProgress(1, animated: true)
        mrcProgressView.setProgress(1, animated: true)
        
        freeResourceCollectionView.delegate = self
        freeResourceCollectionView.dataSource = self
        
        /* Add pull to refresh */
        addPullToRefreshControalToScrollView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewContent()
        
        if AFUserSession.shared.shouldReloadDashboardData {
            
            /* Reset check and load dashboard information */
            AFUserSession.shared.shouldReloadDashboardData = false
            
            /* Call dash board API to load latest info */
            getDashboardInformation(sender: nil, shouldShowLoader: true, callOtherAPIS: false)
            
        } else {
            /*  Load updated information of dashboard */
            updateDashBoardInformation()
        }
        
        //checking dynamic link redirection
        if AFUserSession.shared.dynamicLinkRedirectionURL == nil || AFUserSession.shared.dynamicLinkRedirectionURL == "" {
            // do nothing
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.01) {
                BaseVC.redirectUserToDeepLinkScreen(deepLinkStr: AFUserSession.shared.dynamicLinkRedirectionURL ?? "dynamic link is not found" )
                AFUserSession.shared.dynamicLinkRedirectionURL = nil
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /* show RateUs alert if user curently on dashboard */
        self.checkAndLoadRateUsAlert()
        
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "DashboardScreenViews")+1, forKey: "DashboardScreenViews")
        UserDefaults.standard.synchronize()
        self.showInAppSurvey()
    }
    
    //MARK: - IBAction
    @IBAction func roamingPressed(_ sender: UIButton) {
        
        isRoamingSelected = !isRoamingSelected
        /*if isRoamingSelected {
         roamingContentView.addBubleAnimation(circleColor: .black)
         } else {
         roamingContentView.addBubleAnimation(circleColor: .afWhite)
         }*/
        
        /* Update roaming button layout */
        updateRoamingButtonLayout()
        
        /* reload free resource information */
        self.freeResourceCollectionView.reloadData()
    }
    
    @IBAction func notificationPressed(_ sender: UIButton) {
        
        /* Update notification count */
        AFUserSession.shared.loggedInUsers?.currentUser?.appConfig?.unreadNotificationCount = "0"
        AFUserSession.shared.saveLoggedInUsersCurrentStateInfo()
        self.updateNotificationCount()
        
        if let notifications :NotificationsVC = NotificationsVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(notifications, animated: true)
        }
        
    }
    
    @IBAction func addFreeResourcePressed(_ sender: UIButton) {
        print(sender.tag)
        
        if let supplementaryVC :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard(),
            let contentType = homepageData?.freeResources?.getTypeFreeResourcesFor(Roaming: isRoamingSelected, userType: currentUserType),
            contentType.count > 0,
            contentType.indices.contains(sender.tag) {
            
            switch contentType[sender.tag] {
                
            case .internet:
                supplementaryVC.selectedTabType = .internet
            case .sms:
                supplementaryVC.selectedTabType = .sms
            case .call:
                supplementaryVC.selectedTabType = .call
            }
            self.navigationController?.pushViewController(supplementaryVC, animated: true)
        }
    }
    
    @IBAction func currentPackagePressed(_ sender: UIButton) {
        if let indexForTariff = AFUserSession.shared.appMenu?.menuHorizontal?.index(where: { $0.identifier == Constants.HorizontalMenus.tariffs.rawValue }) {
            
            // Get instance of tariff viewController
            if let tariffVC = self.tabBarController?.viewControllers?[indexForTariff] as? TariffVC {
                tariffVC.redirectToTariff = (true, AFUserSession.shared.userInfo?.offeringId)
            }
            self.tabBarController?.selectedIndex = indexForTariff
        }
        
        print("currentPackagePressed")
    }
    
    @IBAction func addCreditPressed(_ sender: UIButton) {
        if let loanMainVC :LoanMainVC = LoanMainVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(loanMainVC, animated: true)
        }
    }
    
    @IBAction func exchangeServicePressed(_ sender: UIButton) {
        if let exchangeServcVC : ExchangeServiceVC = ExchangeServiceVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(exchangeServcVC, animated: true)
        }
    }
    
    @IBAction func mainBalancePressed(_ sender: UIButton) {
        if let topUpVC :TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(topUpVC, animated: true)
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
    }
    
    /** Add Pull to refresh to get latest dashboard data */
    func addPullToRefreshControalToScrollView() {
        
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(self,
                                 action: #selector(didPulledToRefresh),
                                 for: .valueChanged)
        if #available(iOS 10.0, *) {
            
            parentScrollView.refreshControl = refreshControl
        } else {
            parentScrollView.addSubview(refreshControl)
        }
    }
    
    @objc func didPulledToRefresh(sender: UIRefreshControl?) {
        getDashboardInformation(sender: sender, shouldShowLoader: false)
    }
    
    func loadDashboardInformation() -> Bool {
        
        var hasDashboardInfo :Bool = false
        
        if let homepageHandler :HomePageModel = HomePageModel.loadFromUserDefaults(key: APIsType.homePage.selectedLocalizedAPIKey()) {
            
            self.homepageData = homepageHandler
            AFUserSession.shared.homePageData = homepageHandler
            hasDashboardInfo = true
            
        } else {
            self.homepageData = nil
            AFUserSession.shared.homePageData = nil
            hasDashboardInfo = false
        }
        /* Load updated information of dashboard */
        self.updateDashBoardInformation()
        
        return hasDashboardInfo
    }
    
    func updateDashBoardInformation() {
        
        if self.homepageData == nil,
            let homepageHandler :HomePageModel = HomePageModel.loadFromUserDefaults(key: APIsType.homePage.selectedLocalizedAPIKey()) {
            self.homepageData = homepageHandler
            AFUserSession.shared.homePageData = homepageHandler
        }
        
        
        /* If viewController view is not loaded yet and called after Menu API then don't load information*/
        if self.isViewLoaded  == false {
            return
        }
        
        /* Get user type */
        currentUserType = AFUserSession.shared.userType
        
        if let offeringName = homepageData?.offeringNameDisplay,
            offeringName.isBlank == false {
            currentPackageLabel.text = offeringName
        } else {
            currentPackageLabel.text = AFUserSession.shared.userInfo?.offeringNameDisplay
        }
        
        
        if let statusString = homepageData?.status,
            statusString.isBlank == false {
            currentStatusLabel.text = "•\(statusString)"
        } else {
            currentStatusLabel.text = "•\(AFUserSession.shared.userInfo?.status ?? "")"
        }
        
        // currentPackageLabel.text = AFUserSession.shared.userInfo?.offeringNameDisplay
        // currentStatusLabel.text = "•\(AFUserSession.shared.userInfo?.status ?? "")"
        // currentStatusLabel.textColor = AFUserSession.shared.isStatusColor()
        
        var mrcInfo :Mrc?
        var creditInfo :Credit?
        
        switch currentUserType {
            
        case .prepaid, .prepaidFull, .prepaidDataSIM:
            
            postpaidAmountHeightConstraint.constant = 0
            creditLimitHeightConstraint.isActive = false
            totalPaymentHeightConstraint.isActive = false
            outstandingDeptHeightConstraint.isActive = false
            postpaidAmountView.isHidden = true
            
            creditView.isHidden = false
            mrcView.isHidden = false
            bonusLabel.isHidden = false
            
            /*if currentUserType == .prepaidFull {
                exchangeButton.isHidden = false
            } else {
                exchangeButton.isHidden = true
            }*/
            if currentUserType == .prepaid || currentUserType == .prepaidFull {
                exchangeButton.isHidden = false
            } else {
                exchangeButton.isHidden = true
            }
            
            if let prepaidBalanceInfo = homepageData?.balance?.prepaid {
                self.balanceTitleLabel.text = prepaidBalanceInfo.mainWallet?.balanceTypeName
                
                var balanceLableColor = UIColor.afPurplishBrown
                
                if let lowerLimit = prepaidBalanceInfo.mainWallet?.lowerLimit?.toDouble,
                    let amount = prepaidBalanceInfo.mainWallet?.amount?.toDouble,
                    amount <= lowerLimit{
                    
                    balanceLableColor = UIColor.afDarkPink
                }
                
                balanceValueLabel.attributedText = (prepaidBalanceInfo.mainWallet?.amount ?? "0.0").createAttributedStringWithManatSignImage(mainStringColor: balanceLableColor, mainStringFont: UIFont.h0Grey, imageSize: CGSize(width: 22, height: 14))
                
                //check if wallet amount is <=0 than hide it else show it
                if prepaidBalanceInfo.bounusWallet?.amount?.toDouble ?? 0 > 0 {
                    if let amount = prepaidBalanceInfo.bounusWallet?.amount?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.b3LightGrey, imageSize: CGSize(width: 7, height: 4)) {
                        
                        let balanceTypeName = "\(prepaidBalanceInfo.bounusWallet?.balanceTypeName ?? "") ".createAttributedString(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.b3LightGrey)
                        
                        let bounceValue = NSMutableAttributedString(attributedString: balanceTypeName)
                        bounceValue.append(amount)
                        
                        bonusLabel.attributedText = bounceValue
                        
                    } else {
                        bonusLabel.text = ""
                    }
                } else {
                    bonusLabel.text = ""
                }
                
            } else {
                self.balanceTitleLabel.text = ""
                self.balanceValueLabel.attributedText = "0.0".createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h0Grey, imageSize: CGSize(width: 22, height: 14))
                self.bonusLabel.text = ""
            }
            
            /* Get MRC and Credit info */
            mrcInfo = homepageData?.mrc
            creditInfo = homepageData?.credit
            
            break
            
        case .postpaid, .postpaidDataSIM:
            
            postpaidAmountHeightConstraint.constant = 64
            creditLimitHeightConstraint.isActive = true
            totalPaymentHeightConstraint.isActive = true
            outstandingDeptHeightConstraint.isActive = true
            postpaidAmountView.isHidden = false
            
            mrcView.isHidden = false
            mrcAmountView.isHidden = false
            bonusLabel.isHidden = true
            
            exchangeButton.isHidden = true
            
            if let postpaidBalanceInfo = homepageData?.balance?.postpaid {
                
                self.balanceTitleLabel.text = postpaidBalanceInfo.balanceLabel
                
                
                var balanceLableColor = UIColor.afPurplishBrown
                
                /* these is remove after comunication with Arif
                 let lowerLimit = postpaidBalanceInfo.currentCreditLimit?.toDouble*/
                
                if let lowerLimit = homepageData?.balance?.prepaid?.mainWallet?.lowerLimit?.toDouble,
                    let amount = postpaidBalanceInfo.balanceIndividualValue?.toDouble,
                    amount <= lowerLimit{
                    
                    balanceLableColor = UIColor.afDarkPink
                }
                
                balanceValueLabel.attributedText = postpaidBalanceInfo.balanceIndividualValue?.createAttributedStringWithManatSignImage(mainStringColor: balanceLableColor, mainStringFont: UIFont.h0Grey, imageSize: CGSize(width: 22, height: 14))
                
                /* Credit Limit balance */
                creditLimitTitleLabel.text = postpaidBalanceInfo.currentCreditLimitLabel
                creditLimitValueLabel.attributedText = postpaidBalanceInfo.currentCreditLimit?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afWhite, mainStringFont: UIFont.h1Grey, imageSize: CGSize(width: 11, height: 7))
                
                
                /* Total Payments */
                totalPaymentTitleLabel.text = postpaidBalanceInfo.totalPaymentsLabel
                totalPaymentValueLabel.attributedText = postpaidBalanceInfo.totalPayments?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afWhite, mainStringFont: UIFont.h1Grey, imageSize: CGSize(width: 11, height: 7))
                
                /* Outstanding Dept */
                outstandingDeptTitleLabel.text = postpaidBalanceInfo.outstandingIndividualDebtLabel
                outstandingDeptValueLabel.attributedText = postpaidBalanceInfo.outstandingIndividualDebt?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afWhite, mainStringFont: UIFont.h1Grey, imageSize: CGSize(width: 11, height: 7))
                
                
            } else {
                self.balanceTitleLabel.text = ""
                self.balanceValueLabel.text = ""
            }
            
            /* Get MRC and Credit info */
            mrcInfo = homepageData?.mrc
            
            /*
             if currentUserType == .postpaid {
             creditInfo = homepageData?.credit
             } else {
             creditInfo = nil
             }
             */
            
            creditInfo = nil
            
            break
        }
        
        if let creditData = creditInfo {
            creditView.isHidden = false
            creditTitleLabel.text = creditData.creditTitleLabel
            creditValueLabel.attributedText = creditData.creditTitleValue?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afWhite, mainStringFont: UIFont.h2Grey, imageSize: CGSize(width: 11, height: 7))
            creditProgressValueLabel.text = creditData.creditDateLabel
            
            
            let computedValuesFromDates = AFProgressUtilities.calculateProgressValues(from: creditData.creditInitialDate ?? "",
                                                                                      to: creditData.creditDate ?? "",
                                                                                      dateFormate: Constants.kNewAPIFormat,
                                                                                      displayDateFormate: Constants.kDisplayFormat,
                                                                                      AdditionalValueInEndDate: -1)
            
            creditProgressDateLabel.text = computedValuesFromDates.endDate
            
            if let totalDays = creditData.daysDifferenceTotal?.toInt,
                let daysDiffrance = creditData.daysDifferenceCurrent?.toInt {
                
                let remainingDays = (totalDays - daysDiffrance)
                creditProgressDaysLabel.text = "\(remainingDays >= 0 ? remainingDays : 0) \(Localized("Info_days"))"
                
                let progressPercentage = Float(totalDays - remainingDays)/Float(totalDays)
                
                creditProgressView.setProgress(progressPercentage, animated: true)
                
            } else {
                creditProgressDaysLabel.text = ""
                creditProgressView.setProgress(1, animated: true)
            }
            /*
            creditProgressDaysLabel.text = computedValuesFromDates.daysLeftDisplayValue
            creditProgressDateLabel.text = computedValuesFromDates.endDate
            creditProgressView.setProgress(computedValuesFromDates.progressValue, animated: true)
             */
            
        } else {
            creditView.isHidden = true
        }
        
        /* */
        if let mrcData = mrcInfo,
            mrcData.mrcTitleLabel?.isBlank == false,
            mrcData.mrcTitleValue?.isBlank == false {
            mrcView.isHidden = false
            mrcTitleLabel.text = mrcData.mrcTitleLabel
            mrcValueLabel.attributedText = mrcData.mrcTitleValue?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afWhite, mainStringFont: UIFont.h2Grey, imageSize: CGSize(width: 11, height: 7))
            
            mrcProgressValueLabel.text = mrcData.mrcDateLabel
            mrcProgressDateLabel.text = mrcData.mrcDate
            
            if mrcInfo?.mrcDate?.isBlank == false &&
                mrcInfo?.mrcInitialDate?.isBlank == false {
            }
            
            let computedValuesFromDates = AFProgressUtilities.calculateProgressValuesForMRC(from: mrcInfo?.mrcInitialDate ?? "",
                                                                                            to: mrcInfo?.mrcDate ?? "",
                                                                                            type: mrcInfo?.mrcType ?? "",
                                                                                            dateFormate: Constants.kNewAPIFormat,
                                                                                            displayDateFormate: Constants.kDisplayFormat)
            
            /* MRC Expired check added */
            if let mrcDateString = mrcInfo?.mrcDate,
                let mrcDate  = AFUserSession.shared.dateFormatter.createDate(from: mrcDateString, dateFormate: Constants.kNewAPIFormat),
                mrcDate <= Date().todayDate(dateFormate: Constants.kNewAPIFormat){
                
                mrcProgressDaysLabel.text = Localized("Info_Expired")
                
            } else {
                mrcProgressDaysLabel.text = computedValuesFromDates.daysLeftDisplayValue
            }
            
            
            mrcProgressDateLabel.text = computedValuesFromDates.endDate
            mrcProgressView.setProgress(computedValuesFromDates.progressValue, animated: true)
            
        } else {
            mrcView.isHidden = true
        }
        
        /* Update roaming button layout */
        updateRoamingButtonLayout()
        /* reload free resource information */
        freeResourceCollectionView.reloadData()
        
        /* set unread count */
        updateNotificationCount()
        
        // Redirect user to notification screen if user select screen when application is close
        if AFUserSession.shared.isPushNotificationSelected {
            
            // Reset to false
            AFUserSession.shared.isPushNotificationSelected = false
            
            self.notificationPressed(UIButton())
        }
        
        /* Check and show promo message */
        if  self.homepageData != nil &&
            (AFUserSession.shared.promoMessage?.isBlank ?? true) == false {
            
            /* Show promo message to user */
            self.showAlert(title: "", message: AFUserSession.shared.promoMessage)
            /* Clear Promo message, Only show it once */
            AFUserSession.shared.promoMessage = nil
        }
    }
    
    func updateNotificationCount() {
        
        /* Setting Unreead Notification Count */
        
        if let notificationCount = AFUserSession.shared.loggedInUsers?.currentUser?.appConfig?.unreadNotificationCount,
            notificationCount.toInt > 0 {
            self.notificartionCountLabel?.text = notificationCount
            self.notificartionCountLabel?.isHidden = false
        } else {
            self.notificartionCountLabel?.text = ""
            self.notificartionCountLabel?.isHidden = true
        }
        
    }
    
    func checkAndLoadRateUsAlert() {
        
        /* Check if dashboard visible then check where to show rateus or not. */
        if let kyDrawerController = self.navigationController?.visibleViewController,
            kyDrawerController.isKind(of: KYDrawerController.classForCoder()),
            let selectedVC = self.tabBarController?.selectedViewController,
            selectedVC.isKind(of: self.classForCoder),
            (AFUserSession.shared.userInfo?.rateus_ios?.isEqual("0", ignorCase: true) ?? false),
            let parentTabbarController = self.tabBarController as? TabbarController,
            parentTabbarController.isAppResumeAPICallInProcess == false {
            
            var showRateUsAlert :Bool = false
            
            /* Check is Rate Us is presented to user before*/
            if AFUtilities.isRateUsShownBefore() == false {
                
                let timeDiffranceInHours = AFDateUtilities.calculateTotalHoursBetweenTwoDates(startingDate: AFUtilities.getLoginTime(), endingDate: Date().todayDateString(), dateFormate: Constants.kNewAPIFormat)
                
                if timeDiffranceInHours >= (AFUserSession.shared.userInfo?.firstPopup?.toInt ?? 0) {
                    showRateUsAlert = true
                }
                
            } else {
                let timeDiffranceInDays = AFDateUtilities.calculateTotalDaysBetween(startingDate: AFUtilities.getRateUsLaterTime(), endingDate: Date().todayDateString(), dateFormate: Constants.kNewAPIFormat)
                
                if timeDiffranceInDays >= (AFUserSession.shared.userInfo?.lateOnPopup?.toInt ?? 0) {
                    showRateUsAlert = true
                }
            }
            
            /* Show Alert if it matched above conditions */
            if showRateUsAlert {
                if let rateUsAlert : RateUsAlertVC = RateUsAlertVC.fromNib() {
                    
                    rateUsAlert.setRateUs(titleMessage: AFUserSession.shared.userInfo?.popupTitle, {
                        
                        self.redirectUserToAppStore()
                    })
                    
                    kyDrawerController.presentPOPUP(rateUsAlert, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    func updateRoamingButtonLayout() {
        if isRoamingSelected {
            roamingButton.setImage(UIImage(named: Localized("Img_Btn_Roaming_selected")), for: .normal)
        } else {
            roamingButton.setImage(UIImage(named: Localized("Img_Btn_Roaming")), for: .normal)
        }
    }
    
    //MARK: - API's Calls
    
    /** call to get Home page Information and Stop UIRefresh controal animation if loading */
    func getDashboardInformation(sender: UIRefreshControl?, shouldShowLoader :Bool = true, callOtherAPIS :Bool = true) {
        
        if shouldShowLoader {
            self.showActivityIndicator()
        }
        _ = AFAPIClient.shared.getHomePage({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            sender?.endRefreshing()
            
            if shouldShowLoader {
                self.hideActivityIndicator()
            }
            
            if error != nil {
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.AFAPIStatusCode.succes.rawValue ,
                    let homepageHandler = Mapper<HomePageModel>().map(JSONObject: resultData) {
                    
                    /*Save data into user defaults*/
                    homepageHandler.saveInUserDefaults(key: APIsType.homePage.selectedLocalizedAPIKey())
                    
                    /* Overwrite home page info */
                    self.homepageData = homepageHandler
                    AFUserSession.shared.homePageData = homepageHandler
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            /* Load updated information of dashboard */
            self.updateDashBoardInformation()
            
            if callOtherAPIS {
                /* Load number of unread notification */
                self.getNotificationsCount()
                
                /* Load Rate application configration */
                self.getRateUsConfig()
                
                /* Check and call FCM Token registration API */
                if (AFUserSession.shared.appConfig?.fcmTokenRegisterdForLogin ?? false) == false {
                    /* Try to add FCM id if this is not added previosly. */
                    AFAddFCMUtilities.addFCMId(sucesscompletion: {
                        
                        // AFUserSession.shared.loggedInUsers?.currentUser?.appConfig?.fcmTokenRegisterdForLogin = true
                        AFUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                        
                    }, failureCompletion: {
                        
                        AFUserSession.shared.loggedInUsers?.currentUser?.appConfig?.fcmTokenRegisterdForLogin = false
                        AFUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                    })
                }
            }
        })
    }
    
    /**
     call to get unread Notifications count Information.
     */
    func getNotificationsCount() {
        
        /*API call to get data*/
        _ = AFAPIClient.shared.getNotificationsCount({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            // handling data from API response.
            if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                let notificationsCountResponse = Mapper<NotificationsCountModel>().map(JSONObject:resultData) {
                
                /* Saving notification unread count */
                AFUserSession.shared.loggedInUsers?.currentUser?.appConfig?.unreadNotificationCount = notificationsCountResponse.notificationUnreadCount
                AFUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                /* set unread count */
                self.updateNotificationCount()
            }
        })
    }
    
    /**
     call to get RateUS.
     - returns: void
     */
    func getRateUsConfig() {
        
        /*API call to get data*/
        _ = AFAPIClient.shared.getRateUs( { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if resultCode == Constants.AFAPIStatusCode.succes.rawValue,
                let rateUsResponse = Mapper<RateUsModel>().map(JSONObject:resultData) {
                
                AFUserSession.shared.loggedInUsers?.currentUser?.userInfo?.rateus_ios = rateUsResponse.rateus_ios
                AFUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                
                /* show RateUs alert if user curently on dashboard */
                self.checkAndLoadRateUsAlert()
                
            }
        })
    }
    
    
    
    func showInAppSurvey(){
        if let survey = AFUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.dashboard.rawValue}),
           survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
           survey.visitLimit != "-1",
           survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "DashboardScreenViews") {
            if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                inAppSurveyVC.showTopUpStackView = false
                inAppSurveyVC.currentSurvay = survey
                inAppSurveyVC.surveyScreen = SurveyScreenName.dashboard.rawValue
                self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
            }
            UserDefaults.standard.set(0, forKey: "DashboardScreenViews")
            UserDefaults.standard.synchronize()
        }
    }
}

//MARK: - UICollectionViewDelegate
extension DashboardVC : UICollectionViewDelegate, UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if let contentType = homepageData?.freeResources?.getTypeFreeResourcesFor(Roaming: isRoamingSelected, userType: currentUserType),
            contentType.count == 1 {
            
            let collectionViewWidth = (UIScreen.main.bounds.width - 60)
            let leftInset = collectionViewWidth / 3
            let rightInset = leftInset
            
            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        } else {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth = (UIScreen.main.bounds.width - 60)
        
        if let contentType = homepageData?.freeResources?.getTypeFreeResourcesFor(Roaming: isRoamingSelected, userType: currentUserType),
            contentType.count == 2 {
            
            return CGSize(width: collectionViewWidth / 2, height: 85)
            
        } else {
            return CGSize(width: collectionViewWidth / 3, height: 85)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let dataCount = homepageData?.freeResources?.getTypeFreeResourcesFor(Roaming: isRoamingSelected, userType: currentUserType).count ?? 0
        if dataCount <= 0 {
            roamingNoDataLabel.text = Localized("Info_NoRoamingData")
            roamingNoDataLabel.isHidden = false
        } else {
            roamingNoDataLabel.text = ""
            roamingNoDataLabel.isHidden = true
        }
        
        return dataCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FreeResourceCell", for: indexPath) as? FreeResourceCell {
            
            if let contentType = homepageData?.freeResources?.getTypeFreeResourcesFor(Roaming: isRoamingSelected, userType: currentUserType),
                contentType.count > 0 {
                
                switch contentType[indexPath.item] {
                case .internet:
                    cell.setLayoutValues(freeResourceData: homepageData?.freeResources?.getFreeResourcesFor(type: .internet, forRoaming: isRoamingSelected),
                                         type: .internet,
                                         isRoaming: isRoamingSelected,
                                         itemIndex: indexPath.item)
                case .sms:
                    cell.setLayoutValues(freeResourceData: homepageData?.freeResources?.getFreeResourcesFor(type: .sms, forRoaming: isRoamingSelected),
                                         type: .sms,
                                         isRoaming: isRoamingSelected,
                                         itemIndex: indexPath.item)
                case .call:
                    cell.setLayoutValues(freeResourceData: homepageData?.freeResources?.getFreeResourcesFor(type: .call, forRoaming: isRoamingSelected),
                                         type: .call,
                                         isRoaming: isRoamingSelected,
                                         itemIndex: indexPath.item)
                }
            } else {
                cell.setLayoutValues(freeResourceData: nil,
                                     type: .internet,
                                     isRoaming: isRoamingSelected,
                                     itemIndex: indexPath.item)
            }
            
            cell.addButton.tag = indexPath.item
            cell.addButton.addTarget(self, action: #selector(addFreeResourcePressed), for: .touchUpInside)
            return cell
            
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let mySubscriptionsVC :MySubscriptionsVC = MySubscriptionsVC.instantiateViewControllerFromStoryboard(),
            let contentType = homepageData?.freeResources?.getTypeFreeResourcesFor(Roaming: isRoamingSelected, userType: currentUserType),
            contentType.count > 0,
            contentType.indices.contains(indexPath.item) {
            
            switch contentType[indexPath.item] {
                
            case .internet:
                mySubscriptionsVC.selectedTabType = .internet
            case .sms:
                mySubscriptionsVC.selectedTabType = .sms
            case .call:
                mySubscriptionsVC.selectedTabType = .call
            }
            mySubscriptionsVC.redirectedFromHomeScreen = true
            self.navigationController?.pushViewController(mySubscriptionsVC, animated: true)
        }
    }
}
