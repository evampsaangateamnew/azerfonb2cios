//
//  BaseVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper


class BaseVC: UIViewController {
    
    var isPopGestureEnabled : Bool = false
    
    @IBOutlet var titleLabel: AFLabel?
    @IBOutlet var backButton: UIButton?
    @IBOutlet var menuButton: UIButton?
    @IBOutlet var titleBackgroundImageView: UIImageView?
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //MARK:- viewController Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .afSuperPink
        self.titleBackgroundImageView?.image = UIImage(named: "nav_bg_main")
        setStatusBarBackgroundColor(color: .afSuperPink)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        /* if Configration is debug then logout */
        #if DEBUG
        //BaseVC.logout()
        #endif
        
        /* if device is jailbroken then logout */
        if UIDevice.isDeviceJailbroken() {
            BaseVC.logout()
        }
    }
    
    //MARK:- Functions
    
    /**
     Set color of status bar.
     
     - parameter color: Color which will be set to status bar.
     
     - returns: void
     */
    func setStatusBarBackgroundColor(color: UIColor) {
        let tag = 13254
        if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
            statusBar.backgroundColor = color
        } else {
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            statusBarView.tag = tag
            UIApplication.shared.keyWindow?.addSubview(statusBarView)
            statusBarView.backgroundColor = color
        }
    }
    
    /**
     Clear user information from userDefaults and navigate user to login screen.
     
     - returns: void
     */
    class public func logout() {
        
        // try to get rootView controller and pop accordingly
        if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
            
            // Clear information in userDefaults
            AFUserSession.shared.clearUserSession()
            
            navigationController.popToLoginViewController()
            
            AFAPIClient.shared.cancelAllRequests()
        } else {
            UIApplication.shared.delegate?.window??.rootViewController?.navigationController?.popToRootViewController(animated: true)
        }
        AFActivityIndicator.shared.removeAllActivityIndicator()
    }
    
    
    /**
     Redirect user to notification screen if he tab on notification.
     
     - returns: void
     */
    class func redirectUserToNotificationScreen() {
        
        // check user if logged in
        if AFUserSession.shared.isLoggedIn() {
            
            // search for Notifications screen from naviagetion
            if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
                
                var isFoundNotificationController = false
                
                navigationController.viewControllers.reversed().forEach({ (aViewController) in
                    
                    if aViewController.isKind(of: NotificationsVC().classForCoder) {
                        
                        isFoundNotificationController = true
                        navigationController.popToViewController(aViewController, animated: true)
                    }
                })
                
                // check if Notification screen found in main navigation
                if isFoundNotificationController != true {
                    
                    // Search for DashBoard from current navigation
                    var isFoundController = false
                    navigationController.viewControllers.forEach({ (aViewController) in
                        
                        if aViewController.isKind(of: KYDrawerController.classForCoder()) {
                            
                            isFoundController = true
                        }
                    })
                    
                    // push notifiaction screen on KYDrawerController controller if found
                    if isFoundController {
                        
                        if let notifications :NotificationsVC = NotificationsVC.instantiateViewControllerFromStoryboard() {
                            navigationController.pushViewController(notifications, animated: true)
                        }
                        
                    } else {
                        // Enable check to redirect user to Notification screen
                        AFUserSession.shared.isPushNotificationSelected = true
                    }
                }
            }
        } else {
            // Enable check to redirect user to Notification screen
            AFUserSession.shared.isPushNotificationSelected = true
        }
    }
    
    /*
     Rexdirectiing user to the Deeplink identifier screen
     */
    class func redirectUserToDeepLinkScreen(deepLinkStr:String) {
       
       // check user if logged in
       if AFUserSession.shared.isLoggedIn() {
        
        AFUserSession.shared.dynamicLinkRedirectionURL = ""
           
           // search for Notifications screen from naviagetion
        let rootVC = UIApplication.shared.delegate?.window??.rootViewController
        /*if #available(iOS 13.0, *) {
            guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                  let sceneDelegate = windowScene.delegate as? SceneDelegate
            else {
                return
            }
            rootVC = sceneDelegate.window?.rootViewController
        } else {
            // Fallback on earlier versions
        }*/
           if let navigationController  = rootVC as? MainNavigationController {
               
               var isFoundNotificationController = false
               
               navigationController.viewControllers.reversed().forEach({ (aViewController) in
                
                if deepLinkStr.containsSubString(subString: "Side_Menu") {
                    if deepLinkStr.containsSubString(subString: "notifications"){
                        if aViewController.isKind(of: NotificationsVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    } else if deepLinkStr.containsSubString(subString: "contact_us"){
                        if aViewController.isKind(of: ContactUsVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    } else if deepLinkStr.containsSubString(subString: "tutorial_and_faqs"){
                        if aViewController.isKind(of: TutorialsVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    } else if deepLinkStr.containsSubString(subString: "settings"){
                        if aViewController.isKind(of: SettingsVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    } else if deepLinkStr.containsSubString(subString: "store_locator"){
                        if aViewController.isKind(of: StoreLocatorMainVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    } else if deepLinkStr.containsSubString(subString: "terms_and_conditions"){
                        if aViewController.isKind(of: TermsAndConditionsVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    } else if deepLinkStr.containsSubString(subString: "live_chat"){
                        if aViewController.isKind(of: LiveChatVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                        //openLiveChatNow()
                        
                    } else if deepLinkStr.containsSubString(subString: "faq"){
                        if aViewController.isKind(of: FAQsVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    } else if deepLinkStr.containsSubString(subString: "manage_account"){
                        if aViewController.isKind(of: ManageAccountsVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    }
                }
                else if deepLinkStr.containsSubString(subString: "Bottom_Menu") ||
                            deepLinkStr.containsSubString(subString: "BottomMenu") {
                    if deepLinkStr.containsSubString(subString: "Screen_Name=dashboard") {
                        if aViewController.isKind(of: DashboardVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    } else if deepLinkStr.containsSubString(subString: "Screen_Name=tariffs") {
                        if aViewController.isKind(of: TariffVC().classForCoder) {
                            isFoundNotificationController = true
                            if let tariffVC:TariffVC = aViewController as? TariffVC {
                                tariffVC.canUserGoBack = false
                                if deepLinkStr.containsSubString(subString: "OfferingId") {
                                    tariffVC.redirectToTariff = (true, String(deepLinkStr.split(separator: "=").last ?? ""))
                                }
                                navigationController.popToViewController(tariffVC, animated: true)
                            }
                        }
                    } else if deepLinkStr.containsSubString(subString: "Screen_Name=services") {
                        if let drawer = navigationController.viewControllers.last as? KYDrawerController ,
                            let tabBarCtr = drawer.mainViewController as? UITabBarController {
                            tabBarCtr.selectedIndex = 2
                        }
                        if deepLinkStr.containsSubString(subString: "SubScreen=services_supplementary_offers") {
                            if aViewController.isKind(of: SupplementaryVC().classForCoder) {
                                isFoundNotificationController = true
                                if let supplementaryVC :SupplementaryVC = aViewController as? SupplementaryVC {
                                    if deepLinkStr.containsSubString(subString: "TabName=Calls") {
                                        supplementaryVC.selectedTabType = .call
                                    } else if deepLinkStr.containsSubString(subString: "TabName=Internet") {
                                        supplementaryVC.selectedTabType = .internet
                                    } else if deepLinkStr.containsSubString(subString: "TabName=SMS") {
                                        supplementaryVC.selectedTabType = .sms
                                    } else if deepLinkStr.containsSubString(subString: "TabName=Hybrid") {
                                        supplementaryVC.selectedTabType = .hybrid
                                    } else if deepLinkStr.containsSubString(subString: "TabName=Campaign") {
                                        supplementaryVC.selectedTabType = .campaign
                                    } else if deepLinkStr.containsSubString(subString: "TabName=TM") {
                                        supplementaryVC.selectedTabType = .tm
                                    } else if deepLinkStr.containsSubString(subString: "TabName=Roaming") {
                                        supplementaryVC.selectedTabType = .roaming
                                    } else if deepLinkStr.containsSubString(subString: "TabName=All_Inclusive_Call") {
                                        supplementaryVC.selectedTabType = .allInclusiveCall
                                    } else if deepLinkStr.containsSubString(subString: "TabName=All_Inclusive_SMS") {
                                        supplementaryVC.selectedTabType = .allInclusiveSMS
                                    } else if deepLinkStr.containsSubString(subString: "TabName=All_Inclusive_Internet") {
                                        supplementaryVC.selectedTabType = .allInclusiveInternet
                                    }
                                    if deepLinkStr.containsSubString(subString: "OfferingId") {
                                        supplementaryVC.redirectToSupplementory = (true, String(deepLinkStr.split(separator: "=").last ?? ""))
                                    }
                                    // now will navigate to supplemmantry Offering screen
                                    navigationController.popToViewController(supplementaryVC, animated: true)
                                    
                                }
                            }
                            
                        } else if deepLinkStr.containsSubString(subString: "SubScreen=services_special_offers") {
                            if aViewController.isKind(of: SpecialOffersVC().classForCoder) {
                                isFoundNotificationController = true
                                let specialOffersVC :SpecialOffersVC = aViewController as! SpecialOffersVC
                                if deepLinkStr.containsSubString(subString: "OfferingId") {
                                    specialOffersVC.redirectToSpecial = (true, String(deepLinkStr.split(separator: "=").last ?? ""))
                                }
                                navigationController.popToViewController(specialOffersVC, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "SubScreen=services_free_sms") {
                            if aViewController.isKind(of: FreeSmsVC().classForCoder) {
                                isFoundNotificationController = true
                                navigationController.popToViewController(aViewController, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "SubScreen=services_fnf") {
                            if aViewController.isKind(of: FNFVC().classForCoder) {
                                isFoundNotificationController = true
                                navigationController.popToViewController(aViewController, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "SubScreen=services_vas_services") {
                            if aViewController.isKind(of: VASServicesVC().classForCoder) {
                                isFoundNotificationController = true
                                navigationController.popToViewController(aViewController, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "SubScreen=services_exchange_service") {
                            if aViewController.isKind(of: ExchangeServiceVC().classForCoder) {
                                isFoundNotificationController = true
                                navigationController.popToViewController(aViewController, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "SubScreen=services_nar_tv") {
                            if aViewController.isKind(of: NarTvVC().classForCoder) {
                                isFoundNotificationController = true
                                navigationController.popToViewController(aViewController, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "SubScreen=service_topup") {
                            if aViewController.isKind(of: TopUpMainVC().classForCoder) {
                                isFoundNotificationController = true
                                navigationController.popToViewController(aViewController, animated: true)
                            }
                        }
                    } else if deepLinkStr.containsSubString(subString: "Screen_Name=topup") {
                        if aViewController.isKind(of: TopUpMainVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    } else if deepLinkStr.containsSubString(subString: "Screen_Name=myaccount") {
                        if aViewController.isKind(of: MyAccountVC().classForCoder) {
                            isFoundNotificationController = true
                            navigationController.popToViewController(aViewController, animated: true)
                        }
                    }
                }
               })
               
               // check if Notification screen found in main navigation
               if isFoundNotificationController != true {
                   
                   // Search for DashBoard from current navigation
                   var isFoundController = false
                   navigationController.viewControllers.forEach({ (aViewController) in
                       
                       if aViewController.isKind(of: KYDrawerController.classForCoder()) {
                           
                           isFoundController = true
                       }
                   })
                   
                   // push notifiaction screen on KYDrawerController controller if found
                   if isFoundController {
                    
                    if deepLinkStr.containsSubString(subString: "Side_Menu") {
                        if deepLinkStr.containsSubString(subString: "notifications"){
                            if let notifications :NotificationsVC = NotificationsVC.instantiateViewControllerFromStoryboard() {
                                navigationController.pushViewController(notifications, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "contact_us"){
                            if let contactUsVC :ContactUsVC = ContactUsVC.instantiateViewControllerFromStoryboard() {
                                navigationController.pushViewController(contactUsVC, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "tutorial_and_faqs"){
                            if let tutorialsVC :TutorialsVC = TutorialsVC.instantiateViewControllerFromStoryboard() {
                                navigationController.pushViewController(tutorialsVC, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "settings"){
                            if let settingsVC :SettingsVC = SettingsVC.instantiateViewControllerFromStoryboard() {
                                navigationController.pushViewController(settingsVC, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "store_locator"){
                            if let store :StoreLocatorMainVC = StoreLocatorMainVC.instantiateViewControllerFromStoryboard() {
                                navigationController.pushViewController(store, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "terms_and_conditions"){
                            if let termsAndConditionsVC :TermsAndConditionsVC = TermsAndConditionsVC.instantiateViewControllerFromStoryboard() {
                                navigationController.pushViewController(termsAndConditionsVC, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "live_chat"){
                            if AFUserSession.shared.liveChatObject == nil ,
                                let liveChatVC :LiveChatVC = LiveChatVC.instantiateViewControllerFromStoryboard() {
                                
                                AFUserSession.shared.liveChatObject = liveChatVC
                                navigationController.pushViewController(liveChatVC, animated: true)
                                
                            } else {
                                navigationController.pushViewController(AFUserSession.shared.liveChatObject!, animated: true)
                            }
                            //openLiveChatNow()
                            
                        } else if deepLinkStr.containsSubString(subString: "faq"){
                            if let faqs :FAQsVC = FAQsVC.instantiateViewControllerFromStoryboard() {
                                navigationController.pushViewController(faqs, animated: true)
                            }
                        } else if deepLinkStr.containsSubString(subString: "manage_account"){
                            if let manageAccounts :ManageAccountsVC = ManageAccountsVC.instantiateViewControllerFromStoryboard() {
                                navigationController.pushViewController(manageAccounts, animated: true)
                            }
                        }
                    }
                    else if deepLinkStr.containsSubString(subString: "Bottom_Menu") ||
                                deepLinkStr.containsSubString(subString: "BottomMenu")  {
                        if deepLinkStr.containsSubString(subString: "Screen_Name=dashboard") {
                            if let drawer = navigationController.viewControllers.last as? KYDrawerController ,
                                let tabBarCtr = drawer.mainViewController as? UITabBarController {
                                tabBarCtr.selectedIndex = 0
                            }
                        } else if deepLinkStr.containsSubString(subString: "Screen_Name=tariffs") {
                            if let drawer = navigationController.viewControllers.last as? KYDrawerController ,
                                let tabBarCtr = drawer.mainViewController as? UITabBarController {
                                tabBarCtr.selectedIndex = 1
                                if deepLinkStr.containsSubString(subString: "OfferingId") {
                                    DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                                        if ((tabBarCtr.selectedViewController?.isKind(of: TariffVC.classForCoder())) != nil) {
                                            if let tariffVC:TariffVC = tabBarCtr.selectedViewController as? TariffVC {
                                                tariffVC.redirectToTariff = (true, String(deepLinkStr.split(separator: "=").last ?? ""))
                                                tariffVC.loadTariffDetails()
                                            }
                                        }
                                    }
                                }
                            }
                        } else if deepLinkStr.containsSubString(subString: "Screen_Name=services") {
                            if let drawer = navigationController.viewControllers.last as? KYDrawerController ,
                                let tabBarCtr = drawer.mainViewController as? UITabBarController {
                                tabBarCtr.selectedIndex = 2
                                
                                // navigating to sub classes
                                if deepLinkStr.containsSubString(subString: "SubScreen=services_supplementary_offers") {
                                    if let supplementaryVC :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard() {
                                        if deepLinkStr.containsSubString(subString: "TabName=Calls") {
                                            supplementaryVC.selectedTabType = .call
                                        } else if deepLinkStr.containsSubString(subString: "TabName=Internet") || deepLinkStr.containsSubString(subString: "TabName=Data") {
                                            supplementaryVC.selectedTabType = .internet
                                        } else if deepLinkStr.containsSubString(subString: "TabName=SMS") {
                                            supplementaryVC.selectedTabType = .sms
                                        } else if deepLinkStr.containsSubString(subString: "TabName=Hybrid") {
                                            supplementaryVC.selectedTabType = .hybrid
                                        } else if deepLinkStr.containsSubString(subString: "TabName=Campaign") {
                                            supplementaryVC.selectedTabType = .campaign
                                        } else if deepLinkStr.containsSubString(subString: "TabName=Bonuses") {
                                            supplementaryVC.selectedTabType = .hybrid
                                        }  else if deepLinkStr.containsSubString(subString: "TabName=TM") {
                                            supplementaryVC.selectedTabType = .tm
                                        } else if deepLinkStr.containsSubString(subString: "TabName=Roaming") {
                                            supplementaryVC.selectedTabType = .roaming
                                        } else if deepLinkStr.containsSubString(subString: "TabName=All_Inclusive_Call") {
                                            supplementaryVC.selectedTabType = .allInclusiveCall
                                        } else if deepLinkStr.containsSubString(subString: "TabName=All_Inclusive_SMS") {
                                            supplementaryVC.selectedTabType = .allInclusiveSMS
                                        } else if deepLinkStr.containsSubString(subString: "TabName=All_Inclusive_Internet") {
                                            supplementaryVC.selectedTabType = .allInclusiveInternet
                                        }
                                        if deepLinkStr.containsSubString(subString: "OfferingId") {
                                            supplementaryVC.redirectToSupplementory = (true, String(deepLinkStr.split(separator: "=").last ?? ""))
                                        }
                                        // now will navigate to supplemmantry Offering screen
                                        navigationController.pushViewController(supplementaryVC, animated: true)
                                        
                                    }
                                } else if deepLinkStr.containsSubString(subString: "SubScreen=services_special_offers") {
                                    if let specialOffersVC :SpecialOffersVC = SpecialOffersVC.instantiateViewControllerFromStoryboard() {
                                        if deepLinkStr.containsSubString(subString: "OfferingId") {
                                            specialOffersVC.redirectToSpecial = (true, String(deepLinkStr.split(separator: "=").last ?? ""))
                                        }
                                        navigationController.pushViewController(specialOffersVC, animated: true)
                                    }
                                } else if deepLinkStr.containsSubString(subString: "SubScreen=services_free_sms") {
                                    if let freeSmsVC :FreeSmsVC = FreeSmsVC.instantiateViewControllerFromStoryboard() {
                                        navigationController.pushViewController(freeSmsVC, animated: true)
                                    }
                                } else if deepLinkStr.containsSubString(subString: "SubScreen=services_fnf") {
                                    if let fNFVC :FNFVC = FNFVC.instantiateViewControllerFromStoryboard() {
                                        navigationController.pushViewController(fNFVC, animated: true)
                                    }
                                } else if deepLinkStr.containsSubString(subString: "SubScreen=services_vas_services") {
                                    if let vasVC :VASServicesVC = VASServicesVC.instantiateViewControllerFromStoryboard() {
                                        navigationController.pushViewController(vasVC, animated: true)
                                    }
                                } else if deepLinkStr.containsSubString(subString: "SubScreen=services_exchange_service") {
                                    if let vasVC :ExchangeServiceVC = ExchangeServiceVC.instantiateViewControllerFromStoryboard() {
                                        navigationController.pushViewController(vasVC, animated: true)
                                    }
                                } else if deepLinkStr.containsSubString(subString: "SubScreen=services_nar_tv") {
                                    if let vasVC :NarTvVC = NarTvVC.instantiateViewControllerFromStoryboard() {
                                        navigationController.pushViewController(vasVC, animated: true)
                                    }
                                } else if deepLinkStr.containsSubString(subString: "SubScreen=service_topup") {
                                    if let mainTopup :ServicesVC = tabBarCtr.selectedViewController as? ServicesVC {
                                        mainTopup.redirectUserToSelectedController(Identifier: "service_topup")
                                        if deepLinkStr.containsSubString(subString: "StepOneScreen=topup_topup")  {
                                            if let topupVc :TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard(){
                                                navigationController.pushViewController(topupVc, animated: true)
                                            }
                                        } else if deepLinkStr.containsSubString(subString: "StepOneScreen=topup_fasttopup")  {
                                            if let fastTopupVc :FastTopupVC = FastTopupVC.instantiateViewControllerFromStoryboard2(){
                                                navigationController.pushViewController(fastTopupVc, animated: true)
                                            }
                                        } else if deepLinkStr.containsSubString(subString: "StepOneScreen=topup_autopayment")  {
                                            if let autoPaymentVC :AutoPaymentVC = AutoPaymentVC.instantiateViewControllerFromStoryboard2(){
                                                navigationController.pushViewController(autoPaymentVC, animated: true)
                                            }
                                        } else if deepLinkStr.containsSubString(subString: "StepOneScreen=topup_money_transfer")  {
                                            if let moneyTransferVC :MoneyTransferVC = MoneyTransferVC.instantiateViewControllerFromStoryboard(){
                                                navigationController.pushViewController(moneyTransferVC, animated: true)
                                            }
                                        } else if deepLinkStr.containsSubString(subString: "StepOneScreen=topup_money_transfer")  {
                                            if let moneyRequestVC :MoneyRequestVC = MoneyRequestVC.instantiateViewControllerFromStoryboard(){
                                                navigationController.pushViewController(moneyRequestVC, animated: true)
                                            }
                                        } else if deepLinkStr.containsSubString(subString: "StepOneScreen=topup_loan")  {
                                            if let loanMainVC :LoanMainVC = LoanMainVC.instantiateViewControllerFromStoryboard(){
                                                navigationController.pushViewController(loanMainVC, animated: true)
                                            }
                                        } else if deepLinkStr.containsSubString(subString: "StepOneScreen=topup_loan")  {
                                            if let loanMainVC :LoanMainVC = LoanMainVC.instantiateViewControllerFromStoryboard(){
                                                navigationController.pushViewController(loanMainVC, animated: true)
                                            }
                                        }
                                    }
                                }
                            }
                            
                        } else if deepLinkStr.containsSubString(subString: "Screen_Name=topup123") {
                            if let drawer = navigationController.viewControllers.last as? KYDrawerController ,
                                let tabBarCtr = drawer.mainViewController as? UITabBarController {
                                tabBarCtr.selectedIndex = 3
                            }
                            if deepLinkStr.containsSubString(subString: "SubScreen=topup_topup") {
                                if let topUpVC :TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard() {
                                    navigationController.pushViewController(topUpVC, animated: true)
                                }
                            } else if deepLinkStr.containsSubString(subString: "SubScreen=topup_fasttopup") {
                                if let fastTopupVC :FastTopupVC = FastTopupVC.instantiateViewControllerFromStoryboard2() {
                                    navigationController.pushViewController(fastTopupVC, animated: true)
                                }
                            } else if deepLinkStr.containsSubString(subString: "SubScreen=topup_autopayment") {
                                if let autoPaymentVC :AutoPaymentVC = AutoPaymentVC.instantiateViewControllerFromStoryboard2() {
                                    navigationController.pushViewController(autoPaymentVC, animated: true)
                                }
                            } else if deepLinkStr.containsSubString(subString: "SubScreen=topup_money_transfer") {
                                if let moneyTransferVC :MoneyTransferVC = MoneyTransferVC.instantiateViewControllerFromStoryboard() {
                                    navigationController.pushViewController(moneyTransferVC, animated: true)
                                }
                            } else if deepLinkStr.containsSubString(subString: "SubScreen=topup_money_request") {
                                if let moneyRequestVC :MoneyRequestVC = MoneyRequestVC.instantiateViewControllerFromStoryboard() {
                                    navigationController.pushViewController(moneyRequestVC, animated: true)
                                }
                            } else if deepLinkStr.containsSubString(subString: "SubScreen=topup_loan") {
                                if let loanMainVC :LoanMainVC = LoanMainVC.instantiateViewControllerFromStoryboard() {
                                    navigationController.pushViewController(loanMainVC, animated: true)
                                }
                            }
                            
                        } else if deepLinkStr.containsSubString(subString: "Screen_Name=myaccount") {
                            if let drawer = navigationController.viewControllers.last as? KYDrawerController ,
                                let tabBarCtr = drawer.mainViewController as? UITabBarController {
                                tabBarCtr.selectedIndex = 4
                            }
                            if deepLinkStr.containsSubString(subString: "SubScreen=myaccount_usage_history") {
                                if let usageHistoryMainVC :UsageHistoryMainVC = UsageHistoryMainVC.instantiateViewControllerFromStoryboard() {
                                    navigationController.pushViewController(usageHistoryMainVC, animated: true)
                                }
                            } else if deepLinkStr.containsSubString(subString: "SubScreen=myaccount_operations_history") {
                                if let operationHistoryVC :OperationHistoryVC = OperationHistoryVC.instantiateViewControllerFromStoryboard() {
                                    navigationController.pushViewController(operationHistoryVC, animated: true)
                                }
                            } else if deepLinkStr.containsSubString(subString: "SubScreen=myaccount_my_subscriptions") {
                                if let mySubscriptionsVC :MySubscriptionsVC = MySubscriptionsVC.instantiateViewControllerFromStoryboard() {
                                    if deepLinkStr.containsSubString(subString: "TabName=Calls") {
                                        mySubscriptionsVC.selectedTabType = .call
                                    } else if deepLinkStr.containsSubString(subString: "TabName=Internet") {
                                        mySubscriptionsVC.selectedTabType = .internet
                                    } else if deepLinkStr.containsSubString(subString: "TabName=SMS") {
                                        mySubscriptionsVC.selectedTabType = .sms
                                    } else if deepLinkStr.containsSubString(subString: "TabName=Hybrid") {
                                        mySubscriptionsVC.selectedTabType = .hybrid
                                    } else if deepLinkStr.containsSubString(subString: "TabName=Bonuses") {
                                        mySubscriptionsVC.selectedTabType = .hybrid
                                    } else if deepLinkStr.containsSubString(subString: "TabName=Campaign") {
                                        mySubscriptionsVC.selectedTabType = .campaign
                                    } else if deepLinkStr.containsSubString(subString: "TabName=TM") {
                                        mySubscriptionsVC.selectedTabType = .tm
                                    } else if deepLinkStr.containsSubString(subString: "TabName=Roaming") {
                                        mySubscriptionsVC.selectedTabType = .roaming
                                    } else if deepLinkStr.containsSubString(subString: "TabName=All_Inclusive_Call") {
                                        mySubscriptionsVC.selectedTabType = .allInclusiveCall
                                    } else if deepLinkStr.containsSubString(subString: "TabName=All_Inclusive_SMS") {
                                        mySubscriptionsVC.selectedTabType = .allInclusiveSMS
                                    } else if deepLinkStr.containsSubString(subString: "TabName=All_Inclusive_Internet") {
                                        mySubscriptionsVC.selectedTabType = .allInclusiveInternet
                                    }
                                    navigationController.pushViewController(mySubscriptionsVC, animated: true)
                                }
                            }
                        }
                    }
                   }
               }
           }
       }
   }
    
    //MARK:- IBACTIONS
    @IBAction func backPressed(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuPressed(_ sender: AnyObject){
        if let drawerController = self.tabBarController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
}

extension BaseVC: UIGestureRecognizerDelegate {
    
    func interactivePop(_ isEnable:Bool = true) {
        self.isPopGestureEnabled = isEnable
        /*if let navigationVC = self.navigationController as? MainNavigationController {
         navigationVC.interactivePop(isEnable)
         }*/
    }
}

//MARK:- KYDrawerController extension for status color
extension KYDrawerController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
