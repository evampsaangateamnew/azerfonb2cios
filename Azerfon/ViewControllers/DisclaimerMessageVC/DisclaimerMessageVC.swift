//
//  DisclaimerMessageVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 6/29/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

enum AFDisclaimerType: String {
    case operationHistory
    case usageHistory
    case unspecified
}

class DisclaimerMessageVC: BaseVC {
    
    //MARK:- Properties
    fileprivate var okBtncompletionHandlerBlock : (_ dontShowAgain : Bool) -> Void = { _ in }
    var descriptionText : String = ""
    var okBtnTitle : String = ""
    var isDontShowAgainChecked : Bool = false
    
    //MARK:- IBOutlet
    @IBOutlet var contentView: UIView!
    @IBOutlet var descriptionLabel: AFLabel!
    @IBOutlet var okButton: AFButton!
    @IBOutlet var checkBoxButton: UIButton!
    @IBOutlet var dontShowLabel: AFLabel!
    
    //MARK:- ViewControllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.afBlackTransparent
        
        contentView.roundAllCorners(radius: 10)
        
        descriptionLabel.text = descriptionText
        dontShowLabel.text = Localized("DontShowAgain_Description")
        okButton.setTitle(okBtnTitle, for: .normal)
       
        
        if isDontShowAgainChecked {
            checkBoxButton.setImage(UIImage.imageFor(name:  "check_on"), for: UIControl.State.normal)
        } else {
            checkBoxButton.setImage(UIImage.imageFor(name:  "check_off"), for: UIControl.State.normal)
        }
        
    }
    
    
    //MARK:- Functions
    
    func setDisclaimerAlertWith(description: String = Localized("Disclaimer_Description"),
                                btnTitle: String = Localized("BtnTitle_OK"),
                                okBtnClickedBlock : @escaping (_ dontShowAgain : Bool) -> Void = { _ in }) {
        
        descriptionText = description
        okBtnTitle = btnTitle
        okBtncompletionHandlerBlock = okBtnClickedBlock
    }
    
    class public func canShowDisclaimerOfType(_ alertDisclaimerType: AFDisclaimerType) -> Bool {
        
        switch alertDisclaimerType {
            
        case .operationHistory:
            return UserDefaults.standard.value(forKey: "OperationHistoryDisclaimerValue") as? Bool ?? true
            
        case .usageHistory:
            return UserDefaults.standard.value(forKey: "UsageHistoryDisclaimerValue") as? Bool ?? true
            
        default:
            return true
        }
        
    }
    
    class public func setDisclaimerStatusForType(_ alertDisclaimerType: AFDisclaimerType, canShow: Bool) {
        
        
        switch alertDisclaimerType {
            
        case .operationHistory:
            UserDefaults.standard.set(canShow, forKey: "OperationHistoryDisclaimerValue")
            UserDefaults.standard.synchronize()
            
        case .usageHistory:
            UserDefaults.standard.set(canShow, forKey: "UsageHistoryDisclaimerValue")
            UserDefaults.standard.synchronize()
            
        default:
            return
        }
    }
    
    //MARK:- IBACTIONS
    @IBAction func YesBtnPressed(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion:nil)
        self.okBtncompletionHandlerBlock(isDontShowAgainChecked)
    }
    
    @IBAction func checkBoxBtnPressed(_ sender: UIButton) {
        
        if isDontShowAgainChecked {
            checkBoxButton.setImage(UIImage.imageFor(name:  "check_off"), for: UIControl.State.normal)
            isDontShowAgainChecked = false
            
        } else {
            checkBoxButton.setImage(UIImage.imageFor(name:  "check_on"), for: UIControl.State.normal)
            isDontShowAgainChecked = true
        }
    }
    
    
}
