//
//  SplashVC.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/7/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
import LocalAuthentication
import KYDrawerController

class SplashVC: BaseVC {
    
    //MARK: - Properties
    var isAppStarted = false
    var context :LAContext?
    
    //MARK: - IBOutlet
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var lblenvironment:AFLabel!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Setting user initial language */
        AFLanguageManager.setInitialUserLanguage()
        
        /* Verifing current app version */
        verifyAppVersion()
        
        lblenvironment.text = ""
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        print("splash viewWillAppear")
        loadViewContent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /* Check if user previously opened application and redirected to Home Page then redirect without calling AppVersion API */
        if isAppStarted == true {
            navigateUserToAppropriateScreen()
        }
        
        if Constants.kAFAPIClientBaseURL == "https://ecarestg.nar.az:16444/azerfon/api/" {
            lblenvironment.text = "Staging"
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        context = nil
    }
    //MARK: - IBAction
    
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        logoImageView.image = UIImage.imageFor(name: Localized("Img_NARLogo"))
    }
    
    /**
     Redirect user from splash screen to walkThrough, Login/SignUp and Dashboard ViewController.
     
     - returns: void
     */
    @objc func navigateUserToAppropriateScreen() {
        
        // Set to true, can verify user redirected first time from splash screen.
        isAppStarted = true
        
        // Check if user loggedIn and Customer information is loaded sucessfully.
        if AFUserSession.shared.isLoggedIn() && AFUserSession.shared.loadUserInfomation() {
            
            if AFUserSession.shared.isBiometricEnabled() {
                self.authenticateWithTouchID()
            } else {
                self.redirectToDashboardScreen()
            }
        } else {
            // Redirect user to login screen.
            self.redirectToLoginScreen()
        }
    }
    
    func redirectToLoginScreen() {
        DispatchQueue.main.async {
            if let loginVC :LoginVC = LoginVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }
    }
    
    func redirectToDashboardScreen() {
        
        if let mainViewController :TabbarController = TabbarController.instantiateViewControllerFromStoryboard() ,
            let drawerViewController :SideMenuVC = SideMenuVC.instantiateViewControllerFromStoryboard() {
            
            let drawerController = KYDrawerController(drawerDirection: .left,drawerWidth: (UIScreen.main.bounds.width * 0.85))
            drawerController.mainViewController = mainViewController
            drawerController.drawerViewController = drawerViewController
            
            self.navigationController?.pushViewController(drawerController, animated: true)
        }
    }
    
    func authenticateWithTouchID() {
        
        context = LAContext()
        
        /* First check if we have the needed hardware support. */
        var error: NSError?
        if context?.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) ?? false {
            
            let reason = Localized("Biometric_Login")
            context?.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                DispatchQueue.main.async {
                    if success {
                        self.redirectToDashboardScreen()
                        
                    } else {
                        /* Show error alert if biometry/TouchID/FaceID is lockout or not enrolled */
                        self.showBioAuthError(errorCode: error?._code)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                /* Show error alert if biometry/TouchID/FaceID is lockout or not enrolled */
                self.showBioAuthError(errorCode: error?._code)
            }
        }
    }
    
    func showBioAuthError(errorCode: Int?) {
        let errorDescription = self.evaluateAuthenticationPolicyMessageForLA(errorCode: errorCode)
        if errorDescription.isUserAction == false {
            self.showErrorAlertWithMessage(message: errorDescription.message, {
                self.redirectToLoginScreen()
            })
        } else {
            self.redirectToLoginScreen()
        }
    }
    
    /**
     Redirect user to AppStore
     
     - returns: void
     */
    func redirectUserToAppStore(appStoreURL urlString :String?, byForce: Bool = false) {
        
        var appStoreURL :URL?
        
        if let storeURLString = urlString,
            let responseStoreURL = URL(string: storeURLString),
            UIApplication.shared.canOpenURL(responseStoreURL) {
            
            appStoreURL = responseStoreURL
            
        } else {
            
            if let constaintStoreURL = URL(string: AFUtilities.getAppStoreURL()),
                UIApplication.shared.canOpenURL(constaintStoreURL) {
                
                appStoreURL = constaintStoreURL
            }
        }
        
        if let storeURL = appStoreURL,
            UIApplication.shared.canOpenURL(storeURL) {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(storeURL, options: [:], completionHandler: { (success: Bool) in
                    
                    // If failded to open URL
                    if success == false {
                        UIApplication.shared.openURL(storeURL)
                    }
                    // Close application
                    exit(0)
                })
            } else {
                UIApplication.shared.openURL(storeURL)
                // Close application
                exit(0)
            }
            
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_CannotAbleToRedirect"), {
                if byForce == true {
                    // Close application
                    exit(0)
                } else {
                    self.navigateUserToAppropriateScreen()
                }
            })
        }
    }
    
    //MARK: - API's Calls
    /**
     verify AppVersion API call
     
     - returns: void
     */
    func verifyAppVersion() {
        
        // Check if connected to internet then call API
        if Connectivity.isConnectedToInternet == true {
            
            _ = AFAPIClient.shared.verifyAppVersion({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                
                if let appVersionHandler = Mapper<VerifyAppVersion>().map(JSONObject: resultData) {
                    
                    /* Save AppStore URL in User Defaults */
                    AFUtilities.updateAppStoreURL(appVersionHandler.appStore)
                    
                    // handling data from API response.
                    if resultCode == Constants.AFAPIStatusCode.succes.rawValue { /* Application is updated */
                        
                        /* Redirect user to appropriate screen */
                        self.navigateUserToAppropriateScreen()
                        
                    } else if resultCode == Constants.AFAPIStatusCode.forceUpdate.rawValue { /* New version of application is available */
                        /* Show force update alert. */
                        self.showAlert(title: Localized("Title_Update"), message: resultDesc, btnTitle: Localized("BtnTitle_UPDATE"), {
                            /* Redirect user to appStore in case user select update button */
                            self.redirectUserToAppStore(appStoreURL: appVersionHandler.appStore, byForce: true)
                        })
                        
                    } else if resultCode == Constants.AFAPIStatusCode.optionalUpdate.rawValue {
                        
                        /* SHOW Aler and then redirect according to user selection */
                        self.showConfirmationAlert(message: resultDesc, okBtnTitle: Localized("BtnTitle_UPDATE"), cancelBtnTitle: Localized("BtnTitle_CANCEL"), alertType: .other, {
                            
                            /* Redirect user to appStore in case user select update button */
                            self.redirectUserToAppStore(appStoreURL: appVersionHandler.appStore)
                        }, {
                            /* Redirect user to appropriate screen */
                            self.navigateUserToAppropriateScreen()
                        })
                        
                    } else if resultCode == Constants.AFAPIStatusCode.serverDown.rawValue {
                        /* Redirect user to appropriate screen */
                        self.navigateUserToAppropriateScreen()
                        
                    } else {
                        /* Redirect user to appropriate screen */
                        self.navigateUserToAppropriateScreen()
                    }
                    
                } else {
                    /* Redirect user to appropriate screen */
                    self.navigateUserToAppropriateScreen()
                }
            })
            
        } else {
            
            /* Redirect user to appropriate screen with delay */
            if isAppStarted == false {
                perform(#selector(navigateUserToAppropriateScreen), with: nil, afterDelay: Constants.SPLASH_DURATION)
            } else {
                /* Redirect user to appropriate screen */
                self.navigateUserToAppropriateScreen()
            }
        }
    }
    
    
    
}
