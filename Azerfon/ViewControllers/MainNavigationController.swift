//
//  MainNavigationController.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/4/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    //  var isPopGestureEnabled : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.interactivePopGestureRecognizer?.isEnabled = true
        self.interactivePopGestureRecognizer?.delegate = self
    }
}

extension MainNavigationController: UIGestureRecognizerDelegate {
    //MARK:- Functions
    
    /*
     func interactivePop(_ isEnable:Bool = true) {
     isPopGestureEnabled = isEnable
     }
     */
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if let aVC = self.visibleViewController as? BaseVC {
            print("gestureRecognizerShouldBegin: \(aVC.isPopGestureEnabled)")
            return aVC.isPopGestureEnabled
        }
        return false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if let aVC = self.visibleViewController as? BaseVC {
            return aVC.isPopGestureEnabled
        }
        return false
    }
    /*
     func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive press: UIPress) -> Bool {
     
     print("press")
     return true
     }
     
     func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
     print("touch")
     return true
     }
     */
}
