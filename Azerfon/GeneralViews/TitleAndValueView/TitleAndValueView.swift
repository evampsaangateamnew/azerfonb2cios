//
//  TitleAndValueView.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TitleAndValueView: UIView {

    //MARK: - IBOutlets
    @IBOutlet var titleLabel: AFLabel!
    @IBOutlet var valueLabel: AFLabel!
  
    //MARK: - Functions
    
    /**
     Sets value for Title and value.
     
     - parameter title: title text.
     - parameter value: value text.
     - parameter titleTextColor: title text color.
     - parameter valueTextColor: value text color.
     
     - returns: void.
     */
    func setTitleAndValue(_ title :String?,
                          titleTextColor :UIColor = .afGunmetal,
                          value :String?,
                          valueTextColor :UIColor = .afGunmetal) {
        
        titleLabel.text = title ?? ""
        valueLabel.text = value ?? ""
        
        titleLabel.textColor = titleTextColor
        valueLabel.textColor = valueTextColor
        
        
    }
}


