//
//  ExpandableTitleHeaderView.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 9/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class ExpandableTitleHeaderView: AFAccordionTableViewHeaderView {
    
    //MARK:- Properties
    var viewType : Constants.AFSectionType = Constants.AFSectionType.unSpecified
    
    //MARK: - IBOutlet
    @IBOutlet var myContentView     : UIView!
    @IBOutlet var titleLabel        : AFMarqueeLabel!
    @IBOutlet var stateIcon         : UIImageView!
    @IBOutlet var seperatorView     : AFView!
    
    //MARK: - View Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    /**
     Set Expanded header details.
     
     - parameter title: Title text.
     - parameter isSectionSelected: Wheter section is selected or not.
     - parameter headerType: Section type.
     
     - returns: void.
     */
    func setViewWithTitle(title : String?,
                          isSectionSelected: Bool,
                          headerType :Constants.AFSectionType ) {
        
        //View Type
        viewType = headerType
        
        updateStateIcon(isSelected: isSectionSelected)
        myContentView.backgroundColor = UIColor.veryLightPinkThree
        
        // Setting offer name
        if let title = title {
            titleLabel.text = title
        } else {
            titleLabel.text = ""
        }
        
        seperatorView.backgroundColor = UIColor.white
    }
    
    func updateStateIcon(isSelected :Bool) {
        
        if isSelected {
            titleLabel.textColor = UIColor.afSuperPink
            stateIcon.image = UIImage.imageFor(name: "pinkMinusIcon")
        } else {
            titleLabel.textColor = UIColor.afPurplishBrown
            stateIcon.image = UIImage.imageFor(name: "pinkPlusIcon")
        }
    }
}
