//
//  AttributeDetailView.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class AttributeDetailView: UIView {

    //MARK: - IBOutlets
    @IBOutlet var titleLabel        : AFMarqueeLabel!
    @IBOutlet var leftValueLabel    : AFMarqueeLabel!
    @IBOutlet var rightValueLabel   : AFMarqueeLabel!

    @IBOutlet var centerSepratorView: UIView!

    @IBOutlet var leftValueViewWidthConstraint: NSLayoutConstraint!

     //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // Using in supplementary offers
    func setAttributeValues(showManatSign : Bool , aAttribute : AttributeList?) {
        
        // Hiding left value
        leftValueViewWidthConstraint.constant = 0
        leftValueLabel.text = ""
        
        centerSepratorView.isHidden = true
        
        if let title = aAttribute?.title {
            titleLabel.text = title
        } else {
            titleLabel.text = ""
        }
        
        if showManatSign == true &&
            aAttribute?.value?.isBlank == false &&
            (aAttribute?.unit?.isEqual("manat", ignorCase: true) == true ||
                aAttribute?.value?.isStringAnNumber() == true ) {
            
            rightValueLabel.attributedText = aAttribute?.value?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))

        } else {
             rightValueLabel.attributedText = "\(aAttribute?.value ?? "") \(aAttribute?.unit ?? "")".createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
        }
    }


    /// Using in tariff Header section for call type
    func setAttributeValuesForCall(showManatSign : Bool , aAttribute : CallAttributes?, cellType : Constants.AFOfferType, priceTemplate : String?) {

        centerSepratorView.isHidden = true
        
        // Hiding left value
        leftValueViewWidthConstraint.constant = 0
        leftValueLabel.text = ""
        if let title = aAttribute?.title {
          
            titleLabel.text = title
        } else {
            titleLabel.text = ""
        }

        var rightValue : String = ""

        //Showing left and right values both in case of call
        if cellType == .call || cellType == .callPayG {
            // Setting left label Value (in middle middle of view)
            if priceTemplate?.isEqual( Constants.k_DoubleTitlesKey, ignorCase: true) ?? false ||
                priceTemplate?.isEqual( Constants.k_DoubleValueKey, ignorCase: true) ?? false {

                //set width
                leftValueViewWidthConstraint.constant = 80

                if  aAttribute?.valueLeft?.isBlank == false &&
                    aAttribute?.valueRight?.isBlank == false {

                    centerSepratorView.isHidden = false
                } else {
                    centerSepratorView.isHidden = true
                }


                // get Value
                if let leftValue = aAttribute?.valueLeft {

                    leftValueLabel.text = leftValue

                    // Check for free resource value
                    if leftValue.isHasFreeOrUnlimitedText() {

                        /*leftValueLabel.attributedText = leftValue.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h3GreyRight)*/
                        leftValueLabel.attributedText = leftValue.createAttributedString(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight)

                    } else  {
                       leftValueLabel.attributedText = leftValue.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
                    }

                } else  {
                    leftValueLabel.text = ""
                }

            } else {

                centerSepratorView.isHidden = true
                leftValueLabel.text = ""
                leftValueViewWidthConstraint.constant = 0
            }

            // Setting right value
            rightValue = aAttribute?.valueRight ?? ""

        } else {
            leftValueViewWidthConstraint.constant = 0

            // Setting right value
            rightValue =  aAttribute?.value ?? ""
        }

        // Setting right value
        if rightValue.isBlank {
            rightValueLabel.text = ""

        } else {
            if rightValue.isHasFreeOrUnlimitedText() {

                /*rightValueLabel.attributedText = rightValue.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h3GreyRight)*/
                rightValueLabel.attributedText = rightValue.createAttributedString(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight)

            } else {
                rightValueLabel.attributedText = rightValue.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
            }

            // Setting color of right value
            if (cellType == .call || cellType == .callPayG) &&
                aAttribute?.valueLeft?.isBlank == false &&
                aAttribute?.valueRight?.isBlank == false &&
                (priceTemplate?.isEqual( Constants.k_DoubleTitlesKey, ignorCase: true) ?? false || priceTemplate?.isEqual( Constants.k_DoubleValueKey, ignorCase: true) ?? false) {

                /*rightValueLabel.attributedText = rightValue.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))*/
                rightValueLabel.attributedText = rightValue.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
                
            }
        }
    }

    /// Using in tariff Header section for Internet type
    func setAttributeValuesForInternet(subTitle : String?, subTitleValue : String?)  {

        centerSepratorView.isHidden = true
        // Hiding left value
        leftValueViewWidthConstraint.constant = 0
        leftValueLabel.text = ""
    
        titleLabel.text = subTitle ?? ""
        
        if subTitleValue?.isHasFreeOrUnlimitedText() == true {

            /*rightValueLabel.attributedText = subTitleValue?.createAttributedString(mainStringColor: UIColor.afSuperPink, mainStringFont: UIFont.h3GreyRight)*/
            rightValueLabel.attributedText = subTitleValue?.createAttributedString(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight)
        } else {
            rightValueLabel.attributedText = subTitleValue?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))
        }
    }
    
}


