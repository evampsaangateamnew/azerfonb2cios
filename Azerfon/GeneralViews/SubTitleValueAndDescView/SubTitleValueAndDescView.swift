//
//  SubTitleValueAndDescView.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SubTitleValueAndDescView: UIView {
    //MARK: - IBOutlets
    @IBOutlet var subTitleLabel         : AFMarqueeLabel!
    @IBOutlet var subValueLable         : AFLabel!
    @IBOutlet var subDescriptionLabel   : AFMarqueeLabel!

    //MARK: - Functions
    
    /**
     Set Sub Title and values.
     
     - parameter aAttribute: AttributeList.
     
     - returns: CGFloat cell heigt.
     */
    func setSubTitleValueAndDescription(aAttribute : TitleSubAttributesList?) -> CGFloat {

        if let aAttributeObject = aAttribute {

            subTitleLabel.text = aAttributeObject.title ?? ""

            if aAttributeObject.unit?.isEqual("AZN", ignorCase: true) ?? false {
                
                subValueLable.attributedText = aAttributeObject.value?.createAttributedStringWithManatSignImage(mainStringColor: UIColor.afPurplishBrown, mainStringFont: UIFont.h3GreyRight, imageSize: CGSize(width: 10, height: 6))

            } else {
                
                subValueLable.text = "\(aAttributeObject.value ?? "") \(aAttributeObject.unit?.removeNullValues() ?? "")"

            }
            
            subDescriptionLabel.loadHTMLString(htmlString: aAttributeObject.description ?? "")

            return 35
            
        } else {
            subTitleLabel.text = ""
            subValueLable.text = ""
            subDescriptionLabel.text = ""
        }
        return 0.0
    }

}
