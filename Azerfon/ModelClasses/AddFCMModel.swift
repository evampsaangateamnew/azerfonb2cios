//
//  AddFCMModel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/21/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct AddFCMModel : Mappable {
    var isEnable :  String          = ""
    var ringingStatus :  String     = ""
    
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
        
        isEnable        <- map["isEnable"]
        ringingStatus   <- map["ringingStatus"]
    }
}
