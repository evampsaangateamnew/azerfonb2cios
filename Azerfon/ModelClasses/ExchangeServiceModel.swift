//
//  ExchangeServiceModel.swift
//  Azerfon
//
//  Created by Mian Waqas Umar on 25/04/2019.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

class ExchangeServiceModel : Mappable {
    
    var isServiceEnabled : String?
    var count : String?
    var voicevalues : [ExchangeServiceValueModel]?
    var datavalues : [ExchangeServiceValueModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        isServiceEnabled      <- map["isServiceEnabled"]
        count                 <- map["count"]
        voicevalues           <- map["voicevalues"]
        datavalues            <- map["datavalues"]
    }
}

class ExchangeServiceValueModel : Mappable {
    
    var data : String?
    var voice : String?
    var dataUnit : String?
    var voiceUnit : String?

    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        data             <- map["data"]
        voice            <- map["voice"]
        dataUnit         <- map["dataUnit"]
        voiceUnit        <- map["voiceUnit"]
    }
}
