//
//  TextWithPoints.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 6/20/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct TextWithPoints : Mappable {
    var title : String?
    var titleIcon : String?
    var pointsList : [String?]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        title <- map["title"]
        titleIcon <- map["titleIcon"]
        pointsList <- map["pointsList"]
    }
    
}
