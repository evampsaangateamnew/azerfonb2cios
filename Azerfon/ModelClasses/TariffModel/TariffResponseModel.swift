/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct TariffResponseModel : Mappable {
	var tariffResponseList : [TariffResponseList]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		tariffResponseList <- map["tariffResponseList"]
	}

    func filterOfferBySearchString(searchString : String?) -> [TariffResponseList]? {
        if let searchString = searchString,
            searchString.isBlank == false {
            
            var filteredTariffResponseList : [TariffResponseList]? = []
            
            for i in 0..<(self.tariffResponseList?.count ?? 0) {
                
                if let aTariffGroupObject = self.tariffResponseList?[i] {
                    
                    var aFilteredTariffGroup : TariffResponseList = aTariffGroupObject
                    
                    aFilteredTariffGroup.items = aTariffGroupObject.items?.filter() {
                        if let offerName = ($0 as OfferItem).header?.name {
                            
                            return offerName.containsSubString(subString: searchString)
                            
                        } else {
                            return false
                        }
                    }
                    
                    filteredTariffResponseList?.append(aFilteredTariffGroup)
                }
            }
            
            return filteredTariffResponseList
            
        } else {
          return self.tariffResponseList
        }
    }
    
    func filterOfferByOfferingID(offeringID : String?) -> (tariffGroupIndex :Int?, tariffItemIndex :Int?) {
        
        if let searchOfferingID = offeringID,
            searchOfferingID.isBlank == false {
            
            for i in 0..<(self.tariffResponseList?.count ?? 0) {
                
                let tariffItemIndex = self.tariffResponseList?[i].items?.index(where:{
                    ($0 as OfferItem).header?.offeringId?.isEqual(searchOfferingID, ignorCase: true) ?? false
                })
                
                if tariffItemIndex != nil {
                    return (i,tariffItemIndex)
                }
            }
            
            return (nil,nil)
            
        } else {
            return (nil,nil)
        }
    }
}
