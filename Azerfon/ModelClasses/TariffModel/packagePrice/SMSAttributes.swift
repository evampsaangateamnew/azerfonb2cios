//
//  SMSAttributes.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/10/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct SMSAttributes : Mappable {
    
    var title       : String?
    var value   : String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
        title       <- map["title"]
        value       <- map["value"]
    }
    
}
