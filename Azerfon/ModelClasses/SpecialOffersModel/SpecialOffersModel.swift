//
//  SpecialOffersModel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/25/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//


import ObjectMapper

struct SpecialOffersModel : Mappable {
    var special     : OffersModel?
    var tm          : OffersModel?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        special     <- map["special"]
        tm          <- map["tm"]
    }
    
    
    static func filterOfferBySearchString(searchString : String?, offers :[SupplementaryOfferItem]?) -> [SupplementaryOfferItem]? {
        if let searchString = searchString,
            searchString.isBlank == false {
            
            var filteredOffers : OffersModel? = OffersModel()
            
            filteredOffers?.offers = offers?.filter() {
                if let offerName = ($0 as SupplementaryOfferItem).header?.offerName {
                    
                    return offerName.containsSubString(subString: searchString)
                    
                } else {
                    return false
                }
            }
            
            return filteredOffers?.offers
            
        } else {
            return offers
        }
    }
    
}
