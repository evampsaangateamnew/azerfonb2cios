//
//  SupplementaryResponse.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/18/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct SupplementaryResponse : Mappable {
    var internet                : OffersModel?
    var campaign                : OffersModel?
    var sms                     : OffersModel?
    var call                    : OffersModel?
    var tm                      : OffersModel?
    var hybrid                  : OffersModel?
    var daily                   : OffersModel?
    var weekly                  : OffersModel?
    var monthly                 : OffersModel?
    var hourly                  : OffersModel?
    var all                     : OffersModel?
    var roaming                 : Roaming?
    var internetInclusiveOffers : InclusiveOffers?
    var voiceInclusiveOffers    : InclusiveOffers?
    var smsInclusiveOffers      : InclusiveOffers?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        internet                <- map["internet"]
        campaign                <- map["campaign"]
        sms                     <- map["sms"]
        call                    <- map["call"]
        tm                      <- map["tm"]
        hybrid                  <- map["hybrid"]
        daily                   <- map["dailyBundles"]
        weekly                  <- map["weeklyBundles"]
        monthly                 <- map["monthlyBundles"]
        hourly                  <- map["hourlyBundles"]
        all                     <- map["allBundles"]
        roaming                 <- map["roaming"]
        internetInclusiveOffers <- map["internetInclusiveOffers"]
        voiceInclusiveOffers    <- map["voiceInclusiveOffers"]
        smsInclusiveOffers      <- map["smsInclusiveOffers"]
    }
    
    
    func typeAndIndexOfOfferingId(offeringId : String?) -> (Constants.AFOfferTabType, Int) {
        
        /* Filter offers of Call section */
        var filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.call?.offers)
        
        if filterResponse.isFoundValue {
            return (.call,filterResponse.index)
        }
        
        /* Filter offers of Internet section */
        filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.internet?.offers)
        
        if filterResponse.isFoundValue {
            return (.internet,filterResponse.index)
        }
        
        /* Filter offers of SMS section */
        filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.sms?.offers)
        
        if filterResponse.isFoundValue {
            return (.sms,filterResponse.index)
        }
        
        /*
         
         /* Filter offers of Campaign section */
         filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: self.campaign?.offers)
         
         if filterResponse.isFoundValue {
         return (.Campaign,filterResponse.index)
         }
         
         /* Filter offers of TM section */
         filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: self.tm?.offers)
         
         if filterResponse.isFoundValue {
         return (.TM,filterResponse.index)
         }
         */
        
        /* Filter offers of Hybrid section */
        filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.hybrid?.offers)
        
        if filterResponse.isFoundValue {
            return (.hybrid,filterResponse.index)
        }
        
        /* Filter offers of Roaming section */
        filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.roaming?.offers)
        
        if filterResponse.isFoundValue {
            return (.roaming,filterResponse.index)
        }
        
        // If no information is found then retrun call as default
        return (.call,0)
    }
    func typeAndIndexOfIntentnetOfferingId(offeringId : String?) -> (Constants.AFInternetTabType, Int) {
        
        
        
        
        
        /* Filter offers of all section */
        var filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.all?.offers)
        
        if filterResponse.isFoundValue {
            return (.all,filterResponse.index)
        }
        
        /* Filter offers of All section */
        filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.daily?.offers)
        
        if filterResponse.isFoundValue {
            return (.daily,filterResponse.index)
        }
        
        /* Filter offers of Weekly section */
        filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.weekly?.offers)
        
        if filterResponse.isFoundValue {
            return (.weekly,filterResponse.index)
        }
        
        /* Filter offers of SMS section */
        filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.monthly?.offers)
        
        if filterResponse.isFoundValue {
            return (.monthly,filterResponse.index)
        }
        
        /* Filter offers of SMS section */
        filterResponse = SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId, offerArray: self.hourly?.offers)
        
        if filterResponse.isFoundValue {
            return (.hourly,filterResponse.index)
        }
        
        return (.all,0)
    }
    
    static func containsOfferByOfferingId(offeringId : String? , offerArray : [SupplementaryOfferItem]? ) -> (isFoundValue : Bool, index : Int) {
        
        var foundOffer = false
        
        // return 0 if offeringId is blank
        if offeringId?.isBlank != false {
            return (foundOffer, 0)
        }
        // return 0 if offerArray is nil
        if offerArray == nil {
            return (foundOffer, 0)
        }
        
        // Filtering offeringId index from array
        let indexOfOffer = offerArray?.index() {
            
            if let aOfferingId = ($0 as SupplementaryOfferItem).header?.offeringId,
                aOfferingId.isEqual( offeringId, ignorCase: true) {
                
                foundOffer = true
            }
            
            return foundOffer
        }
        
        return (foundOffer, (indexOfOffer ?? 0))
    }
    
    func containsOfferAgainstOfferId(offeringId : String?) -> Bool {
        
        // return complete array if string is empty
        if offeringId?.isBlank == true {
            return false
        } else {
            var containsOffer : Bool = false
            if SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId ?? "", offerArray: self.call?.offers).isFoundValue {
                containsOffer = true
            } else if SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId ?? "", offerArray: self.call?.offers).isFoundValue {
                containsOffer = true
            } else if SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId ?? "", offerArray: self.sms?.offers).isFoundValue {
                containsOffer = true
            } else if SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId ?? "", offerArray: self.internet?.offers).isFoundValue {
                containsOffer = true
            } else if SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId ?? "", offerArray: self.tm?.offers).isFoundValue {
                containsOffer = true
            } else if SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId ?? "", offerArray: self.hybrid?.offers).isFoundValue {
                containsOffer = true
            } else if SupplementaryResponse.containsOfferByOfferingId(offeringId: offeringId ?? "", offerArray: self.roaming?.offers).isFoundValue {
                containsOffer = true
            }
            return containsOffer
        }
    }
    
}
