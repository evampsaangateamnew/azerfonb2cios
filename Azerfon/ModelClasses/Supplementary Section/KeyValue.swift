//
//  KeyValue.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/18/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct KeyValue: Mappable {
    var key : String?
    var value : String?
    
    init?(map: Map) {
        
    }
    
    init(instance: KeyValue) {
        self.key = instance.key
        self.value = instance.value
    }
    
    init(key : String , value : String) {
        self.key = key
        self.value = value
    }
    
    mutating func mapping(map: Map) {
        
        key             <- map["key"]
        value           <- map["value"]
    }
}
