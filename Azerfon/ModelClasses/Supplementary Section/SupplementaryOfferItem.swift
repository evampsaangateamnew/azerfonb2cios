//
//  SupplementaryOfferItem.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/19/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct SupplementaryOfferItem : Mappable {
    var header      : SupplementaryHeader?
    var details     : Details?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        header <- map["header"]
        details <- map["details"]
    }
    
}

struct SupplementaryHeader: Mappable {
    
    var id : String?
    var type : String?
    var offerName : String?
    var stickerLabel : String?
    var stickerColorCode : String?
    var validityTitle : String?
    var validityInformation : String?
    var validityValue : String?
    var price : String?
    var appOfferFilter : String?
    var offeringId : String?
    var offerLevel : String?
    var btnDeactivate : String?
    var btnRenew : String?
    var status : String?
    var offerGroup : OfferGroup?
    var attributeList : [AttributeList]?
    var usage : [UsageModel]?
    var isDaily : String?
    var isAlreadySusbcribed : String?
    
    var isTopUp : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id                      <- map["id"]
        type                    <- map["type"]
        offerName               <- map["offerName"]
        stickerLabel            <- map["stickerLabel"]
        stickerColorCode        <- map["stickerColorCode"]
        validityTitle           <- map["validityTitle"]
        validityInformation     <- map["validityInformation"]
        validityValue           <- map["validityValue"]
        price                   <- map["price"]
        appOfferFilter          <- map["appOfferFilter"]
        offeringId              <- map["offeringId"]
        offerLevel              <- map["offerLevel"]
        btnDeactivate           <- map["btnDeactivate"]
        btnRenew                <- map["btnRenew"]
        status                  <- map["status"]
        offerGroup              <- map["offerGroup"]
        attributeList           <- map["attributeList"]
        usage                   <- map["usage"]
        
        isTopUp                 <- map["isTopUp"]
        isDaily                 <- map["isDaily"]
        isAlreadySusbcribed     <- map["isAlreadySusbcribed"]
    }
}

struct OfferGroup: Mappable {
    var groupName   : String?
    var groupValue  : String?
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        
        groupName       <- map["groupName"]
        groupValue      <- map["groupValue"]
        
    }
}

struct SupplementaryAttributeList: Mappable {
    var title       : String?
    var value       : String?
    var iconMap     : String?
    var description : String?
    var unit        : String?
    var onnetLabel  : String?
    var onnetValue  : String?
    var offnetLabel : String?
    var offnetValue : String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        iconMap         <- map["iconMap"]
        description     <- map["description"]
        unit            <- map["unit"]
        onnetLabel      <- map["onnetLabel"]
        onnetValue      <- map["onnetValue"]
        offnetLabel     <- map["offnetLabel"]
        offnetValue     <- map["offnetValue"]
    }
}


struct UsageModel : Mappable {
    var activationDate : String?
    var iconName : String?
    var remainingTitle : String?
    var remainingUsage : String?
    var renewalDate : String?
    var renewalTitle : String?
    var totalUsage : String?
    var unit : String?
    var type : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        activationDate <- map["activationDate"]
        iconName <- map["iconName"]
        remainingTitle <- map["remainingTitle"]
        remainingUsage <- map["remainingUsage"]
        renewalDate <- map["renewalDate"]
        renewalTitle <- map["renewalTitle"]
        totalUsage <- map["totalUsage"]
        unit <- map["unit"]
        type <- map["type"]
    }
    
}
