/* 
 Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
import ObjectMapper

struct Roaming : Mappable {
    var countries   : [Countries]?
    var offers      : [SupplementaryOfferItem]?
    var filters     : Filters?
    
    init() {
        
    }
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        countries   <- map["countries"]
        offers      <- map["offers"]
        filters     <- map["filters"]
    }
    
    func filterOfferByCountryName(name : String?) -> [SupplementaryOfferItem]? {
        // Filtering offers array
        var filteredOffers : [SupplementaryOfferItem] = []
        
        
        if let offers = self.offers {
            
            filteredOffers = offers.filter() {
                
                if let roamingDetailsCountriesList = ($0 as SupplementaryOfferItem).details?.roamingDetails?.roamingDetailsCountriesList {
                    
                    let  aRoamingDetailsCountry = roamingDetailsCountriesList.filter() {
                        
                        if let aCountryName = ($0 as RoamingDetailsCountriesList).countryName,
                            aCountryName.trimmWhiteSpace.isEqual(name?.trimmWhiteSpace, ignorCase: true) {
                            
                            return true
                        } else {
                            return false
                        }
                        
                    }
                    
                    return aRoamingDetailsCountry.count > 0 ? true : false
                    
                } else {
                    return false
                }
            }
        }
        
        
        return filteredOffers
    }
    
    func filterOfferBySearchString(searchString : String?) -> [SupplementaryOfferItem]? {
        if let searchString = searchString,
            searchString.isBlank == false {
            
            var filteredOffers : OffersModel? = OffersModel()
            
            filteredOffers?.offers = self.offers?.filter() {
                if let offerName = ($0 as SupplementaryOfferItem).header?.offerName {
                    
                    return offerName.containsSubString(subString: searchString)
                    
                } else {
                    return false
                }
            }
            
            return filteredOffers?.offers
            
        } else {
            return self.offers
        }
    }
    
    func filterOfferByGroupValue(selectedFilters : [FilterContent]? ) -> [SupplementaryOfferItem]?  {
        
        if (selectedFilters?.count ?? 0) <= 0 {
            return self.offers
        }
        
        var filteredOffers : [SupplementaryOfferItem] = []
        
        // Filtering offers array
        if let offers = self.offers {
            
            filteredOffers = offers.filter() {
                
                if let appOfferFilter = $0.header?.offerGroup?.groupName {
                    return selectedFilters?.contains(where: {$0.value?.isEqual(appOfferFilter, ignorCase: true) ?? false}) ?? false
                    
                } else {
                    return false
                }
            }
        }
        
        return filteredOffers
    }
}


struct Filters : Mappable {
    var app     : [FilterContent]?
    var desktop : [FilterContent]?
    var tab     : [FilterContent]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        app     <- map["app"]
        desktop <- map["desktop"]
        tab     <- map["tab"]
    }
    
}

struct FilterContent : Mappable {
    var key     : String?
    var value   : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        key     <- map["key"]
        value   <- map["value"]
    }
    
}



struct Countries : Mappable {
    var flag    : String?
    var name    : String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        flag    <- map["flag"]
        name    <- map["name"]
    }
    
}
