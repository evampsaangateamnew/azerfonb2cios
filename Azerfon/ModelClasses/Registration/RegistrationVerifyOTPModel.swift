//
//  RegistrationVerifyOTPModel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/13/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct RegistrationVerifyOTPModel : Mappable {
    var message : String?
    var content : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        message <- map["message"]
        content <- map["content"]
    }
    
}
