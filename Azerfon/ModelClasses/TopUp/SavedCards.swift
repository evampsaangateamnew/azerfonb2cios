//
//  File.swift
//  Azerfon
//
//  Created by Touseef Sarwar on 26/04/2021.
//  Copyright © 2021 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

class SavedCardsObj : Mappable{
    
    var cardDetails : [SavedCards]?
    var lastAmount: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        cardDetails      <- map["cardDetails"]
        lastAmount      <- map["lastAmount"]
        
    }
}



class SavedCards : Mappable{
    var id: String  = ""
    var cardType : String = ""
    var number : String = ""
    var paymentKey: String = ""
    var cardMaskNumber: String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id              <- map["id"]
        cardType        <- map["cardType"]
        number          <- map["number"]
        paymentKey      <- map["paymentKey"]
        cardMaskNumber  <- map["cardMaskNumber"]
        
    }
}
