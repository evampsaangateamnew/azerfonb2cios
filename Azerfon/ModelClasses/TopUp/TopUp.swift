//
//  TopUp.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 9/20/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class TopUp : Mappable{
    
    var newBalance : String = ""
    var oldBalance : String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        newBalance        <- map["newBalance"]
        oldBalance        <- map["oldBalance"]
        
    }
}
