//
//  CoreServices.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 10/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class CoreServices : Mappable {
    var coreServices :  [CoreService]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        coreServices    <- map["coreServices"]
    }
}

class CoreService : Mappable {

    var coreServiceCategory :  String = ""
    var coreServicesList :  [CoreServicesList]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {

        coreServiceCategory <- map["coreServiceCategory"]
        coreServicesList    <- map["coreServicesList"]
    }
}

class CoreServicesList : Mappable {
    
    var id: Int?
    var name :  String          = ""
    var description :  String   = ""
    var categoryId : Int?
    var price:  String          = ""
    var validity:  String       = ""
    var offeringId:  String     = ""
    var status:  String         = ""
    var storeId: Int?
    var sortOrder:  String      = ""
    var forwardNumber:  String  = ""
    var renewable: Int?
    var freeFor:  String        = ""
    var effectiveDate:  String  = ""
    var expireDate:  String     = ""
    var validityLabel:  String  = ""
    var progressTitle:  String  = ""
    var progressDateLabel: String  = ""

    required init?(map: Map) {
    }
    func mapping(map: Map) {

        id              <- map["id"]
        name            <- map["name"]
        description     <- map["description"]
        categoryId      <- map["categoryId"]
        price           <- map["price"]
        validity        <- map["validity"]
        offeringId      <- map["offeringId"]
        status          <- map["status"]
        storeId         <- map["storeId"]
        sortOrder       <- map["sortOrder"]
        forwardNumber   <- map["forwardNumber"]
        renewable       <- map["renewable"]
        freeFor         <- map["freeFor"]
        effectiveDate   <- map["effectiveDate"]
        expireDate      <- map["expireDate"]
        validityLabel   <- map["validityLabel"]
        progressTitle   <- map["progressTitle"]
        progressDateLabel   <- map["progressDateLabel"]
    }
}



class PAYGservice : Mappable {
    
    var paygStatus: String?
    var offeringId :  String = ""

    required init?(map: Map) {
    }
    func mapping(map: Map) {

        paygStatus          <- map["status"]
        offeringId      <- map["offeringId"]
    }
}
