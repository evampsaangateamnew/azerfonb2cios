//
//  UsageDetailsHistoryHandler.swift
//  Nar+
//
//  Created by Awais Shehzad on 25/09/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct UsageDetailsHistoryHandler: Mappable {
    
    var records : [DetailsHistory]?
    
    init?() { }
    
    init?(map: Map) { }
    
    init(instance: UsageDetailsHistoryHandler) {
        self.records = instance.records
    }
    
    mutating func mapping(map: Map) {
        records          <- map["records"]
    }
}

//DetailsHistory : [...]
struct DetailsHistory: Mappable {
    var chargedAmount : String = ""
    var destination : String = ""
    var startDateTime : String = ""
    var endDateTime : String = ""
    var number : String = ""
    var period : String = ""
    var service : String = ""
    var type : String?
    var usage : String = ""
    var unit : String = ""
    var zone : String = ""
    
    var isSectionExpanded : Bool = false
    
     init?(map : Map) { }
   
    mutating func mapping(map: Map) {
        
        chargedAmount       <- map["chargedAmount"]
        destination         <- map["destination"]
        startDateTime       <- map["startDateTime"]
        endDateTime         <- map["endDateTime"]
        number              <- map["number"]
        period              <- map["period"]
        service             <- map["service"]
        type                <- map["type"]
        usage               <- map["usage"]
        unit                <- map["unit"]
        zone                <- map["zone"]
    }
}
