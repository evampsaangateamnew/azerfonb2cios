//
//  UsageSummaryHistoryHandler.swift
//  Nar+
//
//  Created by Awais Shehzad on 25/09/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct UsageSummaryHistoryHandler: Mappable {
    
    var summaryList : [SummaryList]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        summaryList     <- map["summaryList"]
    }
}

//SummaryList : [...]
struct SummaryList: Mappable {
    var name : String = ""
    var totalUsage : String = ""
    var totalCharge : String = ""
    var records : [Records]?

    var isSectionExpanded :Bool?
    
    init?(map : Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        name            <- map["name"]
        totalUsage      <- map["totalUsage"]
        totalCharge     <- map["totalCharge"]
        records         <- map["records"]
    }
}

struct Records: Mappable {
    
    var service_type : String = ""
    var unit : String = ""
    var total_usage : String = ""
    var chargeable_amount : String = ""
    var count : String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        service_type        <- map["service_type"]
        unit                <- map["unit"]
        total_usage         <- map["total_usage"]
        chargeable_amount   <- map["chargeable_amount"]
        count               <- map["count"]
    }
}
