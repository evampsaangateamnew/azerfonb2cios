//
//  UploadImage.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 10/9/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class UploadImage : Mappable {

//    "imageURL": "<Base URL>/profileImages/557514843.jpg"
    var imageURL :  String = ""
    required init?(map: Map) {
    }
    func mapping(map: Map) {

        imageURL       <- map["imageURL"]
    }
}
