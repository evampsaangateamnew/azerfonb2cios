//
//  Postpaid.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct Postpaid : Mappable {
    var availableBalanceCorporateValue : String?
    var availableBalanceIndividualValue : String?
    var availableCreditLabel : String?
    var balanceCorporateValue : String?
    var balanceIndividualValue : String?
    var balanceLabel : String?
    var corporateLabel : String?
    var currentCreditCorporateValue : String?
    var currentCreditIndividualValue : String?
    var currentCreditLabel : String?
    var individualLabel : String?
    var outstandingIndividualDebt : String?
    var outstandingIndividualDebtLabel : String?
    var template : String?
    var totalPayments : String?
    var totalPaymentsLabel : String?
    var currentCreditLimit : String?
    var currentCreditLimitLabel : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        availableBalanceCorporateValue <- map["availableBalanceCorporateValue"]
        availableBalanceIndividualValue <- map["availableBalanceIndividualValue"]
        availableCreditLabel <- map["availableCreditLabel"]
        balanceCorporateValue <- map["balanceCorporateValue"]
        balanceIndividualValue <- map["balanceIndividualValue"]
        balanceLabel <- map["balanceLabel"]
        corporateLabel <- map["corporateLabel"]
        currentCreditCorporateValue <- map["currentCreditCorporateValue"]
        currentCreditIndividualValue <- map["currentCreditIndividualValue"]
        currentCreditLabel <- map["currentCreditLabel"]
        individualLabel <- map["individualLabel"]
        outstandingIndividualDebt <- map["outstandingIndividualDebt"]
        outstandingIndividualDebtLabel <- map["outstandingIndividualDebtLabel"]
        template <- map["template"]
        totalPayments <- map["totalPayments"]
        totalPaymentsLabel <- map["totalPaymentsLabel"]
        currentCreditLimit <- map["currentCreditLimit"]
        currentCreditLimitLabel <- map["currentCreditLimitLabel"]
    }
    
}
