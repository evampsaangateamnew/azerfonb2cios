//
//  HomePageModel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct HomePageModel : Mappable {
	var notificationUnreadCount : String?
	var balance : Balance?
	var installments : Installments?
	var mrc : Mrc?
	var credit : Credit?
	var freeResources : FreeResources?
    var offeringNameDisplay : String?
    var status :String?
    
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		notificationUnreadCount <- map["notificationUnreadCount"]
		balance                 <- map["balance"]
		installments            <- map["installments"]
		mrc                     <- map["mrc"]
		credit                  <- map["credit"]
		freeResources           <- map["freeResources"]
        offeringNameDisplay     <- map["offeringNameDisplay"]
        status                  <- map["status"]
	}

}
