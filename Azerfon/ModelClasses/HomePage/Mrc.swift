//
//  Mrc.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct Mrc : Mappable {
	var mrcTitleLabel : String?
	var mrcTitleValue : String?
	var mrcCurrency : String?
	var mrcDateLabel : String?
	var mrcDate : String?
	var mrcInitialDate : String?
	var mrcLimit : String?
	var mrcType : String?
	var mrcStatus : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		mrcTitleLabel <- map["mrcTitleLabel"]
		mrcTitleValue <- map["mrcTitleValue"]
		mrcCurrency <- map["mrcCurrency"]
		mrcDateLabel <- map["mrcDateLabel"]
		mrcDate <- map["mrcDate"]
		mrcInitialDate <- map["mrcInitialDate"]
		mrcLimit <- map["mrcLimit"]
		mrcType <- map["mrcType"]
		mrcStatus <- map["mrcStatus"]
	}

}
