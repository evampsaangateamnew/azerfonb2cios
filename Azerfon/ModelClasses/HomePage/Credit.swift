//
//  Credit.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct Credit : Mappable {
	var creditTitleLabel : String?
	var creditTitleValue : String?
	var creditCurrency : String?
	var creditDateLabel : String?
	var creditDate : String?
	var creditInitialDate : String?
	var creditLimit : String?
    var daysDifferenceTotal : String?
    var daysDifferenceCurrent : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		creditTitleLabel <- map["creditTitleLabel"]
		creditTitleValue <- map["creditTitleValue"]
		creditCurrency <- map["creditCurrency"]
		creditDateLabel <- map["creditDateLabel"]
		creditDate <- map["creditDate"]
		creditInitialDate <- map["creditInitialDate"]
		creditLimit <- map["creditLimit"]
        daysDifferenceTotal <- map["daysDifferenceTotal"]
        daysDifferenceCurrent <- map["daysDifferenceCurrent"]
	}

}
