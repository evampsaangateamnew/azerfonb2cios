//
//  Balance.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct Balance : Mappable {
	var prepaid : Prepaid?
	var postpaid : Postpaid?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		prepaid <- map["prepaid"]
		postpaid <- map["postpaid"]
	}

}
