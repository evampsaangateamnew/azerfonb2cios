//
//  MainWallet.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct MainWallet : Mappable {
	var balanceTypeName : String?
	var currency : String?
	var amount : String?
	var effectiveTime : String?
	var expireTime : String?
	var lowerLimit : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		balanceTypeName <- map["balanceTypeName"]
		currency <- map["currency"]
		amount <- map["amount"]
		effectiveTime <- map["effectiveTime"]
		expireTime <- map["expireTime"]
		lowerLimit <- map["lowerLimit"]
	}

}
