//
//  Installments.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct Installments : Mappable {
	var installmentDescription : String?
	var installmentTitle : String?
	var installments : [String]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		installmentDescription <- map["installmentDescription"]
		installmentTitle <- map["installmentTitle"]
		installments <- map["installments"]
	}

}
