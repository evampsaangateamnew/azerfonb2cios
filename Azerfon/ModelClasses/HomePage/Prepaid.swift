//
//  Prepaid.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct Prepaid : Mappable {
	var mainWallet : MainWallet?
	var countryWideWallet : CountryWideWallet?
	var bounusWallet : BounusWallet?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		mainWallet <- map["mainWallet"]
		countryWideWallet <- map["countryWideWallet"]
		bounusWallet <- map["bounusWallet"]
	}

}
