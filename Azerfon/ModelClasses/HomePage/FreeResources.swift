//
//  FreeResources.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct FreeResources : Mappable {
	var freeResources : [FreeResourceAndRoaming]?
	var freeResourcesRoaming : [FreeResourceAndRoaming]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		freeResources <- map["freeResources"]
		freeResourcesRoaming <- map["freeResourcesRoaming"]
	}

    /**
     Help to find and return FreeResourceAndRoaming object accourding to type parameter.
     
     - parameter type: type of data which is required.
     
     - returns: FreeResourceAndRoaming object accourding to type.
     */
    func getFreeResourcesFor(type : Constants.AFFreeResourceType, forRoaming :Bool) -> FreeResourceAndRoaming? {
        
        var freeResourceData : FreeResourceAndRoaming?
        
        if forRoaming == true {
            
            if let freeResourcesRoaming = self.freeResourcesRoaming {
                
                if let i = freeResourcesRoaming.index(where: { $0.resourceType.lowercased() == type.rawValue.lowercased() }) {
                    
                    freeResourceData = freeResourcesRoaming[i]
                }
            }
        } else {
            
            if let freeResources = self.freeResources {
                
                if let i = freeResources.index(where: { $0.resourceType.lowercased() == type.rawValue.lowercased() }) {
                    
                    freeResourceData = freeResources[i]
                }
            }
        }
        
        return freeResourceData
    }
    
    
    /**
     Help to find type of information present in free resources.
     
     - parameter isRoaming: data if roaming.
     
     - returns: type if data in free resource.
     */
    func getTypeFreeResourcesFor(Roaming isRoaming :Bool, userType :Constants.AFUserDashboardType) -> [Constants.AFFreeResourceType] {
        
        var freeResourceDataType : [Constants.AFFreeResourceType]? = []
        var freeResourcesData :[FreeResourceAndRoaming]? = []
        
        if isRoaming == true {
            
            if let freeResourcesRoaming = self.freeResourcesRoaming {
                
               freeResourcesData = freeResourcesRoaming
            }
        } else {
            
            if let freeResources = self.freeResources {
                
                freeResourcesData = freeResources
            }
        }
        
        freeResourcesData?.forEach({ (aFreeResource) in
            if let aType = Constants.AFFreeResourceType(rawValue: aFreeResource.resourceType) {
                
                if (userType == .postpaidDataSIM || userType == .prepaidDataSIM) {
                    
                    if aType == .internet {
                        freeResourceDataType?.append(aType)
                    }
                    
                } else {
                   freeResourceDataType?.append(aType)
                }
            }
        })
        
        freeResourceDataType?.sort{ $0.sortId() < $1.sortId() }
         return freeResourceDataType ?? []
    }
}


class FreeResourceAndRoaming: Mappable {
    
    
    var resourcesTitleLabel: String = ""
    var resourceType: String = ""
    var resourceInitialUnits: String = ""
    var resourceRemainingUnits: String = ""
    var resourceUnitName: String = ""
    var resourceDiscountedText : String = ""
    var remainingFormatted : String = ""
    
    init() {
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        resourcesTitleLabel             <- map["resourcesTitleLabel"]
        resourceType                    <- map["resourceType"]
        resourceInitialUnits            <- map["resourceInitialUnits"]
        resourceRemainingUnits          <- map["resourceRemainingUnits"]
        resourceUnitName                <- map["resourceUnitName"]
        resourceDiscountedText          <- map["resourceDiscountedText"]
        remainingFormatted              <- map["remainingFormatted"]
        
    }
    
}
