//
//  VerifyAppVersion.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct VerifyAppVersion : Mappable {
    var versionConfig : String?
    var message : String?
    var timeStamps : [TimeStamps]?
    var appStore : String?
    var playStore : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        versionConfig <- map["versionConfig"]
        message <- map["message"]
        timeStamps <- map["timeStamps"]
        appStore <- map["appStore"]
        playStore <- map["playStore"]
    }
}


struct TimeStamps : Mappable {
    var cacheType : String?
    var timeStamp : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        cacheType <- map["cacheType"]
        timeStamp <- map["timeStamp"]
    }
}
