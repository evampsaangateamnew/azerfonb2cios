//
//  PrimaryOffering.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct PrimaryOffering : Mappable {
	var offeringId : String?
	var offeringName : String?
	var offeringCode : String?
	var offeringShortName : String?
	var status : String?
	var networkType : String?
	var effectiveTime : String?
	var expiredTime : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		offeringId <- map["offeringId"]
		offeringName <- map["offeringName"]
		offeringCode <- map["offeringCode"]
		offeringShortName <- map["offeringShortName"]
		status <- map["status"]
		networkType <- map["networkType"]
		effectiveTime <- map["effectiveTime"]
		expiredTime <- map["expiredTime"]
	}

}
