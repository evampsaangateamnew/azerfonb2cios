//
//  LoginModel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct LoginModel : Mappable {
	var customerData                : CustomerData?
	var primaryOffering             : PrimaryOffering?
	var supplementaryOfferingList   : [SupplementaryOfferingList]?
	var predefinedData              : PredefinedData?
    var promoMessage                : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		customerData                <- map["customerData"]
		primaryOffering             <- map["primaryOffering"]
		supplementaryOfferingList   <- map["supplementaryOfferingList"]
		predefinedData              <- map["predefinedData"]
        promoMessage                <- map["promoMessage"]
        
	}

}
