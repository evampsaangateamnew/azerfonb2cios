//
//  TariffMigrationPrices.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct TariffMigrationPrices : Mappable {
	var key : String?
	var value : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		key <- map["key"]
		value <- map["value"]
	}

}
