//
//  NarTvDataModel.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 6/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

import ObjectMapper

struct NarTvDataModel : Mappable {
    var narTvRedirectionLinkAndroid     : String?
    var narTvRedirectionLinkIos         : String?
    var narTvRedirectionLinkWebEnglish  : String?
    var narTvRedirectionLinkWebAzeri    : String?
    var narTvRedirectionLinkWebRussian  : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        narTvRedirectionLinkAndroid     <- map["narTvRedirectionLinkAndroid"]
        narTvRedirectionLinkIos         <- map["narTvRedirectionLinkIos"]
        narTvRedirectionLinkWebEnglish  <- map["narTvRedirectionLinkWebEnglish"]
        narTvRedirectionLinkWebAzeri    <- map["narTvRedirectionLinkWebAzeri"]
        narTvRedirectionLinkWebRussian  <- map["narTvRedirectionLinkWebRussian"]
    }
    
}
