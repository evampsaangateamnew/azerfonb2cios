//
//  CustomerData.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct CustomerData : Mappable {
	var title : String?
	var firstName : String?
	var middleName : String?
	var lastName : String?
	var customerType : String?
	var gender : String?
	var dob : String?
	var accountId : String?
	var effectiveDate : String?
	var expiryDate : String?
	var subscriberType : String?
	var status : String?
	var statusDetails : String?
	var brandId : String?
	var brandName : String?
	var loyaltySegment : String?
	var offeringId : String?
	var msisdn : String?
	var token : String?
	var imageURL : String?
	var billingLanguage : String?
	var language : String?
	var customerId : String?
	var entityId : String?
	var customerStatus : String?
	var magCustomerId : String?
	var groupType : String?
	var rateus_android : String?
	var rateus_ios : String?
	var firstPopup : String?
	var lateOnPopup : String?
	var popupTitle : String?
	var popupContent : String?
	var hideNumberTariffIds : [String]?
	var email : String?
	var sim : String?
	var pin : String?
	var puk : String?
	var offeringName : String?
	var offeringNameDisplay : String?
    var groupIds : String?
    
    var specialOffersTariffData : SpecialOffersTariffData?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		title <- map["title"]
		firstName <- map["firstName"]
		middleName <- map["middleName"]
		lastName <- map["lastName"]
		customerType <- map["customerType"]
		gender <- map["gender"]
		dob <- map["dob"]
		accountId <- map["accountId"]
		effectiveDate <- map["effectiveDate"]
		expiryDate <- map["expiryDate"]
		subscriberType <- map["subscriberType"]
		status <- map["status"]
		statusDetails <- map["statusDetails"]
		brandId <- map["brandId"]
		brandName <- map["brandName"]
		loyaltySegment <- map["loyaltySegment"]
		offeringId <- map["offeringId"]
		msisdn <- map["msisdn"]
		token <- map["token"]
		imageURL <- map["imageURL"]
		billingLanguage <- map["billingLanguage"]
		language <- map["language"]
		customerId <- map["customerId"]
		entityId <- map["entityId"]
		customerStatus <- map["customerStatus"]
		magCustomerId <- map["magCustomerId"]
		groupType <- map["groupType"]
		rateus_android <- map["rateus_android"]
		rateus_ios <- map["rateus_ios"]
		firstPopup <- map["firstPopup"]
		lateOnPopup <- map["lateOnPopup"]
		popupTitle <- map["popupTitle"]
		popupContent <- map["popupContent"]
		hideNumberTariffIds <- map["hideNumberTariffIds"]
		email <- map["email"]
		sim <- map["sim"]
		pin <- map["pin"]
		puk <- map["puk"]
		offeringName <- map["offeringName"]
		offeringNameDisplay <- map["offeringNameDisplay"]
        
        specialOffersTariffData <- map["specialOffersTariffData"]
        groupIds <- map["groupIds"]
	}

}

struct SpecialOffersTariffData : Mappable {
    var tariffSpecialIds    : [String]?
    var offersSpecialIds    : [String]?
    var specialOffersMenu   : String?
    var specialTariffMenu   : String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        tariffSpecialIds    <- map["tariffSpecialIds"]
        offersSpecialIds    <- map["offersSpecialIds"]
        specialOffersMenu   <- map["specialOffersMenu"]
        specialTariffMenu   <- map["specialTariffMenu"]
        
    }
    
}


