//
//  Topup.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct Topup : Mappable {
	var moneyTransfer : MoneyTransfer?
	var getLoan : [LoanAmount]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		moneyTransfer <- map["moneyTransfer"]
		getLoan <- map["getLoan.selectAmount.prepaid"]
	}

}

struct LoanAmount: Mappable {
    var currency : String = ""
    var amount : String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        currency <- map["currency"]
        amount <- map["amount"]
    }
}

struct MoneyTransfer : Mappable {
    
    var selectAmount : SelectAmount?
    var termsAndCondition : TermsAndCondition?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        selectAmount <- map["selectAmount"]
        termsAndCondition <- map["termsAndCondition"]
    }
}

struct SelectAmount : Mappable {
    var prepaid : [PrepaidAmount]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        prepaid <- map["prepaid"]
    }
    
}

struct TermsAndCondition : Mappable {
    var prepaid : [PrepaidAmount]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        prepaid <- map["prepaid"]
    }
}

struct PrepaidAmount : Mappable {
    var currency : String?
    var amount : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        currency <- map["currency"]
        amount  <- map["amount"]
    }
}
