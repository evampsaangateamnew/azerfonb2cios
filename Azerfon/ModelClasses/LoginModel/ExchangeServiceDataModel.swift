//
//  ExchangeServiceModel.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 6/12/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

import ObjectMapper

struct ExchangeServiceDataModel : Mappable {
    var dov  : Dov?
    var vod  : Vod?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        dov     <- map["dov"]
        vod     <- map["vod"]
    }
}

struct Dov : Mappable {
    var header : String?
    var footer : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        header     <- map["header"]
        footer     <- map["footer"]
    }
}

struct Vod : Mappable {
    var header : String?
    var footer : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        header     <- map["header"]
        footer     <- map["footer"]
    }
}
