//
//  RedirectionLinks.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct RedirectionLinks : Mappable {
	var onlinePaymentGatewayURL_AZ : String?
	var onlinePaymentGatewayURL_EN : String?
	var onlinePaymentGatewayURL_RU : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		onlinePaymentGatewayURL_AZ <- map["onlinePaymentGatewayURL_AZ"]
		onlinePaymentGatewayURL_EN <- map["onlinePaymentGatewayURL_EN"]
		onlinePaymentGatewayURL_RU <- map["onlinePaymentGatewayURL_RU"]
	}

}
