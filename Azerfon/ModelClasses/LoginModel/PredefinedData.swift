//
//  PredefinedData.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct PredefinedData : Mappable {
	var topup                   : Topup?
	var fnf                     : Fnf?
    var exchangeService         : ExchangeServiceDataModel?
    var narTv                   : NarTvDataModel?
	var redirectionLinks        : RedirectionLinks?
	var tariffMigrationPrices   : [TariffMigrationPrices]?
	var liveChat                : String?
    var tncContent              : String?
    var cevirEligibleTariffs    : [String]?
    var cevirErrorMessage       : String?
    var plasticCards: [PlasticCard]?
    var goldenPayMessages: GoldenPayMessages?
    
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		topup                   <- map["topup"]
		fnf                     <- map["fnf"]
        exchangeService         <- map["exchangeService"]
        narTv                   <- map["narTv"]
		redirectionLinks        <- map["redirectionLinks"]
		tariffMigrationPrices   <- map["tariffMigrationPrices"]
		liveChat                <- map["liveChat"]
        tncContent              <- map["tnc.content"]
        cevirEligibleTariffs    <- map["cevirEligibleTariffs"]
        cevirErrorMessage       <- map["cevirErrorMessage"]
        plasticCards            <- map["topup.plasticCard.cardTypes"]
        goldenPayMessages       <- map["goldenPayMessages"]
	}

}

struct GoldenPayMessages : Mappable {
    var goldenpaymessage500ru : String?
    var goldenpaymessage116ru : String?
    var goldenpaymessage200ru : String?
    var goldenpaymessage116en : String?
    var goldenpaymessage116az : String?
    var goldenpaymessage500en : String?
    var goldenpaymessage500az : String?
    var goldenpaymessage200en : String?
    var goldenpaymessage200az : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        goldenpaymessage500ru <- map["goldenpay_message_500_ru"]
        goldenpaymessage116ru <- map["goldenpay_message_116_ru"]
        goldenpaymessage200ru <- map["goldenpay_message_200_ru"]
        goldenpaymessage116en <- map["goldenpay_message_116_en"]
        goldenpaymessage116az <- map["goldenpay_message_116_az"]
        goldenpaymessage500en <- map["goldenpay_message_500_en"]
        goldenpaymessage500az <- map["goldenpay_message_500_az"]
        goldenpaymessage200en <- map["goldenpay_message_200_en"]
        goldenpaymessage200az <- map["goldenpay_message_200_az"]
    }

}



struct PlasticCard:Mappable {
    var cardKey : Int?
    var cardVale: String?
    
    init(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        cardKey    <- map["key"]
        cardVale    <- map["value"]
    }
}
