//
//  Fnf.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct Fnf : Mappable {
	var maxFNFCount : String?
	var fnFAllowed : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		maxFNFCount <- map["maxFNFCount"]
		fnFAllowed <- map["fnFAllowed"]
	}

}
