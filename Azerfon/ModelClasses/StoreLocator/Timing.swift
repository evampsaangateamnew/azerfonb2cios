//
//  Timing.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct Timing : Mappable {
	var day : String?
	var timings : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		day         <- map["day"]
		timings     <- map["timings"]
	}

}
