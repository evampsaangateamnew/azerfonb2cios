//
//  Stores.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct Stores : Mappable {
	var id : String?
	var store_name : String?
	var address : String?
	var city : String?
	var type : String?
	var latitude : String?
	var longitude : String?
	var contactNumbers : [String]?
	var timing : [Timing]?
    var distance : Double = 0.0
    
    var isSectionExpanded :Bool?

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		id              <- map["id"]
		store_name      <- map["store_name"]
		address         <- map["address"]
		city            <- map["city"]
		type            <- map["type"]
		latitude        <- map["latitude"]
		longitude       <- map["longitude"]
		contactNumbers  <- map["contactNumbers"]
		timing          <- map["timing"]
        distance                    <- map["distance"]
	}

}
