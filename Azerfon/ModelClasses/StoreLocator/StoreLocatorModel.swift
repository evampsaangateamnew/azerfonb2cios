//
//  StoreLocatorModel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct StoreLocatorModel : Mappable {
	var type : [String]?
	var city : [String]?
	var stores : [Stores]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		type    <- map["type"]
		city    <- map["city"]
		stores  <- map["stores"]
	}

    /**
     Filter Store by Parameters.
     
     - parameter city: City name
     - parameter type: Store Type
     - parameter stores: Stores List
     
     - returns: stores
     */
    public static func filterStoreBy(city: String?, type:String?, stores : [Stores]? ) -> [Stores]? {
        
        guard let myType = type else {
            return nil
        }
        
        guard let myCity = city else {
            return nil
        }
        
        guard let StoreListDetails = stores else {
            return nil
        }
        
        if(myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == true && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == true) {
            
            return stores
            
        } else if(myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == false && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == false){
            
            let  filterdStoreDetails = StoreListDetails.filter() {
                
                if ($0 as Stores).city?.lowercased() == myCity.lowercased() {
                    
                    if ($0 as Stores).type?.lowercased() == myType.lowercased() {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            }
            return filterdStoreDetails
            
        } else if (myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == false && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == true) {
            
            let  filterdStoreDetails = StoreListDetails.filter() {
                if ($0 as Stores).city?.lowercased() == myCity.lowercased() {
                    return true
                } else {
                    return false
                }
                
            }
            return filterdStoreDetails
        } else if (myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == true && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == false) {
            
            let  filterdStoreDetails = StoreListDetails.filter() {
                if ($0 as Stores).type?.lowercased() == myType.lowercased() {
                    return true
                } else {
                    return false
                }
                
            }
            return filterdStoreDetails
            
        } else {
            return stores
        }
        
    }
}
