//
//  LoanHistryResponse.swift
//  Nar+
//
//  Created by Awais Shehzad on 21/09/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class LoanHistoryListHandler: Mappable {
    
    var loanHistory : [LoanHistory]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        loanHistory          <- map["loan"]
    }
}

//loanHistory : [...]
class LoanHistory: Mappable {
    var loanID : String = ""
    var dateTime : String = ""
    var amount : String = ""
    var status : String = ""
    var paid : String = ""
    var remaining : String = ""

    var isSectionExpanded :Bool?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        loanID          <- map["loanID"]
        dateTime        <- map["dateTime"]
        amount          <- map["amount"]
        status          <- map["status"]
        paid            <- map["paid"]
        remaining       <- map["remaining"]
    }
}

class NewBalance: Mappable {
    
    var newBalance : String?
    var newCredit : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        newBalance          <- map["newBalance"]
        newCredit           <- map["newCredit"]
    }
}

class UsagePin: Mappable {
    
    var usagePin : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        usagePin          <- map["pin"]
    }
}
