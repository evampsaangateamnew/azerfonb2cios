//
//  NotificationsModel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/20/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper


struct NotificationsCountModel : Mappable {
    
    //    "notificationUnreadCount": "0"
    
    var notificationUnreadCount :  String?
    
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
        
        notificationUnreadCount         <- map["notificationUnreadCount"]
    }
}

