
//
//  MySubscription.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/30/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct MySubscriptionModel : Mappable{
    
    var message: String?
    var mySubscriptionsData : SupplementaryResponse?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        message                 <- map["message"]
        mySubscriptionsData     <- map["mySubscriptionsData"]
        
    }
}
