//
//  ContactUs.swift
//  Nar+
//
//  Created by AbdulRehman Warraich on 10/3/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class ContactUsModel : Mappable{

    var address : String?
    var phone : String?
    var fax : String?
    var email : String?
    var website : String?
    var customerCareNo : String?
    var facebookLink : String?
    var twitterLink : String?
    var googleLink : String?
    var youtubeLink : String?
    var instagramLink : String?
    var linkedinLink : String?
    var addressLat : String?
    var addressLong : String?
    var inviteFriendText : String?

    init() {
    }
    required init?(map: Map) {
    }

    func mapping(map: Map) {

        address                 <- map["address"]
        phone                   <- map["phone"]
        fax                     <- map["fax"]
        email                   <- map["email"]
        website                 <- map["website"]
        customerCareNo          <- map["customerCareNo"]
        facebookLink            <- map["facebookLink"]
        twitterLink             <- map["twitterLink"]
        googleLink              <- map["googleLink"]
        youtubeLink             <- map["youtubeLink"]
        instagramLink           <- map["instagram"]
        linkedinLink            <- map["linkedinLink"]
        addressLat              <- map["addressLat"]
        addressLong             <- map["addressLong"]
        inviteFriendText        <- map["inviteFriendText"]


    }
}

