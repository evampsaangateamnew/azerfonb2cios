//
//  RateUsModel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/20/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct RateUsModel : Mappable {
    var rateus_android : String?
    var rateus_ios : String?
    var pic_tnc : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        rateus_android  <- map["rateus_android"]
        rateus_ios      <- map["rateus_ios"]
        pic_tnc         <- map["pic_tnc"]
    }
    
}
