//
//  NarTvPlanModel.swift
//  Azerfon
//
//  Created by Mian Waqas Umar on 24/04/2019.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

class NarTvResponseModel : Mappable {
    
    var subscriptions : [NarTvPlanModel]?
    var subscriberId : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        subscriptions      <- map["subscriptions"]
        subscriberId       <- map["subscriberId"]
    }
}

class NarTvPlanModel : Mappable {
    
    var title : String?
    var description : String?
    var price : String?
    var status : String?
    var planId : String?
    var startDate : String?
    var endDate : String?
    var labelToRenew : String?
    var labelRenewalDate : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title               <- map["title"]
        description         <- map["description"]
        price               <- map["price"]
        status              <- map["status"]
        planId              <- map["planId"]
        startDate           <- map["startDate"]
        endDate             <- map["endDate"]
        labelToRenew        <- map["labelToRenew"]
        labelRenewalDate    <- map["labelRenewalDate"]
        
    }
}

