//
//  AFUserInfoUtilities.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/2/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

//MARK: - Save info
class AFUserInfoUtilities {
    
    ///Load customer information from userdefault.
    class func loadCustomerDataFromUserDefaults() -> (hasInfo :Bool, usersInfo: ManageAccountModel?) {
        
        if let dataString = UserDefaults.standard.string(forKey: APIsType.loggedInUserInfo.simpleRawValue())?.aesDecrypt(key: AFUserSession.shared.aSecretKey),
            let loggedInUsersInfo = Mapper <ManageAccountModel> ().map(JSONString: dataString),
            (loggedInUsersInfo.users?.count ?? 0) > 0 {
            /* Set stored info */
            AFUserSession.shared.loggedInUsers  = loggedInUsersInfo
            
            return (true, loggedInUsersInfo)
        } else {
            return (false, nil)
        }
        /* Load stored info of logged in users*/
        /*if let loggedInUsersInfo : ManageAccountModel = ManageAccountModel.loadFromUserDefaults(key: APIsType.loggedInUserInfo.simpleRawValue()),
         (loggedInUsersInfo.users?.count ?? 0) > 0 {
         
         /* Set stored info */
         AFUserSession.shared.loggedInUsers  = loggedInUsersInfo
         
         return (true, loggedInUsersInfo)
         } else {
         return (false, nil)
         }*/
    }
    
    class func addNewUser(newUserMSISDN :String, loginInfo :LoginModel) {
        
        AFUserInfoUtilities.updateLoggedInUserInfo(loggedInUserMSISDN: newUserMSISDN,
                                                   loginInfo: loginInfo,
                                                   appConfig: AppConfig(isTokenRegisterd: false, unreadCount: nil))
        
    }
    
    class func switchToUser(newMSISDN : String, selectedUserInfo : UserDataModel?) {
        
        /* Save loggedin user information */
        AFUserInfoUtilities.saveCustomerDataInUserDefaults(loggedInUserMSISDN: newMSISDN, userInfo: selectedUserInfo)
    }
    
    class func saveLoginInfo(loggedInUserMSISDN :String, loginInfo :LoginModel) {
        
        /* Save user loggeding time*/
        AFUtilities.updateLoginTime()
        
        AFUserInfoUtilities.updateLoggedInUserInfo(loggedInUserMSISDN: loggedInUserMSISDN,
                                                   loginInfo: loginInfo,
                                                   appConfig: AppConfig(isTokenRegisterd: false, unreadCount: nil))
        
    }
    
    class func updateLoggedInUserInfo(loggedInUserMSISDN :String, loginInfo :LoginModel, appConfig: AppConfig?) {
        
        
        let aUserinfo :UserDataModel = UserDataModel(current: loginInfo.customerData,
                                                     predefinedInfo: loginInfo.predefinedData,
                                                     appConfig: appConfig == nil ? AppConfig(isTokenRegisterd: false, unreadCount: nil) : appConfig)
        
        /* Save loggedin user information */
        AFUserInfoUtilities.saveCustomerDataInUserDefaults(loggedInUserMSISDN: loggedInUserMSISDN, userInfo: aUserinfo)
    }
    
    class func updateUserInfo(loggedInUserMSISDN : String, userInfo : UserDataModel?) {
        
        /* Save loggedin user information */
        AFUserInfoUtilities.saveCustomerDataInUserDefaults(loggedInUserMSISDN: loggedInUserMSISDN, userInfo: userInfo)
    }
    
    class func removeCustomerDataOf(msisdn : String) {
        
        let userDefaults = UserDefaults.standard
        
        /*if var loggedInUsersInfo : ManageAccountModel = ManageAccountModel.loadFromUserDefaults(key: APIsType.loggedInUserInfo.simpleRawValue())*/
        if let dataString = UserDefaults.standard.string(forKey: APIsType.loggedInUserInfo.simpleRawValue())?.aesDecrypt(key: AFUserSession.shared.aSecretKey),
            var loggedInUsersInfo = Mapper <ManageAccountModel> ().map(JSONString: dataString) {
            
            /* Remove Current user info */
            if msisdn.isEqual(loggedInUsersInfo.currentUser?.userInfo?.msisdn, ignorCase: true) {
                loggedInUsersInfo.currentUser = nil
            }
            /* remove user info against msisdn */
            if var usersInfo = loggedInUsersInfo.users {
                
                /* Remove customer info if previously saved */
                usersInfo.removeAll(where: {($0.userInfo?.msisdn ?? "").isEqual(msisdn, ignorCase: true)})
                loggedInUsersInfo.users = usersInfo
                
            } else {
                loggedInUsersInfo.users = []
            }
            /* Save Users info */
            //loggedInUsersInfo.saveInUserDefaults(key: APIsType.loggedInUserInfo.simpleRawValue())
            userDefaults.set(loggedInUsersInfo.toJSONString()?.aesEncrypt(key: AFUserSession.shared.aSecretKey), forKey: APIsType.loggedInUserInfo.simpleRawValue())
            
            /* Load Users info */
            AFUserSession.shared.loggedInUsers = loggedInUsersInfo
        }
        userDefaults.synchronize()
    }
    
    ///Save PIC user in userdefault.
    private class func saveCustomerDataInUserDefaults(loggedInUserMSISDN : String, userInfo : UserDataModel?) {
        
        let userDefaults = UserDefaults.standard
        
        if var customerInfo = userInfo {
            
            customerInfo.userInfo?.msisdn = loggedInUserMSISDN
            
            /*if var loggedInUsersInfo : ManageAccountModel = ManageAccountModel.loadFromUserDefaults(key: APIsType.loggedInUserInfo.simpleRawValue())*/
            if let dataString = UserDefaults.standard.string(forKey: APIsType.loggedInUserInfo.simpleRawValue())?.aesDecrypt(key: AFUserSession.shared.aSecretKey),
                var loggedInUsersInfo = Mapper <ManageAccountModel> ().map(JSONString: dataString) {
                
                if var usersInfo = loggedInUsersInfo.users {
                    
                    /* Remove customer info if previously saved */
                    usersInfo.removeAll(where: {($0.userInfo?.msisdn ?? "").isEqual(loggedInUserMSISDN, ignorCase: true)})
                    loggedInUsersInfo.users = usersInfo
                    
                } else {
                    loggedInUsersInfo.users = []
                }
                
                /* set current user info */
                loggedInUsersInfo.users?.insert(customerInfo, at: 0)
                loggedInUsersInfo.currentUser = customerInfo
                
                /* Save Users info */
                //loggedInUsersInfo.saveInUserDefaults(key: APIsType.loggedInUserInfo.simpleRawValue())
                
                userDefaults.set(loggedInUsersInfo.toJSONString()?.aesEncrypt(key: AFUserSession.shared.aSecretKey), forKey: APIsType.loggedInUserInfo.simpleRawValue())
                
                /* Load Users info */
                AFUserSession.shared.loggedInUsers = loggedInUsersInfo
                
            } else {
                let newloggedInUsersInfo = ManageAccountModel(current: customerInfo, users: [customerInfo])
                
                /* Save Users info */
                //newloggedInUsersInfo.saveInUserDefaults(key: APIsType.loggedInUserInfo.simpleRawValue())
                userDefaults.set(newloggedInUsersInfo.toJSONString()?.aesEncrypt(key: AFUserSession.shared.aSecretKey), forKey: APIsType.loggedInUserInfo.simpleRawValue())
                
                /* Load Users info */
                AFUserSession.shared.loggedInUsers = newloggedInUsersInfo
            }
            
            userDefaults.set(true, forKey: Constants.kIsUserLoggedInKey)
            
        } else {
            userDefaults.set(false, forKey: Constants.kIsUserLoggedInKey)
        }
        userDefaults.synchronize()
    }
    
    /// Save JSON string into user defaults
    class func saveJSONStringInToUserDefaults(jsonString:String?, key : String) {
        let userDefaults = UserDefaults.standard
        
        if let jsonString = jsonString {
            
            userDefaults.set(jsonString, forKey: key)
            
        } else {
            userDefaults.set("", forKey: key)
        }
        userDefaults.synchronize()
    }
    
    /// Load JSON string from UserDefaults
    class func loadJSONStringFromUserDefaults(key : String) -> String {
        
        let userDefaults = UserDefaults.standard
        
        if let jsonString = userDefaults.object(forKey:key) as? String {
            return jsonString
            
        } else {
            return ""
        }
    }
    
    /// Clear JSON string from UserDefault for key
    class func clearJSONStringFromUserDefaults(key : String) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: key)
        userDefaults.synchronize()
        
    }
    
}
