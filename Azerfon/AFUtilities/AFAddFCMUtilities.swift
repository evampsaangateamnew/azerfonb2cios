//
//  AFAddFCMUtilities.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/21/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import FirebaseMessaging
import ObjectMapper

class AFAddFCMUtilities :NSObject {
    /**
     Determine password complexity according to predefied rules.
     
     - parameter isForLogin: check calling when user login or from diffrent screen.
     
     - returns: void
     */
    static var numberOfTimeAddFCMCalled : Int = 1
    
    @objc static func addFCMId(sucesscompletion :@escaping () -> () = {}, failureCompletion :@escaping () -> () = {}) {
        
        // Get and check token is valid
        let token = Messaging.messaging().fcmToken
        
        if token?.isBlank == false {
            
            /* Call add FCMId API*/
            _ = AFAPIClient.shared.addFCMId(fcmKey: token ?? "", ringingStatus: .Tone, isEnable: true, isFromLogin: true, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                
                // Check if error occur
                if error != nil {
                    // Do nothing
                    // error?.showServerErrorInViewController(self)
                    
                } else {
                    
                    // Check if API responded successfuly
                    if resultCode == Constants.AFAPIStatusCode.succes.rawValue {
                        
                        // Parssing response data
                        
                        if let addFCMResponseHandler = Mapper<AddFCMModel>().map(JSONObject:resultData) {
                            
                            // Save Notification configration
                            addFCMResponseHandler.saveInUserDefaults(key: APIsType.notificationConfigurations.selectedLocalizedAPIKey())
                        }
                        
                        // Reset value of count
                        numberOfTimeAddFCMCalled = 1
                        /* Token added sucessfully */
                        sucesscompletion()
                        
                    } else { // Call three time in case of failure
                        
                        if numberOfTimeAddFCMCalled <= 3 {
                            
                            // Increase number of times API called and all again after 10n seconds
                            numberOfTimeAddFCMCalled += 1
                            perform(#selector(addFCMId), with: nil, afterDelay: 10)
                        } else {
                            // Reset value of count
                            numberOfTimeAddFCMCalled = 1
                            /* Failed to add FCM token */
                            failureCompletion()
                        }
                    }
                }
            })
        }
    }
}
