//
//  AFProgressUtilities.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/22/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
public class AFProgressUtilities {
    
    /**
     Calculate Progress.
     
     - parameter total: Total Value
     - parameter remaining: Remaining Value.
     
     - returns: Progress (Float Value).
     */
    class func calculateProgress(total: Double = 0, remaining: Double = 0) -> Float {
        
        if total <= 0 || remaining <= 0 {
            return 1.0
        } else {
            return Float(total - remaining)/Float(total)
        }
    }
    
    /*/**
     Calculate Progress.
     
     - parameter total: Total Value
     - parameter remaining: Remaining Value.
     
     - returns: Progress (Float Value).
     */
     class func calculateProgress(total: Double = 0, remaining: Double = 0) -> Float {
     
     if total <= 0 || remaining <= 0 {
     return 1.0
     } else {
     return Float(remaining)/Float(total)
     }
     }*/
    
    
    /**
     Calculate Progress Bar valus and other variables.
     
     - parameter startDateString: Start Date.
     - parameter endDateString: End Date.
     - parameter dateFormate: Date Formate.
     - parameter displaydateFormate: Date Formate or return date.
     - parameter AdditionalValueInEndDate: Addition in end date. By default it's value is '0'.
     
     - returns: (startDate, endDate, daysLeft, daysLeftDisplayValue, totalDays, progressValue ).
     */
    class func calculateProgressValues(from startDateString: String,
                                       to endDateString: String,
                                       dateFormate:String = Constants.kNewAPIFormat,
                                       displayDateFormate :String = Constants.kDisplayFormat,
                                       AdditionalValueInEndDate additionalValue: Int = 0)
                                        ->  (startDate: String, endDate: String, daysLeft: Int, daysLeftDisplayValue: String, totalDays: Int, progressValue: Float ) {
            
            AFUserSession.shared.dateFormatter.timeZone = TimeZone.appTimeZone()
            
            let startDateDisplayValue = AFDateUtilities.displayFormatedDateString(dateString: startDateString, dateFormate: dateFormate, returnDateFormate: displayDateFormate)
            
            let endDateDisplayValue = AFDateUtilities.displayFormatedDateString(dateString: endDateString, dateFormate: dateFormate, returnDateFormate: displayDateFormate)
            
            let dayBeforeEndDate =  AFDateUtilities.displayFormatedDateString(from: endDateString, additionalValue: additionalValue, dateFormate: dateFormate, returnDateFormate: dateFormate)
            
            
            let daysLeft = AFDateUtilities.calculateDaysLeft (startingDate: startDateString, endingDate: dayBeforeEndDate, dateFormate: dateFormate)
            
            let totalDays = AFDateUtilities.calculateTotalDaysBetween(startingDate: startDateString, endingDate: dayBeforeEndDate, dateFormate: dateFormate)
            
            var daysLeftUnit = ""
            
            if daysLeft <= 1 {
                daysLeftUnit = Localized("Info_days")
            } else {
                daysLeftUnit = Localized("Info_days")
            }
                                            
            let daysLeftDisplayValue = "\(daysLeft) \(daysLeftUnit)"
            
            let progress : Float =  AFProgressUtilities.calculateProgress(total: totalDays.toDouble(), remaining: daysLeft.toDouble())
            
            return (startDateDisplayValue, endDateDisplayValue, daysLeft, daysLeftDisplayValue, totalDays, progress)
    }
    
    /**
     Calculate Progress Bar valus and other variables for MRC.
     
     - parameter startDateString: Start Date.
     - parameter endDateString: End Date.
     - parameter type:  MRC Type Daily or other.
     - parameter dateFormate: Date Formate.
     - parameter displaydateFormate: Date Formate or return date.
     
     - returns: (startDate, endDate, daysLeft, daysLeftDisplayValue, totalDays, progressValue )
     */
    class func calculateProgressValuesForMRC(from startDateString: String,
                                             to endDateString: String,
                                             type: String = "",
                                             dateFormate:String = Constants.kNewAPIFormat,
                                             displayDateFormate :String = Constants.kDisplayFormat)
                                                -> (startDate: String, endDate: String, daysLeft: Int, daysLeftDisplayValue: String, totalDays: Int, progressValue: Float ) {
        
        if type.isEqual("daily", ignorCase: true) {
            
            AFUserSession.shared.dateFormatter.timeZone = TimeZone.appTimeZone()
            
            let startDateDisplayValue = AFDateUtilities.displayFormatedDateString(dateString: startDateString, dateFormate: dateFormate, returnDateFormate: displayDateFormate)
            let endDateDisplayValue = AFDateUtilities.displayFormatedDateString(dateString: endDateString, dateFormate: dateFormate, returnDateFormate: displayDateFormate)
            
            let totalHours = AFDateUtilities.calculateTotalHoursBetweenTwoDates(startingDate: startDateString, endingDate: endDateString, dateFormate: dateFormate)
            
            var passedHours = AFDateUtilities.calculateTotalHoursBetweenTwoDates(startingDate: startDateString, endingDate: Date().todayDateString(dateFormate: dateFormate), dateFormate: dateFormate)
            
            if passedHours > 0 {
                passedHours = passedHours + 1
            }
            
            //  let currentHourValue = Date().currentHour()
            var remainingHours = totalHours - passedHours
            
            var hoursLeftUnit = ""
            
            if remainingHours <= 1 {
                hoursLeftUnit = Localized("title_Hours")
                
                if remainingHours <= 0 {
                    remainingHours = 0
                }
                
            } else {
                hoursLeftUnit = Localized("title_Hours")
            }
            let hoursLeftDisplayValue = "\(remainingHours) \(hoursLeftUnit)"
            
            let progress : Float =  AFProgressUtilities.calculateProgress(total: totalHours.toDouble(), remaining: remainingHours.toDouble())
            
            
            return (startDateDisplayValue,endDateDisplayValue,remainingHours,hoursLeftDisplayValue, totalHours,progress)
            
        } else {
            
            return AFProgressUtilities.calculateProgressValues(from: startDateString, to: endDateString, dateFormate: dateFormate, displayDateFormate: displayDateFormate, AdditionalValueInEndDate: -1)
        }
    }
    
    
}
