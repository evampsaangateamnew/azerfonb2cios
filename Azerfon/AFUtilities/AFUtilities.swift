//
//  AFUtilities.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 3/11/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation

class AFUtilities {
    
    
    class func isLaunchedBefore() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.kIsLaunchedBefore)
    }
    
    class func updateLaunchedBeforeStatus() {
        UserDefaults.standard.set("YES", forKey:Constants.kIsLaunchedBefore)
    }
    
    public class func getAppStoreURL() -> String {
        
        if let appStroreURL = UserDefaults.standard.string(forKey: Constants.K_AppStoreURL),
            appStroreURL.isBlank == false {
            return appStroreURL
        } else {
            return Localized("appStore_URL")
        }
    }
    
    public class func updateAppStoreURL(_ URLString :String?) {
        UserDefaults.standard.set(URLString, forKey: Constants.K_AppStoreURL)
        UserDefaults.standard.synchronize()
    }
    
    public class func updateLoginTime() {
        UserDefaults.standard.set(Date().todayDateString(), forKey: Constants.K_UserLoggedInTime)
    }
    
    public class func getLoginTime() -> String {
        if let loginTime = UserDefaults.standard.string(forKey: Constants.K_UserLoggedInTime),
            loginTime.isBlank == false {
            return loginTime
        }
        return ""
    }
    
    public class func updateRateUsLaterTime() {
        UserDefaults.standard.set(Date().todayDateString(), forKey: Constants.K_RateUsLaterTime)
    }
    //Current Date
    class func todayDate(dateFormate: String = Constants.kNewAPIFormat ) -> Date {
        //        return Date()
        
        let now = Date()
        
        AFUserSession.shared.dateFormatter.timeZone = TimeZone.current
        
        AFUserSession.shared.dateFormatter.dateFormat = dateFormate
        
        let currentDateString = AFUserSession.shared.dateFormatter.string(from: now)
        
        AFUserSession.shared.dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        return AFUserSession.shared.dateFormatter.date(from: currentDateString) ?? now
    }
    
    public class func getRateUsLaterTime() -> String {
        if let rateUsTime = UserDefaults.standard.string(forKey: Constants.K_RateUsLaterTime),
            rateUsTime.isBlank == false {
            return rateUsTime
        }
        
        return ""
    }
    
    public class func updateRateUsShownStatus(isShow :Bool) {
        UserDefaults.standard.set(isShow, forKey: Constants.K_IsRateUsShownBefore)
    }
    
    public class func isRateUsShownBefore() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.K_IsRateUsShownBefore)
    }
    
}


//MARK: - Password strength
extension AFUtilities {
    
    /**
     Determines the enterd password strength.
     
     - parameter Text:   Password string
     
     - Conditions:
     
     1. Weak: ( Password length is greater or equal than '6', and password lengt is less than '8', and contains special characters or contains alphbeats or contains digits)
     2. Weak: ( Password length is greater or equal than '6')
     3. Medium: ( Password length is greater or equal than '6', and password lengt is less or equal than '8', and contains special characters, and constains capital alphabets, and constains small alphabets, and contains digits)
     4. Medium: ( Password lengt is greater or equal than '8', and contains special characters, and constains alphabets, and contains digits)
     5. Medium: ( Password lengt is greater or equal than '8', and  ( ( Constains small alphabets, and contains digits, and constains capital alphabets ) or( Constains small alphabets, and contains special character, and constains capital alphabets ) ) )
     6. Strong: ( Password length is less or equal than '15', and password lengt greater than '8', and contains special characters, and constains capital alphabets, and constains small alphabets, and contains digits)
     7. didNotMatchCriteria: ( Password is less or equal to '0', or password is greater than '15', or password is les than '6')
     8. didNotMatchCriteria: ( If all above conditions does not match. )
     
     - returns: Password strenght based on above condition.
     */
    
    class func determinePasswordStrength(Text passwordString:String) -> Constants.AFPasswordStrength {
        
        /* Check if values are between min max value */
        if passwordString.count > Constants.maxPasswordLength ||
            passwordString.count < Constants.minPasswordLength {
            
            return Constants.AFPasswordStrength.didNotMatchCriteria
        }
            // Strong
        else if passwordString.count > 8 &&
            passwordString.count <= Constants.maxPasswordLength &&
            passwordString.containsSpecialCharacters() &&
            passwordString.constainsSmallAlphabets() && passwordString.constainsCapitalAlphabets() &&
            passwordString.containsDigits() {
            
            return Constants.AFPasswordStrength.Strong
            
        }
            // Medium
        else if passwordString.count >= Constants.minPasswordLength &&
            passwordString.count <= 8 &&
            passwordString.containsSpecialCharacters() &&
            passwordString.constainsCapitalAlphabets() && passwordString.constainsSmallAlphabets() &&
            passwordString.containsDigits() {
            
            return Constants.AFPasswordStrength.Medium
            
        } else if passwordString.count >= 8 &&
            (passwordString.containsSpecialCharacters() && passwordString.containsAlphabets() && passwordString.containsDigits()) {
            
            return Constants.AFPasswordStrength.Medium
            
        }
        else if passwordString.count >= 8 && (passwordString.containsDigits() && passwordString.constainsSmallAlphabets() && passwordString.constainsCapitalAlphabets()) || (passwordString.constainsSmallAlphabets() && passwordString.constainsCapitalAlphabets() && passwordString.containsSpecialCharacters()){
            
            return Constants.AFPasswordStrength.Medium
            
        }
            // Weak
        else if passwordString.count >= Constants.minPasswordLength &&
            passwordString.count < 8 &&
            (passwordString.containsAlphabets() || passwordString.containsDigits() || passwordString.containsSpecialCharacters()) {
            
            return Constants.AFPasswordStrength.Week
            
        } else if passwordString.count >= Constants.minPasswordLength {
            
            return Constants.AFPasswordStrength.Week
            
        } else {
            
            return Constants.AFPasswordStrength.didNotMatchCriteria
            
        }
    }
}
