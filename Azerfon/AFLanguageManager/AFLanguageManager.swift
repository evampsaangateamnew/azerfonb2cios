 //
//  AFLanguageManager.swift
 //  Azerfon
 //
 //  Created by AbdulRehman Warraich on 3/4/19.
 //  Copyright © 2019 Evamp&Saanga. All rights reserved.
 //

import Foundation

class AFLanguageManager: NSObject {

    ///Initialzes user language
    class func setInitialUserLanguage(){
        _ = AFLanguageManager.userSelectedLanguage()
    }

    ///Returns user selected language.
    class func userSelectedLanguage() -> Constants.AFLanguage {

        let userDefaults = UserDefaults.standard

        if let userSelectedLanguage : String = userDefaults.value(forKey: Constants.kUserSelectedLanguage) as? String {

            switch userSelectedLanguage {

            case "2":
                return Constants.AFLanguage.russian

            case "3":
                return Constants.AFLanguage.english

            case "4":
                return Constants.AFLanguage.azeri

            default:
                AFLanguageManager.setUserLanguage(selectedLanguage: Constants.defaultAppLanguage)
                return Constants.defaultAppLanguage
            }

        } else {
            AFLanguageManager.setUserLanguage(selectedLanguage: Constants.defaultAppLanguage)
            return Constants.defaultAppLanguage
        }
    }

    ///Change user language.
    class func setUserLanguage(selectedLanguage : Constants.AFLanguage) {

        let userDefaults = UserDefaults.standard

        userDefaults.set(selectedLanguage.rawValue, forKey: Constants.kUserSelectedLanguage)

        userDefaults.synchronize()

        switch selectedLanguage {

        case Constants.AFLanguage.english:
            Localize.setCurrentLanguage("en")
            break

        case Constants.AFLanguage.russian:
            Localize.setCurrentLanguage("ru")
            break

        case Constants.AFLanguage.azeri:
            Localize.setCurrentLanguage("az-Cyrl")
            break
            
        }

    }

    ///Returns user selected language name.
    class func userSelectedLanguageName() -> String {

        let languageName = AFLanguageManager.userSelectedLanguage()

        switch languageName {

        case Constants.AFLanguage.russian:
            return "Russian"

        case Constants.AFLanguage.english:
            return "English"

        case Constants.AFLanguage.azeri:
            return "Azeri"
            
        }
    }
    
    ///Returns user selected language short code.
    class func userSelectedLanguageShortCode() -> String {
        
        let languageName = AFLanguageManager.userSelectedLanguage()
        
        switch languageName {
            
        case Constants.AFLanguage.russian:
            return "ru"
            
        case Constants.AFLanguage.english:
            return "en"
            
        case Constants.AFLanguage.azeri:
            return "az-Cyrl"
            
        }
    }
}
 
