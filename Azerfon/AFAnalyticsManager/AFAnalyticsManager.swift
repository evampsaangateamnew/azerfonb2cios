//
//  AFAnalyticsManager.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 06/26/19.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import Firebase

class AFAnalyticsManager: NSObject {

    public class func logEvent(screenName: String, contentType: String, successStatus: String) {

        let aScreenName = (screenName.lowercased()).replaceSpaceWithHyphen

        Analytics.logEvent(aScreenName, parameters: [
            AnalyticsParameterContentType: contentType as NSObject,
            AnalyticsParameterSuccess: successStatus as NSObject
            ])
    }
    
    public class func logEvent(screenName: String, contentType: String, searchTerm: String) {
        
        let aScreenName = (screenName.lowercased()).replaceSpaceWithHyphen
        
        Analytics.logEvent(aScreenName, parameters: [
            AnalyticsParameterContentType: contentType as NSObject,
            AnalyticsParameterSearchTerm: searchTerm as NSObject
            ])
    }
    
}
